﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace uniqco.email.Class
{
    public class ServiceWrapper
    {
        public static void Run_MainLoop()
        {
            while (true)
            {
                uniqco.email.email_main.Main();
                System.Threading.Thread.Sleep(TimeSpan.FromMinutes(30)); //periodically runs every 30 minutes.
            }
        }
    }
}
