﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using OpenPop.Pop3;
using OpenPop.Mime;
using System.Collections.Generic;
using System.Text;
using uniqco.Business;
using System.ComponentModel;
using System.Net.Mail;
using NLog;
namespace uniqco.email
{
    class Email
    {
        public Email()
        {

        }
        public string UID { get; set; }
        public string From { get; set; }
        public List<string> To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public List<Attachment> Attachments { get; set; }

    }
    class Attachment
    {
        public Attachment()
        {

        }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
    }
    class email_main
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void Main()
        {
            try
            {
                using (OpenPop.Pop3.Pop3Client client = new Pop3Client())
                {
                    logger.Info("Start Processing zoho mail");
                    client.Connect("poppro.zoho.com", 995, true);
                    logger.Info("Start Authenticating no-reply@nanosoft.com.au");
                    client.Authenticate("no-reply@nanosoft.com.au", "notastrongpassword", AuthenticationMethod.UsernameAndPassword); //logs into ryans email account
                    logger.Info("Finished Authenticating no-reply@nanosoft.com.au");
                    if (client.Connected)
                    {
                        logger.Info("Client is connected.");
                        List<string> uids = client.GetMessageUids();
                        List<Email> AllEmail = new List<Email>();//all email from uniqco_data@nanosoft.com.au

                        #region getALL
                        logger.Info("Preparing mail list to process");
                        for (int i = 0; i < uids.Count; i++)
                        {
                            Message msg = client.GetMessage(i + 1); // get all mail from inbox
                            List<string> to = new List<string>();
                            foreach (var x in msg.Headers.To)
                                to.Add(x.Address.ToString());

                            if (to.Contains("uniqco_data@nanosoft.com.au")) //if email was to uniqco_data@nanosoft.com.au, then process
                            {
                                logger.Info("Processing email from uniqco_data@nanosoft.com.au");
                                Email newEmail = new Email();
                                List<Attachment> newEmailAttachments = new List<Attachment>();

                                MessagePart plainText = msg.FindFirstPlainTextVersion();
                                List<MessagePart> att = msg.FindAllAttachments();

                                newEmail.UID = uids[i];
                                newEmail.To = to;
                                newEmail.From = msg.Headers.From.Address.ToString();
                                newEmail.Subject = msg.Headers.Subject;
                                logger.Info("Email Subject:" + msg.Headers.Subject);
                                //newEmail.Content = plainText.GetBodyAsText();
                                foreach (MessagePart ado in att)
                                {
                                    Attachment newAttachment = new Attachment();
                                    newAttachment.FileName = ado.FileName;
                                    newAttachment.ContentType = ado.ContentType.MediaType;
                                    newAttachment.Content = ado.Body;
                                    newEmailAttachments.Add(newAttachment);
                                    //ado.Save(new System.IO.FileInfo(System.IO.Path.Combine(@"C:\UNIQCO\", ado.FileName)));
                                   
                                }
                                newEmail.Attachments = newEmailAttachments;
                                AllEmail.Add(newEmail);//save to AllEmail
                            }
                            //Console.SetCursorPosition(0, Console.CursorTop); this is a test
                            //Console.Write("Reading {0} of {1}", i, uids.Count - 1);
                        }
                        #endregion
                        #region processing
                        client.Disconnect();
                        client.Dispose();

                        List<string> ProcessedUids = Business.DataObjects.ProcessedEmail.GetALLUID(); // list of UniqueId of processed email from database
                        logger.Info("Getting all Processed UIDs");
                        foreach (Email eml in AllEmail)
                        {
                            logger.Info("UID  " + eml.UID + " already processed: " + ProcessedUids.Contains(eml.UID));
                            logger.Info("Email Subject: " + eml.Subject);
                            if (!ProcessedUids.Contains(eml.UID))//check if already processed
                            {
                                logger.Info("Start validating unprocesssed emails.");
                                bool hasfailed = false;
                                bool proccessed = false;
                                string errormsg = "";
                                string reportfile = "";
                                Business.DataObjects.Company company = null;
                                try
                                {
                                    //check if email has the needed CSV attachments..
                                    foreach (Attachment a in eml.Attachments)
                                    {
                                        if (a.FileName.ToLower().EndsWith("csv")) //CSV file found
                                        {
                                            logger.Info("Getting CSV file to be parsed.");
                                            logger.Info("CSV File:" + a.FileName);
                                            proccessed = true;
                                            reportfile = a.FileName;
                                            //Process
                                            //get companyinfo from subject(temp)
                                            //create if doesnt exist
                                            char[] c = new char[] { '-' };
                                            string[] cmName = eml.Subject.Split(c, 2,StringSplitOptions.RemoveEmptyEntries);
                                            if (cmName.Length != 2)
                                                throw new Exception("Invalid Subject '" + eml.Subject + "'");

                                            string companyName = cmName[1].Trim();
                                            bool companyexist = Business.DataObjects.Company.GetFromName(companyName) != null;
                                            if (!companyexist)
                                            {
                                                Business.DataObjects.Company newcom = new Business.DataObjects.Company()
                                                {
                                                    Name = companyName,
                                                    Address = "auto-created from the data upload process at " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss")
                                                };
                                                Business.DataObjects.Company.Create(newcom);

                                            }
                                            company = Business.DataObjects.Company.GetFromName(companyName);

                                            //handle FileName ------------------------------------->>
                                            //----------------------------------------------------->>
                                            string[] f = a.FileName.Split('_');
                                            string reporttype ="";
                                            if (f.Length != 2)
                                            {
                                                logger.Info("Invalid filename : handle filename'" + a.FileName + "'");
                                                throw new Exception("Invalid filename '" + a.FileName + "'");
                                            }

                                            //get reporttype
                                            if (f[0].ToLower() == "riskcomposite") //else if (other report type)
                                                reporttype = "Composite";
                                            else
                                            {
                                                logger.Info("Invalid filename : get reporttype'" + a.FileName + "'");
                                                throw new Exception("Invalid filename '" + a.FileName + "'");
                                            }
                                            
                                            string[] dateParts = f[1].Split('.')[0].Split('-');
                                            if (dateParts.Length != 3)//contains month,day,year
                                            {
                                                logger.Info("Invalid filename : contains month,day,year'" + a.FileName + "'");
                                                throw new Exception("Invalid filename '" + a.FileName + "'");
                                            }
                                            foreach (string dp in dateParts)
                                            {
                                                int ix = 0;
                                                if (!int.TryParse(dp, out ix))
                                                    throw new Exception("Invalid filename '" + a.FileName + "'");
                                            }
                                            //DateTime reportdate = new DateTime(Convert.ToInt32(dateParts[2]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[0])).AddMonths(-1); //get reportdate
                                            //DateTime reportdate = new DateTime(Convert.ToInt32(dateParts[2]), Convert.ToInt32(dateParts[1]), 25); //get reportdate 'Changes by Aman on 20170316
                                            DateTime reportdate = new DateTime(Convert.ToInt32(dateParts[2]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[0])).AddMonths(-1);
                                            //=====================================================>>


                                            Business.DataObjects.CompanyReport report = company.GetReportforDate(reportdate);

                                            //If the company report was interrupted last time, 
                                            //then delete the report and all its data and reprocess
                                            if ((report != null) && !(report.ImportWasSuccess))
                                            {
                                                logger.Info("If the company report was interrupted last time");
                                                report.DeleteReportAndAllRelatedData();
                                                report = null;
                                            }
                                            bool reportexist = report != null;

                                            logger.Info("Report Already Exists:'" + reportexist + "'");
                                            if (!reportexist)
                                            {
                                                //insert report
                                                Business.DataObjects.CompanyReport newrept = new Business.DataObjects.CompanyReport()
                                               {
                                                   CompanyId = company.CompanyID,
                                                   Notes = "created automatically on " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss"),
                                                   ProcessedDate = DateTime.Now,
                                                   Reportdate = reportdate,
                                                   ReportType = reporttype,
                                                   ImportWasSuccess = false
                                               };                                      
                                                Business.DataObjects.CompanyReport.Create(newrept);
                                                logger.Info("Insert to Company Report with report ID: " + newrept.CompanyReportID);
                                                newrept.CreateReportcontentFromFile(a.Content); //Read contents of CSV Attachment
                                                logger.Info("CreateReportcontentFromFile");
                                                newrept.MarkImportAsSuccess();
                                                logger.Info("Mark Import As Success");
                                                logger.Info("<------------------------------------->");
                                            }
                                        }
                                    }
                                }
                                catch(Exception ex)
                                {
                                    hasfailed = true;
                                    errormsg = ex.Message;
                                }
                                if (proccessed)
                                {
                                    //A log must be kept in SQL stating if the email was processed and when. If there was an issue, then the exception / issue must be noted and an email sent to the user who sent the data
                                    Business.DataObjects.ProcessedEmail newemail = new Business.DataObjects.ProcessedEmail()
                                    {

                                        ProcessedDate = DateTime.Now,
                                        UID = eml.UID,
                                        Subject = eml.Subject,
                                        Sender = eml.From,
                                        Exception = hasfailed,// if error occured during process
                                        ExceptionMessage = errormsg,
                                        CompanyId = (company == null) ? Guid.Empty : company.CompanyID,
                                        Report = reportfile
                                    };

                                    Business.DataObjects.ProcessedEmail.Create(newemail);
                                    ProcessedUids.Add(eml.UID); //Remeber Uid; i
                                    if (hasfailed)
                                    {
                                        using (var mailclient = new SmtpClient())
                                        {
                                            mailclient.Port = 587; //465
                                            mailclient.Host = "smtp.zoho.com";
                                            mailclient.EnableSsl = true;
                                            mailclient.Timeout = 10000;
                                            mailclient.DeliveryMethod = SmtpDeliveryMethod.Network;
                                            mailclient.UseDefaultCredentials = false;
                                            mailclient.Credentials = new System.Net.NetworkCredential("no-reply@nanosoft.com.au", "notastrongpassword!");


                                            var mm = new MailMessage();
                                            mm.From = new MailAddress("no-reply@nanosoft.com.au");
                                            mm.Subject = "Error Alert";
                                            mm.Body = "Failed in parsing report : '" + reportfile + "' from " + ((company == null) ? "[]" : company.Name) + ". \nError = " + errormsg + ". \n\nVisit the Uniqco Audit page in http://uniqcoserver.nanosoft.com.au/ For more details";
                                            mm.To.Add(new MailAddress("dave@nanosoft.com.au"));
                                            mm.To.Add(new MailAddress("ryan@nanosoft.com.au"));
                                            mm.BodyEncoding = UTF8Encoding.UTF8;
                                            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                                            mailclient.Send(mm);
                                        }
                                    }
                                }

                            }
                        }
                        #endregion

                        logger.Info("<------------ End Process ------------>");
                    }
                }
            }

            catch (Exception ex) 
            {
                logger.Info("Error: " + ex.Message);
                //Cannot open database 'uniqco' requested by the login. The login failed. Login failed for user 'WORKGROUP\UNIQCO$'.
                //Fixed by using SQL Server Authetication, User 'uniqcoserver'
            }
        }
        
    }
}
