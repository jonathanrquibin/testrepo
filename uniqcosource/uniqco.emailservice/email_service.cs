﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
namespace uniqco.emailservice
{
    public partial class email_service : ServiceBase
    {
        public System.Threading.Thread main_thread;
        public email_service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //Add code here to start your service. This method should set things
            //in motion so your service can do its work.

            main_thread = new System.Threading.Thread(uniqco.email.Class.ServiceWrapper.Run_MainLoop);
            main_thread.Start();
        }
        protected override void OnPause()
        {
            base.OnPause();

            if (main_thread != null) main_thread.Suspend();
        }
        protected override void OnContinue()
        {
            base.OnContinue();
        }
        protected override void OnStop()
        {
            if (main_thread != null) main_thread.Abort();
            //Add code here to perform any tear-down necessary to stop your service.
        }
    }
}
