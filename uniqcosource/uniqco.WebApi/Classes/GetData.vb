﻿Imports System.IO
Imports uniqco.Business

Public Class GetData
    Public Shared Function GetDataFromQuery(userName As String, password As String, GetQuery As String) As String
        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)
        Dim strMessage As String = String.Empty
        If Not login Is Nothing Then
            Dim RiskCompositeTest = DataObjects.RiskComposite.GetQuery(GetQuery)
            Dim response As HttpResponse = HttpContext.Current.Response
            Dim filename As String = "BIToolReport_" & Date.Now().ToString("MMddyyyyHHmmss").ToString() & ".xls"

            response.Clear()
            response.Charset = ""
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment;filename=""" + filename + """")

            Using sw As New StringWriter()
                Using htw As New HtmlTextWriter(sw)
                    Dim dg As New DataGrid()
                    dg.DataSource = RiskCompositeTest.Tables(0)
                    dg.DataBind()
                    dg.RenderControl(htw)

                    Dim strHtml As String = htw.InnerWriter.ToString()
                    response.Write(strHtml)
                    response.[End]()
                End Using
            End Using
            strMessage = "Successfully downloaded."
        Else
            strMessage = "Unauthorized Access."
        End If
        Return strMessage
    End Function

    Public Shared Function GetRiskCompositeData(userName As String, password As String, StartDate As Date, EndDate As Date) As String
        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)
        Dim strMessage As String = String.Empty
        If Not login Is Nothing Then
            Dim RiskCompositeList = DataObjects.RiskComposite.GetRiskCompositeTable(StartDate, EndDate)
            Dim response As HttpResponse = HttpContext.Current.Response
            Dim filename As String = "RiskComposite_" & Date.Now().ToString("MMddyyyyHHmmss").ToString() & ".xls"

            response.Clear()
            response.Charset = ""
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment;filename=""" + filename + """")
            Using sw As New StringWriter()
                Using htw As New HtmlTextWriter(sw)
                    ' instantiate a datagrid  
                    Dim dg As New DataGrid()
                    dg.DataSource = RiskCompositeList

                    dg.DataBind()
                    dg.RenderControl(htw)

                    Dim strHtml As String = htw.InnerWriter.ToString()
                    response.Write(strHtml)
                    response.[End]()
                End Using
            End Using
            strMessage = "Successfully downloaded."
        Else
            strMessage = "Unauthorized Access."
        End If
        Return strMessage
    End Function
End Class
