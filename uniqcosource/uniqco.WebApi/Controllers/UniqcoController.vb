﻿Imports System.Net
Imports System.Web.Http

Public Class UniqcoController
    Inherits ApiController

    ''' <summary>
    ''' 'Get the RiskComposite data 
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="StartDate"></param>
    ''' <param name="EndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRiskComposite(ByVal UserName As String, ByVal Password As String, ByVal StartDate As Date, EndDate As Date) As String
        Return GetData.GetRiskCompositeData(UserName, Password, StartDate, EndDate)
    End Function
    ''' <summary>
    ''' 'Get the RiskComposite data 
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="GetQuery"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRiskCompositeByQuery(UserName As String, Password As String, GetQuery As String) As String
        Dim retMessage As String = GetData.GetDataFromQuery(UserName, Password, GetQuery)
        Return retMessage
    End Function
End Class