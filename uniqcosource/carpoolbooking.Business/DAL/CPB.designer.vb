﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Data.Linq
Imports System.Data.Linq.Mapping
Imports System.Linq
Imports System.Linq.Expressions
Imports System.Reflection


<Global.System.Data.Linq.Mapping.DatabaseAttribute(Name:="CPB")>  _
Partial Public Class CPBDataContext
	Inherits System.Data.Linq.DataContext
	
	Private Shared mappingSource As System.Data.Linq.Mapping.MappingSource = New AttributeMappingSource()
	
  #Region "Extensibility Method Definitions"
  Partial Private Sub OnCreated()
  End Sub
  Partial Private Sub InsertCompany(instance As Company)
    End Sub
  Partial Private Sub UpdateCompany(instance As Company)
    End Sub
  Partial Private Sub DeleteCompany(instance As Company)
    End Sub
  Partial Private Sub InsertUser(instance As User)
    End Sub
  Partial Private Sub UpdateUser(instance As User)
    End Sub
  Partial Private Sub DeleteUser(instance As User)
    End Sub
  #End Region
	
	Public Sub New()
		MyBase.New(Global.carpoolbooking.Business.My.MySettings.Default.CPBConnectionString, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As String)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As System.Data.IDbConnection)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As String, ByVal mappingSource As System.Data.Linq.Mapping.MappingSource)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As System.Data.IDbConnection, ByVal mappingSource As System.Data.Linq.Mapping.MappingSource)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public ReadOnly Property Companies() As System.Data.Linq.Table(Of Company)
		Get
			Return Me.GetTable(Of Company)
		End Get
	End Property
	
	Public ReadOnly Property Users() As System.Data.Linq.Table(Of User)
		Get
			Return Me.GetTable(Of User)
		End Get
	End Property
End Class

<Global.System.Data.Linq.Mapping.TableAttribute(Name:="dbo.Company")>  _
Partial Public Class Company
	Implements System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	
	Private Shared emptyChangingEventArgs As PropertyChangingEventArgs = New PropertyChangingEventArgs(String.Empty)
	
	Private _CompanyId As System.Guid
	
	Private _Name As String
	
	Private _Address As String
	
	Private _PhoneNumber As String
	
	Private _LogoBytes As System.Data.Linq.Binary
	
	Private _Users As EntitySet(Of User)
	
    #Region "Extensibility Method Definitions"
    Partial Private Sub OnLoaded()
    End Sub
    Partial Private Sub OnValidate(action As System.Data.Linq.ChangeAction)
    End Sub
    Partial Private Sub OnCreated()
    End Sub
    Partial Private Sub OnCompanyIdChanging(value As System.Guid)
    End Sub
    Partial Private Sub OnCompanyIdChanged()
    End Sub
    Partial Private Sub OnNameChanging(value As String)
    End Sub
    Partial Private Sub OnNameChanged()
    End Sub
    Partial Private Sub OnAddressChanging(value As String)
    End Sub
    Partial Private Sub OnAddressChanged()
    End Sub
    Partial Private Sub OnPhoneNumberChanging(value As String)
    End Sub
    Partial Private Sub OnPhoneNumberChanged()
    End Sub
    Partial Private Sub OnLogoBytesChanging(value As System.Data.Linq.Binary)
    End Sub
    Partial Private Sub OnLogoBytesChanged()
    End Sub
    #End Region
	
	Public Sub New()
		MyBase.New
		Me._Users = New EntitySet(Of User)(AddressOf Me.attach_Users, AddressOf Me.detach_Users)
		OnCreated
	End Sub
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CompanyId", DbType:="UniqueIdentifier NOT NULL", IsPrimaryKey:=true)>  _
	Public Property CompanyId() As System.Guid
		Get
			Return Me._CompanyId
		End Get
		Set
			If ((Me._CompanyId = value)  _
						= false) Then
				Me.OnCompanyIdChanging(value)
				Me.SendPropertyChanging
				Me._CompanyId = value
				Me.SendPropertyChanged("CompanyId")
				Me.OnCompanyIdChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Name", DbType:="NVarChar(80)")>  _
	Public Property Name() As String
		Get
			Return Me._Name
		End Get
		Set
			If (String.Equals(Me._Name, value) = false) Then
				Me.OnNameChanging(value)
				Me.SendPropertyChanging
				Me._Name = value
				Me.SendPropertyChanged("Name")
				Me.OnNameChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Address", DbType:="NVarChar(150)")>  _
	Public Property Address() As String
		Get
			Return Me._Address
		End Get
		Set
			If (String.Equals(Me._Address, value) = false) Then
				Me.OnAddressChanging(value)
				Me.SendPropertyChanging
				Me._Address = value
				Me.SendPropertyChanged("Address")
				Me.OnAddressChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_PhoneNumber", DbType:="NVarChar(15)")>  _
	Public Property PhoneNumber() As String
		Get
			Return Me._PhoneNumber
		End Get
		Set
			If (String.Equals(Me._PhoneNumber, value) = false) Then
				Me.OnPhoneNumberChanging(value)
				Me.SendPropertyChanging
				Me._PhoneNumber = value
				Me.SendPropertyChanged("PhoneNumber")
				Me.OnPhoneNumberChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_LogoBytes", DbType:="VarBinary(MAX)", CanBeNull:=true, UpdateCheck:=UpdateCheck.Never)>  _
	Public Property LogoBytes() As System.Data.Linq.Binary
		Get
			Return Me._LogoBytes
		End Get
		Set
			If (Object.Equals(Me._LogoBytes, value) = false) Then
				Me.OnLogoBytesChanging(value)
				Me.SendPropertyChanging
				Me._LogoBytes = value
				Me.SendPropertyChanged("LogoBytes")
				Me.OnLogoBytesChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.AssociationAttribute(Name:="Company_User", Storage:="_Users", ThisKey:="CompanyId", OtherKey:="CompanyId")>  _
	Public Property Users() As EntitySet(Of User)
		Get
			Return Me._Users
		End Get
		Set
			Me._Users.Assign(value)
		End Set
	End Property
	
	Public Event PropertyChanging As PropertyChangingEventHandler Implements System.ComponentModel.INotifyPropertyChanging.PropertyChanging
	
	Public Event PropertyChanged As PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
	
	Protected Overridable Sub SendPropertyChanging()
		If ((Me.PropertyChangingEvent Is Nothing)  _
					= false) Then
			RaiseEvent PropertyChanging(Me, emptyChangingEventArgs)
		End If
	End Sub
	
	Protected Overridable Sub SendPropertyChanged(ByVal propertyName As [String])
		If ((Me.PropertyChangedEvent Is Nothing)  _
					= false) Then
			RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
		End If
	End Sub
	
	Private Sub attach_Users(ByVal entity As User)
		Me.SendPropertyChanging
		entity.Company = Me
	End Sub
	
	Private Sub detach_Users(ByVal entity As User)
		Me.SendPropertyChanging
		entity.Company = Nothing
	End Sub
End Class

<Global.System.Data.Linq.Mapping.TableAttribute(Name:="dbo.[User]")>  _
Partial Public Class User
	Implements System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	
	Private Shared emptyChangingEventArgs As PropertyChangingEventArgs = New PropertyChangingEventArgs(String.Empty)
	
	Private _UserId As System.Guid
	
	Private _UserName As String
	
	Private _Password As String
	
	Private _Active As Boolean
	
	Private _Rights As String
	
	Private _CompanyId As System.Guid
	
	Private _EmailAddress As String
	
	Private _Pic As System.Data.Linq.Binary
	
	Private _Company As EntityRef(Of Company)
	
    #Region "Extensibility Method Definitions"
    Partial Private Sub OnLoaded()
    End Sub
    Partial Private Sub OnValidate(action As System.Data.Linq.ChangeAction)
    End Sub
    Partial Private Sub OnCreated()
    End Sub
    Partial Private Sub OnUserIdChanging(value As System.Guid)
    End Sub
    Partial Private Sub OnUserIdChanged()
    End Sub
    Partial Private Sub OnUserNameChanging(value As String)
    End Sub
    Partial Private Sub OnUserNameChanged()
    End Sub
    Partial Private Sub OnPasswordChanging(value As String)
    End Sub
    Partial Private Sub OnPasswordChanged()
    End Sub
    Partial Private Sub OnActiveChanging(value As Boolean)
    End Sub
    Partial Private Sub OnActiveChanged()
    End Sub
    Partial Private Sub OnRightsChanging(value As String)
    End Sub
    Partial Private Sub OnRightsChanged()
    End Sub
    Partial Private Sub OnCompanyIdChanging(value As System.Guid)
    End Sub
    Partial Private Sub OnCompanyIdChanged()
    End Sub
    Partial Private Sub OnEmailAddressChanging(value As String)
    End Sub
    Partial Private Sub OnEmailAddressChanged()
    End Sub
    Partial Private Sub OnPicChanging(value As System.Data.Linq.Binary)
    End Sub
    Partial Private Sub OnPicChanged()
    End Sub
    #End Region
	
	Public Sub New()
		MyBase.New
		Me._Company = CType(Nothing, EntityRef(Of Company))
		OnCreated
	End Sub
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_UserId", DbType:="UniqueIdentifier NOT NULL", IsPrimaryKey:=true)>  _
	Public Property UserId() As System.Guid
		Get
			Return Me._UserId
		End Get
		Set
			If ((Me._UserId = value)  _
						= false) Then
				Me.OnUserIdChanging(value)
				Me.SendPropertyChanging
				Me._UserId = value
				Me.SendPropertyChanged("UserId")
				Me.OnUserIdChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_UserName", DbType:="NVarChar(50)")>  _
	Public Property UserName() As String
		Get
			Return Me._UserName
		End Get
		Set
			If (String.Equals(Me._UserName, value) = false) Then
				Me.OnUserNameChanging(value)
				Me.SendPropertyChanging
				Me._UserName = value
				Me.SendPropertyChanged("UserName")
				Me.OnUserNameChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Password", DbType:="NVarChar(30)")>  _
	Public Property Password() As String
		Get
			Return Me._Password
		End Get
		Set
			If (String.Equals(Me._Password, value) = false) Then
				Me.OnPasswordChanging(value)
				Me.SendPropertyChanging
				Me._Password = value
				Me.SendPropertyChanged("Password")
				Me.OnPasswordChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Active", DbType:="Bit NOT NULL")>  _
	Public Property Active() As Boolean
		Get
			Return Me._Active
		End Get
		Set
			If ((Me._Active = value)  _
						= false) Then
				Me.OnActiveChanging(value)
				Me.SendPropertyChanging
				Me._Active = value
				Me.SendPropertyChanged("Active")
				Me.OnActiveChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Rights", DbType:="NVarChar(20)")>  _
	Public Property Rights() As String
		Get
			Return Me._Rights
		End Get
		Set
			If (String.Equals(Me._Rights, value) = false) Then
				Me.OnRightsChanging(value)
				Me.SendPropertyChanging
				Me._Rights = value
				Me.SendPropertyChanged("Rights")
				Me.OnRightsChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CompanyId", DbType:="UniqueIdentifier NOT NULL")>  _
	Public Property CompanyId() As System.Guid
		Get
			Return Me._CompanyId
		End Get
		Set
			If ((Me._CompanyId = value)  _
						= false) Then
				If Me._Company.HasLoadedOrAssignedValue Then
					Throw New System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException()
				End If
				Me.OnCompanyIdChanging(value)
				Me.SendPropertyChanging
				Me._CompanyId = value
				Me.SendPropertyChanged("CompanyId")
				Me.OnCompanyIdChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_EmailAddress", DbType:="VarChar(200)")>  _
	Public Property EmailAddress() As String
		Get
			Return Me._EmailAddress
		End Get
		Set
			If (String.Equals(Me._EmailAddress, value) = false) Then
				Me.OnEmailAddressChanging(value)
				Me.SendPropertyChanging
				Me._EmailAddress = value
				Me.SendPropertyChanged("EmailAddress")
				Me.OnEmailAddressChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Pic", DbType:="VarBinary(MAX)", CanBeNull:=true, UpdateCheck:=UpdateCheck.Never)>  _
	Public Property Pic() As System.Data.Linq.Binary
		Get
			Return Me._Pic
		End Get
		Set
			If (Object.Equals(Me._Pic, value) = false) Then
				Me.OnPicChanging(value)
				Me.SendPropertyChanging
				Me._Pic = value
				Me.SendPropertyChanged("Pic")
				Me.OnPicChanged
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.AssociationAttribute(Name:="Company_User", Storage:="_Company", ThisKey:="CompanyId", OtherKey:="CompanyId", IsForeignKey:=true)>  _
	Public Property Company() As Company
		Get
			Return Me._Company.Entity
		End Get
		Set
			Dim previousValue As Company = Me._Company.Entity
			If ((Object.Equals(previousValue, value) = false)  _
						OrElse (Me._Company.HasLoadedOrAssignedValue = false)) Then
				Me.SendPropertyChanging
				If ((previousValue Is Nothing)  _
							= false) Then
					Me._Company.Entity = Nothing
					previousValue.Users.Remove(Me)
				End If
				Me._Company.Entity = value
				If ((value Is Nothing)  _
							= false) Then
					value.Users.Add(Me)
					Me._CompanyId = value.CompanyId
				Else
					Me._CompanyId = CType(Nothing, System.Guid)
				End If
				Me.SendPropertyChanged("Company")
			End If
		End Set
	End Property
	
	Public Event PropertyChanging As PropertyChangingEventHandler Implements System.ComponentModel.INotifyPropertyChanging.PropertyChanging
	
	Public Event PropertyChanged As PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
	
	Protected Overridable Sub SendPropertyChanging()
		If ((Me.PropertyChangingEvent Is Nothing)  _
					= false) Then
			RaiseEvent PropertyChanging(Me, emptyChangingEventArgs)
		End If
	End Sub
	
	Protected Overridable Sub SendPropertyChanged(ByVal propertyName As [String])
		If ((Me.PropertyChangedEvent Is Nothing)  _
					= false) Then
			RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
		End If
	End Sub
End Class
