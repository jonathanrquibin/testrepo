﻿Namespace DataObjects
    <Serializable()>
Public Class User

#Region "properties"

        Public Property UserID As Guid
        Public Property UserName As String
        Public Property Password As String
        Public Property Active As Boolean
        Public Property Rights As String
        Public Property CompanyName As String

        Public Property EmailAddress As String

        Public Property CompanyID As Guid

        Public Property Pic() As Byte()
#End Region


#Region "constructors"

        ''' <summary>
        ''' required for serialization
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

        Public Sub New(l As Business.User)

            With l

                Me.Active = .Active
                Me.CompanyName = .Company.Name
                Me.CompanyID = .CompanyId
                Me.UserID = .UserId
                Me.Password = .Password
                Me.Rights = .Rights
                Me.UserName = .UserName
                Me.EmailAddress = .EmailAddress
                Me.Pic = If(.Pic Is Nothing, Nothing, .Pic.ToArray)
            End With
        End Sub

#End Region


#Region "gets & sets"

        Public Shared Function GetAll() As List(Of DataObjects.User)

            Dim retobj As New List(Of DataObjects.User)

            With New CPBDataContext

                retobj = .Users.Select(Function(x) New DataObjects.User(x)).ToList
                .Dispose()
            End With

            Return retobj

        End Function

        Public Shared Function GetRightsList() As List(Of String)

            Dim retobj As New List(Of String) From
                {
                    "ADMIN",
                    "GENERAL"
                    }

            Return retobj

        End Function


        Public Shared Function GetAllForId(x As Guid) As DataObjects.User

            Dim retobj As DataObjects.User = Nothing

            With New CPBDataContext

                retobj = (From l In .Users Where l.UserId = x _
                            Select New DataObjects.User(l)).FirstOrDefault

                .Dispose()
            End With

            Return retobj

        End Function

        Public Shared Function GetAllForCompany(companyName As String) As List(Of DataObjects.User)

            Dim retobj As New List(Of DataObjects.User)

            With New CPBDataContext

                retobj = (From l In .Users Where l.Company.Name.ToLower = companyName.ToLower _
                            Select New DataObjects.User(l)).ToList

                .Dispose()
            End With

            Return retobj

        End Function


        Public Shared Function GetFromUserDetails(emailaddress As String, password As String) As Business.DataObjects.User

            Dim retObj As Business.DataObjects.User = Nothing

            With New CPBDataContext

                Dim foundUser As Business.User

                foundUser = .Users.Where(Function(x) x.EmailAddress = emailaddress _
                                                AndAlso x.Password = password AndAlso x.Active = True).SingleOrDefault

                If foundUser IsNot Nothing Then retObj = New Business.DataObjects.User(foundUser)

                .Dispose()
            End With

            Return retObj

        End Function
        Public Shared Function GetAllFromUser(User As DataObjects.User) As List(Of DataObjects.User)
            Dim retobj As New List(Of DataObjects.User)

            With New CPBDataContext
                If User Is Nothing Then 'temporary
                    retobj = GetAll()
                Else
                    retobj = (From l In .Users Where l.CompanyId = User.CompanyID _
                                Select New DataObjects.User(l)).ToList
                End If
                .Dispose()
            End With
            Return retobj
        End Function
#End Region

#Region "CRUD"

        Public Shared Function Create(x As Business.DataObjects.User) As Guid

            Dim newdbobj As New Business.User

            With New CPBDataContext

                With newdbobj
                    .Active = x.Active
                    .CompanyId = x.CompanyID
                    .UserId = If(x.UserID = Guid.Empty, Guid.NewGuid, x.UserID)
                    .Password = x.Password
                    .Rights = x.Rights
                    .UserName = x.UserName
                    .EmailAddress = x.EmailAddress
                    If x.Pic IsNot Nothing Then _
                            newdbobj.Pic = New System.Data.Linq.Binary(x.Pic)
                End With

                .Users.InsertOnSubmit(newdbobj)
                .SubmitChanges()
                .Dispose()
            End With

            Return newdbobj.UserId

        End Function

        Public Shared Sub Update(alteredUser As DataObjects.User)

            'get the db object
            Dim dbobj As Business.User

            With New CPBDataContext

                dbobj = .Users.Where(Function(x) x.UserId = alteredUser.UserID).Single

                dbobj.Active = alteredUser.Active
                dbobj.Password = If(String.IsNullOrEmpty(alteredUser.Password), dbobj.Password, alteredUser.Password)
                dbobj.Rights = alteredUser.Rights
                dbobj.UserName = alteredUser.UserName
                dbobj.EmailAddress = alteredUser.EmailAddress

                If alteredUser.Pic IsNot Nothing Then _
                        dbobj.Pic = New System.Data.Linq.Binary(alteredUser.Pic)
                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Sub Delete(delObj As Business.DataObjects.User)


            Dim dbo As Business.User

            With New CPBDataContext

                dbo = .Users.Where(Function(x) x.UserId = delObj.UserID).Single

                .Users.DeleteOnSubmit(dbo)

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

#End Region




    End Class

End Namespace