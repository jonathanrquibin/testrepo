﻿Namespace DataObjects

    Public Class Company

#Region "properties"

        Public Property CompanyID As Guid
        Public Property Name As String
        Public Property Address As String
        Public Property PhoneNumber As String

        Public Property CompanyLogo() As Byte()

#End Region

#Region "constructors"

        ''' <summary>
        ''' This is required for serialization to work 
        ''' if there is no blank constructor, then i wont be to build an XML/JSON template
        ''' </summary>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' This constructor accepts C as an instance of the 
        ''' Company object from the lts LINQtoSQL Data Context
        ''' Form this, the DataObjects.company object is built
        ''' </summary>
        ''' <param name="c"></param>
        Public Sub New(c As Business.Company)

            With c

                Me.Address = c.Address
                Me.Name = c.Name
                Me.CompanyID = c.CompanyId
                Me.PhoneNumber = c.PhoneNumber

                Me.CompanyLogo = If(c.LogoBytes Is Nothing, Nothing, c.LogoBytes.ToArray)
                'If IsNot Nothing Then dbobj.ValueObj = New System.Data.Linq.Binary(x.ValueObj)

            End With

        End Sub

#End Region

#Region "CRUD"

        'Create,  update and delete options go here
        'They all need to be static/shared so we can use them without
        'instantiating a class in future

        Public Shared Sub Create(c As DataObjects.Company)

            With New CPBDataContext

                Dim newDbObj As New Business.Company

                'the reason we have these POCO classes is so that we can do things like this
                'in the past i have had issues with the entity framework and LINQtoSQL classes
                'when trying to do specific things when performing an insert / delete etc.
                newDbObj.CompanyId = If(c.CompanyID = Guid.Empty, Guid.NewGuid, c.CompanyID)
                newDbObj.Name = c.Name
                newDbObj.Address = c.Address
                newDbObj.PhoneNumber = c.PhoneNumber

                'çonvert to a linq.binary object to save in the DB, another reason for the POCO class !
                If c.CompanyLogo IsNot Nothing Then _
                            newDbObj.LogoBytes = New System.Data.Linq.Binary(c.CompanyLogo)

                .Companies.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                
                .Dispose()
            End With

        End Sub

        Public Shared Sub Update(c As DataObjects.Company)

            With New CPBDataContext

                Dim dbObj As Business.Company = .Companies.Where(Function(x) x.CompanyId = c.CompanyID).Single

                'we shouldnt have to alter the ID as that is the PK
                dbObj.Name = c.Name
                dbObj.Address = c.Address
                dbObj.PhoneNumber = c.PhoneNumber

                dbObj.LogoBytes = New Data.Linq.Binary(c.CompanyLogo)

                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Shared Sub Delete(c As DataObjects.Company)

            With New CPBDataContext

                Dim dbObj As Business.Company = .Companies.Where(Function(x) x.CompanyId = c.CompanyID).Single

                .Companies.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With

        End Sub

#End Region

#Region "Gets and Sets"

        Public Shared Function GetAll() As List(Of DataObjects.Company)

            Dim retLst As List(Of DataObjects.Company)

            With New CPBDataContext
                'grabs the objects from the database, then creates a list of Dataobjects.Company
                'using the database object as the parameter in the dataobjects.company object
                'in its constructor
                retLst = .Companies.Select(Function(x) New DataObjects.Company(x)).ToList

                'always dispose, cleans the connectoin pool
                .Dispose()
            End With

            Return retLst
        End Function

        Public Shared Function GetFromUser(User As DataObjects.User) As DataObjects.Company
            Dim retobj As DataObjects.Company = Nothing

            With New CPBDataContext

                retobj = (From c In .Companies Where c.CompanyId = User.CompanyID _
                            Select New DataObjects.Company(c)).FirstOrDefault

                .Dispose()
            End With

            Return retobj
        End Function

        Public Shared Function GetFromName(CompanyName As String) As DataObjects.Company
            Dim retobj As DataObjects.Company = Nothing

            With New CPBDataContext

                retobj = (From c In .Companies Where c.Name = CompanyName _
                            Select New DataObjects.Company(c)).FirstOrDefault

                .Dispose()
            End With

            Return retobj
        End Function

        Public Shared Function GetFromID(CompanyID As Guid) As DataObjects.Company
            Dim retobj As DataObjects.Company = Nothing

            With New CPBDataContext

                retobj = (From c In .Companies Where c.CompanyId = CompanyID _
                            Select New DataObjects.Company(c)).FirstOrDefault

                .Dispose()
            End With

            Return retobj
        End Function

#End Region



    End Class


End Namespace
