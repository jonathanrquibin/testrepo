﻿Namespace DataObjects
    Public Class usp_GetManualOverrideDataHistory
#Region "properties"
        Public Property PK As System.Nullable(Of Integer)
        Public Property OverrideLabel As String
        Public Property OverrideValue As String
        Public Property UserID As String
        Public Property OverrideDate As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(modeHistory As Business.usp_GetManualOverrideDataHistoryResult)
            With modeHistory
                Me.PK = .PK
                Me.OverrideLabel = .OverrideLabel
                Me.OverrideValue = .OverrideValue
                Me.UserID = .UserID
                Me.OverrideDate = .OverrideDate
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetManualOverrideHistoryByPkAndLabel(pk As Integer, label As String) As List(Of DataObjects.usp_GetManualOverrideDataHistory)
            Dim retLst As List(Of DataObjects.usp_GetManualOverrideDataHistory)

            With New ltsDataContext
                retLst = .usp_GetManualOverrideDataHistory(pk, label).Select(Function(x) New DataObjects.usp_GetManualOverrideDataHistory(x)).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
#End Region
    End Class
End Namespace
