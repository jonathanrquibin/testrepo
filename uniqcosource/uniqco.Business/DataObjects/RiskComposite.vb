﻿Imports System.Globalization
Imports System.Data.SqlClient

Namespace DataObjects

    Public Class RiskCompositeList
        Inherits List(Of RiskComposite)

        Public Property CompanyName As String

        Public Property ReportDate As Date

    End Class

    Public Class RiskComposite
        'A - AJ
        Private Shared ReadOnly Main As New List(Of String) From
                {"PlantNumber", "ParentPlant", "FleetNumber", "AssetNumber", "Category", "Group", "Type", "Make", "Model", "ModelYear", "RegoNo", _
                 "RegoDate", "RegoState", "MeterType", "CurrentAsset", "PlantImage", "VINNumber", "EngineNumber", "ChassisNumber", "BodyNumber", _
                 "BuildDate", "PreviousRego", "RadioSerialNo", "RadioCallSign", "MiscInformationDoc", "Region", "BusinessUnit", "Department", _
                 "Location", "OperatorDriver", "UsageType", "Supervisor", "SubjectToFBT", "ChargeAccount", "PurchaseOrLease", "PurchaseDate", "LabourHoursSched", "Deviation"}
#Region "properties"
        Public Property ReportDate As Date

        Public Property CompanyReportID As Integer
        Public Property PK As Integer

        Public Property PlantNumber As String
        Public Property ParentPlant As String
        Public Property FleetNumber As String
        Public Property AssetNumber As String
        Public Property Category As String
        Public Property [Group] As String
        Public Property Type As String
        Public Property Make As String
        Public Property Model As String
        Public Property ModelYear As Integer
        Public Property RegoNo As String
        Public Property RegoDate As DateTime?
        Public Property RegoState As String
        Public Property MeterType As String
        Public Property CurrentAsset As String
        Public Property PlantImage As String
        Public Property VINNumber As String
        Public Property EngineNumber As String
        Public Property ChassisNumber As String
        Public Property BodyNumber As String
        Public Property BuildDate As DateTime?
        Public Property PreviousRego As String
        Public Property RadioSerialNo As String
        Public Property RadioCallSign As String
        Public Property MiscInformationDoc As String
        Public Property Region As String
        Public Property BusinessUnit As String
        Public Property Department As String
        Public Property Location As String
        Public Property OperatorDriver As String
        Public Property UsageType As String
        Public Property Supervisor As String
        Public Property SubjectToFBT As String
        Public Property ChargeAccount As String
        Public Property PurchaseOrLease As String
        Public Property PurchaseDate As DateTime?
        Public Property AnnualPredictedIncomeUnits As Integer
        Public Property AnnualBudgetIncomeUnits As Integer
        Public Property IncomeUnitsVar As Integer
        Public Property KmsHrsVar As Integer
        Public Property AnnualAveKms As Integer
        Public Property AnnualAveHrs As Integer
        Public Property AnnualBudgetKms As Integer
        Public Property AnnualBudgetHrs As Integer
        Public Property Hours As Integer
        Public Property LitresHr As Decimal
        Public Property ExpectedFuelEconHr As Decimal
        Public Property LitresPerHour As Decimal
        Public Property FuelTypeHr As String
        Public Property Kilometre As Integer
        Public Property LitresKms As Decimal
        Public Property ExpectedFuelEconKm As Decimal
        Public Property LitresPer100Kms As Decimal
        Public Property FuelTypeKms As String
        Public Property Deviation As Decimal
        Public Property OverheadCost As String
        Public Property FuelCost As String
        Public Property MaintenanceCost As String
        Public Property FBTCost As String
        Public Property LeaseCost As String
        Public Property MonthlyCharge As String
        Public Property HireCharge As String
        Public Property TimeSheetHrs As Decimal
        Public Property CostSubTotal As Decimal
        Public Property IncomeSubTotal As Decimal
        Public Property ProfitLoss As Decimal?
        Public Property ReserveOverRecovery As Decimal?
        Public Property CostVariation As Decimal
        Public Property BudgetReplacementPrice As Decimal
        Public Property BudgetReplacementPriceExcGST As Decimal
        Public Property BudgetDisposalPrice As Decimal
        Public Property BudgetDisposalPriceExcGST As Decimal
        Public Property BudgetChangeoverExcGST As Decimal
        Public Property OptimumReplacementDate As DateTime?
        Public Property CurrentKmsHrs As Decimal
        Public Property DowntimeHrs As Decimal
        Public Property LabourHoursNonSched As Decimal
        Public Property LabourHoursSched As Decimal
        Public Property LabourHoursAdmin As Decimal
        Public Property DowntimeVariation As Decimal
        Public Property LastSafetyServiceDate As DateTime?
        Public Property NextSafetyServiceDue As DateTime?
        Public Property NextStandardServiceDue As DateTime?
        Public Property FlatRate As Decimal
        Public Property LabourHrs As Decimal
        Public Property FlatRateVar As Decimal
        Public Property DowntimeHours As Decimal
        Public Property TotalNWT As String
        Public Property TotalMaintenance As String
        Public Property NWTVar As String
        Public Property AssetDataRisk As String
        Public Property FuelDataRisk As String
        Public Property CostDataRisk As String

#End Region

        Public ReadOnly Property ID As Guid
            Get
                Return Guid.NewGuid
            End Get
        End Property

        Public Shared Property _conn As New ltsDataContext

#Region "Constructor"

        Public Sub New()
        End Sub

        Public Sub New(x As Business.RiskComposite)


            Me.ReportDate = x.ReportDate
            Me.PK = x.pk


            Me.PlantNumber = x.PlantNumber
            Me.ParentPlant = x.ParentPlant
            Me.FleetNumber = x.FleetNumber
            Me.AssetNumber = x.AssetNumber
            Me.Category = x.Category
            Me.Group = x.Group
            Me.Type = x.Type
            Me.Make = x.Make
            Me.Model = x.Model
            Me.ModelYear = x.ModelYear
            Me.RegoNo = x.RegoNo
            Me.RegoDate = x.RegoDate
            Me.RegoState = x.RegoState
            Me.MeterType = x.MeterType
            Me.CurrentAsset = x.CurrentAsset
            Me.PlantImage = x.PlantImage
            Me.VINNumber = x.VINNumber
            Me.EngineNumber = x.EngineNumber
            Me.ChassisNumber = x.ChassisNumber
            Me.BodyNumber = x.BodyNumber
            Me.BuildDate = x.BuildDate
            Me.PreviousRego = x.PreviousRego
            Me.RadioSerialNo = x.RadioSerialNo
            Me.RadioCallSign = x.RadioCallSign
            Me.MiscInformationDoc = x.MiscInformationDoc
            Me.Region = x.Region
            Me.BusinessUnit = x.BusinessUnit
            Me.Department = x.Department
            Me.Location = x.Location
            Me.OperatorDriver = x.OperatorDriver
            Me.UsageType = x.UsageType
            Me.Supervisor = x.Supervisor
            Me.SubjectToFBT = x.SubjectToFBT
            Me.ChargeAccount = x.ChargeAccount
            Me.PurchaseOrLease = x.PurchaseOrLease
            Me.PurchaseDate = x.PurchaseDate
            Me.AnnualPredictedIncomeUnits = x.AnnualPredictedIncomeUnits
            Me.AnnualBudgetIncomeUnits = x.AnnualBudgetIncomeUnits
            Me.IncomeUnitsVar = x.IncomeUnitsVar
            Me.KmsHrsVar = x.KmsHrsVar
            Me.AnnualAveKms = x.AnnualAveKms
            Me.AnnualAveHrs = x.AnnualAveHrs
            Me.AnnualBudgetKms = x.AnnualBudgetKms
            Me.AnnualBudgetHrs = x.AnnualBudgetHrs
            Me.Hours = x.Hours
            Me.LitresHr = x.LitresHr
            Me.ExpectedFuelEconHr = x.ExpectedFuelEconHr
            Me.LitresPerHour = x.LitresPerHour
            Me.FuelTypeHr = x.FuelTypeHr
            Me.Kilometre = x.Kilometre
            Me.LitresKms = x.LitresKms
            Me.ExpectedFuelEconKm = x.ExpectedFuelEconKm
            Me.LitresPer100Kms = x.LitresPer100Kms
            Me.FuelTypeKms = x.FuelTypeKms
            Me.Deviation = x.Deviation
            Me.OverheadCost = x.OverheadCost
            Me.FuelCost = x.FuelCost
            Me.MaintenanceCost = x.MaintenanceCost
            Me.FBTCost = x.FBTCost
            Me.LeaseCost = x.LeaseCost
            Me.MonthlyCharge = x.MonthlyCharge
            Me.HireCharge = x.HireCharge
            Me.TimeSheetHrs = x.TimeSheetHrs
            Me.CostSubTotal = x.CostSubTotal
            Me.IncomeSubTotal = x.IncomeSubTotal
            Me.ProfitLoss = x.ProfitLoss
            Me.ReserveOverRecovery = x.ReserveOverRecovery
            Me.CostVariation = x.CostVariation
            Me.BudgetReplacementPrice = x.BudgetReplacementPrice
            Me.BudgetReplacementPriceExcGST = x.BudgetReplacementPriceExcGST
            Me.BudgetDisposalPrice = x.BudgetDisposalPrice
            Me.BudgetDisposalPriceExcGST = x.BudgetDisposalPriceExcGST
            Me.BudgetChangeoverExcGST = x.BudgetChangeoverExcGST
            Me.OptimumReplacementDate = x.OptimumReplacementDate
            Me.CurrentKmsHrs = x.CurrentKmsHrs
            Me.DowntimeHrs = x.DowntimeHrs
            Me.LabourHoursNonSched = x.LabourHoursNonSched
            Me.LabourHoursSched = x.LabourHoursSched
            Me.LabourHoursAdmin = x.LabourHoursAdmin
            Me.DowntimeVariation = x.DowntimeVariation
            Me.LastSafetyServiceDate = x.LastSafetyServiceDate
            Me.NextSafetyServiceDue = x.NextSafetyServiceDue
            Me.NextStandardServiceDue = x.NextStandardServiceDue
            Me.FlatRate = x.FlatRate
            Me.LabourHrs = x.LabourHrs
            Me.FlatRateVar = x.FlatRateVar
            Me.DowntimeHours = x.DowntimeHours
            Me.LabourHoursNonSched = x.LabourHoursNonSched
            Me.LabourHoursSched = x.LabourHoursSched
            Me.LabourHoursAdmin = x.LabourHoursAdmin
            Me.DowntimeVariation = x.DowntimeVariation
            Me.TotalNWT = x.TotalNWT
            Me.TotalMaintenance = x.TotalMaintenance
            Me.NWTVar = x.NWTVar
            Me.AssetDataRisk = x.AssetDataRisk
            Me.FuelDataRisk = x.FuelDataRisk
            Me.CostDataRisk = x.CostDataRisk
            Me.CompanyReportID = x.CompanyReportID

        End Sub
        Public Sub New(excellst As List(Of ExcelParam))

            For Each ep As ExcelParam In excellst

                Try

                    Select Case ep.PropertyName


                        Case "PlantNumber" : Me.PlantNumber = formatParam(ep.PropertyValue)
                        Case "ParentPlant" : Me.ParentPlant = formatParam(ep.PropertyValue)
                        Case "FleetNumber" : Me.FleetNumber = formatParam(ep.PropertyValue)
                        Case "AssetNumber" : Me.AssetNumber = formatParam(ep.PropertyValue)
                        Case "Category" : Me.Category = formatParam(ep.PropertyValue)
                        Case "Group" : Me.Group = formatParam(ep.PropertyValue)
                        Case "Type" : Me.Type = formatParam(ep.PropertyValue)
                        Case "Make" : Me.Make = formatParam(ep.PropertyValue)
                        Case "Model" : Me.Model = formatParam(ep.PropertyValue)
                        Case "ModelYear" : Me.ModelYear = formatParam(ep.PropertyValue)
                        Case "RegoNo" : Me.RegoNo = formatParam(ep.PropertyValue)
                        Case "RegoDate" : Me.RegoDate = formatParam(ep.PropertyValue)
                        Case "RegoState" : Me.RegoState = formatParam(ep.PropertyValue)
                        Case "MeterType" : Me.MeterType = formatParam(ep.PropertyValue)
                        Case "CurrentAsset" : Me.CurrentAsset = formatParam(ep.PropertyValue)
                        Case "PlantImage" : Me.PlantImage = formatParam(ep.PropertyValue)
                        Case "VINNumber" : Me.VINNumber = formatParam(ep.PropertyValue)
                        Case "EngineNumber" : Me.EngineNumber = formatParam(ep.PropertyValue)
                        Case "ChassisNumber" : Me.ChassisNumber = formatParam(ep.PropertyValue)
                        Case "BodyNumber" : Me.BodyNumber = formatParam(ep.PropertyValue)
                        Case "BuildDate" : Me.BuildDate = formatParam(ep.PropertyValue)
                        Case "PreviousRego" : Me.PreviousRego = formatParam(ep.PropertyValue)
                        Case "RadioSerialNo" : Me.RadioSerialNo = formatParam(ep.PropertyValue)
                        Case "RadioCallSign" : Me.RadioCallSign = formatParam(ep.PropertyValue)
                        Case "MiscInformationDoc" : Me.MiscInformationDoc = formatParam(ep.PropertyValue)
                        Case "Region" : Me.Region = formatParam(ep.PropertyValue)
                        Case "BusinessUnit" : Me.BusinessUnit = formatParam(ep.PropertyValue)
                        Case "Department" : Me.Department = formatParam(ep.PropertyValue)
                        Case "Location" : Me.Location = formatParam(ep.PropertyValue)
                        Case "OperatorDriver" : Me.OperatorDriver = formatParam(ep.PropertyValue)
                        Case "UsageType" : Me.UsageType = formatParam(ep.PropertyValue)
                        Case "Supervisor" : Me.Supervisor = formatParam(ep.PropertyValue)
                        Case "SubjectToFBT" : Me.SubjectToFBT = formatParam(ep.PropertyValue)
                        Case "ChargeAccount" : Me.ChargeAccount = formatParam(ep.PropertyValue)
                        Case "PurchaseOrLease" : Me.PurchaseOrLease = formatParam(ep.PropertyValue)
                        Case "PurchaseDate" : Me.PurchaseDate = formatParam(ep.PropertyValue)
                        Case "AnnualPredictedIncomeUnits" : Me.AnnualPredictedIncomeUnits = formatParam(ep.PropertyValue)
                        Case "AnnualBudgetIncomeUnits" : Me.AnnualBudgetIncomeUnits = formatParam(ep.PropertyValue)
                        Case "IncomeUnitsVar" : Me.IncomeUnitsVar = formatParam(ep.PropertyValue)
                        Case "KmsHrsVar" : Me.KmsHrsVar = formatParam(ep.PropertyValue)
                        Case "AnnualAveKms" : Me.AnnualAveKms = formatParam(ep.PropertyValue)
                        Case "AnnualAveHrs" : Me.AnnualAveHrs = formatParam(ep.PropertyValue)
                        Case "AnnualBudgetKms" : Me.AnnualBudgetKms = formatParam(ep.PropertyValue)
                        Case "AnnualBudgetHrs" : Me.AnnualBudgetHrs = formatParam(ep.PropertyValue)
                        Case "Hours" : Me.Hours = formatParam(ep.PropertyValue)
                        Case "LitresHr" : Me.LitresHr = formatParam(ep.PropertyValue)
                        Case "ExpectedFuelEconHr" : Me.ExpectedFuelEconHr = formatParam(ep.PropertyValue)
                        Case "LitresPerHour" : Me.LitresPerHour = formatParam(ep.PropertyValue)
                        Case "FuelTypeHr" : Me.FuelTypeHr = formatParam(ep.PropertyValue)
                        Case "Kilometre" : Me.Kilometre = formatParam(ep.PropertyValue)
                        Case "LitresKms" : Me.LitresKms = formatParam(ep.PropertyValue)
                        Case "ExpectedFuelEconKm" : Me.ExpectedFuelEconKm = formatParam(ep.PropertyValue)
                        Case "LitresPer100Kms" : Me.LitresPer100Kms = formatParam(ep.PropertyValue)
                        Case "FuelTypeKms" : Me.FuelTypeKms = formatParam(ep.PropertyValue)
                        Case "Deviation" : Me.Deviation = formatParam(ep.PropertyValue)
                        Case "OverheadCost" : Me.OverheadCost = formatParam(ep.PropertyValue)
                        Case "FuelCost" : Me.FuelCost = formatParam(ep.PropertyValue)
                        Case "MaintenanceCost" : Me.MaintenanceCost = formatParam(ep.PropertyValue)
                        Case "FBTCost" : Me.FBTCost = formatParam(ep.PropertyValue)
                        Case "LeaseCost" : Me.LeaseCost = formatParam(ep.PropertyValue)
                        Case "MonthlyCharge" : Me.MonthlyCharge = formatParam(ep.PropertyValue)
                        Case "HireCharge" : Me.HireCharge = formatParam(ep.PropertyValue)
                        Case "TimeSheetHrs" : Me.TimeSheetHrs = formatParam(ep.PropertyValue)
                        Case "CostSubTotal" : Me.CostSubTotal = formatParam(ep.PropertyValue)
                        Case "IncomeSubTotal" : Me.IncomeSubTotal = formatParam(ep.PropertyValue)
                        Case "ProfitLoss" : Me.ProfitLoss = formatParam(ep.PropertyValue)
                        Case "ReserveOverRecovery" : Me.ReserveOverRecovery = formatParam(ep.PropertyValue)
                        Case "CostVariation" : Me.CostVariation = formatParam(ep.PropertyValue)
                        Case "BudgetReplacementPrice" : Me.BudgetReplacementPrice = formatParam(ep.PropertyValue)
                        Case "BudgetReplacementPriceExcGST" : Me.BudgetReplacementPriceExcGST = formatParam(ep.PropertyValue)
                        Case "BudgetDisposalPrice" : Me.BudgetDisposalPrice = formatParam(ep.PropertyValue)
                        Case "BudgetDisposalPriceExcGST" : Me.BudgetDisposalPriceExcGST = formatParam(ep.PropertyValue)
                        Case "BudgetChangeoverExcGST" : Me.BudgetChangeoverExcGST = formatParam(ep.PropertyValue)
                        Case "OptimumReplacementDate" : Me.OptimumReplacementDate = formatParam(ep.PropertyValue)
                        Case "CurrentKmsHrs" : Me.CurrentKmsHrs = formatParam(ep.PropertyValue)
                        Case "DowntimeHrs" : Me.DowntimeHrs = formatParam(ep.PropertyValue)
                        Case "LabourHoursNonSched" : Me.LabourHoursNonSched = formatParam(ep.PropertyValue)
                        Case "LabourHoursSched" : Me.LabourHoursSched = formatParam(ep.PropertyValue)
                        Case "LabourHoursAdmin" : Me.LabourHoursAdmin = formatParam(ep.PropertyValue)
                        Case "DowntimeVariation" : Me.DowntimeVariation = formatParam(ep.PropertyValue)
                        Case "LastSafetyServiceDate" : Me.LastSafetyServiceDate = formatParam(ep.PropertyValue)
                        Case "NextSafetyServiceDue" : Me.NextSafetyServiceDue = formatParam(ep.PropertyValue)
                        Case "NextStandardServiceDue" : Me.NextStandardServiceDue = formatParam(ep.PropertyValue)
                        Case "FlatRate" : Me.FlatRate = formatParam(ep.PropertyValue)
                        Case "LabourHrs" : Me.LabourHrs = formatParam(ep.PropertyValue)
                        Case "FlatRateVar" : Me.FlatRateVar = formatParam(ep.PropertyValue)
                        Case "DowntimeHours" : Me.DowntimeHours = formatParam(ep.PropertyValue)
                        Case "TotalNWT" : Me.TotalNWT = formatParam(ep.PropertyValue)
                        Case "TotalMaintenance" : Me.TotalMaintenance = formatParam(ep.PropertyValue)
                        Case "NWTVar" : Me.NWTVar = formatParam(ep.PropertyValue)
                        Case "AssetDataRisk" : Me.AssetDataRisk = formatParam(ep.PropertyValue)
                        Case "FuelDataRisk" : Me.FuelDataRisk = formatParam(ep.PropertyValue)
                        Case "CostDataRisk" : Me.CostDataRisk = formatParam(ep.PropertyValue)

                    End Select

                Catch ex As Exception
                    Throw
                End Try

            Next

        End Sub
#End Region
#Region "Methods"

        Public Function formatParam(val As String) As Object

            If val Is Nothing Then Return Nothing

            If val.ToLower.Trim = "[unknown]" Then Return Nothing
            If val.ToLower.Trim = "[n/a]" Then Return Nothing
            If val.ToLower.Trim = "n/a" Then Return Nothing
            val = val.Trim
            If val.StartsWith("(") And val.EndsWith(")") Then
                If val(1).Equals("$"c) Then
                    val = val.TrimStart("(").TrimStart("$").TrimEnd(")").Insert(0, "-")
                End If
            End If

            val = val.Trim("'").Trim("'").Trim("$").Trim(" "c)

            If String.IsNullOrEmpty(val) Then Return Nothing

            Dim parsedDate As Date
            Dim parsedDec As Decimal

            If Decimal.TryParse(val, parsedDec) Then Return parsedDec
            'TODO [IMPORTANT]: BUGGED !?
            If Date.TryParseExact(val, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, parsedDate) Then
                Return parsedDate
            End If
            If Date.TryParseExact(val, "d/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, parsedDate) Then
                Return parsedDate
            End If
            If Date.TryParseExact(val, "dd/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, parsedDate) Then
                Return parsedDate
            End If
            If Date.TryParseExact(val, "d/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, parsedDate) Then
                Return parsedDate
            End If
            Return val

        End Function

        Public Shared Sub Create(lst As DataObjects.RiskCompositeList)

            Dim newlst As New List(Of Business.RiskComposite)

            For Each o In lst

                newlst.Add(makeDBObj(o))
            Next

            _conn.RiskComposites.InsertAllOnSubmit(newlst)
            _conn.SubmitChanges()

        End Sub

        Public Shared Sub Create(x As DataObjects.RiskComposite)

            Dim newobj As Business.RiskComposite = makeDBObj(x)

            _conn.RiskComposites.InsertOnSubmit(newobj)
            _conn.SubmitChanges()

        End Sub

        Public Shared Sub Create(x As DataObjects.RiskComposite, com As Guid)

            Dim newobj As Business.RiskComposite = makeDBObj(x)

            _conn.RiskComposites.InsertOnSubmit(newobj)
            Dim cr As Business.CompositeAsset
            Dim lstcr = (From a In _conn.CompositeAssets Where a.Make = newobj.Make And a.Model = newobj.Model Select a).ToList()
            If lstcr.Count = 0 Then
                cr = New Business.CompositeAsset()
                _conn.CompositeAssets.InsertOnSubmit(cr)
            Else
                cr = lstcr.First
            End If

            Dim fc = If(newobj.MeterType = "kms", newobj.LitresPer100Kms, newobj.LitresPerHour)
            cr.Averagefuelconsumption += fc
            cr.Make = newobj.Make
            cr.Model = newobj.Model
            cr.Year = newobj.ModelYear
            cr.CompanyId = com
            _conn.SubmitChanges()

        End Sub

        Private Shared Function makeDBObj(x As DataObjects.RiskComposite) As Business.RiskComposite

            Dim newObj As New uniqco.Business.RiskComposite

            Try


                With newObj

                    'the report date (custom)
                    .ReportDate = x.ReportDate
                    'the companyreportID (custom, for indexing and ref integ etc)
                    .CompanyReportID = x.CompanyReportID

                    'the rest of the values (Excel generated)
                    .PlantNumber = x.PlantNumber
                    .ParentPlant = x.ParentPlant
                    .FleetNumber = x.FleetNumber
                    .AssetNumber = x.AssetNumber
                    .Category = x.Category
                    .Group = x.Group
                    .Type = x.Type
                    .Make = x.Make
                    .Model = x.Model
                    .ModelYear = x.ModelYear
                    .RegoNo = x.RegoNo
                    .RegoDate = x.RegoDate
                    .RegoState = x.RegoState
                    .MeterType = x.MeterType
                    .CurrentAsset = x.CurrentAsset
                    .PlantImage = x.PlantImage
                    .VINNumber = x.VINNumber
                    .EngineNumber = x.EngineNumber
                    .ChassisNumber = x.ChassisNumber
                    .BodyNumber = x.BodyNumber
                    .BuildDate = x.BuildDate
                    .PreviousRego = x.PreviousRego
                    .RadioSerialNo = x.RadioSerialNo
                    .RadioCallSign = x.RadioCallSign
                    .MiscInformationDoc = x.MiscInformationDoc
                    .Region = x.Region
                    .BusinessUnit = x.BusinessUnit
                    .Department = x.Department
                    .Location = x.Location
                    .OperatorDriver = x.OperatorDriver
                    .UsageType = x.UsageType
                    .Supervisor = x.Supervisor
                    .SubjectToFBT = x.SubjectToFBT
                    .ChargeAccount = x.ChargeAccount
                    .PurchaseOrLease = x.PurchaseOrLease
                    .PurchaseDate = x.PurchaseDate
                    .AnnualPredictedIncomeUnits = x.AnnualPredictedIncomeUnits
                    .AnnualBudgetIncomeUnits = x.AnnualBudgetIncomeUnits
                    .IncomeUnitsVar = x.IncomeUnitsVar
                    .KmsHrsVar = x.KmsHrsVar
                    .AnnualAveKms = x.AnnualAveKms
                    .AnnualAveHrs = x.AnnualAveHrs
                    .AnnualBudgetKms = x.AnnualBudgetKms
                    .AnnualBudgetHrs = x.AnnualBudgetHrs
                    .Hours = x.Hours
                    .LitresHr = x.LitresHr
                    .ExpectedFuelEconHr = x.ExpectedFuelEconHr
                    .LitresPerHour = x.LitresPerHour
                    .FuelTypeHr = x.FuelTypeHr
                    .Kilometre = x.Kilometre
                    .LitresKms = x.LitresKms
                    .ExpectedFuelEconKm = x.ExpectedFuelEconKm
                    .LitresPer100Kms = x.LitresPer100Kms
                    .FuelTypeKms = x.FuelTypeKms
                    .Deviation = x.Deviation
                    .OverheadCost = x.OverheadCost
                    .FuelCost = x.FuelCost
                    .MaintenanceCost = x.MaintenanceCost
                    .FBTCost = x.FBTCost
                    .LeaseCost = x.LeaseCost
                    .MonthlyCharge = x.MonthlyCharge
                    .HireCharge = x.HireCharge
                    .TimeSheetHrs = x.TimeSheetHrs
                    .CostSubTotal = x.CostSubTotal
                    .IncomeSubTotal = x.IncomeSubTotal
                    .ProfitLoss = x.ProfitLoss
                    .ReserveOverRecovery = x.ReserveOverRecovery
                    .CostVariation = x.CostVariation
                    .BudgetReplacementPrice = x.BudgetReplacementPrice
                    .BudgetReplacementPriceExcGST = x.BudgetReplacementPriceExcGST
                    .BudgetDisposalPrice = x.BudgetDisposalPrice
                    .BudgetDisposalPriceExcGST = x.BudgetDisposalPriceExcGST
                    .BudgetChangeoverExcGST = x.BudgetChangeoverExcGST
                    .OptimumReplacementDate = x.OptimumReplacementDate
                    .CurrentKmsHrs = x.CurrentKmsHrs
                    .DowntimeHrs = x.DowntimeHrs
                    .LabourHoursNonSched = x.LabourHoursNonSched
                    .LabourHoursSched = x.LabourHoursSched
                    .LabourHoursAdmin = x.LabourHoursAdmin
                    .DowntimeVariation = x.DowntimeVariation
                    .LastSafetyServiceDate = x.LastSafetyServiceDate
                    .NextSafetyServiceDue = x.NextSafetyServiceDue
                    .NextStandardServiceDue = x.NextStandardServiceDue
                    .FlatRate = x.FlatRate
                    .LabourHrs = x.LabourHrs
                    .FlatRateVar = x.FlatRateVar
                    .DowntimeHours = x.DowntimeHours
                    .TotalNWT = x.TotalNWT
                    .TotalMaintenance = x.TotalMaintenance
                    .NWTVar = x.NWTVar
                    .AssetDataRisk = x.AssetDataRisk
                    .FuelDataRisk = x.FuelDataRisk
                    .CostDataRisk = x.CostDataRisk

                End With

            Catch ex As Exception
                Throw
            End Try

            Return newObj

        End Function

        Public Shared Function GetFromDB() As List(Of DataObjects.RiskComposite)

            Return _conn.RiskComposites.Select(Function(x) New DataObjects.RiskComposite(x)).ToList

        End Function

        Public Shared Function GetColumns(header As String) As List(Of String)

            Dim retobj = New List(Of String)
            retobj.AddRange(Main)

            If header.Equals("Utilisation") Then
                retobj.AddRange({"AnnualPredictedIncomeUnits", "AnnualBudgetIncomeUnits", "IncomeUnitsVar", "KmsHrsVar", "AnnualAveKms", "AnnualAveHrs", "AnnualBudgetKms", "AnnualBudgetHrs", "Hours"})
            ElseIf header.Equals("FuelConsumption") Then
                retobj.AddRange({"ExpectedFuelEconHr", "LitresPerHour", "FuelTypeHr", "Kilometre", "LitresKms", "ExpectedFuelEconKm", "LitresPer100Kms", "FuelTypeKms"})
            ElseIf header.Equals("OperationalProductivity") Then
                retobj.AddRange({"IncomeUnitsVar", "KmsHrsVar", "HireCharge"})
            ElseIf header.Equals("OptimumReplacement") Then
                retobj.AddRange({"OptimumReplacementDate", "CurrentKmsHrs"})
            ElseIf header.Equals("ServiceDue") Then
                retobj.AddRange({"LastStandardServiceDate", "LastStandardServiceType", "LastStandardServiceMeter", "NextStandardServiceDue", "NextStandardServiceType", "CurrentMeterReading", "ServiceOrganisation", "LastSafetyServiceDate", "NextSafetyServiceDue"})
            ElseIf header.Equals("WOLCostVariation") Then
                retobj.AddRange({"OverheadCost", "FuelCost", "MaintenanceCost", "FBTCost", "LeaseCost", "MonthlyCharge", "HireCharge", "TimeSheetHrs", "CostSubTotal", "IncomeSubTotal", "ProfitLoss", "ReserveOverRecovery", "CostVariation", "BudgetReplacementPrice", "BudgetReplacementPriceExcGST", "BudgetDisposalPrice", "BudgetDisposalPriceExcGST", "BudgetChangeoverExcGST"})
            ElseIf header.Equals("MaintenanceRatio") Then
                retobj.AddRange({"DowntimeVariation", "LabourHoursNonSched"})
            End If

            Return retobj

        End Function
        Public Shared Function GetDistinct(param As String, com As Guid) As List(Of String)
            Dim x = New List(Of String)
            Select Case param.ToLower()
                Case "group"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Group IsNot Nothing And b.CompanyID = com Select a.Group).Distinct().ToList
                Case "make"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Make IsNot Nothing And b.CompanyID = com Select a.Make).Distinct().ToList
                Case "type"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Type IsNot Nothing And b.CompanyID = com Select a.Type).Distinct().ToList
                Case "model"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Model IsNot Nothing And b.CompanyID = com Select a.Model).Distinct().ToList

            End Select
            Return x
        End Function
        Public Shared Function GetDistinct(param As String) As List(Of String)
            Dim x = New List(Of String)
            Select Case param.ToLower()
                Case "group"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Group IsNot Nothing Select a.Group).Distinct().ToList
                Case "make"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Make IsNot Nothing Select a.Make).Distinct().ToList
                Case "type"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Type IsNot Nothing Select a.Type).Distinct().ToList
                Case "model"
                    x = (From a In _conn.RiskComposites Join b In _conn.CompanyReports On a.CompanyReportID Equals b.CompanyReportID Where a.Model IsNot Nothing Select a.Model).Distinct().ToList

            End Select
            Return x
        End Function


        Public Shared Function GetForRAGChartAmalgamated(repdate As DateTime) As Data.DataTable
            Dim dt = Createdt()

            Dim y = New ltsDataContext
            Dim retLst = (From c In y.usp_AmalgamatedRAGChart_v3(repdate)
                          Order By c.ReportDate Ascending
                            Select dt.LoadDataRow(New Object() {
                                                    c.AssetID,
                                                    c.VehicleType,
                                                    c.BudgetReplacementPriceExcGST,
                                                    c.ReserveOverRecovery,
                                                    c.ProfitLoss,
                                                    c.CurrentAssetValue,
                                                    c.CostOfDownTime,
                                                    c.CostofUnderUtilisation,
                                                    c.FConsumption,
                                                    c.FConsumptionHrs,
                                                    c.FConsumption100Kms,
                                                    c.CostSubTotal,
                                                    c.DowntimeHrs,
                                                    c.LabourHoursSched,
                                                    c.LabourHoursNonSched,
                                                    c.Category,
                                                    c.Group,
                                                    c.Type,
                                                    c.Make,
                                                    c.Model,
                                                    c.ModelYear,
                                                    c.RegoState,
                                                    c.MeterType,
                                                    c.Region,
                                                    c.BusinessUnit,
                                                    c.Department,
                                                    c.Location,
                                                    c.OperatorDriver,
                                                    c.UsageType,
                                                    c.Supervisor,
                                                    c.PurchaseOrLease,
                                                    c.CompanyId,
                                                    c.CompanyName,
                                                    c.ReportDate,
                                                    c.KmsHrsVar,
                                                    c.IncomeUnitsVar,
                                                    c.Deviation,
                                                    c.OptimumReplacementDate,
                                                    c.NextSafetyServiceDue,
                                                    c.CostVariation,
                                                    c.Utilisation,
                                                    c.OperationalProductivity,
                                                    c.FuelConsumption,
                                                    c.OptimumReplacement,
                                                    c.ServiceDue,
                                                    c.WOLCostVariation,
                                                    c.MaintenanceRatio,
                                                    c.ReportDateMonthYear,
                                                    c.RedUtilisation,
                                                    c.AmberUtilisation,
                                                    c.FadedRedUtilisation,
                                                    c.FadedAmberUtilisation,
                                                    c.GreenUtilisation,
                                                    c.ErrorUtilisation,
                                                    c.RedOperationalProductivity,
                                                    c.AmberOperationalProductivity,
                                                    c.FadedRedOperationalProductivity,
                                                    c.FadedAmberOperationalProductivity,
                                                    c.GreenOperationalProductivity,
                                                    c.ErrorOperationalProductivity,
                                                    c.RedFuelConsumption,
                                                    c.AmberFuelConsumption,
                                                    c.FadedRedFuelConsumption,
                                                    c.FadedAmberFuelConsumption,
                                                    c.GreenFuelConsumption,
                                                    c.ErrorFuelConsumption,
                                                    c.RedOptimumReplacement,
                                                    c.AmberOptimumReplacement,
                                                    c.GreenOptimumReplacement,
                                                    c.ErrorOptimumReplacement,
                                                    c.RedServiceDue,
                                                    c.AmberServiceDue,
                                                    c.GreenServiceDue,
                                                    c.ErrorServiceDue,
                                                    c.RedWOLCostVariation,
                                                    c.AmberWOLCostVariation,
                                                    c.FadedRedWOLCostVariation,
                                                    c.FadedAmberWOLCostVariation,
                                                    c.GreenWOLCostVariation,
                                                    c.ErrorWOLCostVariation,
                                                    c.RedMaintenanceRatio,
                                                    c.AmberMaintenanceRatio,
                                                    c.GreenMaintenanceRatio,
                                                    c.ErrorMaintenanceRatio
                                                    }, False))
            Try
                dt = retLst.CopyToDataTable()
            Catch ex As Exception
                dt = Nothing
            End Try
            y.Dispose()
            Return dt

        End Function
        Public Shared Function GetForRAGChart(com As Guid, repdate As DateTime) As Data.DataTable
            Dim dt = Createdt()

            Dim y = New ltsDataContext
            Dim retLst = (From c In y.usp_CompanyRAGChart_v3(repdate, com)
                          Order By c.ReportDate Ascending
                            Select dt.LoadDataRow(New Object() {
                                                    c.AssetID,
                                                    c.VehicleType,
                                                    c.BudgetReplacementPriceExcGST,
                                                    c.ReserveOverRecovery,
                                                    c.ProfitLoss,
                                                    c.CurrentAssetValue,
                                                    c.CostOfDownTime,
                                                    c.CostofUnderUtilisation,
                                                    c.FConsumption,
                                                    c.FConsumption100Kms,
                                                    c.FConsumptionHrs,
                                                    c.CostSubTotal,
                                                    c.DowntimeHrs,
                                                    c.LabourHoursSched,
                                                    c.LabourHoursNonSched,
                                                    c.Category,
                                                    c.Group,
                                                    c.Type,
                                                    c.Make,
                                                    c.Model,
                                                    c.ModelYear,
                                                    c.RegoState,
                                                    c.MeterType,
                                                    c.Region,
                                                    c.BusinessUnit,
                                                    c.Department,
                                                    c.Location,
                                                    c.OperatorDriver,
                                                    c.UsageType,
                                                    c.Supervisor,
                                                    c.PurchaseOrLease,
                                                    c.CompanyId,
                                                    c.CompanyName,
                                                    c.ReportDate,
                                                    c.KmsHrsVar,
                                                    c.IncomeUnitsVar,
                                                    c.Deviation,
                                                    c.OptimumReplacementDate,
                                                    c.NextSafetyServiceDue,
                                                    c.CostVariation,
                                                    c.Utilisation,
                                                    c.OperationalProductivity,
                                                    c.FuelConsumption,
                                                    c.OptimumReplacement,
                                                    c.ServiceDue,
                                                    c.WOLCostVariation,
                                                    c.MaintenanceRatio,
                                                    c.ReportDateMonthYear,
                                                    c.RedUtilisation,
                                                    c.AmberUtilisation,
                                                    c.FadedRedUtilisation,
                                                    c.FadedAmberUtilisation,
                                                    c.GreenUtilisation,
                                                    c.ErrorUtilisation,
                                                    c.RedOperationalProductivity,
                                                    c.AmberOperationalProductivity,
                                                    c.FadedRedOperationalProductivity,
                                                    c.FadedAmberOperationalProductivity,
                                                    c.GreenOperationalProductivity,
                                                    c.ErrorOperationalProductivity,
                                                    c.RedFuelConsumption,
                                                    c.AmberFuelConsumption,
                                                    c.FadedRedFuelConsumption,
                                                    c.FadedAmberFuelConsumption,
                                                    c.GreenFuelConsumption,
                                                    c.ErrorFuelConsumption,
                                                    c.RedOptimumReplacement,
                                                    c.AmberOptimumReplacement,
                                                    c.GreenOptimumReplacement,
                                                    c.ErrorOptimumReplacement,
                                                    c.RedServiceDue,
                                                    c.AmberServiceDue,
                                                    c.GreenServiceDue,
                                                    c.ErrorServiceDue,
                                                    c.RedWOLCostVariation,
                                                    c.AmberWOLCostVariation,
                                                    c.FadedRedWOLCostVariation,
                                                    c.FadedAmberWOLCostVariation,
                                                    c.GreenWOLCostVariation,
                                                    c.ErrorWOLCostVariation,
                                                    c.RedMaintenanceRatio,
                                                    c.AmberMaintenanceRatio,
                                                    c.GreenMaintenanceRatio,
                                                    c.ErrorMaintenanceRatio,
                                                    c.LitresKms,
                                                    c.Kilometre,
                                                    c.LitresHr,
                                                    c.Hours,
                                                    c.HireCharge,
                                                    c.RedAssetDataRisk,
                                                    c.AmberAssetDataRisk,
                                                    c.GreenAssetDataRisk,
                                                    c.AssetData
                                                    }, False))
            Try
                dt = retLst.CopyToDataTable()
            Catch ex As Exception
                dt = Nothing
            End Try
            y.Dispose()
            Return dt

        End Function


        Public Shared Function GetForBenchMark(com As Guid) As Data.DataTable
            Dim dt = BenchMarkCreatedt()

            Dim y = New ltsDataContext
            Dim retLst = (From c In y.[usp_CompanyBenchMark](com)
                            Select dt.LoadDataRow(New Object() {
                                                   c.CostOfDownTime,
                                                   c.Category,
                                                   c.CostSubTotal,
                                                   c.DowntimeHrs,
                                                   c.LabourHoursSched,
                                                   c.LabourHoursNonSched,
                                                   c.LitresKms,
                                                   c.Kilometre,
                                                   c.LitresHr,
                                                   c.Hours,
                                                   c.Group,
                                                   c.Region,
                                                   c.BusinessUnit,
                                                   c.Department,
                                                   c.Location,
                                                  c.VehicleType,
                                                  c.ReportDate,
                                                  c.MonthlyCharge}, False))
            Try
                dt = retLst.CopyToDataTable()
            Catch ex As Exception
                dt = Nothing
            End Try
            y.Dispose()
            Return dt

        End Function

        Private Shared Function BenchMarkCreatedt() As Data.DataTable

            Dim dt = New Data.DataTable
            dt.Columns.Add("CostOfDownTime", GetType(Double))
            dt.Columns.Add("Category")
            dt.Columns.Add("CostSubTotal", GetType(Double))
            dt.Columns.Add("DowntimeHrs", GetType(Double))
            dt.Columns.Add("LabourHoursSched", GetType(Double))
            dt.Columns.Add("LabourHoursNonSched", GetType(Double))
            dt.Columns.Add("LitresKms", GetType(Double))
            dt.Columns.Add("Kilometre", GetType(Double))
            dt.Columns.Add("LitresHr", GetType(Double))
            dt.Columns.Add("Hours", GetType(Double))
            dt.Columns.Add("Group")
            dt.Columns.Add("Region")
            dt.Columns.Add("BusinessUnit")
            dt.Columns.Add("Department")
            dt.Columns.Add("Location")
            dt.Columns.Add("VehicleType")
            dt.Columns.Add("ReportDate")
            dt.Columns.Add("MonthlyCharge")
            Return dt
        End Function
        Public Shared Function Createdt() As Data.DataTable
            Dim dt = New Data.DataTable
            dt.Columns.Add("AssetID")
            dt.Columns.Add("VehicleType")
            dt.Columns.Add("BudgetReplacementPriceExcGST", GetType(Double))
            dt.Columns.Add("ReserveOverRecovery", GetType(Double))
            dt.Columns.Add("ProfitLoss", GetType(Double))
            dt.Columns.Add("CurrentAssetValue", GetType(Double))
            dt.Columns.Add("CostOfDownTime", GetType(Double))
            dt.Columns.Add("CostofUnderUtilisation", GetType(Double))
            dt.Columns.Add("FConsumption", GetType(Double))
            dt.Columns.Add("FConsumption100Kms", GetType(Double))
            dt.Columns.Add("FConsumptionHrs", GetType(Double))
            dt.Columns.Add("CostSubTotal", GetType(Double))
            dt.Columns.Add("DowntimeHrs", GetType(Double))
            dt.Columns.Add("LabourHoursSched", GetType(Double))
            dt.Columns.Add("LabourHoursNonSched", GetType(Double))
            dt.Columns.Add("Category")
            dt.Columns.Add("Group")
            dt.Columns.Add("Type")
            dt.Columns.Add("Make")
            dt.Columns.Add("Model")
            dt.Columns.Add("ModelYear")
            dt.Columns.Add("RegoState")
            dt.Columns.Add("MeterType")
            dt.Columns.Add("Region")
            dt.Columns.Add("BusinessUnit")
            dt.Columns.Add("Department")
            dt.Columns.Add("Location")
            dt.Columns.Add("OperatorDriver")
            dt.Columns.Add("UsageType")
            dt.Columns.Add("Supervisor")
            dt.Columns.Add("PurchaseOrLease")
            dt.Columns.Add("CompanyId")
            dt.Columns.Add("CompanyName")
            dt.Columns.Add("ReportDate")
            dt.Columns.Add("KmsHrsVar")
            dt.Columns.Add("IncomeUnitsVar")
            dt.Columns.Add("Deviation")
            dt.Columns.Add("OptimumReplacementDate")
            dt.Columns.Add("NextSafetyServiceDue")
            dt.Columns.Add("CostVariation")
            dt.Columns.Add("Utilisation")
            dt.Columns.Add("OperationalProductivity")
            dt.Columns.Add("FuelConsumption")
            dt.Columns.Add("OptimumReplacement")
            dt.Columns.Add("ServiceDue")
            dt.Columns.Add("WOLCostVariation")
            dt.Columns.Add("MaintenanceRatio")
            dt.Columns.Add("ReportDateMonthYear")

            dt.Columns.Add("RedUtilisation", GetType(Integer))
            dt.Columns.Add("AmberUtilisation", GetType(Integer))
            dt.Columns.Add("FadedRedUtilisation", GetType(Integer))
            dt.Columns.Add("FadedAmberUtilisation", GetType(Integer))
            dt.Columns.Add("GreenUtilisation", GetType(Integer))
            dt.Columns.Add("ErrorUtilisation", GetType(Integer))
            dt.Columns.Add("RedOperationalProductivity", GetType(Integer))
            dt.Columns.Add("AmberOperationalProductivity", GetType(Integer))
            dt.Columns.Add("FadedRedOperationalProductivity", GetType(Integer))
            dt.Columns.Add("FadedAmberOperationalProductivity", GetType(Integer))
            dt.Columns.Add("GreenOperationalProductivity", GetType(Integer))
            dt.Columns.Add("ErrorOperationalProductivity", GetType(Integer))
            dt.Columns.Add("RedFuelConsumption", GetType(Integer))
            dt.Columns.Add("AmberFuelConsumption", GetType(Integer))
            dt.Columns.Add("FadedRedFuelConsumption", GetType(Integer))
            dt.Columns.Add("FadedAmberFuelConsumption", GetType(Integer))
            dt.Columns.Add("GreenFuelConsumption", GetType(Integer))
            dt.Columns.Add("ErrorFuelConsumption", GetType(Integer))
            dt.Columns.Add("RedOptimumReplacement", GetType(Integer))
            dt.Columns.Add("AmberOptimumReplacement", GetType(Integer))
            dt.Columns.Add("GreenOptimumReplacement", GetType(Integer))
            dt.Columns.Add("ErrorOptimumReplacement", GetType(Integer))
            dt.Columns.Add("RedServiceDue", GetType(Integer))
            dt.Columns.Add("AmberServiceDue", GetType(Integer))
            dt.Columns.Add("GreenServiceDue", GetType(Integer))
            dt.Columns.Add("ErrorServiceDue", GetType(Integer))
            dt.Columns.Add("RedWOLCostVariation", GetType(Integer))
            dt.Columns.Add("AmberWOLCostVariation", GetType(Integer))
            dt.Columns.Add("FadedRedWOLCostVariation", GetType(Integer))
            dt.Columns.Add("FadedAmberWOLCostVariation", GetType(Integer))
            dt.Columns.Add("GreenWOLCostVariation", GetType(Integer))
            dt.Columns.Add("ErrorWOLCostVariation", GetType(Integer))
            dt.Columns.Add("RedMaintenanceRatio", GetType(Integer))
            dt.Columns.Add("AmberMaintenanceRatio", GetType(Integer))
            dt.Columns.Add("GreenMaintenanceRatio", GetType(Integer))
            dt.Columns.Add("ErrorMaintenanceRatio", GetType(Integer))
            dt.Columns.Add("LitresKms", GetType(Double))
            dt.Columns.Add("Kilometre", GetType(Double))
            dt.Columns.Add("LitresHr", GetType(Double))
            dt.Columns.Add("Hours", GetType(Double))
            dt.Columns.Add("HireCharge")
            dt.Columns.Add("RedAssetDataRisk", GetType(Integer))
            dt.Columns.Add("AmberAssetDataRisk", GetType(Integer))
            dt.Columns.Add("GreenAssetDataRisk", GetType(Integer))
            dt.Columns.Add("AssetData")
            Return dt
        End Function
        Public Shared Function GetRiskCompositeTable(ByVal StartDate As Date, ByVal EndDate As Date) As List(Of RiskComposite)
            Dim RiskDataContext = New uniqco.Business.ltsDataContext
            Dim riskList = (From risk In RiskDataContext.RiskComposites
                           Where risk.ReportDate >= StartDate And risk.ReportDate <= EndDate
                           Select New DataObjects.RiskComposite(risk)).ToList
            Return riskList
        End Function

        Public Shared Function GetQuery(ByVal strQuery As String) As DataSet
            Dim dsRet As New DataSet()
            Using sqlConn As SqlConnection = New SqlConnection(Global.uniqco.Business.My.MySettings.Default.uniqcoConnectionString.ToString())
                Using dsReturn As DataSet = New DataSet()
                    Using sqlAdapter As New SqlDataAdapter(strQuery, sqlConn)
                        sqlAdapter.Fill(dsReturn)
                        dsRet = dsReturn
                    End Using
                End Using
            End Using

            Return dsRet
        End Function
#End Region
    End Class
End Namespace
