﻿Namespace DataObjects
    Public Class FrontPageLinks
        Public Property Name As String
        Public Property Href As String

        Public Sub New(ByVal n As String, ByVal u As String)
            Me.Name = n
            Me.Href = u
        End Sub

    End Class
    Public Class CompanySettings

#Region "properties"

        Public Property SettingsId As Guid
        Public Property CompanyID As Guid
        Public Property ObjectName As String
        Public Property Attribute As String
        Public Property Value As String


#End Region

#Region "constructors"

        ''' <summary>
        ''' This is required for serialization to work 
        ''' if there is no blank constructor, then i wont be to build an XML/JSON template
        ''' </summary>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' This constructor accepts C as an instance of the 
        ''' Company object from the lts LINQtoSQL Data Context
        ''' Form this, the DataObjects.company object is built
        ''' </summary>
        ''' <param name="c"></param>
        Public Sub New(c As Business.CompanySetting)

            With c

                Me.SettingsId = c.SettingsId
                Me.CompanyID = c.CompanyId
                Me.ObjectName = c.ObjectName
                Me.Attribute = c.Attribute

                Me.Value = c.Value

            End With

        End Sub

#End Region

#Region "CRUD"

        'Create,  update and delete options go here
        'They all need to be static/shared so we can use them without
        'instantiating a class in future

        Public Shared Sub Create(c As DataObjects.CompanySettings)

            With New ltsDataContext

                Dim newDbObj As New Business.CompanySetting

                'the reason we have these POCO classes is so that we can do things like this
                'in the past i have had issues with the entity framework and LINQtoSQL classes
                'when trying to do specific things when performing an insert / delete etc.
                newDbObj.SettingsId = If(c.SettingsId = Guid.Empty, Guid.NewGuid, c.SettingsId)
                newDbObj.ObjectName = c.ObjectName
                newDbObj.Attribute = c.Attribute
                newDbObj.Value = c.Value
                newDbObj.CompanyId = c.CompanyID

                'çonvert to a linq.binary object to save in the DB, another reason for the POCO class !

                .CompanySettings.InsertOnSubmit(newDbObj)
                .SubmitChanges()

                .Dispose()
            End With

        End Sub

        Public Shared Sub Update(c As DataObjects.CompanySettings)

            With New ltsDataContext

                Dim dbObj As Business.CompanySetting = .CompanySettings.Where(Function(x) x.SettingsId = c.SettingsId).Single

                'we shouldnt have to alter the ID as that is the PK
                dbObj.CompanyId = c.CompanyID
                dbObj.ObjectName = c.ObjectName
                dbObj.Attribute = c.Attribute
                dbObj.Value = c.Value


                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Shared Sub Delete(c As DataObjects.CompanySettings)

            With New ltsDataContext

                Dim dbObj As Business.CompanySetting = .CompanySettings.Where(Function(x) x.SettingsId = c.SettingsId).Single

                .CompanySettings.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With

        End Sub

#End Region

#Region "Gets and Sets"

        Public Shared Function GetAll() As List(Of DataObjects.CompanySettings)

            Dim retLst As List(Of DataObjects.CompanySettings)

            With New ltsDataContext
                'grabs the objects from the database, then creates a list of Dataobjects.Company
                'using the database object as the parameter in the dataobjects.company object
                'in its constructor
                retLst = .CompanySettings.Select(Function(x) New DataObjects.CompanySettings(x)).ToList

                'always dispose, cleans the connectoin pool
                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetFromObjectNameForUser(name As String, login As DataObjects.Login) As Business.DataObjects.CompanySettings

            Dim retObj As Business.DataObjects.CompanySettings = Nothing

            With New ltsDataContext

                Dim foundUser As Business.CompanySetting

                foundUser = .CompanySettings.Where(Function(x) x.ObjectName = name _
                                                       AndAlso x.CompanyId = login.CompanyID).SingleOrDefault

                If foundUser IsNot Nothing Then retObj = New Business.DataObjects.CompanySettings(foundUser)

                .Dispose()
            End With

            Return retObj

        End Function
        Public Shared Function GetAllForUserCompany(login As DataObjects.Login) As List(Of DataObjects.CompanySettings)

            Dim retobj As New List(Of DataObjects.CompanySettings)

            With New ltsDataContext

                retobj = (From c In .CompanySettings Where c.CompanyId = login.CompanyID _
                            Select New DataObjects.CompanySettings(c)).ToList

                .Dispose()
            End With

            Return retobj

        End Function

        Public Shared Function GetFrontPageLinks() As List(Of String)

            Dim retobj As New List(Of String) From
                {
                    "Fleet Policy",
                    "Fleet Strategy",
                    "Specify",
                    "Procure",
                    "Operate",
                    "Dispose",
                    "Procedure, Behaviours & System"
                    }

            Return retobj

        End Function
        Public Shared Function GetStaticFrontPageLinks(classification As String) As List(Of FrontPageLinks)
            If classification IsNot Nothing Then
                If classification.ToLower = "specify" Or classification.ToLower = "procure" Then
                    Return New List(Of FrontPageLinks) From
                        {
                            New FrontPageLinks("Light Fleet Comparison Tool", "LightFleet.aspx"),
                            New FrontPageLinks("WOL Lifecycle Tool", "WOL.aspx"),
                            New FrontPageLinks("Best Value Analysis Tool", "BestValue.aspx"),
                            New FrontPageLinks("Business Case Template", "Business.aspx")
                            }
                ElseIf classification.ToLower = "operate" Then
                    Return New List(Of FrontPageLinks) From
                        {
                            New FrontPageLinks("Asset Registration Tool", "Asset.aspx"),
                            New FrontPageLinks("Maintenance Job Card Tool", "Maintenance.aspx"),
                            New FrontPageLinks("BI Tool", "BITool.aspx")
                            }
                End If

            End If
            
            Return Nothing
        End Function
#End Region
    End Class

End Namespace
