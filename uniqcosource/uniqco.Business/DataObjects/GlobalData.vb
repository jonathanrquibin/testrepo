﻿Namespace DataObjects
    Public Class GlobalData
#Region "properties"
        Public Property GlobalId As System.Guid
        Public Property InflationRate As System.Nullable(Of Integer)
        Public Property CostDiesel As System.Nullable(Of Double)
        Public Property CostULP As System.Nullable(Of Double)
        Public Property CostPULP As System.Nullable(Of Double)
        Public Property CostBioDieselB20 As System.Nullable(Of Double)
        Public Property CostBioDieselB100 As System.Nullable(Of Double)
        Public Property CostOfElectricity As System.Nullable(Of Double)
        Public Property FuelRebate As System.Nullable(Of Double)
        Public Property InterestRate As System.Nullable(Of Double)
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(gd As Business.GlobalData)
            With gd
                Me.GlobalId = .GlobalId
                Me.InflationRate = .InflationRate
                Me.CostDiesel = .CostDiesel
                Me.CostULP = .CostULP
                Me.CostPULP = .CostPULP
                Me.CostBioDieselB20 = .CostBioDieselB20
                Me.CostBioDieselB100 = .CostBioDieselB100
                Me.CostOfElectricity = .CostOfElectricity
                Me.FuelRebate = .FuelRebate
                Me.InterestRate = .InterestRate
            End With
        End Sub

#End Region
#Region "CRUD"
        Public Shared Sub Create(gd As DataObjects.GlobalData)
            With New ltsDataContext

                Dim newDbObj As New Business.GlobalData
                newDbObj.GlobalId = Guid.NewGuid
                newDbObj.InflationRate = gd.InflationRate
                newDbObj.CostDiesel = gd.CostDiesel
                newDbObj.CostULP = gd.CostULP
                newDbObj.CostPULP = gd.CostPULP
                newDbObj.CostBioDieselB20 = gd.CostBioDieselB20
                newDbObj.CostBioDieselB100 = gd.CostBioDieselB100
                newDbObj.CostOfElectricity = gd.CostOfElectricity
                newDbObj.FuelRebate = gd.FuelRebate
                newDbObj.InterestRate = gd.InterestRate

                .GlobalDatas.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(gd As DataObjects.GlobalData)
            With New ltsDataContext

                Dim dbObj As Business.GlobalData = .GlobalDatas.Where(Function(x) x.GlobalId = gd.GlobalId).Single

                dbObj.InflationRate = gd.InflationRate
                dbObj.CostDiesel = gd.CostDiesel
                dbObj.CostULP = gd.CostULP
                dbObj.CostPULP = gd.CostPULP
                dbObj.CostBioDieselB20 = gd.CostBioDieselB20
                dbObj.CostBioDieselB100 = gd.CostBioDieselB100
                dbObj.CostOfElectricity = gd.CostOfElectricity
                dbObj.FuelRebate = gd.FuelRebate
                dbObj.InterestRate = gd.InterestRate

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(gd As DataObjects.GlobalData)
            With New ltsDataContext
                Dim dbObj As Business.GlobalData = .GlobalDatas.Where(Function(x) x.GlobalId = gd.GlobalId).Single

                .GlobalDatas.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.GlobalData)

            Dim retLst As List(Of DataObjects.GlobalData)

            With New ltsDataContext
                retLst = .GlobalDatas.Select(Function(x) New DataObjects.GlobalData(x)).ToList.OrderBy(Function(x) x.InflationRate).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
#End Region
    End Class
End Namespace
