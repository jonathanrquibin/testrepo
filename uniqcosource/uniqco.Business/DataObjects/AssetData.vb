﻿Imports System.Web

Namespace DataObjects
    Public Class AssetData
#Region "properties"
        'Public Property AssetId As System.Guid
        'Public Property Make As String
        'Public Property Model As String
        'Public Property Category As String
        'Public Property Group As String
        'Public Property MeterType As String
        'Public Property FuelType As String
        'Public Property FuelRebate As String
        'Public Property Image As String
        'Public Property DriveType As String
        'Public Property DriveLineType As String
        'Public Property BodyType As String
        'Public Property VehicleBodyType As String
        'Public Property TyreTypeAndSizeAndNumber As String
        'Public Property WarrantyYears As System.Nullable(Of Integer)
        'Public Property WarrantyKMHrs As System.Nullable(Of Integer)
        'Public Property DefaultReplacementYears As System.Nullable(Of Integer)
        'Public Property DefaultReplacementKMHrs As System.Nullable(Of Integer)
        'Public Property BudgetAnnualKMHrs As System.Nullable(Of Integer)
        'Public Property BudgetTimesheetHrs As System.Nullable(Of Integer)
        'Public Property ReplacementPrice As System.Nullable(Of Double)
        'Public Property DisposalPrice As System.Nullable(Of Double)
        'Public Property MaintenanceCostPerKMHRYR As System.Nullable(Of Double)
        'Public Property FuelCostsPerKMHRYR As System.Nullable(Of Double)
        'Public Property TyreCostPerKMHRYR As System.Nullable(Of Double)
        'Public Property FuelConsumption As String
        'Public Property CO2Output As String
        'Public Property AirPollutionRating As String
        'Public Property ManufacturersFirstServiceMonths As String
        'Public Property FirstServiceOnceOnly As System.Nullable(Of Boolean)
        'Public Property InspectionIntervalMonths As System.Nullable(Of Integer)
        'Public Property ManufacturersServiceIntervalMonths As System.Nullable(Of Integer)
        'Public Property ManufacturersServiceIntervalKMHrs As System.Nullable(Of Integer)
        'Public Property SafetyLifeMonths As System.Nullable(Of Integer)
        'Public Property SafetyLifeKMHrs As System.Nullable(Of Integer)
        'Public Property ExcludeFromFuelConsumptionReport As System.Nullable(Of Boolean)
        'Public Property InspectionTaskList As String
        'Public Property ManufacturersMaintenanceTaskList As String
        'Public Property InServiceProgram As System.Nullable(Of Boolean)
        Public Property AssetId As System.Guid
        Public Property Make As String
        Public Property Group As String
        Public Property Type As String
        Public Property Model As String
        Public Property Model_Series_Base As String
        Public Property Model_Series_Level As String
        Public Property Model_Series_Vin As String
        Public Property Model_ID As String
        Public Property Type_Meter As String
        Public Property Type_Transmission As String
        Public Property Type_Body As String
        Public Property Type_Drive As String
        Public Property Type_VehicleBody As String
        Public Property Type_Value As String
        Public Property Type_Image As String
        Public Property Fuel_Type As String
        Public Property Category As String
        Public Property EngineCapacity As System.Nullable(Of Integer)
        Public Property Weight As System.Nullable(Of Integer)
        Public Property GVM As String
        Public Property GCM As String
        Public Property MaxTowingCapacity As System.Nullable(Of Integer)
        Public Property Tyre_Track_Size_Front_Left As System.Nullable(Of Double)
        Public Property Tyre_Track_Number_Front_Left As System.Nullable(Of Double)
        Public Property Tyre_Track_Size_Rear_Right As System.Nullable(Of Double)
        Public Property Tyre_Track_Number_Rear_Right As System.Nullable(Of Double)
        Public Property Tyre_Track_Life_Meter As System.Nullable(Of Double)
        Public Property Warranty_yrs As System.Nullable(Of Integer)
        Public Property Warranty_Meter As System.Nullable(Of Integer)
        Public Property Average_Replacement_Yrs As System.Nullable(Of Integer)
        Public Property Average_Replacement_Meter As System.Nullable(Of Integer)
        Public Property Average_Annual_Meter As System.Nullable(Of Integer)
        Public Property AveragePrice_Today As System.Nullable(Of Double)
        Public Property DisposalPrice_OptimumPct As System.Nullable(Of Double)
        Public Property DisposalPrice_Year1Pct As System.Nullable(Of Double)
        Public Property DisposalPrice_Year2Pct As System.Nullable(Of Double)
        Public Property DisposalPrice_Year3Pct As System.Nullable(Of Double)
        Public Property DisposalPrice_Year4Pct As System.Nullable(Of Double)
        Public Property DisposalPrice_Year5Pct As System.Nullable(Of Double)
        Public Property Maint_Cost_Per_Meter As System.Nullable(Of Double)
        Public Property Fuel_Cons_Meter As System.Nullable(Of Double)
        Public Property Fuel_Rebate_Status As String
        Public Property Fuel_Rebate_Rate As String
        Public Property CO2_Output As String
        Public Property Air_Pollution_Rating As String
        Public Property ANCAP_Crash_Rating As String
        Public Property Manufacturers_First_Service_Meter As String
        Public Property Manufacturers_First_Service_Months As String
        Public Property Manufacturers_First_Service_OnceOnly As String
        Public Property Tyre_Rotation_Meter As System.Nullable(Of Integer)
        Public Property Inspection_Interval_Months As System.Nullable(Of Integer)
        Public Property Manufacturers_Service_Meter As System.Nullable(Of Integer)
        Public Property Manufacturers_Service_Months As String
        Public Property Safety_Insp_Meter As String
        Public Property Safety_Insp_Months As String
        Public Property Exclude_Fuel_Consumption As String
        Public Property In_Service_Program As String
        Public Property ImageBinary() As Byte()
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(ad As Business.AssetData)

            With ad
                AssetId = .AssetId
                Make = .Make
                Group = .Group
                Type = .Type
                Model = .Model
                Model_Series_Base = .Model_Series_Base
                Model_Series_Level = .Model_Series_Level
                Model_Series_Vin = .Model_Series_Vin
                Model_ID = .Model_ID
                Type_Meter = .Type_Meter
                Type_Transmission = .Type_Transmission
                Type_Body = .Type_Body
                Type_Drive = .Type_Drive
                Type_VehicleBody = .Type_VehicleBody
                Type_Value = .Type_Value
                Type_Image = .Type_Image
                Fuel_Type = .Fuel_Type
                Category = .Category
                EngineCapacity = .EngineCapacity
                Weight = .Weight
                GVM = .GVM
                GCM = .GCM
                MaxTowingCapacity = .MaxTowingCapacity
                Tyre_Track_Size_Front_Left = .Tyre_Track_Size_Front_Left
                Tyre_Track_Number_Front_Left = .Tyre_Track_Number_Front_Left
                Tyre_Track_Size_Rear_Right = .Tyre_Track_Size_Rear_Right
                Tyre_Track_Number_Rear_Right = .Tyre_Track_Number_Rear_Right
                Tyre_Track_Life_Meter = .Tyre_Track_Life_Meter
                Warranty_yrs = .Warranty_yrs
                Warranty_Meter = .Warranty_Meter
                Average_Replacement_Yrs = .Average_Replacement_Yrs
                Average_Replacement_Meter = .Average_Replacement_Meter
                Average_Annual_Meter = .Average_Annual_Meter
                AveragePrice_Today = .AveragePrice_Today
                DisposalPrice_OptimumPct = .DisposalPrice_OptimumPct
                DisposalPrice_Year1Pct = .DisposalPrice_Year1Pct
                DisposalPrice_Year2Pct = .DisposalPrice_Year2Pct
                DisposalPrice_Year3Pct = .DisposalPrice_Year3Pct
                DisposalPrice_Year4Pct = .DisposalPrice_Year4Pct
                DisposalPrice_Year5Pct = .DisposalPrice_Year5Pct
                Maint_Cost_Per_Meter = .Maint_Cost_Per_Meter
                Fuel_Cons_Meter = .Fuel_Cons_Meter
                Fuel_Rebate_Status = .Fuel_Rebate_Status
                Fuel_Rebate_Rate = .Fuel_Rebate_Rate
                CO2_Output = .CO2_Output
                Air_Pollution_Rating = .Air_Pollution_Rating
                ANCAP_Crash_Rating = .ANCAP_Crash_Rating
                Manufacturers_First_Service_Meter = .Manufacturers_First_Service_Meter
                Manufacturers_First_Service_Months = .Manufacturers_First_Service_Months
                Manufacturers_First_Service_OnceOnly = .Manufacturers_First_Service_OnceOnly
                Tyre_Rotation_Meter = .Tyre_Rotation_Meter
                Inspection_Interval_Months = .Inspection_Interval_Months
                Manufacturers_Service_Meter = .Manufacturers_Service_Meter
                Manufacturers_Service_Months = .Manufacturers_Service_Months
                Safety_Insp_Meter = .Safety_Insp_Meter
                Safety_Insp_Months = .Safety_Insp_Months
                Exclude_Fuel_Consumption = .Exclude_Fuel_Consumption
                In_Service_Program = .In_Service_Program
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(ad As DataObjects.AssetData)
            With New ltsDataContext
                
                If ValidateAssetData(ad) Then
                    Throw New Exception("Invalid entry. Data already exists.")
                    Exit Sub
                End If
                Dim newDbObj As New Business.AssetData
                newDbObj.AssetId = Guid.NewGuid
                newDbObj.Make = ad.Make
                newDbObj.Group = ad.Group
                newDbObj.Type = ad.Type
                newDbObj.Model = ad.Model
                newDbObj.Model_Series_Base = ad.Model_Series_Base
                newDbObj.Model_Series_Level = ad.Model_Series_Level
                newDbObj.Model_Series_Vin = ad.Model_Series_Vin
                newDbObj.Model_ID = ad.Model_ID
                newDbObj.Type_Meter = ad.Type_Meter
                newDbObj.Type_Transmission = ad.Type_Transmission
                newDbObj.Type_Body = ad.Type_Body
                newDbObj.Type_Drive = ad.Type_Drive
                newDbObj.Type_VehicleBody = ad.Type_VehicleBody
                newDbObj.Type_Value = ad.Type_Value
                newDbObj.Type_Image = ad.Type_Image
                newDbObj.Fuel_Type = ad.Fuel_Type
                newDbObj.Category = ad.Category
                newDbObj.EngineCapacity = ad.EngineCapacity
                newDbObj.Weight = ad.Weight
                newDbObj.GVM = ad.GVM
                newDbObj.GCM = ad.GCM
                newDbObj.MaxTowingCapacity = ad.MaxTowingCapacity
                newDbObj.Tyre_Track_Size_Front_Left = ad.Tyre_Track_Size_Front_Left
                newDbObj.Tyre_Track_Number_Front_Left = ad.Tyre_Track_Number_Front_Left
                newDbObj.Tyre_Track_Size_Rear_Right = ad.Tyre_Track_Size_Rear_Right
                newDbObj.Tyre_Track_Number_Rear_Right = ad.Tyre_Track_Number_Rear_Right
                newDbObj.Tyre_Track_Life_Meter = ad.Tyre_Track_Life_Meter
                newDbObj.Warranty_yrs = ad.Warranty_yrs
                newDbObj.Warranty_Meter = ad.Warranty_Meter
                newDbObj.Average_Replacement_Yrs = ad.Average_Replacement_Yrs
                newDbObj.Average_Replacement_Meter = ad.Average_Replacement_Meter
                newDbObj.Average_Annual_Meter = ad.Average_Annual_Meter
                newDbObj.AveragePrice_Today = ad.AveragePrice_Today
                newDbObj.DisposalPrice_OptimumPct = ad.DisposalPrice_OptimumPct
                newDbObj.DisposalPrice_Year1Pct = ad.DisposalPrice_Year1Pct
                newDbObj.DisposalPrice_Year2Pct = ad.DisposalPrice_Year2Pct
                newDbObj.DisposalPrice_Year3Pct = ad.DisposalPrice_Year3Pct
                newDbObj.DisposalPrice_Year4Pct = ad.DisposalPrice_Year4Pct
                newDbObj.DisposalPrice_Year5Pct = ad.DisposalPrice_Year5Pct
                newDbObj.Maint_Cost_Per_Meter = ad.Maint_Cost_Per_Meter
                newDbObj.Fuel_Cons_Meter = ad.Fuel_Cons_Meter
                newDbObj.Fuel_Rebate_Status = ad.Fuel_Rebate_Status
                newDbObj.Fuel_Rebate_Rate = ad.Fuel_Rebate_Rate
                newDbObj.CO2_Output = ad.CO2_Output
                newDbObj.Air_Pollution_Rating = ad.Air_Pollution_Rating
                newDbObj.ANCAP_Crash_Rating = ad.ANCAP_Crash_Rating
                newDbObj.Manufacturers_First_Service_Meter = ad.Manufacturers_First_Service_Meter
                newDbObj.Manufacturers_First_Service_Months = ad.Manufacturers_First_Service_Months
                newDbObj.Manufacturers_First_Service_OnceOnly = ad.Manufacturers_First_Service_OnceOnly
                newDbObj.Tyre_Rotation_Meter = ad.Tyre_Rotation_Meter
                newDbObj.Inspection_Interval_Months = ad.Inspection_Interval_Months
                newDbObj.Manufacturers_Service_Meter = ad.Manufacturers_Service_Meter
                newDbObj.Manufacturers_Service_Months = ad.Manufacturers_Service_Months
                newDbObj.Safety_Insp_Meter = ad.Safety_Insp_Meter
                newDbObj.Safety_Insp_Months = ad.Safety_Insp_Months
                newDbObj.Exclude_Fuel_Consumption = ad.Exclude_Fuel_Consumption
                newDbObj.In_Service_Program = ad.In_Service_Program

                'newDbObj.Image = FileMaintenance.SaveImageToFolder(HttpContext.Current.Session("ImageLocByByteArray"))

                .AssetDatas.InsertOnSubmit(newDbObj)
                .SubmitChanges()

                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(ad As DataObjects.AssetData)
            With New ltsDataContext
                Dim dbObj As Business.AssetData = .AssetDatas.Where(Function(x) x.AssetId = ad.AssetId).Single
                dbObj.Make = ad.Make
                dbObj.Group = ad.Group
                dbObj.Type = ad.Type
                dbObj.Model = ad.Model
                dbObj.Model_Series_Base = ad.Model_Series_Base
                dbObj.Model_Series_Level = ad.Model_Series_Level
                dbObj.Model_Series_Vin = ad.Model_Series_Vin
                dbObj.Model_ID = ad.Model_ID
                dbObj.Type_Meter = ad.Type_Meter
                dbObj.Type_Transmission = ad.Type_Transmission
                dbObj.Type_Body = ad.Type_Body
                dbObj.Type_Drive = ad.Type_Drive
                dbObj.Type_VehicleBody = ad.Type_VehicleBody
                dbObj.Type_Value = ad.Type_Value
                dbObj.Type_Image = ad.Type_Image
                dbObj.Fuel_Type = ad.Fuel_Type
                dbObj.Category = ad.Category
                dbObj.EngineCapacity = ad.EngineCapacity
                dbObj.Weight = ad.Weight
                dbObj.GVM = ad.GVM
                dbObj.GCM = ad.GCM
                dbObj.MaxTowingCapacity = ad.MaxTowingCapacity
                dbObj.Tyre_Track_Size_Front_Left = ad.Tyre_Track_Size_Front_Left
                dbObj.Tyre_Track_Number_Front_Left = ad.Tyre_Track_Number_Front_Left
                dbObj.Tyre_Track_Size_Rear_Right = ad.Tyre_Track_Size_Rear_Right
                dbObj.Tyre_Track_Number_Rear_Right = ad.Tyre_Track_Number_Rear_Right
                dbObj.Tyre_Track_Life_Meter = ad.Tyre_Track_Life_Meter
                dbObj.Warranty_yrs = ad.Warranty_yrs
                dbObj.Warranty_Meter = ad.Warranty_Meter
                dbObj.Average_Replacement_Yrs = ad.Average_Replacement_Yrs
                dbObj.Average_Replacement_Meter = ad.Average_Replacement_Meter
                dbObj.Average_Annual_Meter = ad.Average_Annual_Meter
                dbObj.AveragePrice_Today = ad.AveragePrice_Today
                dbObj.DisposalPrice_OptimumPct = ad.DisposalPrice_OptimumPct
                dbObj.DisposalPrice_Year1Pct = ad.DisposalPrice_Year1Pct
                dbObj.DisposalPrice_Year2Pct = ad.DisposalPrice_Year2Pct
                dbObj.DisposalPrice_Year3Pct = ad.DisposalPrice_Year3Pct
                dbObj.DisposalPrice_Year4Pct = ad.DisposalPrice_Year4Pct
                dbObj.DisposalPrice_Year5Pct = ad.DisposalPrice_Year5Pct
                dbObj.Maint_Cost_Per_Meter = ad.Maint_Cost_Per_Meter
                dbObj.Fuel_Cons_Meter = ad.Fuel_Cons_Meter
                dbObj.Fuel_Rebate_Status = ad.Fuel_Rebate_Status
                dbObj.Fuel_Rebate_Rate = ad.Fuel_Rebate_Rate
                dbObj.CO2_Output = ad.CO2_Output
                dbObj.Air_Pollution_Rating = ad.Air_Pollution_Rating
                dbObj.ANCAP_Crash_Rating = ad.ANCAP_Crash_Rating
                dbObj.Manufacturers_First_Service_Meter = ad.Manufacturers_First_Service_Meter
                dbObj.Manufacturers_First_Service_Months = ad.Manufacturers_First_Service_Months
                dbObj.Manufacturers_First_Service_OnceOnly = ad.Manufacturers_First_Service_OnceOnly
                dbObj.Tyre_Rotation_Meter = ad.Tyre_Rotation_Meter
                dbObj.Inspection_Interval_Months = ad.Inspection_Interval_Months
                dbObj.Manufacturers_Service_Meter = ad.Manufacturers_Service_Meter
                dbObj.Manufacturers_Service_Months = ad.Manufacturers_Service_Months
                dbObj.Safety_Insp_Meter = ad.Safety_Insp_Meter
                dbObj.Safety_Insp_Months = ad.Safety_Insp_Months
                dbObj.Exclude_Fuel_Consumption = ad.Exclude_Fuel_Consumption
                dbObj.In_Service_Program = ad.In_Service_Program
               
                'dbObj.Image = FileMaintenance.SaveImageToFolder(HttpContext.Current.Session("ImageLocByByteArray"), dbObj.Image)
                
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(ad As DataObjects.AssetData)
            Dim dbObj As Business.AssetData
            With New ltsDataContext

                dbObj = .AssetDatas.Where(Function(x) x.AssetId = ad.AssetId).Single

                .AssetDatas.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
            'FileMaintenance.DeleteImageFile(dbObj.Image)
        End Sub

#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.AssetData)

            Dim retLst As List(Of DataObjects.AssetData)

            With New ltsDataContext
                retLst = .AssetDatas.Select(Function(x) New DataObjects.AssetData(x)).ToList.OrderBy(Function(x) x.Make).ToList()

                .Dispose()
            End With
            'For Each rl In retLst
            '    Dim fileType As String = rl.Image.Split("\").Reverse.ToArray(0).Split(".")(1).ToString()

            '    rl.ImageBinary = FileMaintenance.imgToByteConverter(HttpContext.Current.Server.MapPath("Files/" + rl.Image))

            'Next

            Return retLst
        End Function
#End Region
#Region "Private Shared Functions and Methods"
        Private Shared Function ValidateAssetData(ad As DataObjects.AssetData) As Boolean
            Dim blnRet As Boolean = False
            With New ltsDataContext
                If HttpContext.Current.Session("AssetId") IsNot Nothing Then
                    Dim dbObj As Business.AssetData = .AssetDatas.Where(Function(x) x.AssetId = HttpContext.Current.Session("AssetId")).Single
                    If Convert.ToString(ad.Make) = Convert.ToString(dbObj.Make) And
                        Convert.ToString(ad.Group) = Convert.ToString(dbObj.Group) And
                        Convert.ToString(ad.Type) = Convert.ToString(dbObj.Type) And
                        Convert.ToString(ad.Model) = Convert.ToString(dbObj.Model) And
                        Convert.ToString(ad.Model_Series_Base) = Convert.ToString(dbObj.Model_Series_Base) And
                        Convert.ToString(ad.Model_Series_Level) = Convert.ToString(dbObj.Model_Series_Level) And
                        Convert.ToString(ad.Model_Series_Vin) = Convert.ToString(dbObj.Model_Series_Vin) And
                        Convert.ToString(ad.Model_ID) = Convert.ToString(dbObj.Model_ID) And
                        Convert.ToString(ad.Type_Meter) = Convert.ToString(dbObj.Type_Meter) And
                        Convert.ToString(ad.Type_Transmission) = Convert.ToString(dbObj.Type_Transmission) And
                        Convert.ToString(ad.Type_Body) = Convert.ToString(dbObj.Type_Body) And
                        Convert.ToString(ad.Type_Drive) = Convert.ToString(dbObj.Type_Drive) And
                        Convert.ToString(ad.Type_VehicleBody) = Convert.ToString(dbObj.Type_VehicleBody) And
                        Convert.ToString(ad.Type_Value) = Convert.ToString(dbObj.Type_Value) And
                        Convert.ToString(ad.Type_Image) = Convert.ToString(dbObj.Type_Image) And
                        Convert.ToString(ad.Fuel_Type) = Convert.ToString(dbObj.Fuel_Type) And
                        Convert.ToString(ad.Category) = Convert.ToString(dbObj.Category) And
                        Convert.ToString(ad.EngineCapacity) = Convert.ToString(dbObj.EngineCapacity) And
                        Convert.ToString(ad.Weight) = Convert.ToString(dbObj.Weight) And
                        Convert.ToString(ad.GVM) = Convert.ToString(dbObj.GVM) And
                        Convert.ToString(ad.GCM) = Convert.ToString(dbObj.GCM) And
                        Convert.ToString(ad.MaxTowingCapacity) = Convert.ToString(dbObj.MaxTowingCapacity) And
                        Convert.ToString(ad.Tyre_Track_Size_Front_Left) = Convert.ToString(dbObj.Tyre_Track_Size_Front_Left) And
                        Convert.ToString(ad.Tyre_Track_Number_Front_Left) = Convert.ToString(dbObj.Tyre_Track_Number_Front_Left) And
                        Convert.ToString(ad.Tyre_Track_Size_Rear_Right) = Convert.ToString(dbObj.Tyre_Track_Size_Rear_Right) And
                        Convert.ToString(ad.Tyre_Track_Number_Rear_Right) = Convert.ToString(dbObj.Tyre_Track_Number_Rear_Right) And
                        Convert.ToString(ad.Tyre_Track_Life_Meter) = Convert.ToString(dbObj.Tyre_Track_Life_Meter) And
                        Convert.ToString(ad.Warranty_yrs) = Convert.ToString(dbObj.Warranty_yrs) And
                        Convert.ToString(ad.Warranty_Meter) = Convert.ToString(dbObj.Warranty_Meter) And
                        Convert.ToString(ad.Average_Replacement_Yrs) = Convert.ToString(dbObj.Average_Replacement_Yrs) And
                        Convert.ToString(ad.Average_Replacement_Meter) = Convert.ToString(dbObj.Average_Replacement_Meter) And
                        Convert.ToString(ad.Average_Annual_Meter) = Convert.ToString(dbObj.Average_Annual_Meter) And
                        Convert.ToString(ad.AveragePrice_Today) = Convert.ToString(dbObj.AveragePrice_Today) And
                        Convert.ToString(ad.DisposalPrice_OptimumPct) = Convert.ToString(dbObj.DisposalPrice_OptimumPct) And
                        Convert.ToString(ad.DisposalPrice_Year1Pct) = Convert.ToString(dbObj.DisposalPrice_Year1Pct) And
                        Convert.ToString(ad.DisposalPrice_Year2Pct) = Convert.ToString(dbObj.DisposalPrice_Year2Pct) And
                        Convert.ToString(ad.DisposalPrice_Year3Pct) = Convert.ToString(dbObj.DisposalPrice_Year3Pct) And
                        Convert.ToString(ad.DisposalPrice_Year4Pct) = Convert.ToString(dbObj.DisposalPrice_Year4Pct) And
                        Convert.ToString(ad.DisposalPrice_Year5Pct) = Convert.ToString(dbObj.DisposalPrice_Year5Pct) And
                        Convert.ToString(ad.Maint_Cost_Per_Meter) = Convert.ToString(dbObj.Maint_Cost_Per_Meter) And
                        Convert.ToString(ad.Fuel_Cons_Meter) = Convert.ToString(dbObj.Fuel_Cons_Meter) And
                        Convert.ToString(ad.Fuel_Rebate_Status) = Convert.ToString(dbObj.Fuel_Rebate_Status) And
                        Convert.ToString(ad.Fuel_Rebate_Rate) = Convert.ToString(dbObj.Fuel_Rebate_Rate) And
                        Convert.ToString(ad.CO2_Output) = Convert.ToString(dbObj.CO2_Output) And
                        Convert.ToString(ad.Air_Pollution_Rating) = Convert.ToString(dbObj.Air_Pollution_Rating) And
                        Convert.ToString(ad.ANCAP_Crash_Rating) = Convert.ToString(dbObj.ANCAP_Crash_Rating) And
                        Convert.ToString(ad.Manufacturers_First_Service_Meter) = Convert.ToString(dbObj.Manufacturers_First_Service_Meter) And
                        Convert.ToString(ad.Manufacturers_First_Service_Months) = Convert.ToString(dbObj.Manufacturers_First_Service_Months) And
                        Convert.ToString(ad.Manufacturers_First_Service_OnceOnly) = Convert.ToString(dbObj.Manufacturers_First_Service_OnceOnly) And
                        Convert.ToString(ad.Tyre_Rotation_Meter) = Convert.ToString(dbObj.Tyre_Rotation_Meter) And
                        Convert.ToString(ad.Inspection_Interval_Months) = Convert.ToString(dbObj.Inspection_Interval_Months) And
                        Convert.ToString(ad.Manufacturers_Service_Meter) = Convert.ToString(dbObj.Manufacturers_Service_Meter) And
                        Convert.ToString(ad.Manufacturers_Service_Months) = Convert.ToString(dbObj.Manufacturers_Service_Months) And
                        Convert.ToString(ad.Safety_Insp_Meter) = Convert.ToString(dbObj.Safety_Insp_Meter) And
                        Convert.ToString(ad.Safety_Insp_Months) = Convert.ToString(dbObj.Safety_Insp_Months) And
                        Convert.ToString(ad.Exclude_Fuel_Consumption) = Convert.ToString(dbObj.Exclude_Fuel_Consumption) And
                        Convert.ToString(ad.In_Service_Program) = Convert.ToString(dbObj.In_Service_Program) Then
                        blnRet = True
                    Else
                        blnRet = False
                    End If
                    HttpContext.Current.Session("AssetId") = Nothing
                    .Dispose()
                End If
            End With
            Return blnRet
        End Function
#End Region
    End Class
End Namespace
