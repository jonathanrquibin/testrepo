﻿Namespace DataObjects
    Public Class MaintenanceCostOverride
#Region "properties"
        Public Property MaintenanceCostOverrideID As System.Guid
        Public Property Group As String
        Public Property Make As String
        Public Property Meter As String
        Public Property MainCostPerMeterOverrideValue As String
        Public Property UserID As String
        Public Property MaintenanceCostOverrideDate As System.Nullable(Of Date)
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(mco As Business.MaintenanceCostOverride)
            With mco
                Me.MaintenanceCostOverrideID = .MaintenanceCostOverrideID
                Me.Group = .Group
                Me.Make = .Make
                Me.Meter = .Meter
                Me.MainCostPerMeterOverrideValue = .MainCostPerMeterOverrideValue
                Me.UserID = .UserID
                Me.MaintenanceCostOverrideDate = .MaintenanceCostOverrideDate
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(mco As DataObjects.MaintenanceCostOverride)
            With New ltsDataContext
                Dim newDbObj As New Business.MaintenanceCostOverride
                newDbObj.MaintenanceCostOverrideID = Guid.NewGuid
                newDbObj.Group = mco.Group
                newDbObj.Make = mco.Make
                newDbObj.Meter = mco.Meter
                newDbObj.MainCostPerMeterOverrideValue = mco.MainCostPerMeterOverrideValue
                newDbObj.UserID = mco.UserID
                newDbObj.MaintenanceCostOverrideDate = mco.MaintenanceCostOverrideDate

                .MaintenanceCostOverrides.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(mco As DataObjects.MaintenanceCostOverride)
            With New ltsDataContext
                Dim dbObj As Business.MaintenanceCostOverride = .MaintenanceCostOverrides.Where(Function(x) x.MaintenanceCostOverrideID = mco.MaintenanceCostOverrideID).Single
                dbObj.Group = mco.Group
                dbObj.Make = mco.Make
                dbObj.Meter = mco.Meter
                dbObj.MainCostPerMeterOverrideValue = mco.MainCostPerMeterOverrideValue
                dbObj.UserID = mco.UserID
                dbObj.MaintenanceCostOverrideDate = mco.MaintenanceCostOverrideDate

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(mco As DataObjects.MaintenanceCostOverride)
            With New ltsDataContext

                Dim dbObj As Business.MaintenanceCostOverride = .MaintenanceCostOverrides.Where(Function(x) x.MaintenanceCostOverrideID = mco.MaintenanceCostOverrideID).Single

                .MaintenanceCostOverrides.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.MaintenanceCostOverride)
            Dim retLst As List(Of DataObjects.MaintenanceCostOverride)

            With New ltsDataContext
                retLst = .MaintenanceCostOverrides.Select(Function(x) New DataObjects.MaintenanceCostOverride(x)).ToList.OrderBy(Function(x) x.Group).ThenBy(Function(x) x.Make).ThenBy(Function(x) x.Meter).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetMaintenanceCostOverride(group As String, make As String, meter As String, maintenanceCostPerMeterOverrideValue As String) As List(Of DataObjects.MaintenanceCostOverride)
            Dim retLst As List(Of DataObjects.MaintenanceCostOverride)
            With New ltsDataContext
                retLst = (From m In .MaintenanceCostOverrides
                          Where m.Group.Equals(group) And m.Make.Equals(make) And m.Meter.Equals(meter) And m.MainCostPerMeterOverrideValue.Equals(maintenanceCostPerMeterOverrideValue)
                          Select New DataObjects.MaintenanceCostOverride(m)).ToList()
            End With
            Return retLst
        End Function
#End Region
    End Class
End Namespace