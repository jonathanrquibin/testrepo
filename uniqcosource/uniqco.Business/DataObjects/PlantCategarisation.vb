﻿Namespace DataObjects

    <Serializable()>
    Public Class PlantCategarisation

#Region "properties"

        Public Property MeterType As String
        Public Property Category As String
        Public Property Grouping As String
        Public Property ID As Integer

#End Region


#Region "constructors"


        Public Sub New(pc As Business.PlantCategarisation)

            Me.MeterType = pc.MeterType
            Me.Category = pc.Category
            Me.Grouping = pc.Grouping
            Me.ID = pc.ID

        End Sub

        ''' <summary>
        ''' Required for serialization to work
        ''' </summary>
        Public Sub New()

        End Sub
#End Region


#Region "CRUD"

#End Region


#Region "static/shared functions"

#End Region



        Public Shared Function GetAllFromDB() As List(Of DataObjects.PlantCategarisation)

            With New ltsDataContext

                Return (From s In .PlantCategarisations _
                    Select New DataObjects.PlantCategarisation(s)).ToList
            End With


        End Function



        Public Shared Sub Update(pc As DataObjects.PlantCategarisation)

            With New ltsDataContext

                Dim dbobj As Business.PlantCategarisation = .PlantCategarisations.Where(Function(x) x.ID = pc.ID).Single
                dbobj.Grouping = pc.Grouping
                .SubmitChanges()

            End With

        End Sub
    End Class


End Namespace


