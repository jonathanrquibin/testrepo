﻿
Namespace DataObjects


    Public Class BarChartExampleLine

#Region "properties"


        Public Property ReportDate As Date
        Public Property Utilisation_Type As String
        Public Property count_type As Integer

        'unknown'
        '< 20%'
        '< 30%'
        '+/- 20%'
        '> 20%'
        '> 130%'

        Public ReadOnly Property unknown As Integer
            Get
                Return If(Utilisation_Type = "unknown", count_type, 0)
            End Get
        End Property

        Public ReadOnly Property lessthan20pc As Integer
            Get
                Return If(Utilisation_Type = "< 20%", count_type, 0)
            End Get
        End Property

        Public ReadOnly Property lessthan30pc As Integer
            Get
                Return If(Utilisation_Type = "< 30%", count_type, 0)
            End Get
        End Property

        Public ReadOnly Property plusminus20pc As Integer
            Get
                Return If(Utilisation_Type = "+/- 20%", count_type, 0)
            End Get
        End Property

        Public ReadOnly Property greaterthan20pc As Integer
            Get
                Return If(Utilisation_Type = "> 120%", count_type, 0)
            End Get
        End Property

        Public ReadOnly Property greaterthan30pc As Integer
            Get
                Return If(Utilisation_Type = "> 130%", count_type, 0)
            End Get
        End Property
#End Region

#Region "constructors"

        ''' <summary>
        ''' for serialization purposes only
        ''' </summary>
        Public Sub New()

        End Sub


        Public Sub New(e As usp_BarChartExampleResult)

            Me.ReportDate = e.ReportDate
            Me.Utilisation_Type = e.Utilisation_Type
            Me.count_type = e.count_type

        End Sub
#End Region

#Region "gets and sets"

        Public Shared Function GetForGroup(grpStr As String)



            With New ltsDataContext

                Return (From x In .usp_BarChartExample(grpStr) Select New DataObjects.BarChartExampleLine(x))

            End With

        End Function
#End Region

    End Class
End Namespace