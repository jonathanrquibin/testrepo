﻿Namespace DataObjects
    Public Class ManualOverrideData
#Region "properties"
        Public Property ManualOverrideDataID As System.Guid
        Public Property Make As String
        Public Property Model As String
        Public Property MeterType As String
        Public Property Metric As String
        Public Property UserID As String
        Public Property OverrideDate As System.Nullable(Of Date)
        Public Property Value As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(mo As Business.ManualOverrideData)
            With mo
                Me.ManualOverrideDataID = .ManualOverrideDataID
                Me.Make = .Make
                Me.Model = .Model
                Me.MeterType = .MeterType
                Me.Metric = .Metric
                Me.UserID = .UserID
                Me.OverrideDate = .OverrideDate
                Me.Value = .Value
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(mo As DataObjects.ManualOverrideData)
            With New ltsDataContext
                Dim newDbObj As New Business.ManualOverrideData
                newDbObj.ManualOverrideDataID = Guid.NewGuid
                newDbObj.Make = mo.Make
                newDbObj.Model = mo.Model
                newDbObj.MeterType = mo.MeterType
                newDbObj.Metric = mo.Metric
                newDbObj.UserID = mo.UserID
                newDbObj.OverrideDate = mo.OverrideDate
                newDbObj.Value = mo.Value
                .ManualOverrideDatas.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(mo As DataObjects.ManualOverrideData)
            With New ltsDataContext
                Dim dbObj As Business.ManualOverrideData = .ManualOverrideDatas.Where(Function(x) x.ManualOverrideDataID = mo.ManualOverrideDataID).Single
                dbObj.Make = mo.Make
                dbObj.Model = mo.Model
                dbObj.MeterType = mo.MeterType
                dbObj.Metric = mo.Metric
                dbObj.UserID = mo.UserID
                dbObj.OverrideDate = mo.OverrideDate
                dbObj.Value = mo.Value

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(mo As DataObjects.ManualOverrideData)
            With New ltsDataContext

                Dim dbObj As Business.ManualOverrideData = .ManualOverrideDatas.Where(Function(x) x.ManualOverrideDataID = mo.ManualOverrideDataID).Single

                .ManualOverrideDatas.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.ManualOverrideData)

            Dim retLst As List(Of DataObjects.ManualOverrideData)

            With New ltsDataContext
                retLst = .ManualOverrideDatas.Select(Function(x) New DataObjects.ManualOverrideData(x)).ToList.OrderBy(Function(x) x.Make).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
#End Region
    End Class
End Namespace
