﻿Imports System.Web

Namespace DataObjects
    Public Class usp_GetAssetDataForMaintenance
#Region "properties"
        Public Property MaintenanceAssetDataID As String
        Public Property Group As String
        Public Property Make As String
        Public Property MeterType As String
        Public Property MaintenanceCostPerMeterCalculated As String
        Public Property MaintenanceCostPerMeterOverride As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(usp As Business.usp_GetAssetDataForMaintenanceResult)
            With usp
                Me.MaintenanceAssetDataID = .MaintenanceAssetDataID
                Me.Group = .Group
                Me.Make = .Make
                Me.MeterType = .MeterType
                Me.MaintenanceCostPerMeterCalculated = .MaintenanceCostPerMeterCalculated
                Me.MaintenanceCostPerMeterOverride = .MaintenanceCostPerMeterOverride
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(usp As DataObjects.usp_GetAssetDataForMaintenance)

        End Sub

        Public Shared Sub Update(usp As DataObjects.usp_GetAssetDataForMaintenance)
            Dim mco As New MaintenanceCostOverride()
            Dim maintenanceCost = MaintenanceCostOverride.GetMaintenanceCostOverride(usp.Group, usp.Make, usp.MeterType, usp.MaintenanceCostPerMeterOverride)
            If Not maintenanceCost.Count() > 0 Then
                With usp
                    mco.Group = .Group
                    mco.Make = .Make
                    mco.Meter = .MeterType
                    mco.MainCostPerMeterOverrideValue = .MaintenanceCostPerMeterOverride
                    mco.UserID = DirectCast(HttpContext.Current.Session("UserLogin"), DataObjects.Login).UserName
                    mco.MaintenanceCostOverrideDate = DateTime.Now()
                End With
                MaintenanceCostOverride.Create(mco)
            End If
        End Sub

        Public Shared Sub Delete(usp As DataObjects.usp_GetAssetDataForMaintenance)

        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.usp_GetAssetDataForMaintenance)
            Dim retLst As List(Of DataObjects.usp_GetAssetDataForMaintenance)

            With New ltsDataContext
                retLst = .usp_GetAssetDataForMaintenance.Select(Function(x) New DataObjects.usp_GetAssetDataForMaintenance(x)).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
#End Region
    End Class
End Namespace