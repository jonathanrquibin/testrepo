﻿'TEMP only
Namespace DataObjects
    Public Class MobileAssetData
        Public Property Name As String

        Public Property Asset1 As Object
        Public Property Asset2 As Object
        Public Property Asset3 As Object
        Public Property Asset4 As Object
        Public Property Asset5 As Object

        Public Property Asset1ManualId As Integer
        Public Property Asset2ManualId As Integer
        Public Property Asset3ManualId As Integer
        Public Property Asset4ManualId As Integer
        Public Property Asset5ManualId As Integer

        Public Property Asset1CompositeId As Integer
        Public Property Asset2CompositeId As Integer
        Public Property Asset3CompositeId As Integer
        Public Property Asset4CompositeId As Integer
        Public Property Asset5CompositeId As Integer


        Public Property CompanyId As Guid

        Public ReadOnly Property ID As Guid
            Get
                Return Guid.NewGuid
            End Get
        End Property
#Region "constructors"

        ''' <summary>
        ''' This is required for serialization to work 
        ''' if there is no blank constructor, then i wont be to build an XML/JSON template
        ''' </summary>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' This constructor accepts C as an instance of the 
        ''' Company object from the lts LINQtoSQL Data Context
        ''' Form this, the DataObjects.company object is built
        ''' </summary>



#End Region
#Region "CRUD"
        Public Shared Sub Create(c As DataObjects.MobileAssetData)

            With New ltsDataContext


            End With

        End Sub

        Public Shared Sub Update(c As DataObjects.MobileAssetData)
            UpdateManualEntry(c, c.Asset1ManualId, c.Asset1CompositeId, c.Asset1)
            UpdateManualEntry(c, c.Asset2ManualId, c.Asset2CompositeId, c.Asset2)
            UpdateManualEntry(c, c.Asset3ManualId, c.Asset3CompositeId, c.Asset3)
            UpdateManualEntry(c, c.Asset4ManualId, c.Asset4CompositeId, c.Asset4)
            UpdateManualEntry(c, c.Asset5ManualId, c.Asset5CompositeId, c.Asset5)
            
        End Sub
        Private Shared Function UpdateManualEntry(c As DataObjects.MobileAssetData, Mid As Long, Cid As Long, Val As Object)
            If Cid <> 0 Then
                With New ltsDataContext

                    Dim Manual As Business.ManualEntry
                    Dim Composite = .CompositeAssets.Where(Function(x) x.CompositeAssetId = Cid).Single
                    Dim _Mid = .ManualEntries.Where(Function(x) x.CompositeId = Cid).SingleOrDefault
                    If _Mid Is Nothing Then
                        Manual = New ManualEntry()
                        Manual.CompanyId = c.CompanyId
                        .ManualEntries.InsertOnSubmit(Manual)
                    Else
                        Manual = .ManualEntries.Where(Function(x) x.ManualEntryId = _Mid.ManualEntryId).Single
                    End If

                    Dim column = MainDic().FirstOrDefault(Function(x) x.Value = c.Name).Key
                    Try
                        If Composite IsNot Nothing Then
                            Dim co = Composite.GetType().GetProperty(column).GetValue(Composite)
                            Dim m = Manual.GetType().GetProperty(column).GetValue(Manual)
                            If m Is Nothing And Val = co Then
                                Return Nothing
                            End If
                        End If

                        Manual.GetType().GetProperty(column).SetValue(Manual, Val, Nothing)
                    Catch ex As Exception

                    End Try
                    Manual.CompositeId = Cid
                    .SubmitChanges()

                    .Dispose()
                End With
            End If
            Return Nothing
        End Function
#End Region

#Region "gets and sets"

        Public Shared Function GetForCompany(com As Guid, Make As String) As List(Of DataObjects.MobileAssetData)

            Dim y = New List(Of DataObjects.MobileAssetData)
            Dim lstAsset = New List(Of Object)
            Dim lstcompId = SearchComposite(com, Make)
            If lstcompId IsNot Nothing Then

                For Each ii In lstcompId
                    Dim Asset = New With {
                    .Manual = CreateDictionary(ii, "ManualEntry"),
                    .Comp = CreateDictionary(ii, "CompositeReport")
                    }
                    lstAsset.Add(Asset)
                Next

                For Each s In MainDic()
                    y.Add(New DataObjects.MobileAssetData() With {
                          .Name = s.Value,
                          .Asset1 = If(lstAsset.Count >= 1, If(lstAsset(0).Manual(s.Key) IsNot Nothing, lstAsset(0).Manual(s.Key), lstAsset(0).Comp(s.Key)), Nothing),
                          .Asset2 = If(lstAsset.Count >= 2, If(lstAsset(1).Manual(s.Key) IsNot Nothing, lstAsset(1).Manual(s.Key), lstAsset(1).Comp(s.Key)), Nothing),
                          .Asset3 = If(lstAsset.Count >= 3, If(lstAsset(2).Manual(s.Key) IsNot Nothing, lstAsset(2).Manual(s.Key), lstAsset(2).Comp(s.Key)), Nothing),
                          .Asset4 = If(lstAsset.Count >= 4, If(lstAsset(3).Manual(s.Key) IsNot Nothing, lstAsset(3).Manual(s.Key), lstAsset(3).Comp(s.Key)), Nothing),
                          .Asset5 = If(lstAsset.Count >= 5, If(lstAsset(4).Manual(s.Key) IsNot Nothing, lstAsset(4).Manual(s.Key), lstAsset(4).Comp(s.Key)), Nothing),
                          .Asset1ManualId = If(lstAsset.Count >= 1, If(lstAsset(0).Manual("ManualEntryId") IsNot Nothing, lstAsset(0).Manual("ManualEntryId"), 0), Nothing),
                          .Asset2ManualId = If(lstAsset.Count >= 2, If(lstAsset(1).Manual("ManualEntryId") IsNot Nothing, lstAsset(1).Manual("ManualEntryId"), 0), Nothing),
                          .Asset3ManualId = If(lstAsset.Count >= 3, If(lstAsset(2).Manual("ManualEntryId") IsNot Nothing, lstAsset(2).Manual("ManualEntryId"), 0), Nothing),
                          .Asset4ManualId = If(lstAsset.Count >= 4, If(lstAsset(3).Manual("ManualEntryId") IsNot Nothing, lstAsset(3).Manual("ManualEntryId"), 0), Nothing),
                          .Asset5ManualId = If(lstAsset.Count >= 5, If(lstAsset(4).Manual("ManualEntryId") IsNot Nothing, lstAsset(4).Manual("ManualEntryId"), 0), Nothing),
                          .Asset1CompositeId = If(lstAsset.Count >= 1, If(lstAsset(0).Comp("ManualEntryId") IsNot Nothing, lstAsset(0).Comp("ManualEntryId"), 0), Nothing),
                          .Asset2CompositeId = If(lstAsset.Count >= 2, If(lstAsset(1).Comp("ManualEntryId") IsNot Nothing, lstAsset(1).Comp("ManualEntryId"), 0), Nothing),
                          .Asset3CompositeId = If(lstAsset.Count >= 3, If(lstAsset(2).Comp("ManualEntryId") IsNot Nothing, lstAsset(2).Comp("ManualEntryId"), 0), Nothing),
                          .Asset4CompositeId = If(lstAsset.Count >= 4, If(lstAsset(3).Comp("ManualEntryId") IsNot Nothing, lstAsset(3).Comp("ManualEntryId"), 0), Nothing),
                          .Asset5CompositeId = If(lstAsset.Count >= 5, If(lstAsset(4).Comp("ManualEntryId") IsNot Nothing, lstAsset(4).Comp("ManualEntryId"), 0), Nothing)
                          })
                Next
            End If
            Return y

        End Function
        Public Shared Function GetForPopup(id As Long, col As String)
            Dim Manual = CreateDictionary(id, "ManualEntry")
            Dim Comp = CreateDictionary(id, "CompositeReport")

            Dim column = MainDic().FirstOrDefault(Function(x) x.Value = col).Key

            Dim retval = New With {
                .manual = Manual(column),
                .comp = Comp(column),
                .dump = 0,
                .disp = 0,
                .purch = 0
            }
            Return retval
        End Function
        Public Shared Function GetSearchSource(com As Guid)

            With New ltsDataContext
                Return (From x In .CompositeAssets Where x.CompanyId = com And (x.Make IsNot Nothing) Select x.Make).Distinct.ToList
            End With


        End Function
        Private Shared Function SearchComposite(com As Guid, ss As String)
            Dim ctc = New ltsDataContext
            Return If(String.IsNullOrEmpty(ss), Nothing, (From x In ctc.CompositeAssets Where x.CompanyId = com And (x.Make + x.Model + x.Year).Contains(ss) Select x.CompositeAssetId).Take(5).ToList)
        End Function
        Private Shared Function CreateDictionary(Id As Long, source As String)

            Dim ctc = New ltsDataContext
            Dim retvalSource = (From a In ctc.usp_MAD(Id) Where a.Source = source Select a).ToList()
            Dim retval = If(retvalSource.Count = 0, New usp_MADResult(), retvalSource.OrderByDescending(Function(b) b.ManualEntryId).First())
            'ctc.Dispose()
            Return retval.GetType().GetProperties().ToDictionary(Function(x) x.Name, Function(x) x.GetValue(retval, Nothing))
        End Function
        Private Shared Function MainDic() As Dictionary(Of String, String)
            Dim retval = New Dictionary(Of String, String)
            retval("Make") = "Make"
            retval("Model") = "Model"
            retval("Year") = "Year"
            retval("Description") = "Description"
            retval("PurchasePrice") = "Purchase Price"
            retval("Bodytype") = "Body type"
            retval("OptReplaceyears") = "Opt Replace years"
            retval("OptReplaceKM_Hrs") = "Opt Replace KM/Hrs"
            retval("BudgetannualKM") = "Budget annual KM"
            retval("Bservicetime") = "B service time"
            retval("Bservicekm_hrs") = "B service km/hrs"
            retval("Complianceservicetime") = "Compliance service time"
            retval("ComplianceServiceKM_Hrs") = "Compliance Service KM/Hrs"
            retval("WarrantyYears") = "Warranty Years"
            retval("WarrantyKM") = "Warranty KM"
            retval("Costoftyres_tracks_front") = "Cost of tyres/tracks - front"
            retval("Costoftyres_tracks_rear") = "Cost of tyres/tracks - rear"
            retval("NumberoftyresF") = "Number of tyres F"
            retval("NumberoftyresR") = "Number of tyres R"
            retval("ResaleValue1year15_000km") = "Resale Value 1 year 15,000km"
            retval("ResaleValue1year40_000km") = "Resale Value 1 year 40,000km"
            retval("ResaleValue2year30_000km") = "Resale Value 2 year 30,000km"
            retval("ResaleValue2year80_000km") = "Resale Value 2 year 80,000km"
            retval("Resalevalueendoflife") = "Resale value end of life"
            retval("Averagedepreciation_km_hr") = "Average depreciation /km/hr"
            retval("Averagedepreciationday") = "Average depreciation day"
            retval("Averagefuelconsumption") = "Average fuel consumption"
            retval("AverageMaintandrepair_km_hr_day") = "Average Maint and repair /km/hr/day"
            retval("AveragePurchaseprice") = "Average Purchase price"

            Return retval
        End Function

        Public Shared Function GetCFList() As List(Of String)

            Dim retobj As New List(Of String) From
                {
                    "Group",
                    "Type",
                    "Fuel type",
                    "Meter Type",
                    "Interest rate",
                    "Manufacturers price index",
                    "FBT %",
                    "FBT Factor",
                    "Crash Rating",
                    "CO2 Rating",
                    "Fuel Rebate 1/2/3",
                    "Euro Rating",
                    "Diesel Conversion factor to CO2",
                    "Petrol conversion factor to CO2",
                    "LPG Conversion factor to CO2",
                    "Electric KWh conversion factor to CO2 - Coal - LNG",
                    "Current Market depreciation yr 1"
                    }


            Return retobj

        End Function
#End Region
    End Class
    Public Class CommonFactors

        Public Property CommonFactorsId As Guid
        Public Property CommonFactors As String
        Public Property TableShowingOption As String
        Public Property Include As Boolean
        Public Property Source As String
        Public Property CompanyID As Guid


#Region "constructors"

        ''' <summary>
        ''' This is required for serialization to work 
        ''' if there is no blank constructor, then i wont be to build an XML/JSON template
        ''' </summary>
        Public Sub New()

        End Sub

        Public Sub New(l As Business.CommonFactor)

            With l
                Me.TableShowingOption = .TableShowingOption
                Me.Include = .Include
                Me.Source = .Source
                Me.CompanyID = .CompanyId
                Me.CommonFactors = .CommonFactors
                Me.CommonFactorsId = .CommonFactorsId
            End With
        End Sub

#End Region

#Region "CRUD"

        Public Shared Function Create(x As Business.DataObjects.CommonFactors) As Guid

            Dim newdbobj As New Business.CommonFactor

            With New ltsDataContext

                With newdbobj
                    .CommonFactorsId = If(x.CommonFactorsId = Guid.Empty, Guid.NewGuid, x.CommonFactorsId)
                    .CommonFactors = x.CommonFactors
                    .TableShowingOption = x.TableShowingOption
                    .Include = x.Include
                    .Source = x.Source
                    .CompanyId = x.CompanyID
                End With

                .CommonFactors.InsertOnSubmit(newdbobj)
                .SubmitChanges()
                .Dispose()
            End With

            Return newdbobj.CommonFactorsId

        End Function

        Public Shared Sub Update(alteredCommonFactors As DataObjects.CommonFactors)

            'get the db object
            Dim dbobj As Business.CommonFactor

            With New ltsDataContext

                dbobj = .CommonFactors.Where(Function(x) x.CommonFactorsId = alteredCommonFactors.CommonFactorsId).Single

                dbobj.CompanyId = alteredCommonFactors.CompanyID
                dbobj.TableShowingOption = alteredCommonFactors.TableShowingOption
                dbobj.Include = alteredCommonFactors.Include
                dbobj.Source = alteredCommonFactors.Source

                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Sub Delete(delObj As Business.DataObjects.CommonFactors)


            Dim dbo As Business.CommonFactor

            With New ltsDataContext

                dbo = .CommonFactors.Where(Function(x) x.CommonFactorsId = delObj.CommonFactorsId).Single

                .CommonFactors.DeleteOnSubmit(dbo)

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

#End Region
#Region "Methods"
        Public Shared Function GetForCompany(com As Guid, cf As String) As List(Of DataObjects.CommonFactors)

            Dim retobj As List(Of DataObjects.CommonFactors)

            With New ltsDataContext
                retobj = (From a In .CommonFactors Where a.CompanyId = com And a.CommonFactors = cf Select New DataObjects.CommonFactors(a)).ToList
                'retobj = .CommonFactors.Select(Function(x) New DataObjects.CommonFactors(x)).ToList
                .Dispose()
            End With

            Return retobj

        End Function


#End Region
    End Class
End Namespace
