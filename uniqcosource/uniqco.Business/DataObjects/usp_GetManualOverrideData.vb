﻿Namespace DataObjects
    Public Class usp_GetManualOverrideData
#Region "properties"
        Public Property ColumnHeaders As String
        Public Property Column1 As String
        Public Property Column2 As String
        Public Property Column3 As String
        Public Property Column4 As String
        Public Property Column5 As String
        Public Property Column6 As String
        Public Property Column7 As String
        Public Property Column8 As String
        Public Property Column9 As String
        Public Property Column10 As String
        Public Property Column11 As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(usp As Business.usp_GetManualOverrideDataResult)
            With usp
                Me.ColumnHeaders = .ColumnHeaders
                Me.Column1 = .Column1
                Me.Column2 = .Column2
                Me.Column3 = .Column3
                Me.Column4 = .Column4
                Me.Column5 = .Column5
                Me.Column6 = .Column6
                Me.Column7 = .Column7
                Me.Column8 = .Column8
                Me.Column9 = .Column9
                Me.Column10 = .Column10
                Me.Column11 = .Column11
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(usp As DataObjects.usp_GetManualOverrideData)

        End Sub

        Public Shared Sub Update(usp As DataObjects.usp_GetManualOverrideData)

        End Sub

        Public Shared Sub Delete(usp As DataObjects.usp_GetManualOverrideData)

        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.usp_GetManualOverrideData)
            Dim retLst As List(Of DataObjects.usp_GetManualOverrideData)

            With New ltsDataContext
                retLst = .usp_GetManualOverrideData(1).Select(Function(x) New DataObjects.usp_GetManualOverrideData(x)).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetAllByParameter(Parameter As Integer) As List(Of DataObjects.usp_GetManualOverrideData)
            Dim retLst As List(Of DataObjects.usp_GetManualOverrideData)

            With New ltsDataContext
                retLst = .usp_GetManualOverrideData(Parameter).Select(Function(x) New DataObjects.usp_GetManualOverrideData(x)).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetRiskCompositePages() As Integer
            Dim retLstCount As Integer
            With New ltsDataContext
                retLstCount = (.RiskComposites.Count / 10)
                .Dispose()
            End With
            Return retLstCount
        End Function
#End Region
    End Class
End Namespace