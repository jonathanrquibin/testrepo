﻿Namespace DataObjects
    Public Class FuelConsumptionOverride
#Region "properties"
        Public Property FuelConsumptionOverrideID As System.Guid
        Public Property Make As String
        Public Property Model As String
        Public Property Meter As String
        Public Property MeterOverrideValue As String
        Public Property UserID As String
        Public Property FuelConsumptionOverrideDate As System.Nullable(Of Date)
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(fco As Business.FuelConsumptionOverride)
            With fco
                Me.FuelConsumptionOverrideID = .FuelConsumptionOverrideID
                Me.Make = .Make
                Me.Model = .Model
                Me.Meter = .Meter
                Me.MeterOverrideValue = .MeterOverrideValue
                Me.UserID = .UserID
                Me.FuelConsumptionOverrideDate = .FuelConsumptionOverrideDate
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(fco As DataObjects.FuelConsumptionOverride)
            With New ltsDataContext
                Dim newDbObj As New Business.FuelConsumptionOverride
                newDbObj.FuelConsumptionOverrideID = Guid.NewGuid
                newDbObj.Make = fco.Make
                newDbObj.Model = fco.Model
                newDbObj.Meter = fco.Meter
                newDbObj.MeterOverrideValue = fco.MeterOverrideValue
                newDbObj.UserID = fco.UserID
                newDbObj.FuelConsumptionOverrideDate = fco.FuelConsumptionOverrideDate

                .FuelConsumptionOverrides.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(fco As DataObjects.FuelConsumptionOverride)
            With New ltsDataContext
                Dim dbObj As Business.FuelConsumptionOverride = .FuelConsumptionOverrides.Where(Function(x) x.FuelConsumptionOverrideID = fco.FuelConsumptionOverrideID).Single
                dbObj.Make = fco.Make
                dbObj.Model = fco.Model
                dbObj.Meter = fco.Meter
                dbObj.MeterOverrideValue = fco.MeterOverrideValue
                dbObj.UserID = fco.UserID
                dbObj.FuelConsumptionOverrideDate = fco.FuelConsumptionOverrideDate

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(fco As DataObjects.FuelConsumptionOverride)
            With New ltsDataContext

                Dim dbObj As Business.FuelConsumptionOverride = .FuelConsumptionOverrides.Where(Function(x) x.FuelConsumptionOverrideID = fco.FuelConsumptionOverrideID).Single

                .FuelConsumptionOverrides.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.FuelConsumptionOverride)
            Dim retLst As List(Of DataObjects.FuelConsumptionOverride)

            With New ltsDataContext
                retLst = .FuelConsumptionOverrides.Select(Function(x) New DataObjects.FuelConsumptionOverride(x)).ToList.OrderBy(Function(x) x.Make).ThenBy(Function(x) x.Model).ThenBy(Function(x) x.Meter).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetFuelConsumptionValue(make As String, model As String, meter As String, meteroverridevalue As String) As List(Of DataObjects.FuelConsumptionOverride)
            Dim retLst As List(Of DataObjects.FuelConsumptionOverride)

            With New ltsDataContext
                retLst = (From f In .FuelConsumptionOverrides
                          Where f.Make.Equals(make) And f.Model.Equals(model) And f.Meter.Equals(meter) And f.MeterOverrideValue.Equals(meteroverridevalue)
                          Select New DataObjects.FuelConsumptionOverride(f)).ToList()
            End With
            Return retLst
        End Function
#End Region
    End Class
End Namespace