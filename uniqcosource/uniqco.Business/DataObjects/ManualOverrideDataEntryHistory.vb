﻿Namespace DataObjects
    Public Class ManualOverrideDataEntryHistory
#Region "properties"
        Public Property MODataEntryHistoryId As System.Guid
        Public Property PK As System.Nullable(Of Integer)
        Public Property OverrideLabel As String
        Public Property OverrideValue As String
        Public Property UserID As String
        Public Property OverrideDate As System.Nullable(Of Date)
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(modeHistory As Business.ManualOverrideDataEntryHistory)
            With modeHistory
                Me.MODataEntryHistoryId = .MODataEntryHistoryId
                Me.PK = .PK
                Me.OverrideLabel = .OverrideLabel
                Me.OverrideValue = .OverrideValue
                Me.UserID = .UserID
                Me.OverrideDate = .OverrideDate
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(modeHistory As DataObjects.ManualOverrideDataEntryHistory)
            With New ltsDataContext
                Dim newDbObj As New Business.ManualOverrideDataEntryHistory
                newDbObj.MODataEntryHistoryId = Guid.NewGuid
                newDbObj.PK = modeHistory.PK
                newDbObj.OverrideLabel = modeHistory.OverrideLabel
                newDbObj.OverrideValue = modeHistory.OverrideValue
                newDbObj.UserID = modeHistory.UserID
                newDbObj.OverrideDate = modeHistory.OverrideDate

                .ManualOverrideDataEntryHistories.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(modeHistory As DataObjects.ManualOverrideDataEntryHistory)
            With New ltsDataContext
                Dim dbObj As Business.ManualOverrideDataEntryHistory = .ManualOverrideDataEntryHistories.Where(Function(x) x.MODataEntryHistoryId = modeHistory.MODataEntryHistoryId).Single
                dbObj.PK = modeHistory.PK
                dbObj.OverrideLabel = modeHistory.OverrideLabel
                dbObj.OverrideValue = modeHistory.OverrideValue
                dbObj.UserID = modeHistory.UserID
                dbObj.OverrideDate = modeHistory.OverrideDate

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(modeHistory As DataObjects.ManualOverrideDataEntryHistory)
            With New ltsDataContext

                Dim dbObj As Business.ManualOverrideDataEntryHistory = .ManualOverrideDataEntryHistories.Where(Function(x) x.MODataEntryHistoryId = modeHistory.MODataEntryHistoryId).Single

                .ManualOverrideDataEntryHistories.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.ManualOverrideDataEntryHistory)
            Dim retLst As List(Of DataObjects.ManualOverrideDataEntryHistory)

            With New ltsDataContext
                retLst = .ManualOverrideDataEntryHistories.Select(Function(x) New DataObjects.ManualOverrideDataEntryHistory(x)).ToList.OrderBy(Function(x) x.OverrideDate).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetByPkAndValue(pk As Integer, label As String, value As String) As List(Of DataObjects.ManualOverrideDataEntryHistory)
            Dim retLst As List(Of DataObjects.ManualOverrideDataEntryHistory)

            With New ltsDataContext
                retLst = (From mo In .ManualOverrideDataEntryHistories
                          Where mo.PK.Equals(pk) And mo.OverrideLabel.Equals(label) And mo.OverrideValue.Equals(value)
                          Select New DataObjects.ManualOverrideDataEntryHistory(mo)).ToList()
                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetByPkAndLabel(pk As Integer, label As String) As List(Of DataObjects.usp_GetManualOverrideDataHistory)
            Return DataObjects.usp_GetManualOverrideDataHistory.GetManualOverrideHistoryByPkAndLabel(pk, label)
        End Function
#End Region
    End Class
End Namespace