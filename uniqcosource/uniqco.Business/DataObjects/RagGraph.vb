﻿Namespace DataObjects
    Public Class RagGraph
#Region "Properties"
        Public Property AssetID As Integer
        Public Property VehicleType As String
        Public Property ReportDate As Date
        Public Property RagGraphType As String
        Public Property Utilisation As String
        Public Property OperationalProductivity As String
        Public Property FuelConsumption As String
        Public Property OptimumReplacement As String
        Public Property ServiceDue As String
        Public Property WOLCostVariation As String
        Public Property MaintenanceRatio As String
        Public Property RedUtilisation As Integer
        Public Property AmberUtilisation As Integer
        Public Property FadedRedUtilisation As Integer
        Public Property FadedAmberUtilisation As Integer
        Public Property GreenUtilisation As Integer
        Public Property ErrorUtilisation As Integer
        Public Property RedOperationalProductivity As Integer
        Public Property AmberOperationalProductivity As Integer
        Public Property FadedRedOperationalProductivity As Integer
        Public Property FadedAmberOperationalProductivity As Integer
        Public Property GreenOperationalProductivity As Integer
        Public Property ErrorOperationalProductivity As Integer
        Public Property RedFuelConsumption As Integer
        Public Property AmberFuelConsumption As Integer
        Public Property FadedRedFuelConsumption As Integer
        Public Property FadedAmberFuelConsumption As Integer
        Public Property GreenFuelConsumption As Integer
        Public Property ErrorFuelConsumption As Integer
        Public Property RedOptimumReplacement As Integer
        Public Property AmberOptimumReplacement As Integer
        Public Property GreenOptimumReplacement As Integer
        Public Property ErrorOptimumReplacement As Integer
        Public Property RedServiceDue As Integer
        Public Property AmberServiceDue As Integer
        Public Property GreenServiceDue As Integer
        Public Property ErrorServiceDue As Integer
        Public Property RedWOLCostVariation As Integer
        Public Property AmberWOLCostVariation As Integer
        Public Property FadedRedWOLCostVariation As Integer
        Public Property FadedAmberWOLCostVariation As Integer
        Public Property GreenWOLCostVariation As Integer
        Public Property ErrorWOLCostVariation As Integer
        Public Property RedMaintenanceRatio As Integer
        Public Property AmberMaintenanceRatio As Integer
        Public Property GreenMaintenanceRatio As Integer
        Public Property ErrorMaintenanceRatio As Integer
#End Region

#Region "Methods"
        Public Shared Function GetRagGraphList(dtSource As DataTable) As List(Of RagGraphAll)
            Dim ragGraphList As New List(Of RagGraph)
            For Each dtReport In dtSource.AsEnumerable()
                Dim ragGraph As New uniqco.Business.DataObjects.RagGraph()
                ragGraph.AmberFuelConsumption = dtReport("AmberFuelConsumption")
                ragGraph.AmberMaintenanceRatio = dtReport("AmberMaintenanceRatio")
                ragGraph.AmberOperationalProductivity = dtReport("AmberOperationalProductivity")
                ragGraph.AmberOptimumReplacement = dtReport("AmberOptimumReplacement")
                ragGraph.AmberServiceDue = dtReport("AmberServiceDue")
                ragGraph.AmberUtilisation = dtReport("AmberUtilisation")
                ragGraph.AmberWOLCostVariation = dtReport("AmberWOLCostVariation")
                ragGraph.AssetID = dtReport("AssetID")
                ragGraph.ErrorFuelConsumption = dtReport("ErrorFuelConsumption")
                ragGraph.ErrorMaintenanceRatio = dtReport("ErrorMaintenanceRatio")
                ragGraph.ErrorOperationalProductivity = dtReport("ErrorOperationalProductivity")
                ragGraph.ErrorOptimumReplacement = dtReport("ErrorOptimumReplacement")
                ragGraph.ErrorServiceDue = dtReport("ErrorServiceDue")
                ragGraph.ErrorUtilisation = dtReport("ErrorUtilisation")
                ragGraph.ErrorWOLCostVariation = dtReport("ErrorWOLCostVariation")
                ragGraph.FadedAmberFuelConsumption = dtReport("FadedAmberFuelConsumption")
                ragGraph.FadedAmberOperationalProductivity = dtReport("FadedAmberOperationalProductivity")
                ragGraph.FadedAmberUtilisation = dtReport("FadedAmberUtilisation")
                ragGraph.FadedAmberWOLCostVariation = dtReport("FadedAmberWOLCostVariation")
                ragGraph.FadedRedFuelConsumption = dtReport("FadedRedFuelConsumption")
                ragGraph.FadedRedOperationalProductivity = dtReport("FadedRedOperationalProductivity")
                ragGraph.FadedRedUtilisation = dtReport("FadedRedUtilisation")
                ragGraph.FadedRedWOLCostVariation = dtReport("FadedRedWOLCostVariation")
                ragGraph.FuelConsumption = dtReport("FConsumption")
                ragGraph.GreenFuelConsumption = dtReport("GreenFuelConsumption")
                ragGraph.GreenMaintenanceRatio = dtReport("GreenMaintenanceRatio")
                ragGraph.GreenOperationalProductivity = dtReport("GreenOperationalProductivity")
                ragGraph.GreenOptimumReplacement = dtReport("GreenOptimumReplacement")
                ragGraph.GreenServiceDue = dtReport("GreenServiceDue")
                ragGraph.GreenUtilisation = dtReport("GreenUtilisation")
                ragGraph.GreenWOLCostVariation = dtReport("GreenWOLCostVariation")
                ragGraph.MaintenanceRatio = dtReport("MaintenanceRatio")
                ragGraph.OperationalProductivity = IIf(dtReport("OperationalProductivity").ToString().Equals(""), "", dtReport("OperationalProductivity").ToString())
                ragGraph.OptimumReplacement = dtReport("OptimumReplacement")
                ragGraph.RagGraphType = "RagGraph"
                ragGraph.RedFuelConsumption = dtReport("RedFuelConsumption")
                ragGraph.RedMaintenanceRatio = dtReport("RedMaintenanceRatio")
                ragGraph.RedOperationalProductivity = dtReport("RedOperationalProductivity")
                ragGraph.RedOptimumReplacement = dtReport("RedOptimumReplacement")
                ragGraph.RedServiceDue = dtReport("RedServiceDue")
                ragGraph.RedUtilisation = dtReport("RedUtilisation")
                ragGraph.RedWOLCostVariation = dtReport("RedWOLCostVariation")
                ragGraph.ReportDate = dtReport("ReportDate")
                ragGraph.ServiceDue = dtReport("ServiceDue")
                ragGraph.Utilisation = IIf(dtReport("Utilisation").ToString().Equals(""), "", dtReport("Utilisation").ToString())
                ragGraph.VehicleType = IIf(dtReport("VehicleType").ToString().Equals(""), "", dtReport("VehicleType").ToString())
                ragGraph.WOLCostVariation = IIf(dtReport("WOLCostVariation").ToString().Equals(""), "", dtReport("WOLCostVariation").ToString())
                ragGraphList.Add(ragGraph)
            Next
            Dim rGraphAllList As New List(Of uniqco.Business.DataObjects.RagGraphAll)
            For Each graphCol In ragGraphList.Where(Function(x) Not x.Utilisation.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "Utilisation"
                rGraphAll.GraphColumnOne = graphCol.RedUtilisation
                rGraphAll.GraphColumnTwo = graphCol.AmberUtilisation
                rGraphAll.GraphColumnThree = graphCol.GreenUtilisation
                rGraphAll.GraphColumnFour = graphCol.FadedAmberUtilisation
                rGraphAll.GraphColumnFive = graphCol.FadedRedUtilisation
                rGraphAllList.Add(rGraphAll)
            Next
            For Each graphCol In ragGraphList.Where(Function(x) Not x.OperationalProductivity.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "Productivity"
                rGraphAll.GraphColumnOne = graphCol.RedOperationalProductivity
                rGraphAll.GraphColumnTwo = graphCol.AmberOperationalProductivity
                rGraphAll.GraphColumnThree = graphCol.GreenOperationalProductivity
                rGraphAll.GraphColumnFour = graphCol.FadedAmberOperationalProductivity
                rGraphAll.GraphColumnFive = graphCol.FadedRedOperationalProductivity
                rGraphAllList.Add(rGraphAll)
            Next
            For Each graphCol In ragGraphList.Where(Function(x) Not x.FuelConsumption.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "Consumption"
                rGraphAll.GraphColumnOne = graphCol.RedFuelConsumption
                rGraphAll.GraphColumnTwo = graphCol.AmberFuelConsumption
                rGraphAll.GraphColumnThree = graphCol.GreenFuelConsumption
                rGraphAll.GraphColumnFour = graphCol.FadedAmberFuelConsumption
                rGraphAll.GraphColumnFive = graphCol.FadedRedFuelConsumption
                rGraphAllList.Add(rGraphAll)
            Next
            For Each graphCol In ragGraphList.Where(Function(x) Not x.OptimumReplacement.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "Replacement"
                rGraphAll.GraphColumnOne = graphCol.RedOptimumReplacement
                rGraphAll.GraphColumnTwo = graphCol.AmberOptimumReplacement
                rGraphAll.GraphColumnThree = graphCol.GreenOptimumReplacement
                rGraphAll.GraphColumnFour = 0
                rGraphAll.GraphColumnFive = 0
                rGraphAllList.Add(rGraphAll)
            Next
            For Each graphCol In ragGraphList.Where(Function(x) Not x.ServiceDue.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "ServiceDue"
                rGraphAll.GraphColumnOne = graphCol.RedServiceDue
                rGraphAll.GraphColumnTwo = graphCol.AmberServiceDue
                rGraphAll.GraphColumnThree = graphCol.GreenServiceDue
                rGraphAll.GraphColumnFour = 0
                rGraphAll.GraphColumnFive = 0
                rGraphAllList.Add(rGraphAll)
            Next
            For Each graphCol In ragGraphList.Where(Function(x) Not x.WOLCostVariation.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "Variation"
                rGraphAll.GraphColumnOne = graphCol.RedWOLCostVariation
                rGraphAll.GraphColumnTwo = graphCol.AmberWOLCostVariation
                rGraphAll.GraphColumnThree = graphCol.GreenWOLCostVariation
                rGraphAll.GraphColumnFour = graphCol.FadedAmberWOLCostVariation
                rGraphAll.GraphColumnFive = graphCol.FadedRedWOLCostVariation
                rGraphAllList.Add(rGraphAll)
            Next
            For Each graphCol In ragGraphList.Where(Function(x) Not x.MaintenanceRatio.Equals("")).ToList()
                Dim rGraphAll As New uniqco.Business.DataObjects.RagGraphAll
                rGraphAll.Graph = "Ratio"
                rGraphAll.GraphColumnOne = graphCol.RedMaintenanceRatio
                rGraphAll.GraphColumnTwo = graphCol.AmberMaintenanceRatio
                rGraphAll.GraphColumnThree = graphCol.GreenMaintenanceRatio
                rGraphAll.GraphColumnFour = 0
                rGraphAll.GraphColumnFive = 0
                rGraphAllList.Add(rGraphAll)
            Next
            Return rGraphAllList
        End Function
#End Region
    End Class

    Public Class RagGraphAll
        Public Property Graph As String
        Public Property GraphColumnOne As Integer
        Public Property GraphColumnTwo As Integer
        Public Property GraphColumnThree As Integer
        Public Property GraphColumnFour As Integer
        Public Property GraphColumnFive As Integer
    End Class
End Namespace

