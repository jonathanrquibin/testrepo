﻿Namespace DataObjects

    Public Class Login

#Region "properties"

        Public Property LoginID As Guid
        Public Property UserName As String
        Public Property Password As String
        Public Property Active As Boolean
        Public Property Rights As String
        Public Property CompanyName As String

        Public Property EmailAddress As String

        Public Property CompanyID As Guid

#End Region


#Region "constructors"

        ''' <summary>
        ''' required for serialization
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

        Public Sub New(l As Business.LogIn)

            With l

                Me.Active = .Active
                Me.CompanyName = .Company.Name
                Me.CompanyID = .CompanyId
                Me.LoginID = .LoginId
                Me.Password = .Password
                Me.Rights = .Rights
                Me.UserName = .UserName
                Me.EmailAddress = .EmailAddress
            End With
        End Sub

#End Region


#Region "gets & sets"

        Public Shared Function GetAll() As List(Of DataObjects.Login)

            Dim retobj As New List(Of DataObjects.Login)

            With New ltsDataContext

                retobj = .LogIns.Select(Function(x) New DataObjects.Login(x)).ToList
                .Dispose()
            End With

            Return retobj

        End Function

        Public Shared Function GetRightsList(isUniqco As Boolean) As List(Of String)

            Dim retobj As New List(Of String) From
                {
                    "COMPANY-ADMIN",
                    "COMPANY-GENERAL"
                    }

            If isUniqco.Equals(True) Then
                retobj.AddRange({
                    "UNIQCO"
                })
            End If
            Return retobj

        End Function

        Public Shared Function GetAllForEmailAddress(emailAddress As String) As DataObjects.Login

            Dim retobj As DataObjects.Login = Nothing

            With New ltsDataContext

                retobj = (From l In .LogIns Where l.EmailAddress.ToLower = emailAddress _
                            Select New DataObjects.Login(l)).FirstOrDefault

                .Dispose()
            End With

            Return retobj

        End Function


        Public Shared Function GetAllForId(x As Guid) As DataObjects.Login

            Dim retobj As DataObjects.Login = Nothing

            With New ltsDataContext

                retobj = (From l In .LogIns Where l.LoginId = x _
                            Select New DataObjects.Login(l)).FirstOrDefault

                .Dispose()
            End With

            Return retobj

        End Function

        Public Shared Function GetAllFromUser(login As DataObjects.Login, rights As Boolean) As List(Of DataObjects.Login)
            Dim retobj As New List(Of DataObjects.Login)

            With New ltsDataContext
                If rights = True Then 'uniqco admins has access to all users
                    retobj = GetAll()
                Else 'show users for company
                    retobj = (From l In .LogIns Where l.CompanyId = login.CompanyID _
                                Select New DataObjects.Login(l)).ToList

                End If

                .Dispose()
            End With
            Return retobj
        End Function

        Public Shared Function GetAllForCompany(companyName As String) As List(Of DataObjects.Login)

            Dim retobj As New List(Of DataObjects.Login)

            With New ltsDataContext

                retobj = (From l In .LogIns Where l.Company.Name.ToLower = companyName.ToLower _
                            Select New DataObjects.Login(l)).ToList

                .Dispose()
            End With

            Return retobj

        End Function


        Public Shared Function GetFromLoginDetails(UserName As String, password As String) As Business.DataObjects.Login

            Dim retObj As Business.DataObjects.Login = Nothing

            With New ltsDataContext

                Dim foundUser As Business.LogIn

                foundUser = .LogIns.Where(Function(x) (x.EmailAddress = UserName Or x.UserName = UserName) _
                                                AndAlso x.Password = password AndAlso x.Active = True).SingleOrDefault

                If foundUser IsNot Nothing Then retObj = New Business.DataObjects.Login(foundUser)

                .Dispose()
            End With

            Return retObj

        End Function



#End Region

#Region "CRUD"

        Public Shared Function Create(x As Business.DataObjects.Login) As Guid

            Dim newdbobj As New Business.LogIn

            With New ltsDataContext

                With newdbobj
                    .Active = x.Active
                    .CompanyId = x.CompanyID
                    .LoginId = If(x.LoginID = Guid.Empty, Guid.NewGuid, x.LoginID)
                    .Password = x.Password
                    .Rights = x.Rights
                    .UserName = x.UserName
                    .EmailAddress = x.EmailAddress
                End With

                .LogIns.InsertOnSubmit(newdbobj)
                .SubmitChanges()
                .Dispose()
            End With

            Return newdbobj.LoginId

        End Function

        Public Shared Sub Update(alteredLogin As DataObjects.Login)

            'get the db object
            Dim dbobj As Business.LogIn

            With New ltsDataContext

                dbobj = .LogIns.Where(Function(x) x.LoginId = alteredLogin.LoginID).Single

                dbobj.Active = alteredLogin.Active
                dbobj.CompanyId = alteredLogin.CompanyID
                dbobj.Password = If(String.IsNullOrEmpty(alteredLogin.Password), dbobj.Password, alteredLogin.Password)
                dbobj.Rights = alteredLogin.Rights
                dbobj.UserName = alteredLogin.UserName
                dbobj.EmailAddress = alteredLogin.EmailAddress

                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Sub Delete(delObj As Business.DataObjects.Login)



            Dim dbo As Business.LogIn

            With New ltsDataContext

                dbo = .LogIns.Where(Function(x) x.LoginId = delObj.LoginID).Single

                .LogIns.DeleteOnSubmit(dbo)

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

#End Region




    End Class

End Namespace



