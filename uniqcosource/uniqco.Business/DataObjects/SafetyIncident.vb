﻿Namespace DataObjects
    Public Class SafetyIncident

#Region "properties"

        Public Property IncidentId As Guid
        Public Property CompanyID As Guid
        Public Property [Date] As String
        Public Property Number As String


#End Region
#Region "constructors"

        ''' <summary>
        ''' This is required for serialization to work 
        ''' if there is no blank constructor, then i wont be to build an XML/JSON template
        ''' </summary>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' This constructor accepts C as an instance of the 
        ''' Company object from the lts LINQtoSQL Data Context
        ''' Form this, the DataObjects.company object is built
        ''' </summary>
        ''' <param name="c"></param>
        Public Sub New(c As Business.SafetyIncident)

            With c

                Me.IncidentId = c.IncidentId
                Me.CompanyID = c.CompanyId
                Me.Date = c.Date
                Me.Number = c.Number

            End With

        End Sub

#End Region
#Region "CRUD"

        'Create,  update and delete options go here
        'They all need to be static/shared so we can use them without
        'instantiating a class in future

        Public Shared Sub Create(c As DataObjects.SafetyIncident)

            With New ltsDataContext

                Dim newDbObj As New Business.SafetyIncident

                'the reason we have these POCO classes is so that we can do things like this
                'in the past i have had issues with the entity framework and LINQtoSQL classes
                'when trying to do specific things when performing an insert / delete etc.
                newDbObj.IncidentId = If(c.IncidentId = Guid.Empty, Guid.NewGuid, c.IncidentId)
                newDbObj.Date = c.Date
                newDbObj.Number = c.Number
                newDbObj.CompanyId = c.CompanyID

                'çonvert to a linq.binary object to save in the DB, another reason for the POCO class !

                .SafetyIncidents.InsertOnSubmit(newDbObj)
                .SubmitChanges()

                .Dispose()
            End With

        End Sub

        Public Shared Sub Update(c As DataObjects.SafetyIncident)

            With New ltsDataContext

                Dim dbObj As Business.SafetyIncident = .SafetyIncidents.Where(Function(x) x.IncidentId = c.IncidentId).Single

                'we shouldnt have to alter the ID as that is the PK
                dbObj.CompanyId = c.CompanyID
                dbObj.Date = c.Date
                dbObj.Number = c.Number


                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Shared Sub Delete(c As DataObjects.SafetyIncident)

            With New ltsDataContext

                Dim dbObj As Business.SafetyIncident = .SafetyIncidents.Where(Function(x) x.IncidentId = c.IncidentId).Single

                .SafetyIncidents.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With

        End Sub

#End Region
#Region "Gets and Sets"

        Public Shared Function GetAll() As List(Of DataObjects.SafetyIncident)

            Dim retLst As List(Of DataObjects.SafetyIncident)

            With New ltsDataContext
                'grabs the objects from the database, then creates a list of Dataobjects.Company
                'using the database object as the parameter in the dataobjects.company object
                'in its constructor
                retLst = .SafetyIncidents.Select(Function(x) New DataObjects.SafetyIncident(x)).ToList

                'always dispose, cleans the connectoin pool
                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetAllForUserCompany(login As DataObjects.Login) As List(Of DataObjects.SafetyIncident)

            Dim retobj As New List(Of DataObjects.SafetyIncident)

            With New ltsDataContext

                retobj = (From c In .SafetyIncidents Where c.CompanyId = login.CompanyID _
                            Select New DataObjects.SafetyIncident(c)).ToList

                .Dispose()
            End With

            Return retobj

        End Function

#End Region
    End Class

End Namespace
