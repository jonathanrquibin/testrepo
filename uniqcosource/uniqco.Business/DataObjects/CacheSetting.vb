﻿
Namespace DataObjects


    Public Class CacheSetting

#Region " Properties"


        Public Property CacheSettingID As Guid

        Public Property CacheSettingName As String

        Public Property CacheSettingListStr As String

        Public ReadOnly Property CacheSettingList As List(Of String)
            Get

                If String.IsNullOrEmpty(CacheSettingListStr) Then Return New List(Of String)

                Return CacheSettingListStr.Split("|"c).ToList

            End Get
        End Property

#End Region


#Region "gets and sets"

        Public Shared Function GetSetting(name As String) As DataObjects.CacheSetting

            With New ltsDataContext

                Return (From x In .CacheSettings _
                        Where x.CacheSettingName.ToLower = name.ToLower _
                        Select New DataObjects.CacheSetting(x)).FirstOrDefault

            End With

        End Function

        Public Shared Sub UpsertCacheSetting(name As String, queries As List(Of String))

            Dim dataContext As New ltsDataContext()

            'creates a bar delimited string containing the queries to be stored 
            Dim queryList As String = String.Empty

            For Each s As String In queries

                queryList &= If(String.IsNullOrEmpty(queryList), String.Empty, "|") & s
            Next

            'finds the cacheSetting from the database if it exists
            Dim foundCacheSetting As Business.CacheSetting = _
                dataContext.CacheSettings.Where(Function(x) x.CacheSettingName = name).FirstOrDefault

            'if the cacheSetting doesnt exist, then create one. 
            If foundCacheSetting Is Nothing Then

                foundCacheSetting = New Business.CacheSetting

                foundCacheSetting.CacheSettingID = Guid.NewGuid
                foundCacheSetting.CacheSettingName = name
                foundCacheSetting.CacheSettingList = queryList

                dataContext.CacheSettings.InsertOnSubmit(foundCacheSetting)

            Else

                foundCacheSetting.CacheSettingList = queryList

            End If

            'regardless of if we have created a new item, or just updated an old one, then update changes to the database
            dataContext.SubmitChanges()

            'connection pool management
            dataContext.Dispose()

        End Sub


#End Region


#Region "constructors"


        Public Sub New(db_obj As Business.CacheSetting)

            Me.CacheSettingID = db_obj.CacheSettingID
            Me.CacheSettingListStr = db_obj.CacheSettingList
            Me.CacheSettingName = db_obj.CacheSettingName

        End Sub

        ''' <summary>
        ''' for serialisation only
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

#End Region


    End Class


End Namespace