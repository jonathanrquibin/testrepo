﻿Imports System.Data
Imports System.Web
Namespace DataObjects
    Public Class SummaryData


#Region "properties"

        Public Property Classification As String
        Public Property Name As String
        Public Property Value As String
        Public Property Uniqco As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub
        Public Sub New(c As usp_SummaryData_v3Result)

            With c
                Me.Classification = c.classification
                Me.Name = c.name
                Me.Value = c.value.ToString("#,0.00")
                Me.Uniqco = c.uniqco.ToString("#,0.00")


            End With

        End Sub

#End Region

#Region "gets and sets"
        Private Shared Function GetForUniqcoPAMain(reportDate As DateTime, Optional VehicleType As String = "",
                                                   Optional Group As String = "", Optional BusinessUnit As String = "",
                                                   Optional Department As String = "", Optional Region As String = "") As List(Of DataObjects.SummaryData)
            With New ltsDataContext

                Return (From x In .usp_SummaryData_v3(reportDate, VehicleType, Group, BusinessUnit, Department, Region)
                         Select New DataObjects.SummaryData(x)).ToList

            End With
        End Function

        Public Shared Function GetPerAssetMaintenance(reportDate As Date, data As Dictionary(Of String, Double), fc As String) As List(Of DataObjects.SummaryData)
            Dim x = New List(Of Business.DataObjects.SummaryData)
            Dim y = New List(Of Business.DataObjects.SummaryData)
            Dim FConsumption100Kms As Double = 0.0
            Dim FConsumptionHrs As Double = 0.0
            Dim FCostSubTotal As Double = 0.0
            Dim FCostOfDownTime As Double = 0.0

            Dim FLabourHoursSched As Double = 0.0
            Dim FLabourHoursNonSched As Double = 0.0

            Dim Vehicle As String = ""
            Dim Group As String = ""
            Dim BusinessUnit As String = ""
            Dim Department As String = ""
            Dim Region As String = ""
            For Each filter As String In fc.Replace("And", ":").Split(":")
                If filter.Split("=")(0).Trim() = "[VehicleType]" Then
                    Vehicle = filter.Split("=")(1).Trim().Replace("'", "")
                End If
                If filter.Split("=")(0).Trim() = "[Group]" Then
                    Group = filter.Split("=")(1).Trim().Replace("'", "")
                End If
                If filter.Split("=")(0).Trim() = "[BusinessUnit]" Then
                    BusinessUnit = filter.Split("=")(1).Trim().Replace("'", "")
                End If
                If filter.Split("=")(0).Trim() = "[Department]" Then
                    Department = filter.Split("=")(1).Trim().Replace("'", "")
                End If
                If filter.Split("=")(0).Trim() = "[Region]" Then
                    Region = filter.Split("=")(1).Trim().Replace("'", "")
                End If
            Next

            y = GetForUniqcoPAMain(reportDate, Vehicle, Group, BusinessUnit, Department, Region)


            Dim sched = data("SC")
            Dim nonsched = data("UN")
            Dim t_maintenance = sched + nonsched

            Dim uniqcosched = Convert.ToDecimal(y.SingleOrDefault(Function(z) z.Name.Equals("% Scheduled Maintenance")).Uniqco)
            Dim uniqcononsched = Convert.ToDecimal(y.SingleOrDefault(Function(z) z.Name.Equals("% Unscheduled Maintenance")).Uniqco)
            Dim t_uniqo = uniqcosched + uniqcononsched

            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "perasset", .Name = "Fuel Consumption Per 100KM", .Value = data("FCKMS").ToString("#,0.00")})
            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "perasset", .Name = "Fuel Consumption Per Hour", .Value = data("FCHRS").ToString("#,0.00")})
            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "perasset", .Name = "Total Cost Per Asset", .Value = data("C").ToString("#,0.00")})
            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "perasset", .Name = "Downtime Cost Per Asset", .Value = data("D").ToString("#,0.00")})
            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "maintenance", .Name = "% Scheduled Maintenance", .Value = sched.ToString("#,0.00")})
            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "maintenance", .Name = "% Unscheduled Maintenance", .Value = nonsched.ToString("#,0.00")})
            x.Add(New Business.DataObjects.SummaryData() With {.Classification = "maintenance", .Name = "Safety", .Value = data("S").ToString("#,0.00")})

            If Not t_uniqo.Equals(0) Then
                Dim source = (From a In x Join b In y On a.Name Equals b.Name Select New Business.DataObjects.SummaryData() With {
                       .Classification = a.Classification,
                       .Name = a.Name,
                       .Value = If(a.Name.Equals("Safety") Or a.Name.Equals("Fuel Consumption Per 100KM") Or a.Name.Equals("Fuel Consumption Per Hour") Or a.Name.Equals("Total Cost Per Asset") Or a.Name.Equals("Downtime Cost Per Asset"), a.Value, (Convert.ToDecimal(a.Value)).ToString("#,0.00") + "%"),
                       .Uniqco = If(a.Name.Equals("Safety") Or a.Name.Equals("Fuel Consumption Per 100KM") Or a.Name.Equals("Fuel Consumption Per Hour") Or a.Name.Equals("Total Cost Per Asset") Or a.Name.Equals("Downtime Cost Per Asset"), b.Uniqco, (Convert.ToDecimal(b.Uniqco)).ToString("#,0.00") + "%")
                       }).ToList()
                Return source
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function GetSummary(data As Dictionary(Of String, Double)) As List(Of DataObjects.SummaryData)
            Dim source = New List(Of Business.DataObjects.SummaryData)

            source.Add(New Business.DataObjects.SummaryData() With {.Name = "Asset Replacement Value", .Value = data("ARV").ToString("#,0.00")})
            source.Add(New Business.DataObjects.SummaryData() With {.Name = "Current Asset Value", .Value = data("CAV").ToString("#,0.00")})
            source.Add(New Business.DataObjects.SummaryData() With {.Name = "Depreciation Balance", .Value = data("DB").ToString("#,0.00")})
            source.Add(New Business.DataObjects.SummaryData() With {.Name = "Total Profit/Loss", .Value = data("TPL").ToString("#,0.00")})
            source.Add(New Business.DataObjects.SummaryData() With {.Name = "Cost Of Downtime", .Value = data("CD").ToString("#,0.00")})
            source.Add(New Business.DataObjects.SummaryData() With {.Name = "Cost Of Under Utilisation", .Value = data("CUU").ToString("#,0.00")})
            source.Add(New Business.DataObjects.SummaryData() With {.Name = "No Of Safety Incidents", .Value = data("SI").ToString("#,0.00")})
            Return source
        End Function
#End Region

    End Class
End Namespace
