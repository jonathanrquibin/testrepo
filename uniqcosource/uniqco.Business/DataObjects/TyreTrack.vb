﻿Namespace DataObjects
    Public Class TyreTrack
#Region "properties"
        Public Property TrackId As System.Guid
        Public Property TrackSize As System.Nullable(Of Integer)
        Public Property TrackCost As System.Nullable(Of Double)
        Public Property TrackLift As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(tyre As Business.TyreTrack)
            With tyre
                Me.TrackId = .TrackId
                Me.TrackSize = .TrackSize
                Me.TrackCost = .TrackCost
                Me.TrackLift = .TrackLift
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(tyre As DataObjects.TyreTrack)
            With New ltsDataContext
                Dim newDbObj As New Business.TyreTrack
                newDbObj.TrackId = Guid.NewGuid
                newDbObj.TrackSize = tyre.TrackSize
                newDbObj.TrackCost = tyre.TrackCost
                newDbObj.TrackLift = tyre.TrackLift

                .TyreTracks.InsertOnSubmit(newDbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Update(tyre As DataObjects.TyreTrack)
            With New ltsDataContext
                Dim dbObj As Business.TyreTrack = .TyreTracks.Where(Function(x) x.TrackId = tyre.TrackId).Single
                dbObj.TrackSize = tyre.TrackSize
                dbObj.TrackCost = tyre.TrackCost
                dbObj.TrackLift = tyre.TrackLift

                .SubmitChanges()
                .Dispose()
            End With
        End Sub

        Public Shared Sub Delete(tyre As DataObjects.TyreTrack)
            With New ltsDataContext

                Dim dbObj As Business.TyreTrack = .TyreTracks.Where(Function(x) x.TrackId = tyre.TrackId).Single

                .TyreTracks.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With
        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.TyreTrack)
            Dim retLst As List(Of DataObjects.TyreTrack)

            With New ltsDataContext
                retLst = .TyreTracks.Select(Function(x) New DataObjects.TyreTrack(x)).ToList.OrderBy(Function(x) x.TrackLift).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
#End Region
    End Class
End Namespace
