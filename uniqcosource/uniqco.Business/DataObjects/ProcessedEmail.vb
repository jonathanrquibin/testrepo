﻿Namespace DataObjects
    Public Class ProcessedEmail

#Region "properties"

        Public Property PK As Guid

        Public Property UID As String

        Public Property ProcessedDate As System.Nullable(Of Date)

        Public Property Subject As String

        Public Property Sender As String
        Public Property Exception As Boolean
        Public Property ExceptionMessage As String
        Public Property Report As String

        Public Property CompanyId As System.Guid?

#End Region
#Region "constructors"

        ''' <summary>
        ''' This is required for serialization to work 
        ''' if there is no blank constructor, then i wont be to build an XML/JSON template
        ''' </summary>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' This constructor accepts C as an instance of the 
        ''' Company object from the lts LINQtoSQL Data Context
        ''' Form this, the DataObjects.ProcessedEmail object is built
        ''' </summary>
        ''' <param name="c"></param>
        Public Sub New(c As Business.ProcessedEmail)

            With c

                Me.PK = c.PK
                Me.UID = c.UID
                Me.CompanyID = c.CompanyId
                Me.ProcessedDate = c.ProcessedDate
                Me.Sender = c.Sender
                Me.Exception = c.Exception
                Me.ExceptionMessage = c.ExceptionMessage
                Me.Subject = c.Subject
                Me.Report = c.Report

            End With

        End Sub

#End Region

#Region "CRUD"

        'Create,  update and delete options go here
        'They all need to be static/shared so we can use them without
        'instantiating a class in future

        Public Shared Sub Create(c As DataObjects.ProcessedEmail)

            With New ltsDataContext

                Dim newDbObj As New Business.ProcessedEmail

                'the reason we have these POCO classes is so that we can do things like this
                'in the past i have had issues with the entity framework and LINQtoSQL classes
                'when trying to do specific things when performing an insert / delete etc.
                newDbObj.PK = If(c.PK = Guid.Empty, Guid.NewGuid, c.PK)
                newDbObj.UID = c.UID
                newDbObj.CompanyId = c.CompanyId
                newDbObj.ProcessedDate = c.ProcessedDate
                newDbObj.Sender = c.Sender
                newDbObj.Exception = c.Exception
                newDbObj.ExceptionMessage = c.ExceptionMessage
                newDbObj.Subject = c.Subject
                newDbObj.Report = c.Report

                .ProcessedEmails.InsertOnSubmit(newDbObj)
                .SubmitChanges()

                .Dispose()
            End With

        End Sub

        Public Shared Sub Update(c As DataObjects.ProcessedEmail)

            With New ltsDataContext

                Dim dbObj As Business.ProcessedEmail = .ProcessedEmails.Where(Function(x) x.CompanyId = c.CompanyID).Single

                dbObj.UID = c.UID
                dbObj.CompanyId = c.CompanyId
                dbObj.ProcessedDate = c.ProcessedDate
                dbObj.Sender = c.Sender
                dbObj.Exception = c.Exception
                dbObj.ExceptionMessage = c.ExceptionMessage
                dbObj.Subject = c.Subject
                dbObj.Report = c.Report
                .SubmitChanges()
                .Dispose()
            End With

        End Sub

        Public Shared Sub Delete(c As DataObjects.ProcessedEmail)

            With New ltsDataContext

                Dim dbObj As Business.ProcessedEmail = .ProcessedEmails.Where(Function(x) x.PK = c.PK).Single

                .ProcessedEmails.DeleteOnSubmit(dbObj)
                .SubmitChanges()
                .Dispose()
            End With

        End Sub

#End Region

#Region "Gets and Sets"

        Public Shared Function GetAll() As List(Of DataObjects.ProcessedEmail)

            Dim retLst As List(Of DataObjects.ProcessedEmail)

            With New ltsDataContext
                'grabs the objects from the database, then creates a list of Dataobjects.ProcessedEmail
                'using the database object as the parameter in the dataobjects.ProcessedEmail object
                'in its constructor
                retLst = .ProcessedEmails.Select(Function(x) New DataObjects.ProcessedEmail(x)).ToList

                'always dispose, cleans the connectoin pool
                .Dispose()
            End With

            Return retLst
        End Function
        Public Shared Function GetAllForCompany(com As Guid) As List(Of DataObjects.ProcessedEmail)

            Dim retLst As List(Of DataObjects.ProcessedEmail)

            With New ltsDataContext
                'grabs the objects from the database, then creates a list of Dataobjects.ProcessedEmail
                'using the database object as the parameter in the dataobjects.ProcessedEmail object
                'in its constructor
                retLst = .ProcessedEmails.Where(Function(x) x.CompanyId = com).Select(Function(x) New DataObjects.ProcessedEmail(x)).ToList

                'always dispose, cleans the connectoin pool
                .Dispose()
            End With

            Return retLst
        End Function

#End Region

#Region "misc"


        Public Shared Function GetALLUID() As List(Of String)
            Dim retLst As List(Of String)

            With New ltsDataContext
                'grabs the objects from the database, then creates a list of Dataobjects.ProcessedEmail
                'using the database object as the parameter in the dataobjects.ProcessedEmail object
                'in its constructor
                retLst = (From a In .ProcessedEmails Select a.UID).ToList

                'always dispose, cleans the connectoin pool
                .Dispose()
            End With

            Return retLst
        End Function


#End Region
    End Class
End Namespace