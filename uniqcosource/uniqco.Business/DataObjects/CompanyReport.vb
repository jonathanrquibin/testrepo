﻿
Namespace DataObjects

    Public Class CompanyReport

#Region "properties"
        Public Property CompanyReportID As Integer
        Public Property CompanyId As Guid
        Public Property Reportdate As DateTime
        Public Property ProcessedDate As DateTime
        Public Property Notes As String
        Public Property ReportType As String
        Property ImportWasSuccess As Boolean = False

#End Region


#Region "constructors"
        Public Sub New(cr As Business.CompanyReport)

            With cr
                Me.CompanyId = .CompanyID
                Me.CompanyReportID = .CompanyReportID
                Me.Notes = .notes
                Me.ReportType = .ReportType
                Me.ProcessedDate = .ProcessedDate
                Me.Reportdate = .ReportDate
                Me.ImportWasSuccess = .ImportWasSuccessful

            End With

        End Sub

        Public Sub New()

        End Sub

#End Region

#Region "CRUD"

        Public Shared Sub Create(newobj As DataObjects.CompanyReport)

            With New ltsDataContext

                Dim newdbobj As New Business.CompanyReport

                newdbobj.CompanyID = newobj.CompanyId
                newdbobj.notes = newobj.Notes
                newdbobj.ReportType = newobj.ReportType
                newdbobj.ProcessedDate = newobj.ProcessedDate
                newdbobj.ReportDate = newobj.Reportdate
                newdbobj.ImportWasSuccessful = newobj.ImportWasSuccess

                .CompanyReports.InsertOnSubmit(newdbobj)
                .SubmitChanges()

                'changes the byref object so that the new ID is taken from the db
                newobj.CompanyReportID = newdbobj.CompanyReportID

                .Dispose()
            End With

        End Sub


#End Region

#Region "Gets/Sets"

        Public Sub MarkImportAsSuccess()

            With New ltsDataContext

                'presume that the object exists in the backend DB, if not then this i s
                'totaly unexpected and should cause an exception
                Dim obj = .CompanyReports.Where(Function(x) x.CompanyReportID = Me.CompanyReportID).Single()

                obj.ImportWasSuccessful = True

                .SubmitChanges()
                .Dispose()
            End With

        End Sub
        Public Shared Function GetAll() As List(Of DataObjects.CompanyReport)

            Dim retlst As List(Of DataObjects.CompanyReport)

            With New ltsDataContext

                retlst = (From x In .CompanyReports Order By x.ReportDate _
                    Select New DataObjects.CompanyReport(x)).ToList

                .Dispose()

            End With

            Return retlst

        End Function
        Public Shared Function GetForCompanyID(companyid As Guid) As List(Of DataObjects.CompanyReport)

            Dim retlst As List(Of DataObjects.CompanyReport)

            With New ltsDataContext

                retlst = (From x In .CompanyReports Where x.CompanyID = companyid _
                    Order By x.ReportDate _
                    Select New DataObjects.CompanyReport(x)).ToList

                .Dispose()

            End With

            Return retlst

        End Function

        Public Shared Function GetLatestReportDate() As Date


            With New ltsDataContext

                Return (From x In .CompanyReports Select x.ReportDate).Max()

            End With

        End Function


#End Region

#Region "misc"


        Public Function CreateReportcontentFromFile(dirAndFile As String) As DataObjects.RiskCompositeList

            Dim retlst As New DataObjects.RiskCompositeList


            Console.WriteLine("Processing: {0}", dirAndFile)

            retlst.CompanyName = DataObjects.Company.GetFromID(Me.CompanyId).Name
            retlst.ReportDate = Me.Reportdate

            Dim excelContent As List(Of List(Of ExcelParam)) = MSExcelConnector.GetContentsOfExcelFile(dirAndFile)

            Dim i As Integer = 0
            Dim cnt As Integer = excelContent.Count

            For Each xlc In excelContent

                Try
                    Dim zz = xlc.Where(Function(c) c.PropertyName.Trim = "PlantNumber")(0)
                    If zz IsNot Nothing Then
                        If zz.PropertyValue.StartsWith("ZZ") Then
                            Continue For
                        End If
                    End If
                    Dim x As New DataObjects.RiskComposite(xlc) With {.ReportDate = Reportdate _
                                                                     , .CompanyReportID = Me.CompanyReportID}

                    DataObjects.RiskComposite.Create(x, Me.CompanyId)
                    retlst.Add(x)

                    'TODO: we should really move this outside of the business layer!
                    If i Mod 10 = 0 OrElse i = cnt - 1 Then
                        Console.SetCursorPosition(0, Console.CursorTop)
                        Console.Write("{0} of {1}", i, cnt)
                    End If
                    i += 1

                Catch ex As Exception
                    Throw
                End Try
            Next

            'DataObjects.RiskComposite.Create(retlst)

            Return retlst

        End Function

        Public Function CreateReportcontentFromFile(File As Byte()) As DataObjects.RiskCompositeList

            Dim retlst As New DataObjects.RiskCompositeList


            'Console.WriteLine("Processing: {0}", dirAndFile)

            retlst.CompanyName = DataObjects.Company.GetFromID(Me.CompanyId).Name
            retlst.ReportDate = Me.Reportdate

            Dim excelContent As List(Of List(Of ExcelParam)) = MSExcelConnector.GetContentsOfCSVByte(File)

            'Dim i As Integer = 1
            Dim cnt As Integer = excelContent.Count

            For Each xlc In excelContent

                Try

                    Dim x As New DataObjects.RiskComposite(xlc) With {.ReportDate = Reportdate _
                                                                     , .CompanyReportID = Me.CompanyReportID}

                    DataObjects.RiskComposite.Create(x, Me.CompanyId)
                    retlst.Add(x)

                    'TODO: we should really move this outside of the business layer!
                    'If i Mod 10 = 0 OrElse i = cnt - 1 Then
                    '    Console.SetCursorPosition(0, Console.CursorTop)
                    '    Console.Write("{0} of {1}", i, cnt)
                    'End If
                    'i += 1

                Catch ex As Exception
                    Throw
                End Try
            Next

            'DataObjects.RiskComposite.Create(retlst)

            Return retlst

        End Function


        Public Function DeleteReportAndAllRelatedData() As Boolean

            Dim retbool As Boolean = True

            Try

                With New ltsDataContext

                    'delete all the related risk composite data
                    Dim rc As List(Of Business.RiskComposite) = _
                                .RiskComposites.Where(Function(x) x.CompanyReportID = Me.CompanyReportID).ToList

                    .RiskComposites.DeleteAllOnSubmit(rc)
                    .SubmitChanges()

                    'delete all of the company reports
                    Dim cr As Business.CompanyReport = _
                               .CompanyReports.Where(Function(x) x.CompanyReportID = Me.CompanyReportID).Single()

                    .CompanyReports.DeleteOnSubmit(cr)
                    .SubmitChanges()

                    .Dispose()
                End With


            Catch ex As Exception
                Throw
            End Try

            Return retbool

        End Function

#End Region

    End Class

End Namespace