﻿Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Imports OfficeOpenXml
Imports System.ComponentModel


Public Class MSExcelConnector

    ''' <summary>
    ''' IF the excel file is just one worksheet with a top column containing column headers, 
    ''' then this wil return a list of lists (i.e. each row of the excel sheet with the value and its column name)
    ''' there is a ot of duplicatoin with this but it is v-fast and in a format which is easy to use later (for the BCV objects/classes)
    ''' </summary>    
    Public Shared Function GetContentsOfExcelFile(fileLocation As String) As List(Of List(Of ExcelParam))

        Dim _excelApp As New Excel.Application
        Dim wrbk As Excel.Workbook = _excelApp.Workbooks.Open(fileLocation)
        Dim wkst As Excel.Worksheet = wrbk.Sheets(1)
        Dim rng As Excel.Range = wkst.UsedRange

        Dim valueArray(,) As Object = rng.Value(Excel.XlRangeValueDataType.xlRangeValueDefault)

        Dim retobjs As New List(Of List(Of ExcelParam))
        Dim dict As New Dictionary(Of Integer, String)


        For index0 = 1 To valueArray.GetUpperBound(0)

            Dim thisRowOfExcelParams As New List(Of ExcelParam)

            For index1 = 1 To valueArray.GetUpperBound(1)

                If index0 = 1 Then
                    dict.Add(index1, valueArray(index0, index1))
                Else
                    thisRowOfExcelParams.Add(New ExcelParam With {.PropertyName = dict.Item(index1), .PropertyValue = valueArray(index0, index1)})
                End If
            Next
            If index0 <> 1 Then retobjs.Add(thisRowOfExcelParams)
        Next


        'clean up the COM object(s)
        wrbk.Close(False, Type.Missing, Type.Missing)
        Marshal.ReleaseComObject(wrbk)
        _excelApp.Quit()
        Marshal.FinalReleaseComObject(_excelApp)

        Return retobjs
    End Function


    Public Shared Function GetContentsOfCSVByte(file As Byte()) As List(Of List(Of ExcelParam))

        Dim retobjs As New List(Of List(Of ExcelParam))

        Dim lines As New List(Of String())
        Dim memoryStream = New MemoryStream(file)
        Using reader = New StreamReader(memoryStream) 'Read Byte() and Parse as String
            Dim csv As String
            While True
                csv = reader.ReadLine()
                If csv Is Nothing Then
                    Exit While
                End If

                Dim parser = New TextFieldParser(New StringReader(csv))
                parser.HasFieldsEnclosedInQuotes = True
                parser.SetDelimiters(",")
                While Not parser.EndOfData
                    lines.Add(parser.ReadFields())
                End While
            End While
        End Using

        For row = 1 To lines.Count - 1 'Process string data and return as ExcelParam
            'NOTE: row(0) is the header
            Dim thisRowOfExcelParams As New List(Of ExcelParam)

            For col = 0 To lines(row).Count - 1
                Dim x = New ExcelParam
                x.PropertyName = lines(0)(col)
                x.PropertyValue = lines(row)(col)
                thisRowOfExcelParams.Add(x)
            Next

            retobjs.Add(thisRowOfExcelParams)
        Next
        Return retobjs
    End Function
    
    Private Shared Function GetRiskComposite(obj As DataObjects.RiskComposite, objType As String)
        Dim rrc As New Object
        If objType.Equals("RedRiskCompositeUtilisation") Then
            rrc = New RedRiskCompositeUtilisation
        ElseIf objType.Equals("RedRiskCompositeProductivity") Then
            rrc = New RedRiskCompositeProductivity
        ElseIf objType.Equals("RedRiskCompositeFuelConsumption") Then
            rrc = New RedRiskCompositeFuelConsumption
        ElseIf objType.Equals("RedRiskCompositeReplacement") Then
            rrc = New RedRiskCompositeReplacement
        ElseIf objType.Equals("RedRiskCompositeServicesDue") Then
            rrc = New RedRiskCompositeServicesDue
        ElseIf objType.Equals("RedRiskCompositeCostVRecovery") Then
            rrc = New RedRiskCompositeCostVRecovery
        ElseIf objType.Equals("RedRiskCompositeMaintenanceRatio") Then
            rrc = New RedRiskCompositeMaintenanceRatio
        ElseIf objType.Equals("RedRiskCompositeAssetDataRed") Then
            rrc = New RedRiskCompositeAssetDataRed
        End If
        rrc.PlantNumber = obj.PlantNumber
        rrc.ParentPlant = obj.ParentPlant
        rrc.FleetNumber = obj.FleetNumber
        rrc.AssetNumber = obj.AssetNumber
        rrc.Category = obj.Category
        rrc.Group = obj.Group
        rrc.Type = obj.Type
        rrc.Make = obj.Make
        rrc.Model = obj.Model
        rrc.ModelYear = obj.ModelYear
        rrc.RegoNo = obj.RegoNo
        If obj.RegoDate.HasValue Then
            rrc.RegoDate = obj.RegoDate.Value.ToString("dd/MM/yyyy")
        Else
            rrc.RegoDate = ""
        End If
        rrc.RegoState = obj.RegoState
        rrc.MeterType = obj.MeterType
        rrc.CurrentAsset = obj.CurrentAsset
        rrc.PlantImage = obj.PlantImage
        rrc.VINNumber = obj.VINNumber
        rrc.EngineNumber = obj.EngineNumber
        rrc.ChassisNumber = obj.ChassisNumber
        rrc.BodyNumber = obj.BodyNumber
        If obj.BuildDate.HasValue Then
            rrc.BuildDate = obj.BuildDate.Value.ToString("dd/MM/yyyy")
        Else
            rrc.BuildDate = ""
        End If
        rrc.PreviousRego = obj.PreviousRego
        rrc.RadioSerialNo = obj.RadioSerialNo
        rrc.RadioCallSign = obj.RadioCallSign
        rrc.MiscInformationDoc = obj.MiscInformationDoc
        rrc.Region = obj.Region
        rrc.BusinessUnit = obj.BusinessUnit
        rrc.Department = obj.Department
        rrc.Location = obj.Location
        rrc.OperatorDriver = obj.OperatorDriver
        rrc.UsageType = obj.UsageType
        rrc.Supervisor = obj.Supervisor
        rrc.SubjectToFBT = obj.SubjectToFBT
        rrc.ChargeAccount = obj.ChargeAccount
        rrc.PurchaseOrLease = obj.PurchaseOrLease
        If obj.PurchaseDate.HasValue Then
            rrc.PurchaseDate = obj.PurchaseDate.Value.ToString("dd/MM/yyyy")
        Else
            rrc.PurchaseDate = ""
        End If
        Return rrc
    End Function
    Private Shared Function GetListUtilisation(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeUtilisation)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeUtilisation()
            rrc = GetRiskComposite(obj, "RedRiskCompositeUtilisation")
            rrc.KmsHrsVar = obj.KmsHrsVar
            rrc.AnnualAveKms = obj.AnnualAveKms
            rrc.AnnualAveHrs = obj.AnnualAveHrs
            rrc.AnnualBudgetKms = obj.AnnualBudgetKms
            rrc.AnnualBudgetHrs = obj.AnnualBudgetHrs
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListProductivity(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeProductivity)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeProductivity()
            rrc = GetRiskComposite(obj, "RedRiskCompositeProductivity")
            rrc.IncomeUnitsVar = obj.IncomeUnitsVar
            rrc.KmsHrsVar = obj.KmsHrsVar
            rrc.AnnualPredictedIncomeUnits = obj.AnnualPredictedIncomeUnits
            rrc.AnnualBudgetIncomeUnits = obj.AnnualBudgetIncomeUnits
            rrc.HireCharge = obj.HireCharge
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListFuelConsumption(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeFuelConsumption)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeFuelConsumption()
            rrc = GetRiskComposite(obj, "RedRiskCompositeFuelConsumption")
            rrc.ExpectedFuelEconHr = obj.ExpectedFuelEconHr
            rrc.LitresPerHour = obj.LitresPerHour
            rrc.FuelTypeHr = obj.FuelTypeHr
            rrc.ExpectedFuelEconKm = obj.ExpectedFuelEconKm
            rrc.LitresPer100Kms = obj.LitresPer100Kms
            rrc.FuelTypeKms = obj.FuelTypeKms
            rrc.Deviation = obj.Deviation
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListReplacement(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeReplacement)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeReplacement()
            rrc = GetRiskComposite(obj, "RedRiskCompositeReplacement")
            If obj.OptimumReplacementDate.HasValue Then
                rrc.OptimumReplacementDate = obj.OptimumReplacementDate.Value.ToString("dd/MM/yyyy")
            Else
                rrc.OptimumReplacementDate = ""
            End If

            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListServicesDue(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeServicesDue)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeServicesDue()
            rrc = GetRiskComposite(obj, "RedRiskCompositeServicesDue")
            If obj.LastSafetyServiceDate.HasValue Then
                rrc.LastSafetyServiceDate = obj.LastSafetyServiceDate.Value.ToString("dd/MM/yyyy")
            Else
                rrc.LastSafetyServiceDate = ""
            End If
            If obj.NextSafetyServiceDue.HasValue Then
                rrc.NextSafetyServiceDue = obj.NextSafetyServiceDue.Value.ToString("dd/MM/yyyy")
            Else
                rrc.NextSafetyServiceDue = ""
            End If
            rrc.LastStandardServiceDate = "" 'does not exist in riskcomposite table
            If obj.NextStandardServiceDue.HasValue Then
                rrc.NextStandardServiceDue = obj.NextStandardServiceDue.Value.ToString("dd/MM/yyyy")
            Else
                rrc.NextStandardServiceDue = ""
            End If
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListCostVRecovery(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeCostVRecovery)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeCostVRecovery()
            rrc = GetRiskComposite(obj, "RedRiskCompositeCostVRecovery")
            rrc.OverHeadCost = obj.OverheadCost
            rrc.FuelCost = obj.FuelCost
            rrc.MaintenanceCost = obj.MaintenanceCost
            rrc.FBTCost = obj.FBTCost
            rrc.LeaseCost = obj.LeaseCost
            rrc.CostSubTotal = obj.CostSubTotal
            rrc.MonthlyCharge = obj.MonthlyCharge
            rrc.CostVariation = obj.CostVariation
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListMaintenanceRatio(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeMaintenanceRatio)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeMaintenanceRatio()
            rrc = GetRiskComposite(obj, "RedRiskCompositeMaintenanceRatio")
            rrc.LabourHoursNonSched = obj.LabourHoursNonSched
            rrc.LabourHoursSched = obj.LabourHoursSched
            rrc.DowntimeVariation = obj.DowntimeVariation
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function
    Private Shared Function GetListAssetData(objRisk As List(Of DataObjects.RiskComposite))
        Dim RiskGetFromDB As New List(Of RedRiskCompositeAssetDataRed)
        For Each obj In objRisk
            Dim rrc As New RedRiskCompositeAssetDataRed()
            rrc = GetRiskComposite(obj, "RedRiskCompositeAssetDataRed")
            rrc.AssetDataRisk = obj.AssetDataRisk
            RiskGetFromDB.Add(rrc)
        Next
        Return RiskGetFromDB
    End Function

    Private Shared Function GetRiskCompositeDataByHeader(objRisk As List(Of DataObjects.RiskComposite), headerList As String) As List(Of DataObjects.RiskComposite)
        Dim RiskDb As New List(Of DataObjects.RiskComposite)
        For Each hList In headerList.Split(",")
            Dim hRisk = objRisk.Where(Function(x) x.PK.ToString().Equals(hList)).FirstOrDefault
            If Not hRisk Is Nothing Then
                RiskDb.Add(hRisk)
            End If
        Next
        Return RiskDb
    End Function

    Public Shared Function ExportDataToCsv(lstHeader As List(Of String), objRisk As List(Of DataObjects.RiskComposite)) As Array
        Dim excelPackagex = New ExcelPackage
        For Each header In lstHeader
            Dim hList As String
            hList = header
            Select Case hList.Split(":")(0)
                Case "Utilisation"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListUtilisation(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.KmsHrsVar).ToList)
                    Dim excelWorksheet = excelPackagex.Workbook.Worksheets.Add("Utilisation")
                    excelWorksheet.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "OperationalProductivity"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListProductivity(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.KmsHrsVar - x.IncomeUnitsVar).ToList)
                    Dim excelWorksheet1 = excelPackagex.Workbook.Worksheets.Add("Productivity")
                    excelWorksheet1.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "FuelConsumption"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListFuelConsumption(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.Deviation).ToList)
                    Dim excelWorksheet2 = excelPackagex.Workbook.Worksheets.Add("Fuel Consumption")
                    excelWorksheet2.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "OptimumReplacement"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListReplacement(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.OptimumReplacementDate).ToList)
                    Dim excelWorksheet3 = excelPackagex.Workbook.Worksheets.Add("Replacement")
                    excelWorksheet3.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "ServiceDue"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListServicesDue(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.NextStandardServiceDue).ToList)
                    Dim excelWorksheet4 = excelPackagex.Workbook.Worksheets.Add("Services Due")
                    excelWorksheet4.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "WOLCostVariation"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListCostVRecovery(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.CostVariation).ToList)
                    Dim excelWorksheet5 = excelPackagex.Workbook.Worksheets.Add("Cost VS Recovery")
                    excelWorksheet5.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "MaintenanceRatio"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListMaintenanceRatio(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.DowntimeVariation).ToList)
                    Dim excelWorksheet6 = excelPackagex.Workbook.Worksheets.Add("Maintenance Ratio")
                    excelWorksheet6.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
                Case "AssetData"
                    Dim headerList As String = hList.Split(":")(1)
                    Dim RiskDb As New List(Of DataObjects.RiskComposite)
                    Dim RiskGetFromDB = GetListAssetData(GetRiskCompositeDataByHeader(objRisk, headerList).OrderBy(Function(x) x.KmsHrsVar).ToList)
                    Dim excelWorksheet6 = excelPackagex.Workbook.Worksheets.Add("Asset Data")
                    excelWorksheet6.Cells("A1").LoadFromCollection(RiskGetFromDB, True)
            End Select
        Next
        Return excelPackagex.GetAsByteArray()
    End Function
End Class


Public Class ExcelParam

    Public Property PropertyName As String
    Public Property PropertyValue As String 'always a string, not an object


    Public Sub New()
    End Sub
End Class

Public Class RedRiskCompositeUtilisation
    Inherits UtilisationRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeProductivity
    Inherits ProductivityRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeFuelConsumption
    Inherits FuelConsumptionRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeReplacement
    Inherits ReplacementRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeServicesDue
    Inherits ServicesDueRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeCostVRecovery
    Inherits CostVRecoveryRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeMaintenanceRatio
    Inherits MaintenanceRatioRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class
Public Class RedRiskCompositeAssetDataRed
    Inherits AssetDataRed
    <DisplayName("Plant Number")> _
    Public Property PlantNumber As String
    <DisplayName("Parent Plant")> _
    Public Property ParentPlant As String
    <DisplayName("Fleet Number")> _
    Public Property FleetNumber As String
    <DisplayName("Asset Number")> _
    Public Property AssetNumber As String
    Public Property Category As String
    Public Property Group As String
    Public Property Type As String
    Public Property Make As String
    Public Property Model As String
    <DisplayName("Model Year")> _
    Public Property ModelYear As String
    <DisplayName("Rego No")> _
    Public Property RegoNo As String
    <DisplayName("Rego Date")> _
    Public Property RegoDate As String
    <DisplayName("Rego State")> _
    Public Property RegoState As String
    <DisplayName("Meter Type")> _
    Public Property MeterType As String
    <DisplayName("Current Asset")> _
    Public Property CurrentAsset As String
    <DisplayName("Plant Image")> _
    Public Property PlantImage As String
    <DisplayName("VIN Number")> _
    Public Property VINNumber As String
    <DisplayName("Engine Number")> _
    Public Property EngineNumber As String
    <DisplayName("Chassis Number")> _
    Public Property ChassisNumber As String
    <DisplayName("Body Number")> _
    Public Property BodyNumber As String
    <DisplayName("Build Date")> _
    Public Property BuildDate As String
    <DisplayName("Previous Rego")> _
    Public Property PreviousRego As String
    <DisplayName("Radio Serial No")> _
    Public Property RadioSerialNo As String
    <DisplayName("Radio Call Sign")> _
    Public Property RadioCallSign As String
    <DisplayName("Misc Information Doc")> _
    Public Property MiscInformationDoc As String
    Public Property Region As String
    <DisplayName("Business Unit")> _
    Public Property BusinessUnit As String
    Public Property Department As String
    Public Property Location As String
    <DisplayName("Operator Driver")> _
    Public Property OperatorDriver As String
    <DisplayName("Usage Type")> _
    Public Property UsageType As String
    Public Property Supervisor As String
    <DisplayName("Subject To FBT")> _
    Public Property SubjectToFBT As String
    <DisplayName("Change Account")> _
    Public Property ChargeAccount As String
    <DisplayName("Purchase Or Lease")> _
    Public Property PurchaseOrLease As String
    <DisplayName("Purchase Date")> _
    Public Property PurchaseDate As String
End Class

Public Class UtilisationRed
    Public Property KmsHrsVar As Double
    Public Property AnnualAveKms As Double
    Public Property AnnualAveHrs As Double
    Public Property AnnualBudgetKms As Double
    Public Property AnnualBudgetHrs As Double
End Class

Public Class ProductivityRed
    Public Property IncomeUnitsVar As Double
    Public Property KmsHrsVar As Double
    Public Property AnnualPredictedIncomeUnits As Double
    Public Property AnnualBudgetIncomeUnits As Double
    Public Property HireCharge As String
End Class

Public Class FuelConsumptionRed
    Public Property ExpectedFuelEconHr As Double
    Public Property LitresPerHour As Double
    Public Property FuelTypeHr As String
    Public Property ExpectedFuelEconKm As Double
    Public Property LitresPer100Kms As Double
    Public Property FuelTypeKms As String
    Public Property Deviation As Double
End Class

Public Class ReplacementRed
    Public Property OptimumReplacementDate As String
End Class

Public Class ServicesDueRed
    'Inherits RedRiskComposite

    Public Property LastSafetyServiceDate As String
    Public Property NextSafetyServiceDue As String
    Public Property LastStandardServiceDate As String
    Public Property NextStandardServiceDue As String
End Class

Public Class CostVRecoveryRed
    'Inherits RedRiskComposite

    Public Property OverHeadCost As String
    Public Property FuelCost As String
    Public Property MaintenanceCost As String
    Public Property FBTCost As String
    Public Property LeaseCost As String
    Public Property CostSubTotal As Double
    Public Property MonthlyCharge As String
    Public Property CostVariation As Double
End Class

Public Class MaintenanceRatioRed
    'Inherits RedRiskComposite

    Public Property LabourHoursNonSched As Double
    Public Property LabourHoursSched As Double
    Public Property DowntimeVariation As Double
End Class

Public Class AssetDataRed
    'Inherits RedRiskComposite

    Public Property AssetDataRisk As String
End Class