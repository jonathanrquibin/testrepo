﻿Imports System.Web

Namespace DataObjects
    Public Class usp_GetAssetDataForFuel
#Region "properties"
        Public Property FuelAssetDataID As String
        Public Property Group As String
        Public Property Type As String
        Public Property Make As String
        Public Property Model As String
        Public Property MeterType As String
        Public Property LiterMeterCalculated As String
        Public Property MeterOverrideValue As String
#End Region
#Region "constructors"
        Public Sub New()

        End Sub

        Public Sub New(usp As Business.usp_GetAssetDataForFuelResult)
            With usp
                Me.FuelAssetDataID = .FuelAssetDataID
                Me.Group = .Group
                Me.Type = .Type
                Me.Make = .Make
                Me.Model = .Model
                Me.MeterType = .MeterType
                Me.LiterMeterCalculated = .LiterMeterCalculated
                Me.MeterOverrideValue = .MeterOverrideValue
            End With
        End Sub
#End Region
#Region "CRUD"
        Public Shared Sub Create(usp As DataObjects.usp_GetAssetDataForFuel)

        End Sub

        Public Shared Sub Update(usp As DataObjects.usp_GetAssetDataForFuel)
            Dim fco As New FuelConsumptionOverride()
            Dim getFuelConsumption = FuelConsumptionOverride.GetFuelConsumptionValue(usp.Make, usp.Model, usp.MeterType, usp.MeterOverrideValue)
            If Not getFuelConsumption.Count() > 0 Then
                With usp
                    fco.Make = .Make
                    fco.Model = .Model
                    fco.Meter = .MeterType
                    fco.MeterOverrideValue = .MeterOverrideValue
                    fco.UserID = DirectCast(HttpContext.Current.Session("UserLogin"), DataObjects.Login).UserName
                    fco.FuelConsumptionOverrideDate = DateTime.Now()
                End With
                FuelConsumptionOverride.Create(fco)
            End If
        End Sub

        Public Shared Sub Delete(usp As DataObjects.usp_GetAssetDataForFuel)

        End Sub
#End Region
#Region "Gets and Sets"
        Public Shared Function GetAll() As List(Of DataObjects.usp_GetAssetDataForFuel)
            Dim retLst As List(Of DataObjects.usp_GetAssetDataForFuel)

            With New ltsDataContext
                retLst = .usp_GetAssetDataForFuel.Select(Function(x) New DataObjects.usp_GetAssetDataForFuel(x)).ToList()

                .Dispose()
            End With

            Return retLst
        End Function
#End Region
    End Class
End Namespace