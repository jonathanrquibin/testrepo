﻿Public Class SessionHelper


    Public Shared Property UserLogin As carpoolbooking.Business.DataObjects.User
        Get
            Dim o = CType(HttpContext.Current.Session("CPB_USER"), carpoolbooking.Business.DataObjects.User)

            If o Is Nothing Then Return Nothing Else Return o

        End Get
        Set(value As carpoolbooking.Business.DataObjects.User)
            HttpContext.Current.Session("CPB_USER") = value
        End Set
    End Property

End Class
