﻿Public Class Administrator1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            loadCompanyFields()
        End If
    End Sub

    Protected Sub odsUser_Inserting(sender As Object, e As ObjectDataSourceMethodEventArgs)

        'validate email
        'e.Cancel = True
        Dim c = DirectCast(e.InputParameters("x"), carpoolbooking.Business.DataObjects.User)
        c.CompanyID = SessionHelper.UserLogin.CompanyID
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnGenSetSave.Click
        Dim UserCompany = carpoolbooking.Business.DataObjects.Company.GetFromUser(SessionHelper.UserLogin)
        UserCompany.Address = txtAdress.Text
        UserCompany.Name = txtName.Text
        UserCompany.PhoneNumber = txtNumber.Text
        UserCompany.CompanyLogo = imgLogo.Value

        carpoolbooking.Business.DataObjects.Company.Update(UserCompany)

    End Sub
    Private Sub loadCompanyFields()

        Dim UserCompany = carpoolbooking.Business.DataObjects.Company.GetFromUser(SessionHelper.UserLogin)
        txtAdress.Text = UserCompany.Address
        txtName.Text = UserCompany.Name
        txtNumber.Text = UserCompany.PhoneNumber
        imgLogo.Value = UserCompany.CompanyLogo
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnGenSetReset.Click
        loadCompanyFields()
    End Sub
End Class