﻿<%@ Page Language="vb" MasterPageFile="~/SidePanel.Master" AutoEventWireup="false" CodeBehind="Administrator.aspx.vb" Inherits="uniqco.WEBCPB.Administrator1" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SideNavPlaceHolder" runat="server">
   <dx:ASPxMenu ID="AdminSideNav" AllowSelectItem="true" ClientInstanceName="AdminSideNav" CssClass="sidenavimenu" Theme="SoftOrange" runat="server" 
       ItemStyle-SelectedStyle-Border-BorderStyle="None" EnableAdaptivity="true" Orientation="Vertical" Width="100%" Border-BorderStyle="None">
        <Items>
            <dx:MenuItem Selected="true" Image-Url="#" Text="General Settings" NavigateUrl="javascript:void(0)"></dx:MenuItem>
            <dx:MenuItem Image-Url="#" Text="User Management" NavigateUrl="javascript:void(0)"></dx:MenuItem>
            <dx:MenuItem Image-Url="#" Text="Driver Management" NavigateUrl="javascript:void(0)"></dx:MenuItem>
            <dx:MenuItem Image-Url="#" Text="Vehicle Management" NavigateUrl="javascript:void(0)"> </dx:MenuItem>
            <dx:MenuItem Image-Url="#" Text="Vehicle/ Driver Assignment" NavigateUrl="javascript:void(0)"> </dx:MenuItem>
        </Items>
        <ClientSideEvents ItemClick="function(s,e){
            debugger;
            AdministratorContent.SetActiveTabIndex(e.item.index);
            
           }" />
        </dx:ASPxMenu>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
       <dx:ASPxPageControl ContentStyle-Border-BorderStyle="None" ShowTabs="false" ID="AdministratorContent" ClientInstanceName="AdministratorContent" ActiveTabIndex="0" Width="100%" runat="server">
           <TabPages>
               <dx:TabPage Name="General Settings" Text="General Settings">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div>
                                     
                                    <table >
                                        <tr>
                                            <td>
                                     <dx:ASPxBinaryImage ID="imgLogo" runat="server" Height="206px" Width="241px">
                                         <EditingSettings Enabled="True">
                                         </EditingSettings>
                                     </dx:ASPxBinaryImage></td>
                                            <td style="display:block;padding:0 20px">
                                                <table>
                                                    <tr>
                                                        <td>Company Name</td>
                                                        <td>
                                                             <dx:ASPxTextBox ID="txtName" runat="server" Width="245px">
                                                             </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone Number</td>
                                                        <td>
                                                             <dx:ASPxTextBox ID="txtNumber" runat="server" Width="245px">
                                                             </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Address</td>
                                                        <td>
                                                            <dx:ASPxMemo ID="txtAdress" runat="server" Height="71px" Width="245px"></dx:ASPxMemo>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                   <hr style="width:50%;margin:20px auto" />
                                    <div style="width:300px;margin:0;padding:0;height:30px;display:inline-block">
                                        <dx:ASPxButton Theme="SoftOrange" runat="server" ID="btnGenSetSave" Text="Save" Width="100px" Height="100%" Border-BorderStyle="None" Font-Size="13px"> </dx:ASPxButton>
                                        <dx:ASPxButton Theme="SoftOrange" runat="server" ID="btnGenSetReset" Text="Reset" Width="100px" Height="100%" Border-BorderStyle="None" Font-Size="13px"> </dx:ASPxButton>
                                    </div>
                                </div>
                            </dx:ContentControl>
                         </ContentCollection>
                </dx:TabPage>
               <dx:TabPage Name="User Management" Text="User Management">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div>
                                     <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1"
                                           runat="server" AutoGenerateColumns="False" DataSourceID="odsUser" KeyFieldName="UserID" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                   
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Visible="true" />
        </SettingsPager>
                                    <SettingsSearchPanel Visible="True" />
                                    <SettingsEditing Mode="PopupEditForm" >
                                    </SettingsEditing>
                              <SettingsPopup EditForm-HorizontalAlign="WindowCenter" CustomizationWindow-VerticalAlign="WindowCenter">

<EditForm HorizontalAlign="WindowCenter"></EditForm>

<CustomizationWindow VerticalAlign="WindowCenter"></CustomizationWindow>

                              </SettingsPopup>
                                         
                                    <EditFormLayoutProperties ColCount="2"  >
                                 <Items>
                                    <dx:GridViewColumnLayoutItem ColumnName="Pic" RowSpan="5" Caption="" >
                                    </dx:GridViewColumnLayoutItem>
                                    <dx:GridViewColumnLayoutItem ColumnName="UserName" >
                                    </dx:GridViewColumnLayoutItem>
                                    <dx:GridViewColumnLayoutItem ColumnName="EmailAddress" >
                                    </dx:GridViewColumnLayoutItem>
                                    <dx:GridViewColumnLayoutItem ColumnName="Password" >
                                    </dx:GridViewColumnLayoutItem>
                                    <dx:GridViewColumnLayoutItem ColumnName="Rights" >
                                    </dx:GridViewColumnLayoutItem>
                                    <dx:GridViewColumnLayoutItem ColumnName="Active" >
                                    </dx:GridViewColumnLayoutItem>
                                    <dx:EditModeCommandLayoutItem ColSpan="2" HorizontalAlign="Right">
                                    </dx:EditModeCommandLayoutItem>
                                 </Items>
                              </EditFormLayoutProperties>
                                     <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0"  Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="UserID" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                        </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataBinaryImageColumn  Width="200px" FieldName="Pic" Caption="Picture" ShowInCustomizationForm="True" VisibleIndex="2">
                                             <PropertiesBinaryImage ImageWidth="200px">
                                                <EditingSettings Enabled="True">
                                                </EditingSettings>
                                            </PropertiesBinaryImage>
                                         </dx:GridViewDataBinaryImageColumn>
                                        <dx:GridViewDataTextColumn FieldName="UserName" ShowInCustomizationForm="True" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="EmailAddress" ShowInCustomizationForm="True" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Rights" ShowInCustomizationForm="True" VisibleIndex="5">
                                            <PropertiesComboBox DataSourceID="odsUserRights">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="Password" ShowInCustomizationForm="True" VisibleIndex="6" PropertiesTextEdit-Password="true">
<PropertiesTextEdit Password="True"></PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Active" ShowInCustomizationForm="True" VisibleIndex="7"  Width="100px">
                                        </dx:GridViewDataCheckColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                               <asp:ObjectDataSource ID="odsUser" OnInserting="odsUser_Inserting" runat="server" DataObjectTypeName="carpoolbooking.Business.DataObjects.User" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAllFromUser" TypeName="carpoolbooking.Business.DataObjects.User" UpdateMethod="Update">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="User" SessionField="CPB_USER" Type="Object" />
                                    </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <asp:ObjectDataSource ID="odsUserCompanies" runat="server" SelectMethod="GetAll" TypeName="carpoolbooking.Business.DataObjects.Company"></asp:ObjectDataSource>
                                  
                                    <asp:ObjectDataSource ID="odsUserRights" runat="server" SelectMethod="GetRightsList" TypeName="carpoolbooking.Business.DataObjects.User">
                                       
                                      </asp:ObjectDataSource>
                                </div>
                            </dx:ContentControl>
                         </ContentCollection>
                </dx:TabPage>
               <dx:TabPage Name="Driver Management" Text="Driver Management">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div>
                                    Driver Management
                                </div>
                            </dx:ContentControl>
                         </ContentCollection>
                </dx:TabPage>
               <dx:TabPage Name="Vehicle Management" Text="Vehicle Management">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div>
                                    Vehicle Management
                                </div>
                            </dx:ContentControl>
                         </ContentCollection>
                </dx:TabPage>
               <dx:TabPage Name="Vehicle/ Driver Assignment" Text="Vehicle/ Driver Assignment">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div>
                                    Vehicle/ Driver Assignment
                                </div>
                            </dx:ContentControl>
                         </ContentCollection>
                </dx:TabPage>
           </TabPages>

<ContentStyle Border-BorderStyle="None"></ContentStyle>
       </dx:ASPxPageControl>
</asp:Content>

