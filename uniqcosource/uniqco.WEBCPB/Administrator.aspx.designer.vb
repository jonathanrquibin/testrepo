﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Administrator1

    '''<summary>
    '''AdminSideNav control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AdminSideNav As Global.DevExpress.Web.ASPxMenu

    '''<summary>
    '''AdministratorContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AdministratorContent As Global.DevExpress.Web.ASPxPageControl

    '''<summary>
    '''imgLogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgLogo As Global.DevExpress.Web.ASPxBinaryImage

    '''<summary>
    '''txtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtName As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''txtNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNumber As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''txtAdress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAdress As Global.DevExpress.Web.ASPxMemo

    '''<summary>
    '''btnGenSetSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenSetSave As Global.DevExpress.Web.ASPxButton

    '''<summary>
    '''btnGenSetReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenSetReset As Global.DevExpress.Web.ASPxButton

    '''<summary>
    '''ASPxGridView1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridView1 As Global.DevExpress.Web.ASPxGridView

    '''<summary>
    '''odsUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents odsUser As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''odsUserCompanies control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents odsUserCompanies As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''odsUserRights control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents odsUserRights As Global.System.Web.UI.WebControls.ObjectDataSource
End Class
