﻿Imports uniqco.Business

Module Module1

    Sub Main()

        Console.WriteLine("Starting loop at {0}", Now.ToString("HH:mm:ss dd/MMM/yyyy"))

        'get the root directory
        Dim rootDirList As List(Of String) = System.IO.Directory.EnumerateDirectories(My.Settings.filelocation).ToList

        Dim companies As List(Of DataObjects.Company) = DataObjects.Company.GetAll

        'go to each company directory, if it does not exist, then create it.
        For Each dirName As String In rootDirList

            Dim CompanyName As String = dirName.Split("\").Last

            'check if the compnay name exists 
            Dim companyExists As Boolean = companies.Where(Function(x) x.Name = CompanyName).Count > 0

            'if the company does not exist, then create it.
            If Not companyExists Then

                Dim newCompany As New DataObjects.Company

                With newCompany
                    .Name = CompanyName
                    .Address = "auto-created from the data upload process at " & Now.ToString("dd/MMM/yyyy HH:mm:ss")
                End With
                DataObjects.Company.Create(newCompany)
            End If
        Next

        'go through the list again, knowing that the company names exist this time
        For Each dirname As String In rootDirList.OrderBy(Function(x) x)

            Dim companyName As String = dirname.Split("\").Last
            Dim dataDirLocation As String = String.Format("{0}\Data", dirname)

            Dim company As DataObjects.Company = DataObjects.Company.GetFromName(companyName)

            For Each file In System.IO.Directory.GetFiles(dataDirLocation)

                'if there is an exceptoin caused for any reason we will change the extension to "ERR"
                Dim processedfileExtension As String = ".processed"

                Try

                    Dim fileName As String = file.Split("\").Last

                    Console.WriteLine("processing {0}", fileName)

                    If Not file.ToLower.EndsWith("csv") Then
                        Console.WriteLine("skipping file " & fileName & " as it is not a CSV")
                        Continue For
                    End If

                    Dim dateParts() As String = fileName.Replace("RiskComposite_", "").Split(".")(0).Split("-")
                    Dim reportDate As Date = New Date(dateParts(2), dateParts(1), dateParts(0)).AddMonths(-1)
                    Dim companyReport = company.GetReportforDate(reportDate)

                    'If the company report was interrupted last time, 
                    'then delete the report and all its data and reprocess
                    If companyReport IsNot Nothing AndAlso Not companyReport.ImportWasSuccess Then

                        Console.WriteLine("Report ""{0}"" already exists in an ERR state, deleting old date to reprocess", fileName)
                        companyReport.DeleteReportAndAllRelatedData()
                        companyReport = Nothing
                    End If

                    Dim reportAlreadyExists As Boolean = companyReport IsNot Nothing

                    If Not reportAlreadyExists Then

                        Console.WriteLine("Creating new report for {0}", fileName)

                        'if the report does not already exist, then create it
                        Dim newrept As New DataObjects.CompanyReport With {.CompanyId = company.CompanyID _
                                                                          , .Notes = "created automatically on " & Now.ToString("dd/MMM/yyyy HH:mm:ss") _
                                                                          , .ProcessedDate = Now _
                                                                          , .Reportdate = reportDate _
                                                                          , .ImportWasSuccess = False _
                                                                          , .ReportType = "Composite"}

                        DataObjects.CompanyReport.Create(newrept)

                        newrept.CreateReportcontentFromFile(file)
                        newrept.MarkImportAsSuccess()

                    End If

                Catch ex As Exception

                    Console.WriteLine("ERROR{0}{1}{0}{2}", vbNewLine, ex.Message, ex.StackTrace)
                    processedfileExtension = ".ERROR"
                End Try

                'change the file extension to indicate that the file has been processed
                System.IO.File.Move(file, file & processedfileExtension)
            Next

        Next
        Console.WriteLine("press any key to exit...")
        Console.ReadKey()

    End Sub

End Module
