﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExampleCallback.aspx.vb" Inherits="uniqco.WEBBI.ExampleCallback" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <script src="content/js/page.js"></script>
    <script src="content/js/jquery-3.0.0.min.js"></script>


</head>
<body>
    <form id="form1" runat="server">

        <script type="text/javascript">

            function btnGo_Click(s,e){

                var param = {};

                param.newName = txtName.GetText();
                param.messageFromClient = txtClientMessage.GetText();

                ajaxMethod("MainWCFService.svc/" + 'ExampleWCFGet',
                        param, example_SuccessCallback, example_ErrorCallback, example_FinallyCallback);


            }

            //Public Function ExampleWCFGet(newName As String, messageFromClient As String) As WCFReturnClasses.ExampleObject

            function example_SuccessCallback(result) {

                alert(result.d._TimeFromServer);
            }

            function example_ErrorCallback(result) {

                //normall we would somehow tell the user what went wrong here.
            }

            function example_FinallyCallback() {


            }


        </script>


        <div>


            <table>
                <tr>
                    <td>name</td>
                    <td>
                        <dx:ASPxTextBox ClientInstanceName="txtName" ID="txtName" runat="server" Width="170px"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td>client message</td>
                    <td>
                        <dx:ASPxTextBox ClientInstanceName="txtClientMessage" ID="txtClientMessage" runat="server" Width="170px"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <dx:ASPxButton AutoPostBack="false" ClientInstanceName="btnGo" ID="btnGo" runat="server" Text="send msg to server">

                            <ClientSideEvents  Click="function(s,e){btnGo_Click(s,e);}" />

                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
