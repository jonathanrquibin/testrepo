﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BusinessInformationTool.aspx.vb" Inherits="uniqco.WEBBI.BusinessInformationTool" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.1.Web, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.1.Web, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web.Designer" TagPrefix="dxchartdesigner" %>



<%@ Register TagPrefix="cc1" Namespace="DevExpress.XtraCharts" Assembly="DevExpress.XtraCharts.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <script src="/content/js/jquery-3.0.0.min.js"></script>
      <script src="/content/js/highcharts.js"></script>
        <script src="/content/js/drilldown.js"></script>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="/content/js/bifrontpage.js"></script>
    
      <style>
           .hiddenCalendar .dxeCalendar_SoftOrange
        {
            visibility: hidden !important;
        }
          .hiddenCalendar .dxpc-mainDiv
        {
            height: 0px !important;
        }
          .linklookalike {
              background: none !important;
              border: none;
              padding: 0 !important;
              font: inherit;
              /*border is optional*/
              text-decoration:underline;
              border:none;
              cursor: pointer;
          }
          .selecthead input {
            font-size: 18px !important;
        }
          
              .linklookalike :hover {
                  color:rgb(255, 69, 0);
              }
         #FilterText_edi, #f_editor:focus {
         outline: none;
         }
          @media screen and (min-width: 1600px) {
    #chart-container {
        display:inline-flex;
    }
}
         
        #container text {text-decoration: none !important;}
        
       /*  .reverse-ellipsis {
         
          text-overflow: clip;
          position: relative;
          background-color: #FFF;
        }

        .reverse-ellipsis:before {
          content: '\02026';
          position: absolute;
          z-index: 1;
          left: -1em;
          background-color: inherit;
          padding-left: 1em;
          margin-left: 0.5em;
        }

        .reverse-ellipsis span {
          min-width: 100%;
          position: relative;
          display: inline-block;
          float: right;
          overflow: visible;
          background-color: inherit;
          text-indent: 25px;
        }

        .reverse-ellipsis span:before {
          content: '';
          position: absolute;
          display: inline-block;
          width: 1em;
          height: 1em;
          background-color: inherit;
          z-index: 200;
          left: -.5em;
        } */
        .highcharts-credits{
            display:none;
        }

      </style>
         <div>
            <dx:ASPxHiddenField ID="BIToolParams" ClientInstanceName="BIToolParams" runat="server"></dx:ASPxHiddenField>
            <div id="containerDIV" style="margin-left: 0;  margin-right: 0;width: 100%;">
                <div id="chart-container">
               <div id="left-chart-container" style="margin: 0 auto;width:500px;"> <%-- --%>
                  <div style="margin-top: 50px;padding:0;width:500px; ">
                      <center>
                        <dx:ASPxComboBox ID="txtPieView" ClientInstanceName="txtPieView" CssClass="selecthead" ItemStyle-Font-Size="18px"  Theme="Metropolis" runat="server" Width="275px" ValueType="System.String">
                            <Items>
                                <dx:ListEditItem Text="Region" Value ="Region" />
                                <dx:ListEditItem Text="Business Unit" Value ="BusinessUnit" />
                                <dx:ListEditItem Text="Vehicle Group" Value ="VehicleType" />
                            </Items>
                            <ClientSideEvents ValueChanged="pcv"  />
                              </dx:ASPxComboBox></center>
                     <div id="container" style="min-width: 500px; height: 400px; margin: 0 auto;"></div>
                      <div style="top: -480px; font-family: arial, helvetica, sans-serif; font-size: 15px; color:#afafaf; position: relative;z-index: 500; width: 500px;" class=" reverse-ellipsis"><span id="pieHistory" ></span></div>
                      
                      <input type="button" style="float:right;top: -395px; display:none; right: 15px;position: relative;z-index: 500;height: 35px;" id="btnpieback" value="◁" />
                  </div>
                  <div style="padding:0;margin-top:10px;width:500px;height:120px;">
                     <table style="font-family: arial, helvetica, sans-serif;font-size:12px;border-collapse:separate;border-spacing:15px;">
                         <tr>
                           <td style="padding-left:30px">
                               <dx:ASPxLabel ID="lblReportDate" runat="server" Text="Report Date"></dx:ASPxLabel></td>
                           <td class ="hiddenCalendar" style="padding-left:30px">
                               <dx:ASPxDateEdit ID="txtReportDate" ClientInstanceName="txtReportDate" EditFormat="Custom" EditFormatString="MMM yyyy" UseMaskBehavior="true" DisplayFormatString="MMM yyyy" Theme="SoftOrange" Width="275px" runat="server">
                                   <ClientSideEvents Init="function (s, e) {
                                        var calendar = s.GetCalendar();
                                        calendar.owner = s;
                                    }" DropDown="function (s, e) {
                                        var calendar = s.GetCalendar();
                                        var fastNav = calendar.fastNavigation;
                                        fastNav.activeView = calendar.GetView(0, 0);
                                        fastNav.Prepare();
                                        fastNav.GetPopup().popupVerticalAlign = 'Below';
                                        fastNav.GetPopup().ShowAtElement(s.GetMainElement())


                                        fastNav.OnOkClick = function () {
                                            var parentDateEdit = this.calendar.owner;
                                            var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                                            parentDateEdit.SetDate(currentDate);
                                            parentDateEdit.HideDropDown();
                                            fastNav.GetPopup().Hide();
                                       
                                            BIToolParams.Set('FilterExpression', '');
                                            BIToolParams.Set('FilterExpression_Drilldown', '');
                                            PieSource = [];
                                            RemovedPieView = [];
                                            txtPieView.ClearItems();
                                            txtPieView.AddItem('Region','Region');
                                            txtPieView.AddItem('Business Unit','BusinessUnit');
                                            txtPieView.AddItem('Vehicle Group','VehicleType');
                                            txtPieView.SetSelectedIndex(txtPieView.GetItemCount()-1);
                                            resetFilterForm('clear');
                                            ASPxCallback1.PerformCallback('cc');
                                        }

                                        fastNav.OnCancelClick = function () {
                                            var parentDateEdit = this.calendar.owner;
                                            parentDateEdit.HideDropDown();
                                            fastNav.GetPopup().Hide();
                                        }
                                    }" />
                                   <ClientSideEvents ValueChanged="function(s,e){
                                        BIToolParams.Set('FilterExpression', '');
                                        BIToolParams.Set('FilterExpression_Drilldown', '');
                                        PieSource = [];
                                        RemovedPieView = [];
                                        txtPieView.ClearItems();
                                        txtPieView.AddItem('Region','Region');
                                        txtPieView.AddItem('Business Unit','BusinessUnit');
                                        txtPieView.AddItem('Vehicle Group','VehicleType');
                                        txtPieView.SetSelectedIndex(txtPieView.GetItemCount()-1);
                                        resetFilterForm('clear');
                                        ASPxCallback1.PerformCallback('cc');
                                       }" />
                               </dx:ASPxDateEdit>
                           </td>
                         </tr>
                        <tr>
                           <td style="padding-left:30px">
                               <dx:ASPxLabel ID="lblCompany" runat="server" Text="Company"></dx:ASPxLabel>
                               </td>
                           <td style="padding-left:30px">
                              <dx:ASPxComboBox ID="txtCompany" OnDataBound="txtCompany_DataBound" ClientInstanceName="txtCompany" Theme="SoftOrange" runat="server" Width="275px" ValueType="System.String" DataSourceID="odsloginCompanies" TextField="Name" ValueField="CompanyID">
                                 <ClientSideEvents ValueChanged="function(s, e) {
                                     $('#ContentPlaceHolder1_dnn_dnnLogo_imgLogo').attr('src','/content/CompanyLogo.ashx?id=' + txtCompany.GetValue());
                                    BIToolParams.Set('FilterExpression', '');
                                    BIToolParams.Set('FilterExpression_Drilldown', '');
                                    PieSource = [];
                                    RemovedPieView = [];
                                    txtPieView.ClearItems();
                                    txtPieView.AddItem('Region','Region');
                                    txtPieView.AddItem('Business Unit','BusinessUnit');
                                    txtPieView.AddItem('Vehicle Group','VehicleType');
                                    txtPieView.SetSelectedIndex(txtPieView.GetItemCount()-1);
                                    resetFilterForm('clear');
                                    ASPxCallback1.PerformCallback('cc');
                                    }"  />
                              </dx:ASPxComboBox>
                              <asp:ObjectDataSource ID="odsloginCompanies" runat="server" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.Company"></asp:ObjectDataSource>
                              <dx:ASPxCallback ID="ASPxCallback1" ClientInstanceName="ASPxCallback1" OnCallback="ASPxCallback1_Callback" runat="server">
                                 <ClientSideEvents CallbackComplete ="function(s, e) {
                                     CreateChart(txtPieView.GetValue());
                                     $('#pieHistory').html('');
                                     $('#btnpieback').css('display', 'none');
                                     performchartcallback();
                                     performgridcallback();
                                    }"   />
                              </dx:ASPxCallback>
                           </td>
                        </tr>
                        <tr>
                           <td style="padding-left:30px;vertical-align: top;">
                               <dx:ASPxLabel ID="lblFilters" runat="server" Text="Filters"></dx:ASPxLabel>

                           </td>
                           <td style="padding-left:30px">
                              <dx:ASPxButton ID="btnFilter" runat="server" Text="Set Filter" Theme="SoftOrange" AutoPostBack="False">
                                 <ClientSideEvents Click="function(s, e) {
                                    resetFilterForm('load');
                                    }"/>
                              </dx:ASPxButton>
                              <dx:ASPxButton ID="ASPxButton4" runat="server" Text="Reset All" Theme="SoftOrange" AutoPostBack="False">
                                 <ClientSideEvents Click="function(s, e) {
                                    location.reload();
                                    }"/>
                              </dx:ASPxButton>
                              <div id="filter-popup">
                                 <dx:ASPxPopupControl ClientInstanceName="ASPxPopupClientControl" Width="710px" Height="300px"
                                    MaxWidth="800px" MaxHeight="800px" MinHeight="150px" MinWidth="150px" ID="pcMain"
                                    ShowFooter="true" PopupElementID="btnFilter" HeaderText="Chart Filter"
                                    runat="server" EnableViewState="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" EnableHierarchyRecreation="True" 
                                    PopupAnimationType="None" CloseAnimationType="None" CloseAction="OuterMouseClick" AllowDragging="true" DragElement="Header" AllowResize ="True" ResizingMode="Live" ShowCloseButton="true" >
                                    <FooterTemplate>
                                       <div style="display: table; width: 100%">
                                          <table align="right">
                                             <tr>
                                                <td>
                                                   <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Apply" Theme="SoftOrange" AutoPostBack="False">
                                                      <ClientSideEvents Click="function(s, e) {
                                                         applyFilter();
                                                          performchartcallback();
                                                          performgridcallback();
                                                          CreateChart(txtPieView.GetValue());
                                                          ASPxPopupClientControl.Hide();
                                                         }"/>
                                                   </dx:ASPxButton>
                                                </td>
                                                <td>
                                                   <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="False"  Text="Close" Theme="SoftOrange">
                                                      <ClientSideEvents Click="function(s, e) {
                                                         ASPxPopupClientControl.Hide();
                                                         }"/>
                                                   </dx:ASPxButton>
                                                </td>
                                             </tr>
                                          </table>
                                       </div>
                                    </FooterTemplate>
                                    <ContentCollection>
                                       <dx:PopupControlContentControl runat="server">
                                          <asp:Panel ID="Panel1" runat="server">
                                             <dx:ASPxRoundPanel ID="ASPxRoundPanel1" Theme="SoftOrange"  runat="server" HeaderText="Usage Type" ShowCollapseButton="true" Width="100%">
                                                <PanelCollection>
                                                   <dx:PanelContent>
                                                      <div id="grpUsageType" style="display:inline-flex;width: 100%;">
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkUTBusinessUse" ClientInstanceName="chkUTBusinessUse" Text="Business Use" runat="server"></dx:ASPxCheckBox>
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkUTCommuterUse" ClientInstanceName="chkUTCommuterUse" Text="Commuter Use" runat="server"></dx:ASPxCheckBox>
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkUTPrivateUse" ClientInstanceName="chkUTPrivateUse" Text="Private Use" runat="server"></dx:ASPxCheckBox>
                                                      </div>
                                                   </dx:PanelContent>
                                                </PanelCollection>
                                             </dx:ASPxRoundPanel>
                                             <%-- <br />
                                             <dx:ASPxRoundPanel ID="ASPxRoundPanel2" Theme="SoftOrange"  runat="server" HeaderText="Category" ShowCollapseButton="true" Width="100%">
                                                <PanelCollection>
                                                   <dx:PanelContent>
                                                      <div id="grpCategory" style="display:inline-flex;width: 100%;">
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkCatMajorPlant" ClientInstanceName="chkCatMajorPlant" Text="Major Plant > $10K" runat="server"></dx:ASPxCheckBox>
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkCatMinorPlant" ClientInstanceName="chkCatMinorPlant" Text="Minor Plant < $10K" runat="server"></dx:ASPxCheckBox>
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkCatOtherAssets" ClientInstanceName="chkCatOtherAssets" Text="Other Assets" runat="server"></dx:ASPxCheckBox>
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkCatHire" ClientInstanceName="chkCatHire" Text="Hire" runat="server"></dx:ASPxCheckBox>
                                                         <dx:ASPxCheckBox Theme="SoftOrange" style="margin-right:10px" ID="chkCatLightFleet" ClientInstanceName="chkCatLightFleet" Text="Light Fleet" runat="server"></dx:ASPxCheckBox>
                                                      </div>
                                                   </dx:PanelContent>
                                                </PanelCollection>
                                             </dx:ASPxRoundPanel>--%>
                                             <br />
                                             <div style="padding:0;margin:0">
                                                <div id="grpType" style="width: 100%;">
                                                   <table style="width: 100%;">
                                                      <tr>
                                                         <td style="padding-left: 15px;width:50px;">Classification:</td>
                                                         <td>
                                                            <dx:ASPxComboBox Theme="SoftOrange" ID="cbxTyType" ClientInstanceName="cbxTyType" Width="99%" runat="server" ValueType="System.String" OnCallback="cbxGRGroup_Callback">
                                                               <ClientSideEvents EndCallback="function(s, e) {
                                                                  showresetloading(false);
                                                                  }" />
                                                            </dx:ASPxComboBox>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </div>
                                                <br />
                                                <div id="grpMake" style="width: 100%;">
                                                   <table style="width: 100%;">
                                                      <tr>
                                                         <td style="padding-left: 15px;width:50px;">Make:</td>
                                                         <td>
                                                            <dx:ASPxComboBox Theme="SoftOrange" ID="cbxMkMake" ClientInstanceName="cbxMkMake" Width="99%" runat="server" ValueType="System.String" OnCallback="cbxGRGroup_Callback">
                                                               <ClientSideEvents EndCallback="function(s, e) {
                                                                  showresetloading(false);
                                                                  }" />
                                                            </dx:ASPxComboBox>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </div>
                                             </div>
                                             <br />
                                             <dx:ASPxRoundPanel ID="ASPxRoundPanel6" Theme="SoftOrange"  runat="server" HeaderText="Model" ShowCollapseButton="true" Width="100%">
                                                <PanelCollection>
                                                   <dx:PanelContent>
                                                      <div id="grpModel" style="display:inline-flex;width: 100%;">
                                                         <dx:ASPxComboBox Theme="SoftOrange" ID="cbxMdModel" Caption="Model" style="margin-right:10px" ClientInstanceName="cbxMdModel" Width="48%" runat="server" ValueType="System.String" OnCallback="cbxGRGroup_Callback">
                                                            <ClientSideEvents EndCallback="function(s, e) {
                                                               showresetloading(false);
                                                               }" />
                                                         </dx:ASPxComboBox>
                                                         <dx:ASPxSpinEdit Theme="SoftOrange" ID="cbxMdModelYear" Caption="Model Year" style="margin-right:10px" ClientInstanceName="cbxMdModelYear" Width="48%" MaxValue="9999" MinValue="0" runat="server">
                                                         </dx:ASPxSpinEdit>
                                                      </div>
                                                   </dx:PanelContent>
                                                </PanelCollection>
                                             </dx:ASPxRoundPanel>
                                             <dx:ASPxLoadingPanel ID="loadingPanel" HorizontalAlign="Center" VerticalAlign="Middle" Theme="SoftOrange"  Width="100px" Height="50px" ClientInstanceName="loadingPanel"  runat="server"></dx:ASPxLoadingPanel>
                                          </asp:Panel>
                                       </dx:PopupControlContentControl>
                                    </ContentCollection>
                                 </dx:ASPxPopupControl>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
               <div id="right-chart-container" style="margin: 0 auto;width: 775px;"> <%--style=""--%>
                  <div style="padding:0;height:350px; margin-top: 50px;width:775px">
                   <div  style="margin-bottom:10px">
                       <dx:ASPxComboBox ID="txtChartText" ClientInstanceName="txtChartText" CssClass="selecthead" ItemStyle-Font-Size="18px" Theme="Metropolis" runat="server" Width="275px" ValueType="System.String">
                                 <Items>
                                     <dx:ListEditItem Text="Utilisation" Value ="Utilisation" />
                                     <dx:ListEditItem Text="Productivity" Value ="OperationalProductivity" />
                                     <dx:ListEditItem Text="Fuel Consumption" Value ="FuelConsumption" />
                                     <dx:ListEditItem Text="Replacement" Value ="OptimumReplacement" />
                                     <dx:ListEditItem Text="Service Due" Value ="ServiceDue" />
                                     <dx:ListEditItem Text="Cost Vs Recovery" Value ="WOLCostVariation" />
                                     <dx:ListEditItem Text="Maintenance Ratio" Value ="MaintenanceRatio" />
                                 </Items>
                                  <ClientSideEvents ValueChanged="function(s, e) {
                                        performchartcallback();
                                    }"  />
                              </dx:ASPxComboBox>

                   </div> 
                                        <dxchartsui:WebChartControl Theme="SoftOrange" ID="WebChartControl2"
                                            runat="server"
                                            CrosshairEnabled="False"
                                            Height="275px"
                                            ClientInstanceName="barchartUT"
                                            Width="775px"
                                            OnCustomCallback="WebChartControl_CustomCallback" 
                                            OnDataBinding="WebChartControl2_DataBinding" 
                                            ToolTipEnabled="True">
                                            
                                            <DiagramSerializable>
                                            <cc1:XYDiagram>
                                                <AxisX VisibleInPanesSerializable="-1">
                                                    <tickmarks minorvisible="False" visible="False" />
                                                </AxisX>
                                                <AxisY VisibleInPanesSerializable="-1">
                                                    <tickmarks minorvisible="False" visible="False" />
                                                    <label textpattern="{V:0%}">
                                                    </label>
                                                    <gridlines visible="False">
                                                    </gridlines>
                                                </AxisY>
                                            </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend Direction="LeftToRight" AlignmentHorizontal="Center" AlignmentVertical="BottomOutside"></Legend>
                                            <SeriesSerializable>
                                            <cc1:Series Name="Red" LegendText="< -30%" ArgumentDataMember="ReportDateMonthYear" SummaryFunction="SUM([RedUtilisation])" ValueDataMembersSerializable="RedUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Red" >
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="Amber"  LegendText="< -20%" ArgumentDataMember="ReportDateMonthYear" SummaryFunction="SUM([AmberUtilisation])" ValueDataMembersSerializable="AmberUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Orange" >
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="Green" LegendText="+/- 20%" ArgumentDataMember="ReportDateMonthYear"  SummaryFunction="SUM([GreenUtilisation])" ValueDataMembersSerializable="GreenUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="LimeGreen" >
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="FadedAmber" LegendText="> 20%" ArgumentDataMember="ReportDateMonthYear"  SummaryFunction="SUM([FadedAmberUtilisation])" ValueDataMembersSerializable="FadedAmberUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Moccasin" >
                                                        <border color="Orange" />
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="FadedRed" LegendText="> 30%" ArgumentDataMember="ReportDateMonthYear"  SummaryFunction="SUM([FadedRedUtilisation])" ValueDataMembersSerializable="FadedRedUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Salmon" >
                                                        <border color="Red" />
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            </SeriesSerializable>
                                            <SeriesTemplate LabelsVisibility="False" />
                                            <ClientSideEvents ObjectSelected="function(s, e) {
                                            openChartPopup(e.additionalHitObject.argument,e.additionalHitObject.series.name);
                                            }" EndCallback="function(s, e) {
                                            $('.dxchartsuiTooltip_SoftOrange').remove();
                                            }" />
                                        </dxchartsui:WebChartControl>
                     <dx:ASPxPopupControl CloseAction="OuterMouseClick" ClientInstanceName="ASPxPopupChartControl" Width="1200" Height="550px"
                        ID="ASPxPopupControl1"
                        ShowFooter="false" 
                        runat="server" EnableViewState="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" EnableHierarchyRecreation="True" 
                        PopupAnimationType="None" CloseAnimationType="None" AllowDragging="true" DragElement="Header" AllowResize ="false" ResizingMode="Live" ShowCloseButton="true" >
                        <ContentCollection>
                           <dx:PopupControlContentControl runat="server">
                              <asp:Panel ID="Panel2" runat="server">
                                 <%--<table>
                                    <tr>
                                        <td>Argument: </td>
                                        <td><span id="bcArg"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Series: </td>
                                        <td><span id="bcSer"></span></td>
                                    </tr>
                                    </table>--%>

                                  <dx:ASPxButton ID="ASPxButton3" OnClick="ASPxButton3_Click" Cursor="pointer" CssClass="linklookalike" runat="server" Text="Export CSV" BackColor="Transparent" AutoPostBack="False"></dx:ASPxButton>
                                 <dx:ASPxGridView ID="ASPxGridView2" SettingsBehavior-AllowSort="true" SettingsBehavior-AllowGroup="true" runat="server" ClientInstanceName ="clb"
                                    OnCustomCallback="ASPxGridView2_CustomCallback" OnDataBinding="ASPxGridView2_DataBinding" AutoGenerateColumns="True" EnableTheming="True"
                                    Theme="SoftOrange" style="width:100%;" SettingsBehavior-AutoExpandAllGroups="true" KeyFieldName="ID" >
                                    <%--EnableViewState="true" EnableRowsCache="false"--%> 
                                    <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" />
                                    <SettingsPager PageSize="10">
                                       <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                 </dx:ASPxGridView>
                                  <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView2"></dx:ASPxGridViewExporter>
                              </asp:Panel>
                           </dx:PopupControlContentControl>
                        </ContentCollection>
                     </dx:ASPxPopupControl>
                  </div>
                  <div style="padding:0;width: 775px;">
                     <div id ="summary-data-container" style="width:100%;height:300px;overflow:visible">
                        <div style="min-width: 630px; height: 30px; margin: 0 auto;font-family: arial, helvetica, sans-serif;">
                           <span style="">Summary Data</span>
                        </div>
                        

      
        <dx:ASPxSplitter Theme="SoftOrange" style="font-size:12.96px !important;" Height="202" ID="SummarySplitter" runat="server" Width="100%" ClientInstanceName="SummarySplitter">
        <Styles>
            <Pane>
                <Paddings Padding="0px" />
            </Pane>
        </Styles>
        <Panes>
            <dx:SplitterPane Size="315px" Name="SummaryContainer" >
                <ContentCollection>
                    <dx:SplitterContentControl runat="server">
                        <dx:ASPxGridView ID="ASPxGridView1" OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated" Border-BorderStyle="None" Width="100%" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared" 
                                    ClientInstanceName="ASPxGridView1" OnCustomCallback="ASPxGridView1_CustomCallback" 
                                    runat="server" AutoGenerateColumns="False" EnableTheming="True" Theme="SoftOrange" DataSourceID="odssummarydata">
                                    <SettingsPager Visible="True">
                                    </SettingsPager>
                                    <Settings ShowColumnHeaders="False"></Settings>
                                    <Columns>
                                       <dx:GridViewDataTextColumn Width="215px" FieldName="Name" CellStyle-Cursor="pointer" VisibleIndex="0">
                                           <CellStyle Wrap="False" />
                                       </dx:GridViewDataTextColumn>
                                       <dx:GridViewDataTextColumn Width="100px" FieldName="Value"  VisibleIndex="1">
                                           <CellStyle Wrap="False" />
                                       </dx:GridViewDataTextColumn>
                                    </Columns>
                                 </dx:ASPxGridView>
                                 <asp:ObjectDataSource ID="odssummarydata" runat="server" SelectMethod="GetSummary" OnSelecting="odssummarydata_Selecting" TypeName="uniqco.Business.DataObjects.SummaryData">
                                    <SelectParameters>
                                       <asp:Parameter Name="data" Type="Object" />
                                    </SelectParameters>
                                 </asp:ObjectDataSource>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
            <dx:SplitterPane>
                <Panes>
                    <dx:SplitterPane Size="50%" Name="PerAssetContainer">
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server">
                                <dx:ASPxGridView ID="ASPxGridView3" Border-BorderStyle="None" Width="100%" OnHtmlDataCellPrepared="ASPxGridView3_HtmlDataCellPrepared" 
                                             ClientInstanceName="ASPxGridView3" OnCustomCallback="ASPxGridView1_CustomCallback" 
                                             runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false" EnableTheming="True" Theme="SoftOrange" DataSourceID="odsperassetdata">
                                             <SettingsPager Visible="True">
                                             </SettingsPager>
                                             <Columns>
                                                <dx:GridViewDataTextColumn Width="150px" FieldName="Name" HeaderStyle-Font-Bold="true" Caption="Per Asset"  VisibleIndex="0">
                                           <CellStyle Wrap="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="100px" FieldName="Value" HeaderStyle-Font-Bold="true" Caption="Client"  VisibleIndex="1">
                                           <CellStyle Wrap="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="100px" FieldName="Uniqco" HeaderStyle-Font-Bold="true"  Caption="Benchmark Data"  VisibleIndex="1">
                                           <CellStyle Wrap="False" />
                                                </dx:GridViewDataTextColumn>
                                             </Columns>
                                          </dx:ASPxGridView>
                                          <asp:ObjectDataSource ID="odsperassetdata" runat="server" OnSelecting="odsperassetdata_Selecting" SelectMethod="GetPerAsset" TypeName="uniqco.Business.DataObjects.SummaryData">
                                             <SelectParameters>
                                                <asp:ControlParameter ControlID="txtCompany" Name="com" PropertyName="Value" DbType="Guid" />
                                                <asp:Parameter Name="data" Type="Object" />
                                             </SelectParameters>
                                          </asp:ObjectDataSource>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                    <dx:SplitterPane Name="MaintenanceContainer">
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server">
                                
                                          <dx:ASPxGridView ID="ASPxGridView4" Border-BorderStyle="None" Width="100%" OnHtmlDataCellPrepared="ASPxGridView3_HtmlDataCellPrepared" 
                                             ClientInstanceName="ASPxGridView4" OnCustomCallback="ASPxGridView1_CustomCallback" 
                                             runat="server" AutoGenerateColumns="False"  SettingsBehavior-AllowSort="false" EnableTheming="True" Theme="SoftOrange" 
                                             DataSourceID="odsmaintenancedata">
                                             <SettingsPager Visible="True">
                                             </SettingsPager>
                                             <Columns>
                                                <dx:GridViewDataTextColumn Width="150px" HeaderStyle-Font-Bold="true" FieldName="Name" Caption ="Maintenance" VisibleIndex="0">
                                           <CellStyle Wrap="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="100px" HeaderStyle-Font-Bold="true" FieldName="Value" Caption ="Client"  VisibleIndex="1">
                                           <CellStyle Wrap="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Width="100px" HeaderStyle-Font-Bold="true" FieldName="Uniqco" Caption="Benchmark Data"  VisibleIndex="1">
                                           <CellStyle Wrap="False" />
                                                </dx:GridViewDataTextColumn>
                                             </Columns>
                                          </dx:ASPxGridView>
                                          <asp:ObjectDataSource ID="odsmaintenancedata" runat="server" OnSelecting="odsmaintenancedata_Selecting" SelectMethod="GetPerMaintenance" TypeName="uniqco.Business.DataObjects.SummaryData">
                                             <SelectParameters>
                                                <asp:ControlParameter ControlID="txtCompany" Name="com" PropertyName="Value" DbType="Guid" />
                                                <asp:Parameter Name="data" Type="Object" />
                                             </SelectParameters>
                                          </asp:ObjectDataSource>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>
        </Panes>
    </dx:ASPxSplitter>
                     </div>
                  </div>
               </div>
                    </div>
                
               
            </div>
         </div>
      <div style="padding:0;width:100%;height:50px;display: inline-block;"> 
                     <span style="float:right;font-size:10px"> Version 1.0</span>
                  </div>
    </form>
</body>
</html>
