﻿Public Class Main
    Inherits System.Web.UI.MasterPage

    Public ReadOnly Property WebVersion As String
        Get
            Return My.Settings.WebVersion
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        Dim pathandquery As String = Request.Url.PathAndQuery

        If SessionHelper.UserLogin Is Nothing AndAlso Not pathandquery.ToLower.Contains("login.aspx") Then

            Dim queryStr As String = Request.Url.Query
            queryStr = String.Format("/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)
            Response.Redirect(queryStr)
        End If

    End Sub

    'Protected Sub cbHeartbeat_Callback(source As Object, e As DevExpress.Web.CallbackEventArgs)
    '    HttpContext.Current.Session("Heartbeat") = DateTime.Now
    'End Sub
End Class