﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web

<ServiceContract(Namespace:="")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class MainWCFService

    ' To use HTTP GET, add <WebGet()> attribute. (Default ResponseFormat is WebMessageFormat.Json)
    ' To create an operation that returns XML,
    '     add <WebGet(ResponseFormat:=WebMessageFormat.Xml)>,
    '     and include the following line in the operation body:
    '         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml"
    <OperationContract()>
    Public Sub DoWork()
        ' Add your operation implementation here
    End Sub


    <WebInvoke(Method:="POST", BodyStyle:=WebMessageBodyStyle.WrappedRequest, ResponseFormat:=WebMessageFormat.Json)>
    Public Function ExampleWCFGet(newName As String, messageFromClient As String) As WCFReturnClasses.ExampleObject

        Dim retobj As New WCFReturnClasses.ExampleObject

        retobj.Name = newName
        retobj.MessageFromClient = messageFromClient
        retobj.Number = "test number"
        retobj.TimeFromServer = String.Format("the time is {0}, {1}", Now.ToString("dd/MMM/yyyy HH:mm:ss"), retobj.Name)



        Return retobj

    End Function
    ' Add more operations here and mark them with <OperationContract()>

End Class
