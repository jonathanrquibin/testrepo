﻿Imports uniqco.Business
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Export
Imports System.Globalization

Public Class Login
    Inherits System.Web.UI.Page

    Public Const COOKIE_NAME As String = "uniqco"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Uncomment this one later
        Dim authCookie As HttpCookie = Request.Cookies(COOKIE_NAME)

        If authCookie IsNot Nothing Then

            Dim email As String = authCookie.Value
            Dim login As DataObjects.Login = DataObjects.Login.GetAllForEmailAddress(email)

            If login IsNot Nothing Then
                SessionHelper.UserLogin = login
                Response.Redirect("/AdminDefault.aspx")
                Exit Sub
            End If
        Else
        End If

        If Not IsPostBack Then

            HttpContext.Current.Session("filter_expression") = Nothing

            WebChartControl2.DataBind()
            txtChartText.SelectedIndex = 0

        End If
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

        Dim userName As String = txtUserName.Text
        Dim password As String = txtPassword.Text

        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)


        If login IsNot Nothing Then
            SessionHelper.UserLogin = login
            Response.Cookies.Add(New HttpCookie(COOKIE_NAME) With {.Value = login.EmailAddress, .Expires = Now.AddYears(14)})

            Dim redirectStr As String = Request.QueryString(Constants.queryString_redirect)
            If Not String.IsNullOrEmpty(redirectStr) Then
                Response.Redirect(redirectStr)
            Else

                Response.Redirect("/AdminDefault.aspx")
            End If
        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If HttpContext.Current.Session("gridBound") IsNot Nothing AndAlso CBool(HttpContext.Current.Session("gridBound")) Then
            ASPxGridView2.DataBind()
        End If
    End Sub
  
    

    Protected Sub WebChartControl2_DataBinding(sender As Object, e As EventArgs)


        Dim filterExpression As String = If(HttpContext.Current.Session("filter_expression") IsNot Nothing, HttpContext.Current.Session("filter_expression"), String.Empty)

        Dim dt = ApplicationCache.usp_AmalgamatedRAGChart_v3_DataTable

        If Not String.IsNullOrEmpty(filterExpression) Then dt = dt.Select(filterExpression).CopyToDataTable

        TryCast(sender, WebChartControl).DataSource = dt
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function GetPieData(ByVal ty As String, ByVal fe As String)


        Dim ds = ApplicationCache.usp_AmalgamatedRAGChart_v3_DataTable

        Dim repdate As String = DataObjects.CompanyReport.GetLatestReportDate.ToString("MMM yyyy")

        Dim artnrGroups = If(ds IsNot Nothing, (From a In ds Where a("ReportDateMonthYear").Equals(repdate) Group By PropertyName = a(ty)
        Into PropertyValue = Sum(CType(a("CurrentAssetValue"), Decimal))
                                Where (PropertyName IsNot Nothing Or Not IsDBNull(PropertyName)) And
                                        ((PropertyValue <> Nothing Or Not IsDBNull(PropertyValue)) And PropertyValue > 0)
                           Select New With {
                               .Name = IIf(PropertyName = "Error", "Other", PropertyName), .Value = PropertyValue
                           }).ToList, Nothing)

        Dim drilldown = New Dictionary(Of String, Object)

        Dim retval = New With {
            .main = artnrGroups,
            .sub = drilldown.ToList
            }

        Return retval

    End Function
    Private Function getFilterExpression()
        Dim Fex = New List(Of String)

        If BIToolParams.Contains("FilterExpression_Drilldown") Then
            Dim fe_drilldown = BIToolParams.Get("FilterExpression_Drilldown")
            If Not String.IsNullOrEmpty(fe_drilldown.ToString().Trim) Then Fex.Add(fe_drilldown)
        End If
        If BIToolParams.Contains("FilterExpression") Then
            Dim fe = BIToolParams.Get("FilterExpression")
            If Not String.IsNullOrEmpty(fe.ToString().Trim) Then Fex.Add(fe)
        End If
        Return If(Fex.Count > 0, String.Join(" And ", Fex.ToArray), "")
    End Function
    Protected Sub WebChartControl_CustomCallback(sender As Object, e As CustomCallbackEventArgs)

        Dim chrt As WebChartControl = DirectCast(sender, WebChartControl)

        HttpContext.Current.Session("filter_expression") = getFilterExpression()

        CreateSeriesFor(chrt, txtChartText.Value)

        chrt.DataBind()
    End Sub

    Private Sub CreateSeriesFor(chrt As WebChartControl, name As String)
        If name = "Utilisation" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedUtilisation])"
            chrt.Series(0).ValueDataMembersSerializable = "RedUtilisation"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberUtilisation])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberUtilisation"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenUtilisation])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenUtilisation"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberUtilisation])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberUtilisation"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedUtilisation])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedUtilisation"
        ElseIf name = "OperationalProductivity" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedOperationalProductivity])"
            chrt.Series(0).ValueDataMembersSerializable = "RedOperationalProductivity"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberOperationalProductivity])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberOperationalProductivity"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenOperationalProductivity])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenOperationalProductivity"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberOperationalProductivity])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberOperationalProductivity"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedOperationalProductivity])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedOperationalProductivity"
        ElseIf name = "FuelConsumption" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedFuelConsumption])"
            chrt.Series(0).ValueDataMembersSerializable = "RedFuelConsumption"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberFuelConsumption])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberFuelConsumption"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenFuelConsumption])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenFuelConsumption"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberFuelConsumption])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberFuelConsumption"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedFuelConsumption])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedFuelConsumption"
        ElseIf name = "OptimumReplacement" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "90 Days + Overdue"
            chrt.Series(0).SummaryFunction = "SUM([RedOptimumReplacement])"
            chrt.Series(0).ValueDataMembersSerializable = "RedOptimumReplacement"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< 90 Days Overdue"
            chrt.Series(1).SummaryFunction = "SUM([AmberOptimumReplacement])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberOptimumReplacement"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Current"
            chrt.Series(2).SummaryFunction = "SUM([GreenOptimumReplacement])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenOptimumReplacement"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "ServiceDue" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "Overdue 45+ Days"
            chrt.Series(0).SummaryFunction = "SUM([RedServiceDue])"
            chrt.Series(0).ValueDataMembersSerializable = "RedServiceDue"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "Overdue < 45 and > 14"
            chrt.Series(1).SummaryFunction = "SUM([AmberServiceDue])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberServiceDue"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Overdue < 14 Days"
            chrt.Series(2).SummaryFunction = "SUM([GreenServiceDue])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenServiceDue"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "WOLCostVariation" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -10% of Budget"
            chrt.Series(0).SummaryFunction = "SUM([RedWOLCostVariation])"
            chrt.Series(0).ValueDataMembersSerializable = "RedWOLCostVariation"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -5% of Budget"
            chrt.Series(1).SummaryFunction = "SUM([AmberWOLCostVariation])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberWOLCostVariation"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 5% of Budget"
            chrt.Series(2).SummaryFunction = "SUM([GreenWOLCostVariation])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenWOLCostVariation"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 5% of Budget"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberWOLCostVariation])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberWOLCostVariation"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 10% of Budget"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedWOLCostVariation])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedWOLCostVariation"
        ElseIf name = "MaintenanceRatio" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "Overdue 45+ Days"
            chrt.Series(0).SummaryFunction = "SUM([RedMaintenanceRatio])"
            chrt.Series(0).ValueDataMembersSerializable = "RedMaintenanceRatio"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "Overdue < 45 and > 14"
            chrt.Series(1).SummaryFunction = "SUM([AmberMaintenanceRatio])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberMaintenanceRatio"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Overdue < 14 Days"
            chrt.Series(2).SummaryFunction = "SUM([GreenMaintenanceRatio])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenMaintenanceRatio"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        End If
    End Sub

    Protected Sub ASPxGridView2_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs)
        If e.Parameters = "open" Then
            'ASPxGridView2.Columns.Clear()
            'ASPxGridView2.AutoGenerateColumns = False
            'ViewState("needBind") = True
            Dim header = BIToolParams.Get("header").ToString().Replace(" ", "")
            Dim arg = BIToolParams.Get("arg").ToString()
            Dim series = BIToolParams.Get("series").ToString()
            Dim chartname = txtChartText.Value
            'Dim groups = Business.DataObjects.RagChart.GetForCompanyAsDataTable(txtCompany.Text)


            Try
                Dim IdList = (From a In ApplicationCache.usp_AmalgamatedRAGChart_v3_DataTable _
                            Where a(header).ToString() = series.ToUpper() And a("ReportDateMonthYear") = arg _
                                   Select a("AssetId")).ToList


                Dim datasource = Business.DataObjects.RiskComposite.GetFromDB().Where(Function(x) IdList.Contains(x.PK.ToString()))

                'Sort 
                If chartname = "Utilisation" Then
                    datasource = datasource.OrderBy(Function(x) x.KmsHrsVar)
                ElseIf chartname = "OperationalProductivity" Then
                    datasource = datasource.OrderBy(Function(x) x.KmsHrsVar - x.IncomeUnitsVar)
                ElseIf chartname = "FuelConsumption" Then
                    datasource = datasource.OrderBy(Function(x) x.Deviation)
                ElseIf chartname = "OptimumReplacement" Then
                    datasource = datasource.OrderBy(Function(x) x.OptimumReplacementDate)
                ElseIf chartname = "ServiceDue" Then
                    datasource = datasource.OrderBy(Function(x) x.NextStandardServiceDue)
                ElseIf chartname = "WOLCostVariation" Then
                    datasource = datasource.OrderBy(Function(x) x.CostVariation)
                ElseIf chartname = "MaintenanceRatio" Then
                    datasource = datasource.OrderBy(Function(x) x.DowntimeVariation)
                End If
                HttpContext.Current.Session("gridBound") = True
                HttpContext.Current.Session("popsource") = datasource
                ASPxGridView2.Columns.Clear()
                ASPxGridView2.DataBind()
                Dim l = Business.DataObjects.RiskComposite.GetColumns(header)
                For Each col As GridViewDataColumn In ASPxGridView2.Columns
                    col.Visible = l.Contains(col.FieldName)
                Next
            Catch ex As Exception

            End Try
        End If

    End Sub

    Protected Sub ASPxGridView2_DataBinding(sender As Object, e As EventArgs)
        TryCast(sender, ASPxGridView).DataSource = HttpContext.Current.Session("popsource")
        'TryCast(sender, ASPxGridView).KeyFieldName = "PK"
        'AddColumns()
        'End If
    End Sub
    Private Sub AddColumns()
        ASPxGridView2.Columns.Clear()

        AddTextColumn("PK")
        AddTextColumn("PlantNumber")
        AddTextColumn("ParentPlant")
        AddTextColumn("FleetNumber")
        AddTextColumn("AssetNumber")
        AddTextColumn("Category")
        AddTextColumn("Group")
        AddTextColumn("Type")
        AddTextColumn("Make")
        AddTextColumn("Model")
        AddTextColumn("ModelYear")
        AddTextColumn("RegoNo")
        AddTextColumn("RegoDate")
        AddTextColumn("RegoState")
        AddTextColumn("MeterType")
        AddTextColumn("CurrentAsset")
        AddTextColumn("PlantImage")
        AddTextColumn("VINNumber")
        AddTextColumn("EngineNumber")
        AddTextColumn("ChassisNumber")
        AddTextColumn("BodyNumber")
        AddTextColumn("BuildDate")
        AddTextColumn("PreviousRego")

        'ASPxGridView2.KeyFieldName = "PK"
        'ASPxGridView2.Columns("PK").Visible = True
    End Sub

    Private Sub AddTextColumn(ByVal fieldName As String)
        Dim c As New GridViewDataTextColumn()
        c.FieldName = fieldName
        ASPxGridView2.Columns.Add(c)
    End Sub



    Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs)
        If e.Parameter = "cc" Then
            'createmainds()
        End If
    End Sub


    Protected Sub cbxGRGroup_Callback(sender As Object, e As CallbackEventArgsBase)
        Dim cbx As ASPxComboBox = DirectCast(sender, ASPxComboBox)
        cbx.Items.Add("[None]")
        'If cbx.ID = "cbxGRGroup" Then
        '    cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("group"))
        'ElseIf cbx.ID = "cbxTyType" Then
        '    cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("type"))
        'ElseIf cbx.ID = "cbxMkMake" Then
        '    cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("make"))
        'ElseIf cbx.ID = "cbxMdModel" Then
        '    cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("model"))
        'End If
        cbx.Text = "[None]"
    End Sub

    Protected Sub ASPxButton3_Click(sender As Object, e As EventArgs)
        gridExport.WriteCsvToResponse(txtChartText.Text, New CsvExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Private Function DVal(a As Object)
        Return If(IsDBNull(a), 0, a)
    End Function

End Class