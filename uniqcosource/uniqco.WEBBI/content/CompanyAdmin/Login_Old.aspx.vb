﻿Imports uniqco.Business



Public Class Login_Old
    Inherits System.Web.UI.Page

    Public Const COOKIE_NAME As String = "uniqco"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim authCookie As HttpCookie = Request.Cookies(COOKIE_NAME)

        If authCookie IsNot Nothing Then

            Dim email As String = authCookie.Value
            Dim login As DataObjects.Login = DataObjects.Login.GetAllForEmailAddress(email)

            If login IsNot Nothing Then
                SessionHelper.UserLogin = login
                Response.Redirect("/AdminDefault.aspx")

            End If
        Else
        End If


    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

        Dim userName As String = txtUserName.Text
        Dim password As String = txtPassword.Text

        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)


        If login IsNot Nothing Then
            SessionHelper.UserLogin = login
            Response.Cookies.Add(New HttpCookie(COOKIE_NAME) With {.Value = login.EmailAddress, .Expires = Now.AddYears(14)})

            Dim redirectStr As String = Request.QueryString(Constants.queryString_redirect)
            If Not String.IsNullOrEmpty(redirectStr) Then
                Response.Redirect(redirectStr)
            Else

                Response.Redirect("/AdminDefault.aspx")
            End If
        End If
        'there will be some sort of request here to determine if the login is correct
        'by using bloomtoolsREST API to determine is the username / password is correct

        'temporarily, we will test with what we have in the database (for now only)




    End Sub






End Class