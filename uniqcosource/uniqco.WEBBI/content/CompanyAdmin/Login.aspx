﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Login.aspx.vb" Inherits="uniqco.WEBBI.Login" %>
<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.1.Web, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.1.Web, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web.Designer" TagPrefix="dxchartdesigner" %>



<%@ Register TagPrefix="cc1" Namespace="DevExpress.XtraCharts" Assembly="DevExpress.XtraCharts.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/content/js/jquery-3.0.0.min.js"></script>
      <script src="/content/js/highcharts.js"></script>
        <script src="/content/js/drilldown.js"></script>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="/content/js/login_bifrontpage.js"></script>
    
      <style>
           .hiddenCalendar .dxeCalendar_SoftOrange
        {
            visibility: hidden !important;
        }
          .hiddenCalendar .dxpc-mainDiv
        {
            height: 0px !important;
        }
          .linklookalike {
              background: none !important;
              border: none;
              padding: 0 !important;
              font: inherit;
              /*border is optional*/
              text-decoration:underline;
              border:none;
              cursor: pointer;
          }
          .selecthead input {
            font-size: 18px !important;
        }
          
              .linklookalike :hover {
                  color:rgb(255, 69, 0);
              }
         #FilterText_edi, #f_editor:focus {
         outline: none;
         }
          @media screen and (min-width: 1600px) {
    #chart-container {
        display:inline-flex;
    }
}
         
        #container text {text-decoration: none !important;}
        
       /*  .reverse-ellipsis {
         
          text-overflow: clip;
          position: relative;
          background-color: #FFF;
        }

        .reverse-ellipsis:before {
          content: '\02026';
          position: absolute;
          z-index: 1;
          left: -1em;
          background-color: inherit;
          padding-left: 1em;
          margin-left: 0.5em;
        }

        .reverse-ellipsis span {
          min-width: 100%;
          position: relative;
          display: inline-block;
          float: right;
          overflow: visible;
          background-color: inherit;
          text-indent: 25px;
        }

        .reverse-ellipsis span:before {
          content: '';
          position: absolute;
          display: inline-block;
          width: 1em;
          height: 1em;
          background-color: inherit;
          z-index: 200;
          left: -.5em;
        } */
        .highcharts-credits{
            display:none;
        }

      </style>
    
        <style type="text/css">
            .loginButton {
                float: right;
                margin-top:10px;
            }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width:850px;margin:auto">
        <dx:ASPxSplitter ID="ASPxSplitter1" runat="server" Theme="SoftOrange" ShowSeparatorImage="false" Height="700px" Width="850px"  AllowResize="false" ClientInstanceName="MainSplitter">
        <Styles>
            <Pane>
                <Paddings Padding="0px" />
            </Pane>
        </Styles>
        <Panes>
            <dx:SplitterPane Name="MainContainer"  PaneStyle-Border-BorderStyle="None" ShowCollapseBackwardButton="False" >
                   <Panes>
                    <dx:SplitterPane Name="LoginAndPie" Size="350px" >
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server">
                                <dx:ASPxHiddenField ID="BIToolParams" ClientInstanceName="BIToolParams" runat="server"></dx:ASPxHiddenField> 
                                <div style="display:inline-flex">           
                                  <div style="padding:0;margin-top:10px;width:350px;height:150px;font-family: arial, helvetica, sans-serif;font-size:12px;">
                     
                                                  <div style="margin:auto;width:265px;padding-bottom:25px;padding-top:70px">
            
                            <dx:ASPxRoundPanel ID="panelLoginDetails" runat="server" EnableTheming="True" HeaderText="Please enter login details:" Theme="SoftOrange">
                                <PanelCollection>
                                    <dx:PanelContent runat="server">
                                        <table>
                           
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Email:"></dx:ASPxLabel>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtUserName" AutoCompleteType="Email" runat="server" Width="170px"></dx:ASPxTextBox>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Password:"></dx:ASPxLabel>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtPassword" Password="true" runat="server" Width="170px"></dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <dx:ASPxButton ID="btnLogin" CssClass="loginButton" runat="server" Width="100px" Text="Login" Theme="SoftOrange"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </div>
                                  </div>
                                
                                  <div style="padding:0;width:500px; ">
                                      <center> <p style="font-size: 18px">Vehicle Group</p></center>
                                     <div id="container" style="min-width: 350px; height: 250px; margin: 0 auto;"></div>
                                      <div style="top: -325px; font-family: arial, helvetica, sans-serif; font-size: 15px; color:#afafaf; position: relative;z-index: 500; width: 500px;" class=" reverse-ellipsis"><span id="pieHistory" ></span></div>
                      
                                      <input type="button" style="float:right;top: -215px; display:none; right: 25px;position: relative;z-index: 500;height: 35px;" id="btnpieback" value="◁" />
                                  </div>

                                </div>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                    <dx:SplitterPane Name="RagCharts" ShowCollapseForwardButton="False">
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server">
                                <div style="padding:0;height:350px;width:775px">
                   <div  style="margin-bottom:10px">
                       <dx:ASPxComboBox ID="txtChartText" ClientInstanceName="txtChartText" CssClass="selecthead" ItemStyle-Font-Size="18px" Theme="Metropolis" runat="server" Width="205px" ValueType="System.String">
                                 <Items>
                                     <dx:ListEditItem Text="Utilisation" Value ="Utilisation" />
                                     <dx:ListEditItem Text="Productivity" Value ="OperationalProductivity" />
                                     <dx:ListEditItem Text="Fuel Consumption" Value ="FuelConsumption" />
                                     <dx:ListEditItem Text="Replacement" Value ="OptimumReplacement" />
                                     <dx:ListEditItem Text="Service Due" Value ="ServiceDue" />
                                     <dx:ListEditItem Text="Cost Vs Recovery" Value ="WOLCostVariation" />
                                     <dx:ListEditItem Text="Maintenance Ratio" Value ="MaintenanceRatio" />
                                 </Items>
                                  <ClientSideEvents ValueChanged="function(s, e) {
                                        performchartcallback();
                                    }"  />
                              </dx:ASPxComboBox>

                   </div> 
                                        <dxchartsui:WebChartControl Theme="SoftOrange" ID="WebChartControl2"
                                            runat="server"
                                            CrosshairEnabled="False"
                                            Height="275px"
                                            ClientInstanceName="barchartUT"
                                            Width="775px"
                                            OnCustomCallback="WebChartControl_CustomCallback" 
                                            OnDataBinding="WebChartControl2_DataBinding" 
                                            ToolTipEnabled="True">
                                            
                                            <DiagramSerializable>
                                            <cc1:XYDiagram>
                                                <AxisX VisibleInPanesSerializable="-1">
                                                    <tickmarks minorvisible="False" visible="False" />
                                                </AxisX>
                                                <AxisY VisibleInPanesSerializable="-1">
                                                    <tickmarks minorvisible="False" visible="False" />
                                                    <label textpattern="{V:0%}">
                                                    </label>
                                                    <gridlines visible="False">
                                                    </gridlines>
                                                </AxisY>
                                            </cc1:XYDiagram>
                                            </DiagramSerializable>
                                            <Legend Direction="LeftToRight" AlignmentHorizontal="Center" AlignmentVertical="BottomOutside"></Legend>
                                            <SeriesSerializable>
                                            <cc1:Series Name="Red" LegendText="< -30%" ArgumentDataMember="ReportDateMonthYear" SummaryFunction="SUM([RedUtilisation])" ValueDataMembersSerializable="RedUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Red" >
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="Amber"  LegendText="< -20%" ArgumentDataMember="ReportDateMonthYear" SummaryFunction="SUM([AmberUtilisation])" ValueDataMembersSerializable="AmberUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Orange" >
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="Green" LegendText="+/- 20%" ArgumentDataMember="ReportDateMonthYear"  SummaryFunction="SUM([GreenUtilisation])" ValueDataMembersSerializable="GreenUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="LimeGreen" >
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="FadedAmber" LegendText="> 20%" ArgumentDataMember="ReportDateMonthYear"  SummaryFunction="SUM([FadedAmberUtilisation])" ValueDataMembersSerializable="FadedAmberUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Moccasin" >
                                                        <border color="Orange" />
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            <cc1:Series Name="FadedRed" LegendText="> 30%" ArgumentDataMember="ReportDateMonthYear"  SummaryFunction="SUM([FadedRedUtilisation])" ValueDataMembersSerializable="FadedRedUtilisation" LabelsVisibility="False">
                                                <viewserializable>
                                                    <cc1:FullStackedBarSeriesView Color="Salmon" >
                                                        <border color="Red" />
                                                        <fillstyle fillmode="Solid">
                                                        </fillstyle>
                                                    </cc1:FullStackedBarSeriesView>
                                                </viewserializable>
                                                <labelserializable>
                                                    <cc1:FullStackedBarSeriesLabel TextPattern="{V:F1}">
                                                    </cc1:FullStackedBarSeriesLabel>
                                                </labelserializable>
                                            </cc1:Series>
                                            </SeriesSerializable>
                                            <SeriesTemplate LabelsVisibility="False" />
                                            <ClientSideEvents ObjectSelected="function(s, e) {
                                            openChartPopup(e.additionalHitObject.argument,e.additionalHitObject.series.name);
                                            }" EndCallback="function(s, e) {
                                            $('.dxchartsuiTooltip_SoftOrange').remove();
                                            }" />
                                        </dxchartsui:WebChartControl>
                     <dx:ASPxPopupControl CloseAction="OuterMouseClick" ClientInstanceName="ASPxPopupChartControl" Width="1200" Height="550px"
                        ID="ASPxPopupControl1"
                        ShowFooter="false" 
                        runat="server" EnableViewState="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" EnableHierarchyRecreation="True" 
                        PopupAnimationType="None" CloseAnimationType="None" AllowDragging="true" DragElement="Header" AllowResize ="false" ResizingMode="Live" ShowCloseButton="true" >
                        <ContentCollection>
                           <dx:PopupControlContentControl runat="server">
                              <asp:Panel ID="Panel2" runat="server">
                                 <%--<table>
                                    <tr>
                                        <td>Argument: </td>
                                        <td><span id="bcArg"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Series: </td>
                                        <td><span id="bcSer"></span></td>
                                    </tr>
                                    </table>--%>

                                  <dx:ASPxButton ID="ASPxButton3" Cursor="pointer" CssClass="linklookalike" runat="server" Text="Export CSV" BackColor="Transparent" AutoPostBack="False" > <ClientSideEvents Click="function(s,e){
                                      alert('You must Login first to get comprehensive data analysis');
                                      }" /></dx:ASPxButton>
                                 <dx:ASPxGridView SettingsText-EmptyDataRow="You must Login first to get comprehensive data analysis" ID="ASPxGridView2" SettingsBehavior-AllowSort="true" SettingsBehavior-AllowGroup="true" runat="server" ClientInstanceName ="clb"
                                    OnCustomCallback="ASPxGridView2_CustomCallback" OnDataBinding="ASPxGridView2_DataBinding" AutoGenerateColumns="True" EnableTheming="True"
                                    Theme="SoftOrange" style="width:100%;" SettingsBehavior-AutoExpandAllGroups="true" KeyFieldName="ID" >
                                    <%--EnableViewState="true" EnableRowsCache="false"--%> 
                                    <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" />
                                    <SettingsPager PageSize="10">
                                       <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                 </dx:ASPxGridView>
                                  <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView2"></dx:ASPxGridViewExporter>
                              </asp:Panel>
                           </dx:PopupControlContentControl>
                        </ContentCollection>
                     </dx:ASPxPopupControl>
                  </div>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>
        </Panes>
    </dx:ASPxSplitter>
</div>
</asp:Content>
