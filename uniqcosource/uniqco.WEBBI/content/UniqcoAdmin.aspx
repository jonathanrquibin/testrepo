﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="UniqcoAdmin.aspx.vb" Inherits="uniqco.WEBBI.UniqcoAdmin" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
        <link href="/content/css/app.css" rel="stylesheet" />
        <link href="/content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script src="/content/js/jquery-3.0.0.min.js"></script>
    <script src="/content/js/iframeresize.js"></script>
    <script>
        
        $(document).ready(function () {
            $(".imgAdminlogo").on('click', function () {
                $('#iframeMain').attr('src', '/AdminDefault.aspx');
            });
        });
    </script>
    <style>
        .admin-navi {
            text-decoration:none; 
            color: #002060;
            font-size:15px;
            cursor:pointer;
        }
        .admin-navi:hover {
            text-decoration:none;
            color: rgb(255, 69, 0);
        }
        @media screen and (min-width: 815px) {
            #mainnavi {display:inline-flex;}
            #hypAdmin { float: right;}
            #hypDataEntry { float: right;}
            #hypAuditPage { float: right;}
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <div class="admin-header" style="margin:10px;width:100%;">
        <div class="logo-inner" id="mainnavi" >
            <div style="padding:0;">
            <a id="dnn_dnnLogo_hypLogo" class="imgAdminlogo">
                <dx:ASPxBinaryImage ClientInstanceName="dnn_dnnLogo_imgLogo" ID="dnn_dnnLogo_imgLogo" runat="server" Height="70px" style=" margin: 1px 25px;cursor:pointer;border:none;">
                                     </dx:ASPxBinaryImage></a>
                </div>
            <div style="padding:0;">
            <a id="hypAdmin" class="admin-navi" style=" margin: 25px;" href="javascript:void(0)" onclick="$('#iframeMain').attr('src','/UniqcoAdminMain.aspx');extendIframe($('#iframeMain')[0])">UNIQCO Admin Page</a>
          
                </div>
            <div style="padding:0;">
             <a id="hypDataEntry" class="admin-navi" style=" margin: 25px;" href="javascript:void(0)" onclick="$('#iframeMain').attr('src','/ManualDataEntry.aspx');extendIframe($('#iframeMain')[0])">Data Entry</a>
          
                </div>
           
            <div style="padding:0;">
             <a id="hypAuditPage" class="admin-navi" style=" margin: 25px;" href="javascript:void(0)" onclick="$('#iframeMain').attr('src','/AuditPage.aspx');extendIframe($('#iframeMain')[0])">Audit Page</a>
          
                </div>
                </div>
     <div class="menu-inner" style="float:right;display:inline-block;margin-right:10px">
         <div class="nav nav-pills">
         <dx:ASPxImage ID="ASPxImage1" Height="50px" Width="50px" runat="server" ShowLoadingImage="true" style="float:left;border:none;" ImageUrl="/content/images/profile-pic-blank.png"></dx:ASPxImage>
         <div style=" float: right; margin: 13px; ">

         <dx:ASPxHyperLink ID="HypUsername" runat="server" Text="Name / Login" NavigateUrl="javascript:void(0);" style=" color: #002060;font-size:15px;cursor:pointer;" >
             <ClientSideEvents Click="function(s, e) {
                                    $('#iframeMain').attr('src','/CompanyAdminMain.aspx');
                                    extendIframe($('#iframeMain')[0]);
                                }"  />   
         </dx:ASPxHyperLink>&nbsp; / &nbsp;
             <dx:ASPxHyperLink ID="HypLnkLogout" runat="server" Text="Logout" NavigateUrl="javascript:void(0);" style=" color: #002060;font-size:15px;cursor:pointer;" >
             <ClientSideEvents Click="function(s, e) {
	                                    clb.PerformCallback('Logout');
                                    }"  />   
         </dx:ASPxHyperLink>
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="clb" 
        oncallback="ASPxCallback1_Callback">
    </dx:ASPxCallback>
         <%--<dx:ASPxButton runat="server" ID="HypLogout" Text="Logout" Theme="SoftOrange"></dx:ASPxButton>--%>
                </div>
                </div>
     </div>
     </div>

     <iframe id ="iframeMain" style="width:100%;" src="/AdminDefault.aspx" scrolling="no" onload="extendIframe(this)"></iframe>



</asp:Content>
