﻿Imports uniqco.Business
Public Class CompanyAdmin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If SessionHelper.UserLogin IsNot Nothing Then
            Dim UserCompany As DataObjects.Company = DataObjects.Company.GetFromUser(SessionHelper.UserLogin)
            HypUsername.Text = SessionHelper.UserLogin.UserName
            dnn_dnnLogo_imgLogo.Value = UserCompany.CompanyLogo
        End If
    End Sub
    Protected Sub ASPxCallback1_Callback(source As Object, e As DevExpress.Web.CallbackEventArgs) Handles ASPxCallback1.Callback
        If e.Parameter = "Logout" Then
            Dim authCookie As HttpCookie = Request.Cookies(Login.COOKIE_NAME)

            If authCookie IsNot Nothing Then
                'authCookie.Expires = Now
                Response.Cookies(Login.COOKIE_NAME).Expires = Now
                SessionHelper.UserLogin = Nothing
                DevExpress.Web.ASPxWebControl.RedirectOnCallback("/content/companyadmin/login.aspx")
            End If
        End If
    End Sub
End Class