﻿
var FilterColumns = [
     "VehicleType",
     "Category",
     "Group",
     "Type",
     "Make",
     "Model",
     "ModelYear",
     "RegoState",
     "MeterType",
     "Region",
     "BusinessUnit",
     "Department",
     "Location",
     "OperatorDriver",
     "UsageType",
     "Supervisor",
     "PurchaseOrLease"
];
var FilterOperator = [
     "Equals", //=
     "Does not Equal",//<>
     "Less Than", //<
     "Less Than or Equal", //<=
     "Greater Than", //>
     "Greater Than Equal"//>=
     
     
];
var FilterAndOr = [
     "And",
     "Or"
];
var PieSource = [];

var FilterText = {
    get d() {
        
        var fe = ""
        if (BIToolParams.Get("FilterExpression")) fe = BIToolParams.Get("FilterExpression");

        if (BIToolParams.Get("FilterExpression_Drilldown")) {
            if (fe) fe += " And ";
            fe += BIToolParams.Get("FilterExpression_Drilldown");
        }
        return fe;
    }
};
var chart;
var performchartcallback = function () {
    barchartUT.PerformCallback();
};
var RemovedPieView = [];
var openChartPopup = function (arg, series) {
    
    ASPxPopupChartControl.SetHeaderText(txtChartText.GetText());
    //$("#bcArg").html(arg);
    //$("#bcSer").html(series);
    BIToolParams.Set('header', txtChartText.GetValue());
    BIToolParams.Set('arg', arg);
    BIToolParams.Set('series', series);
    //clb.PerformCallback('open');
    ASPxPopupChartControl.Show();
};
var CreateChart = function (sl, se) {
    
    var fex = FilterText.d;
    $.ajax({
        type: "POST",
        url: '/content/companyadmin/Login.aspx/GetPieData',
        dataType: "json",
        data: JSON.stringify({ ty: sl, fe: fex}),
        contentType: "application/json",
        crossDomain: true,
        success: function (data) {
            // Populate series
            var processed_json = [];
            var processed_dd_json = [];
            var piedata = data.d.main;
            var drilldata = data.d.sub;
            if (piedata) {
                for (i1 = 0; i1 < piedata.length; i1++) {
                    if (piedata[i1].Name) {
                        processed_json.push({
                            name: piedata[i1].Name,
                            y: piedata[i1].Value,
                            drilldown: piedata[i1].Name
                        });
                    }
                }
            }
            if (drilldata){
                for (i2 = 0; i2 < drilldata.length; i2++) {
                    if (drilldata[i2].Key) {
                        var drilldown_json = new Array();
                        var dd_content = drilldata[i2].Value;
                        var d_id = drilldata[i2].Key;
                        for (i3 = 0; i3 < dd_content.length; i3++) {
                            if (dd_content[i3].Name != null) {
                                drilldown_json.push([dd_content[i3].Name, dd_content[i3].Value]);
                            }
                        }
                        processed_dd_json.push({
                            name: 'Vehicle Group',
                            id: d_id,
                            data: drilldown_json
                        });
                    }
                }
            }
            
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },

                legend: {
                    align: 'right',
                    verticalAlign: 'top',
                    layout: 'vertical',
                    x: 0,
                    y: 0
                },
                title: {
                    text: ''
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false
                        },
                        point: {
                            events: {
                                click: function (event) {
                                    //debugger;
                                    BIToolParams.Set("FilterExpression_Drilldown", ((!this.sliced) ? "[VehicleType] = '" + this.drilldown + "'" : ""));
                                    performchartcallback();
                                }
                            }
                        }

                    }
                },

                tooltip: {
                    pointFormat: 'Current Asset Value: <b>${point.y:,.2f} ({point.percentage:.1f}%)</b>',
                    percentageDecimals: 1
                },
                series: [{
                    name: 'Vehicle Group',
                    data: processed_json
                }],
                drilldown: {
                    series: processed_dd_json
                }
            });
           
            
        }
    });
};

var doreset = true;
var resetcounter = 0;
var applyFilter = function () {
    var fe = [];
    var fe_string = "";
    var ut = [];
    var ut_string = "";
    var cat = [];
    var cat_string = "";
    //get for UsageType
    if (chkUTBusinessUse.GetChecked()) {
        ut.push("[UsageType] = '" + chkUTBusinessUse.GetText() + "'");
    }
    if (chkUTCommuterUse.GetChecked()) {
        ut.push("[UsageType] = '" + chkUTCommuterUse.GetText() + "'");
    }
    if (chkUTPrivateUse.GetChecked()) {
        ut.push("[UsageType] = '" + chkUTPrivateUse.GetText() + "'");
    }
    ut_string = ut.join(" or ");
    if (ut_string)
        fe.push('('+ ut_string+')');


    //get for cat
    //if (chkCatMajorPlant.GetChecked()) {
    //    cat.push("[Category] = 'Major Plant > $10K'");
    //}
    //if (chkCatMinorPlant.GetChecked()) {
    //    cat.push("[Category] = 'Minor Plant < $10K'");
    //}
    //if (chkCatOtherAssets.GetChecked()) {
    //    cat.push("[Category] = '" + chkCatOtherAssets.GetText() + "'");
    //}
    //if (chkCatHire.GetChecked()) {
    //    cat.push("[Category] = '" + chkCatHire.GetText() + "'");
    //}
    //if (chkCatLightFleet.GetChecked()) {
    //    cat.push("[Category] = '" + chkCatLightFleet.GetText() + "'");
    //}
    //cat_string = cat.join(" or ");
    //if (cat_string)
    //    fe.push('(' + cat_string + ')');

    if (cbxTyType.GetText()) {
        if (cbxTyType.GetText() != '[None]') {
            fe.push("[Type] = '" + cbxTyType.GetText() + "'");
        }
    }
    if (cbxMkMake.GetText()) {
        if (cbxMkMake.GetText() != '[None]') {
            fe.push("[Make] = '" + cbxMkMake.GetText() + "'");
        }
    }
    if (cbxMdModel.GetText()) {
        if (cbxMdModel.GetText() != '[None]') {
            fe.push("[Model] = '" + cbxMdModel.GetText() + "'");
        }
    }
    if (cbxMdModelYear.GetText()) {
        fe.push("[ModelYear] = '" + cbxMdModelYear.GetText() + "'");
    }
    fe_string = fe.join(" And ");
    BIToolParams.Set('FilterExpression', fe_string);

}
var tcounter = 0;
var resetFilterForm = function (e) {
    if (e == 'load') {
        tcounter = 0;
        if ((!cbxTyType.GetText()) || cbxTyType.GetText() == "[None]") {
            cbxTyType.PerformCallback();
            tcounter++;
        }
        if ((!cbxMkMake.GetText()) || cbxMkMake.GetText() == "[None]") {
            cbxMkMake.PerformCallback();
            tcounter++;
        }
        if ((!cbxMdModel.GetText()) || cbxMdModel.GetText() == "[None]") {
            cbxMdModel.PerformCallback();
            tcounter++;
        }
        showresetloading(true);
    }
    else if (e == 'clear') {
        chkUTBusinessUse.SetChecked(false);
        chkUTCommuterUse.SetChecked(false);
        chkUTPrivateUse.SetChecked(false);

        cbxTyType.SetText('[None]');
        cbxMkMake.SetText('[None]');
        cbxMdModel.SetText('[None]');
        cbxMdModelYear.SetText('');
    }
}
var showresetloading = function (param) {
    
    if (param) {
        loadingPanel.Show();
    }
    else {
        resetcounter++;
        if (resetcounter >= tcounter) { //no of callback performed
            loadingPanel.Hide();
        }
    }
}

//var pcv = function (s, e) {
    
//    CreateChart(txtPieView.GetValue());
//}
//var rdvc = function (s, e) {
//    performgridcallback();
//    CreateChart(txtPieView.GetValue());
//    if (PieSource.length <= 0) {
//        $('#btnpieback').css('display', 'none');
//    }
//}
$(document).ready(function () {
    
    CreateChart('VehicleType');

    //$('#btnpieback').click(function () {
    //    //drillup

        
    //    var x = RemovedPieView[RemovedPieView.length - 1];
    //    RemovedPieView.splice(RemovedPieView.length - 1, 1);

    //    var hist = "";
    //    $.each(RemovedPieView, function (i, s) {

    //        
    //        hist += (s.Text + "='" + s.ActualValue + "' ❭ ");
    //    });
    //    $('#pieHistory').html(hist);
    //    for (var i = 0; i < txtPieView.GetItemCount() ; i++) {
    //        var y = txtPieView.GetItem(i);
    //        if (x.Value == "VehicleType" && y.value == "Group") txtPieView.RemoveItem(y.index);
    //        if (x.Value == "BusinessUnit" && y.value == "Department") txtPieView.RemoveItem(y.index);
    //    }
    //    var y = txtPieView.GetItem(0);
    //    txtPieView.AddItem(x.Text,x.Value);
    //    txtPieView.SetValue(x.Value);
    //    PieSource.splice(PieSource.length - 1, 1);
    //    BIToolParams.Set("FilterExpression_Drilldown", PieSource.join(" And "))


    //    performchartcallback();

    //    if (PieSource.length > 0) {
    //        $('#btnpieback').css('display', 'block');
    //        $('#btnpieback').val('◁ Back to ' + RemovedPieView[RemovedPieView.length - 1].Text);
    //    }
    //    else {
    //        $('#btnpieback').css('display', 'none');
    //        $('#btnpieback').val('◁');
    //    }

    //    CreateChart(txtPieView.GetValue());
    //    //PieSource = [];
    //    //chart.setTitle({ text: txtPieView.GetText() });
    //    //FilterText = BIToolParams.Get("FilterExpression");
    //    //performchartcallback('fc');
    //    //performgridcallback('fc');
    //    //$('#btnpieback').css('display', 'none');
    //    //CreateChart(txtPieView.GetValue(), "");
    //});

});
