﻿$(document).ready(function () {
    $('#triangle-up').on('click mouseover', function () {
        $(this).css('border-color', 'transparent transparent #0000FF transparent').css('cursor', 'pointer');
    });
    $('#triangle-up').on('click mouseout', function () {
        $(this).css('border-color', 'transparent transparent #002e62 transparent');
    });

    $('#rectangle').on('click mouseover', function () {
        $(this).css('background', '#ff9900').css('cursor', 'pointer');
    });
    $('#rectangle').on('click mouseout', function () {
        $(this).css('background', '#ff4500');
    });

    $('#circle1').on('click mouseover', function () {
        $(this).css('background', '#ffff99').css('cursor', 'pointer');
    });
    $('#circle1').on('click mouseout', function () {
        $(this).css('background', '#FFFF00');
    });

    $('#circle2').on('click mouseover', function () {
        $(this).css('background', '#ffe066').css('cursor', 'pointer');
    });
    $('#circle2').on('click mouseout', function () {
        $(this).css('background', '#FFC000');
    });

    $('#circle3').on('click mouseover', function () {
        $(this).css('background', '#ccff33').css('cursor', 'pointer');
    });
    $('#circle3').on('click mouseout', function () {
        $(this).css('background', '#92d050');
    });

    $('#circle4').on('click mouseover', function () {
        $(this).css('background', '#4dffff').css('cursor', 'pointer');
    });
    $('#circle4').on('click mouseout', function () {
        $(this).css('background', '#00b0f0');
    });

    $('#rectangle1').on('click mouseover', function () {
        $(this).css('background', '#eee6ff').css('cursor', 'pointer');
    });
    $('#rectangle1').on('click mouseout', function () {
        $(this).css('background', '#bfbfbf');
    });
});