﻿Imports uniqco.Business

Public Class UniqcoAdminMain
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub odsLogin_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles odsLogin.Selecting
        'e.InputParameters["login"] = 0;
        e.InputParameters("login") = SessionHelper.UserLogin
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If SessionHelper.UserLogin Is Nothing Then

            Dim queryStr As String = Request.Url.Query

            queryStr = String.Format("~/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)

            Response.Redirect(queryStr)

        Else
            If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
                Me.MasterPageFile = "~/content/UniqcoAdminMaster.master"
            ElseIf SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                Me.MasterPageFile = "~/content/CompanyAdminMaster.master"
            Else
                Throw New Exception("ERROR! Failed to load Master Page.")
            End If
        End If
    End Sub

    Private Sub aspxGridViewGEneric_CustomErrorText(sender As Object, e As DevExpress.Web.ASPxGridViewCustomErrorTextEventArgs) _
                                Handles ASPxGridView1.CustomErrorText, ASPxGridView2.CustomErrorText, ASPxGridView3.CustomErrorText

        e.ErrorText = e.Exception.InnerException.Message

    End Sub
End Class