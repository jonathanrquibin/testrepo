﻿Imports DevExpress.XtraCharts
Imports DevExpress.XtraCharts.Web
Imports System.Globalization
Imports System.Data
Imports DevExpress.Web
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Export
Imports System.IO

Public Class BITool
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = New CultureInfo("en-GB")
        If Not IsCallback Then
            If SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                txtCompany.ClientVisible = False
                lblCompany.ClientVisible = False
                txtCompany.ReadOnly = True
            End If
            txtCompany.Value = SessionHelper.UserLogin.CompanyID
        End If

        If Not IsPostBack Then
            HttpContext.Current.Session("fe") = ""
            HttpContext.Current.Session("dt") = Nothing
            HttpContext.Current.Session("popSource") = Nothing
            HttpContext.Current.Session("gridBound") = Nothing
            HttpContext.Current.Session("BenchMark") = Nothing

            txtReportDate.Text = getlatestreport()
            createmainds()
            WebChartControl2.DataBind()
            ASPxGridView3.Columns("Value").Caption = SessionHelper.UserLogin.CompanyName
            txtPieView.SelectedIndex = txtPieView.Items.Count - 1
            txtChartText.SelectedIndex = 0
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If SessionHelper.UserLogin Is Nothing Then
            Dim queryStr As String = Request.Url.Query
            queryStr = String.Format("~/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)
            Response.Redirect(queryStr)
        Else
            If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
                Me.MasterPageFile = "~/content/UniqcoAdminMaster.master"
            ElseIf SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                Me.MasterPageFile = "~/content/CompanyAdminMaster.master"
            Else
                Throw New Exception("ERROR! Failed to load Master Page.")
            End If
        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If HttpContext.Current.Session("gridBound") IsNot Nothing AndAlso CBool(HttpContext.Current.Session("gridBound")) Then
            ASPxGridView2.DataBind()
        End If
    End Sub
    Private Function createmainds()
        Dim repdate = Convert.ToDateTime(txtReportDate.Value).AddMonths(1).AddDays(-1)

        HttpContext.Current.Session("dt") = ApplicationCache.GetRagChartDataTable(Guid.Parse(txtCompany.Value.ToString()), repdate)

        HttpContext.Current.Session("BenchMark") = Business.DataObjects.RiskComposite.GetForBenchMark(Guid.Parse(txtCompany.Value.ToString()))

        Return Nothing
    End Function
    Private Shared Function getchartds(fc As String) As DataTable
        Dim dtSource As DataTable
        Try
            dtSource = (CType(HttpContext.Current.Session("dt"), DataTable)).Select(fc).CopyToDataTable()
        Catch ex As Exception
            dtSource = Nothing
        End Try
        Return dtSource
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetPieData(ByVal com As Guid, ByVal ty As String, ByVal fe As String, ByVal rd As String)
        Dim ds As New DataTable
        
        If Not fe.Equals("") And Not rd.Equals("") Then
            ds = getchartds(fe & " and ReportDateMonthYear='" & rd & "'")
        ElseIf Not fe.Equals("") And rd.Equals("") Then
            ds = getchartds(fe)
        Else
            ds = getchartds("ReportDateMonthYear='" & rd & "'")
        End If

        Dim artnrGroups = If(ds IsNot Nothing, (From a In ds Group By PropertyName = a(ty)
                               Into PropertyValue = Sum(CType(a("CurrentAssetValue"), Decimal))
                               Where (PropertyName IsNot Nothing Or Not IsDBNull(PropertyName)) And
                                       ((PropertyValue <> Nothing Or Not IsDBNull(PropertyValue)) And PropertyValue > 0)
                          Select New With {
                              .Name = PropertyName, .Value = PropertyValue
                          }).ToList, Nothing)

        Dim drilldown = New Dictionary(Of String, Object)

        Dim retval = New With {
            .main = artnrGroups,
            .sub = drilldown.ToList
            }
        Return retval
    End Function
    Private Function getFilterExpression()
        Dim Fex = New List(Of String)

        If BIToolParams.Contains("FilterExpression_Drilldown") Then
            Dim fe_drilldown = BIToolParams.Get("FilterExpression_Drilldown")
            If Not String.IsNullOrEmpty(fe_drilldown.ToString().Trim) Then Fex.Add(fe_drilldown)
        End If
        If BIToolParams.Contains("FilterExpression") Then
            Dim fe = BIToolParams.Get("FilterExpression")
            If Not String.IsNullOrEmpty(fe.ToString().Trim) Then Fex.Add(fe)
        End If
        If PieLegendParams.Contains("FilterLegend") Then
            Dim feLegend = PieLegendParams.Get("FilterLegend")
            If Not String.IsNullOrEmpty(feLegend.ToString().Trim) Then Fex.Add(feLegend)
        End If
        If txtChartText.Text = "Productivity" Then
            Dim fe = "(HireCharge <> '0')"
            Fex.Add(fe)
        End If

        Return If(Fex.Count > 0, String.Join(" And ", Fex.ToArray), "")
    End Function
    Protected Sub WebChartControl_CustomCallback(sender As Object, e As CustomCallbackEventArgs)
        Dim chrt As WebChartControl = DirectCast(sender, WebChartControl)

        Dim TEST = getFilterExpression()

        HttpContext.Current.Session("fe") = getFilterExpression()
        CreateSeriesFor(chrt, txtChartText.Value)
        chrt.DataBind()
    End Sub
    Private Sub CreateSeriesFor(chrt As WebChartControl, name As String)
        If name = "Utilisation" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedUtilisation])"
            chrt.Series(0).ValueDataMembersSerializable = "RedUtilisation"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberUtilisation])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberUtilisation"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenUtilisation])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenUtilisation"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberUtilisation])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberUtilisation"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedUtilisation])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedUtilisation"
        ElseIf name = "OperationalProductivity" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedOperationalProductivity])"
            chrt.Series(0).ValueDataMembersSerializable = "RedOperationalProductivity"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberOperationalProductivity])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberOperationalProductivity"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenOperationalProductivity])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenOperationalProductivity"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberOperationalProductivity])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberOperationalProductivity"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedOperationalProductivity])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedOperationalProductivity"
        ElseIf name = "FuelConsumption" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedFuelConsumption])"
            chrt.Series(0).ValueDataMembersSerializable = "RedFuelConsumption"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberFuelConsumption])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberFuelConsumption"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenFuelConsumption])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenFuelConsumption"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberFuelConsumption])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberFuelConsumption"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedFuelConsumption])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedFuelConsumption"
        ElseIf name = "OptimumReplacement" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "90 Days + Overdue"
            chrt.Series(0).SummaryFunction = "SUM([RedOptimumReplacement])"
            chrt.Series(0).ValueDataMembersSerializable = "RedOptimumReplacement"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< 90 Days Overdue"
            chrt.Series(1).SummaryFunction = "SUM([AmberOptimumReplacement])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberOptimumReplacement"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Current"
            chrt.Series(2).SummaryFunction = "SUM([GreenOptimumReplacement])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenOptimumReplacement"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "ServiceDue" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "Overdue 45+ Days"
            chrt.Series(0).SummaryFunction = "SUM([RedServiceDue])"
            chrt.Series(0).ValueDataMembersSerializable = "RedServiceDue"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "Overdue < 45 and > 14"
            chrt.Series(1).SummaryFunction = "SUM([AmberServiceDue])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberServiceDue"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Overdue < 14 Days"
            chrt.Series(2).SummaryFunction = "SUM([GreenServiceDue])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenServiceDue"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "WOLCostVariation" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -10% of Budget"
            chrt.Series(0).SummaryFunction = "SUM([RedWOLCostVariation])"
            chrt.Series(0).ValueDataMembersSerializable = "RedWOLCostVariation"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -5% of Budget"
            chrt.Series(1).SummaryFunction = "SUM([AmberWOLCostVariation])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberWOLCostVariation"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 5% of Budget"
            chrt.Series(2).SummaryFunction = "SUM([GreenWOLCostVariation])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenWOLCostVariation"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 5% of Budget"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberWOLCostVariation])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberWOLCostVariation"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 10% of Budget"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedWOLCostVariation])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedWOLCostVariation"
        ElseIf name = "MaintenanceRatio" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "Unscheduled 70%+"
            chrt.Series(0).SummaryFunction = "SUM([RedMaintenanceRatio])"
            chrt.Series(0).ValueDataMembersSerializable = "RedMaintenanceRatio"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "Unscheduled < 70% and > 30%"
            chrt.Series(1).SummaryFunction = "SUM([AmberMaintenanceRatio])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberMaintenanceRatio"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Unscheduled < 30%"
            chrt.Series(2).SummaryFunction = "SUM([GreenMaintenanceRatio])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenMaintenanceRatio"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "AssetData" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = ""
            chrt.Series(0).SummaryFunction = "SUM([RedAssetDataRisk])"
            chrt.Series(0).ValueDataMembersSerializable = "RedAssetDataRisk"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = ""
            chrt.Series(1).SummaryFunction = "SUM([AmberAssetDataRisk])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberAssetDataRisk"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = ""
            chrt.Series(2).SummaryFunction = "SUM([GreenAssetDataRisk])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenAssetDataRisk"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        End If
    End Sub
    Protected Sub ASPxGridView2_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs)

        If e.Parameters = "open" Then

            Dim header = BIToolParams.Get("header").ToString().Replace(" ", "")
            Dim arg = BIToolParams.Get("arg").ToString()
            Dim series = BIToolParams.Get("series").ToString()
            Dim chartname = txtChartText.Value

            Try
                Dim IdList = (From a In getchartds(HttpContext.Current.Session("fe")) _
                            Where a(header).ToString() = series.ToUpper() And a("ReportDateMonthYear") = arg _
                                   Select a("AssetId")).ToList

                Dim datasource = Business.DataObjects.RiskComposite.GetFromDB().Where(Function(x) IdList.Contains(x.PK.ToString()))
                If series = "FadedAmber" Or series = "FadedRed" Then
                    If chartname = "Utilisation" Then
                        datasource = datasource.OrderByDescending(Function(x) x.KmsHrsVar)
                    ElseIf chartname = "OperationalProductivity" Then
                        datasource = datasource.OrderByDescending(Function(x) x.KmsHrsVar - x.IncomeUnitsVar)
                    ElseIf chartname = "FuelConsumption" Then
                        datasource = datasource.OrderByDescending(Function(x) x.Deviation)
                    ElseIf chartname = "OptimumReplacement" Then
                        datasource = datasource.OrderByDescending(Function(x) x.OptimumReplacementDate)
                    ElseIf chartname = "ServiceDue" Then
                        datasource = datasource.OrderByDescending(Function(x) x.NextStandardServiceDue)
                    ElseIf chartname = "WOLCostVariation" Then
                        datasource = datasource.OrderByDescending(Function(x) x.CostVariation)
                    ElseIf chartname = "MaintenanceRatio" Then
                        datasource = datasource.OrderByDescending(Function(x) x.DowntimeVariation)
                    End If
                Else
                    If chartname = "Utilisation" Then
                        datasource = datasource.OrderBy(Function(x) x.KmsHrsVar)
                    ElseIf chartname = "OperationalProductivity" Then
                        datasource = datasource.OrderBy(Function(x) x.KmsHrsVar - x.IncomeUnitsVar)
                    ElseIf chartname = "FuelConsumption" Then
                        datasource = datasource.OrderBy(Function(x) x.Deviation)
                    ElseIf chartname = "OptimumReplacement" Then
                        datasource = datasource.OrderBy(Function(x) x.OptimumReplacementDate)
                    ElseIf chartname = "ServiceDue" Then
                        datasource = datasource.OrderBy(Function(x) x.NextStandardServiceDue)
                    ElseIf chartname = "WOLCostVariation" Then
                        datasource = datasource.OrderBy(Function(x) x.CostVariation)
                    ElseIf chartname = "MaintenanceRatio" Then
                        datasource = datasource.OrderBy(Function(x) x.DowntimeVariation)
                    ElseIf chartname = "AssetData" Then
                        datasource = datasource.OrderBy(Function(x) x.KmsHrsVar)
                    End If
                End If

                HttpContext.Current.Session("gridBound") = True
                HttpContext.Current.Session("popsource") = datasource
                ASPxGridView2.Columns.Clear()
                ASPxGridView2.DataBind()
                ASPxGridView2.PageIndex = 0
                Dim l = Business.DataObjects.RiskComposite.GetColumns(header)
                For Each col As GridViewDataColumn In ASPxGridView2.Columns
                    col.Visible = l.Contains(col.FieldName)
                Next
            Catch ex As Exception
                Dim x As String
                x = ex.ToString()
            End Try
        End If
    End Sub

    Protected Sub ASPxGridView2_DataBinding(sender As Object, e As EventArgs)
        TryCast(sender, ASPxGridView).DataSource = HttpContext.Current.Session("popsource")
    End Sub

    Private Sub AddColumns()
        ASPxGridView2.Columns.Clear()

        AddTextColumn("PK")
        AddTextColumn("PlantNumber")
        AddTextColumn("ParentPlant")
        AddTextColumn("FleetNumber")
        AddTextColumn("AssetNumber")
        AddTextColumn("Category")
        AddTextColumn("Group")
        AddTextColumn("Type")
        AddTextColumn("Make")
        AddTextColumn("Model")
        AddTextColumn("ModelYear")
        AddTextColumn("RegoNo")
        AddTextColumn("RegoDate")
        AddTextColumn("RegoState")
        AddTextColumn("MeterType")
        AddTextColumn("CurrentAsset")
        AddTextColumn("PlantImage")
        AddTextColumn("VINNumber")
        AddTextColumn("EngineNumber")
        AddTextColumn("ChassisNumber")
        AddTextColumn("BodyNumber")
        AddTextColumn("BuildDate")
        AddTextColumn("PreviousRego")

    End Sub
    Private Sub AddTextColumn(ByVal fieldName As String)
        Dim c As New GridViewDataTextColumn()
        c.FieldName = fieldName
        ASPxGridView2.Columns.Add(c)
    End Sub

    Protected Sub WebChartControl2_DataBinding(sender As Object, e As EventArgs)
        TryCast(sender, WebChartControl).DataSource = GetSortedDataTable()
        'TryCast(sender, WebChartControl).DataSource = getchartds(HttpContext.Current.Session("fe"))
    End Sub
    Private Function GetSortedDataTable() As DataView
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim filteredDt As New DataTable
        If dt IsNot Nothing Then
            dt.Columns.Add("ReportDt", GetType(Date))
            filteredDt = uniqco.Business.DataObjects.RiskComposite.Createdt
            filteredDt.Columns.Add("ReportDt", GetType(Date))
            For Each dtRow As DataRow In dt.Rows
                If Not dtRow("ReportDateMonthYear").ToString().Equals("") Then
                    Select Case dtRow("ReportDateMonthYear").ToString().Split(" ")(0)
                        Case "Jan"
                            dtRow("ReportDt") = "1/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Feb"
                            dtRow("ReportDt") = "2/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Mar"
                            dtRow("ReportDt") = "3/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Apr"
                            dtRow("ReportDt") = "4/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "May"
                            dtRow("ReportDt") = "5/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Jun"
                            dtRow("ReportDt") = "6/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Jul"
                            dtRow("ReportDt") = "7/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Aug"
                            dtRow("ReportDt") = "8/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Sep"
                            dtRow("ReportDt") = "9/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Oct"
                            dtRow("ReportDt") = "10/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Nov"
                            dtRow("ReportDt") = "11/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                        Case "Dec"
                            dtRow("ReportDt") = "12/1/" & dtRow("ReportDateMonthYear").ToString().Split(" ")(1).ToString()
                    End Select
                    filteredDt.Rows.Add(dtRow.ItemArray)
                End If
            Next
        End If
        Dim dtSorted As New DataView(filteredDt)
        If dtSorted.Count > 0 Then
            dtSorted.Sort = "ReportDt ASC"
            Return dtSorted
        Else
            Return Nothing
        End If
    End Function
    Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs)
        If e.Parameter = "cc" Then
            createmainds()
        End If
    End Sub

    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)
        Dim grid As ASPxGridView = DirectCast(sender, ASPxGridView)
        HttpContext.Current.Session("fe") = getFilterExpression()
        grid.DataBind()
        updateCH(grid)
    End Sub

    Private Function updateCH(grid As ASPxGridView)
        Try
            grid.Columns("Value").Caption = txtCompany.Text
        Catch ex As Exception

        End Try
        Return Nothing
    End Function

    Protected Sub ASPxGridView1_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "Name" Then
            e.Cell.ForeColor = Color.White
            e.Cell.Font.Bold = True
            e.Cell.BackColor = Color.Gray
        End If
    End Sub

    Protected Sub ASPxGridView3_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs)
        If e.DataColumn.FieldName = "Name" Then
            e.Cell.BackColor = Color.LightGray
        End If
    End Sub

    Protected Sub ASPxGridView1_HtmlRowCreated(sender As Object, e As ASPxGridViewTableRowEventArgs)
        e.Row.Height = 27
    End Sub

    Protected Sub ASPxGridView4_HtmlRowCreated(sender As Object, e As ASPxGridViewTableRowEventArgs)
        e.Row.Height = 22.5
    End Sub

    Protected Sub odsloginCompanies_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs)
    End Sub

    Protected Sub txtCompany_DataBound(sender As Object, e As EventArgs)

        txtCompany.SelectedIndex = txtCompany.Items.IndexOfText(SessionHelper.UserLogin.CompanyName)
    End Sub

    Protected Sub cbxGRGroup_Callback(sender As Object, e As CallbackEventArgsBase)
        Dim cbx As ASPxComboBox = DirectCast(sender, ASPxComboBox)
        Dim text = cbx.Text
        cbx.Items.Clear()

        cbx.Items.Add("[None]")
        If cbx.ID = "cbxGRGroup" Then
            cbx.Items.AddRange((From a In getchartds(HttpContext.Current.Session("fe")) Where a("Group") IsNot DBNull.Value Select CType(a("Group"), String)).Distinct().ToList())
        ElseIf cbx.ID = "cbxTyType" Then
            cbx.Items.AddRange((From a In getchartds(HttpContext.Current.Session("fe")) Where a("Type") IsNot DBNull.Value Select CType(a("Type"), String)).Distinct().ToList())
        ElseIf cbx.ID = "cbxMkMake" Then
            cbx.Items.AddRange((From a In getchartds(HttpContext.Current.Session("fe")) Where a("Make") IsNot DBNull.Value Select CType(a("Make"), String)).Distinct().ToList())
        ElseIf cbx.ID = "cbxMdModel" Then
            cbx.Items.AddRange((From a In getchartds(HttpContext.Current.Session("fe")) Where a("Model") IsNot DBNull.Value Select CType(a("Model"), String)).Distinct().ToList())
        End If
        cbx.Text = "[None]"
    End Sub

    Protected Sub ASPxButton3_Click(sender As Object, e As EventArgs)
        gridExport.WriteCsvToResponse(txtChartText.Text, New CsvExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub odssummarydata_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim data = New Dictionary(Of String, Double)
        data.Add("ARV", If(dt IsNot Nothing, DVal(dt.Compute("Sum(BudgetReplacementPriceExcGST)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("CAV", If(dt IsNot Nothing, DVal(dt.Compute("Sum(CurrentAssetValue)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("DB", If(dt IsNot Nothing, DVal(dt.Compute("Sum(ReserveOverRecovery)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))

        data.Add("TPL", If(dt IsNot Nothing, DVal(dt.Compute("Sum(ProfitLoss)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("CD", If(dt IsNot Nothing, DVal(dt.Compute("Sum(CostOfDownTime)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("CUU", If(dt IsNot Nothing, DVal(dt.Compute("Sum(CostofUnderUtilisation)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("SI", 0)
        e.InputParameters("data") = data
    End Sub

    Protected Sub odsperassetdata_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim data = New Dictionary(Of String, Double)
        Dim intLightFleet As Integer
        Dim intAsset As Integer
        Dim filteredDt As New DataTable
        filteredDt = uniqco.Business.DataObjects.RiskComposite.Createdt

        If dt IsNot Nothing Then
            For Each dtRow As DataRow In dt.Rows
                If dtRow("ReportDateMonthYear") = txtReportDate.Text And (Not (dtRow("KmsHrsVar") > 300 Or dtRow("KmsHrsVar") < 5)) And
                    (Not (dtRow("Deviation") > 100 Or dtRow("Deviation") < -100)) Then
                    filteredDt.Rows.Add(dtRow.ItemArray)
                End If
            Next
        End If

        If dt IsNot Nothing Then
            intLightFleet = dt.Select("(Category='Light Fleet' OR Category='Major Plant > $10K') AND ReportDateMonthYear='" + txtReportDate.Text + "'").Count
            intAsset = (dt.Select("(CostSubTotal <> 0) and ReportDateMonthYear='" + txtReportDate.Text + "'").Count)
            Dim sumSched = DVal(dt.Compute("Sum(LabourHoursSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) + DVal(dt.Compute("Sum(LabourHoursNonSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))
            'Dim totalFuelKmMeterx = DVal(dt.Compute("Sum(LitresKms)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))
            Dim totalFuelKmMeter = DVal(filteredDt.Compute("Sum(LitresKms)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))
            Dim totalKMkmMeter = DVal(filteredDt.Compute("Sum(Kilometre)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))
            Dim totalFuelKrMeter = DVal(filteredDt.Compute("Sum(LitresHr)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))
            'Dim totalHoursHrMeter = DVal(dt.Compute("Sum(Hours)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))
            Dim totalHoursHrMeter = DVal(filteredDt.Compute("Sum(Hours)", "ReportDateMonthYear = '" + txtReportDate.Text + "'"))

            If totalFuelKmMeter = 0 And totalKMkmMeter = 0 Then
                data.Add("FCKMS", 0)
            Else
                data.Add("FCKMS", If(dt IsNot Nothing, DVal((totalFuelKmMeter / totalKMkmMeter) * 100), 0))
            End If

            If totalFuelKrMeter = 0 And totalHoursHrMeter = 0 Then
                data.Add("FCHRS", 0)
            Else
                data.Add("FCHRS", If(dt IsNot Nothing, DVal(totalFuelKrMeter / totalHoursHrMeter), 0))
            End If

            If intAsset = 0 Then
                data.Add("C", 0)
            Else
                data.Add("C", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(CostSubTotal)", "(CostSubTotal <> 0) and ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / intAsset, 0), 0))
            End If

            If intLightFleet = 0 Then
                data.Add("D", 0)
            Else
                data.Add("D", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(CostOfDownTime)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) / intLightFleet), 0), 0))
            End If

            If sumSched = 0 Then
                data.Add("SC", 0)
            Else
                data.Add("SC", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(LabourHoursSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) / sumSched) * 100, 0), 0))
            End If

            If sumSched = 0 Then
                data.Add("UN", 0)
            Else
                data.Add("UN", If(dt IsNot Nothing, If(dt.Rows.Count > 0, 100 - ((DVal(dt.Compute("Sum(LabourHoursSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) / sumSched) * 100), 0), 0))
            End If
        Else
            data.Add("FCKMS", 0)
            data.Add("FCHRS", 0)
            data.Add("C", 0)
            data.Add("D", 0)
            data.Add("SC", 0)
            data.Add("UN", 0)
        End If

        data.Add("S", 0)
        e.InputParameters("data") = data
        e.InputParameters("fc") = Convert.ToString(HttpContext.Current.Session("fe"))
    End Sub

    Protected Sub odsmaintenancedata_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim data = New Dictionary(Of String, Double)
        data.Add("SC", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(LabourHoursSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / dt.Rows.Count, 0), 0))
        data.Add("UN", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(LabourHoursNonSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / dt.Rows.Count, 0), 0))
        data.Add("S", 0)
        e.InputParameters("data") = data
    End Sub

    Private Function DVal(a As Object)
        Return If(IsDBNull(a), 0, a)
    End Function

    Private Function getlatestreport()
        Dim ds = Business.DataObjects.CompanyReport.GetForCompanyID(txtCompany.Value).Last
        Return ds.Reportdate.ToString("MMM yyyy")
    End Function

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim Series As String = "RED"
        Dim Series2 As String = "FADEDRED"
        Dim HeaderArr As String = "Utilisation,OperationalProductivity,FuelConsumption,OptimumReplacement,ServiceDue,WOLCostVariation,MaintenanceRatio,AssetData"
        Dim IdList As List(Of Object) = Nothing
        Dim IdListHeaderList As New List(Of String)
        Dim headerSeries As String = String.Empty
        Dim reportDate As String = "ReportDateMonthYear = '" + Convert.ToDateTime(txtReportDate.Value).ToString("MMM yyyy") + "'"
        For Each Header As String In HeaderArr.Split(",")
            If Header.Equals("OperationalProductivity") Then
                Dim hireCharge As String = "(HireCharge <> '0')"
                If Not getchartds(hireCharge) Is Nothing Then
                    If getchartds(hireCharge).Select(reportDate).Length > 0 Then
                        IdList = (From a In getchartds(hireCharge).Select(reportDate).CopyToDataTable _
                                    Where a(Header).ToString() = Series.ToUpper() Or a(Header).ToString() = Series2.ToUpper() _
                                           Select a("AssetId")).ToList
                        headerSeries = Header + ":" + String.Join(",", IdList)
                        IdListHeaderList.Add(headerSeries)
                    End If
                End If
            Else
                Dim paramString As String = HttpContext.Current.Session("fe")
                Dim paramFe As String = paramString.Replace("(HireCharge <> '0')", "")
                If Not getchartds(paramFe) Is Nothing Then
                    If getchartds(paramFe).Select(reportDate).Length > 0 Then
                        IdList = (From a In getchartds(paramFe).Select(reportDate).CopyToDataTable _
                                    Where a(Header).ToString() = Series.ToUpper() Or a(Header).ToString() = Series2.ToUpper() _
                                           Select a("AssetId")).ToList
                        headerSeries = Header + ":" + String.Join(",", IdList)
                        IdListHeaderList.Add(headerSeries)
                    End If
                End If
            End If
        Next
        If IdListHeaderList.Count > 0 Then
            Dim filename As String = "BizToolRed_" & Date.Now().ToString("MMddyyyyHHmmss").ToString() & ".xlsx"
            Dim excelPackage = uniqco.Business.MSExcelConnector.ExportDataToCsv(IdListHeaderList, Business.DataObjects.RiskComposite.GetFromDB())
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;  filename=" + filename)
            Dim stream As MemoryStream = New MemoryStream(excelPackage)

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.End()
        Else
            Response.Clear()
            Response.AddHeader("content-disposition", "")
            Response.ContentType = "application/vnd.ms-excel"
            Response.Flush()
            Response.End()
        End If
    End Sub
End Class