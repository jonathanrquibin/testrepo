﻿Imports System.Net
Imports System.Web.Http
Imports System.Net.Http

Public Class QueryViewerController
    Inherits ApiController

    ''' <summary>
    ''' 'Get the RiskComposite data 
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="StartDate"></param>
    ''' <param name="EndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <HttpGet>
    Public Function ViewQuery(ByVal UserName As String, ByVal Password As String, ByVal StartDate As Date, EndDate As Date) As String
        Return GetData.ViewQueryData(UserName, Password, StartDate, EndDate)
    End Function
    ''' <summary>
    ''' 'Get the RiskComposite data 
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="GetQuery"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <HttpGet>
    Public Function ViewQueryAsExcel(UserName As String, Password As String, GetQuery As String) As String
        Return GetData.ViewQueryAsExcel(UserName, Password, GetQuery)
    End Function
    ''' <summary>
    ''' 'Get the RiskComposite data 
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="GetQuery"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <HttpGet>
    Public Function ViewQueryAsTable(UserName As String, Password As String, GetQuery As String) As HttpResponseMessage
        Return GetData.ViewQueryAsTable(UserName, Password, GetQuery)
    End Function
    ''' <summary>
    ''' 'Get the RiskComposite data 
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="GetQuery"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <HttpGet>
    Public Function ViewQueryAsJson(UserName As String, Password As String, GetQuery As String) As HttpResponseMessage
        Return GetData.ViewQueryAsJson(UserName, Password, GetQuery)
    End Function
End Class