﻿Imports uniqco.Business

Public Class CompanyAdminMain
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If SessionHelper.UserLogin.Rights.Equals("COMPANY-GENERAL") Then
                ASPxPageControl2.TabPages(0).Enabled = False
                ASPxPageControl2.TabPages(2).Enabled = False
                ASPxPageControl2.TabPages(3).Enabled = False
            Else
                loadCompanyFields()
                loadSettingFields()
            End If
        End If
        If txtSelected.Value = "Specify" Or txtSelected.Value = "Procure" Or txtSelected.Value = "Operate" Then
            'txtUrl.ClientVisible = False
            'ASPxComboBox1.ClientVisible = True
        Else
            'txtUrl.ClientVisible = True
            'ASPxComboBox1.ClientVisible = False
        End If
        
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If SessionHelper.UserLogin Is Nothing Then
            Dim queryStr As String = Request.Url.Query
            queryStr = String.Format("~/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)
            Response.Redirect(queryStr)
        Else
            If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
                Me.MasterPageFile = "~/content/UniqcoAdminMaster.master"
            ElseIf SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                Me.MasterPageFile = "~/content/CompanyAdminMaster.master"
            Else
                Throw New Exception("ERROR! Failed to load Master Page.")
            End If
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnCDSave.Click
        Dim UserCompany As DataObjects.Company = DataObjects.Company.GetFromUser(SessionHelper.UserLogin)
        UserCompany.Address = txtAdress.Text
        UserCompany.Name = txtName.Text
        UserCompany.PhoneNumber = txtNumber.Text
        UserCompany.CompanyLogo = imgLogo.Value

        DataObjects.Company.Update(UserCompany)

    End Sub
    Private Sub loadSettingFields()
        Dim Settings As List(Of DataObjects.CompanySettings) = DataObjects.CompanySettings.GetAllForUserCompany(SessionHelper.UserLogin)

        For Each ss As DataObjects.CompanySettings In Settings
            If ss.Attribute = "href" Then
                Select Case ss.ObjectName
                    Case "Fleet Policy"
                        HypTriangle.Attributes.Add("navi", ss.Value)
                    Case "Fleet Strategy"
                        HypRectangle_1.Attributes.Add("navi", ss.Value)
                    Case "Specify"
                        HypCircle_1.Attributes.Add("navi", ss.Value)
                    Case "Procure"
                        HypCircle_2.Attributes.Add("navi", ss.Value)
                    Case "Operate"
                        HypCircle_3.Attributes.Add("navi", ss.Value)
                    Case "Dispose"
                        HypCircle_4.Attributes.Add("navi", ss.Value)
                    Case "Procedure, Behaviours & System"
                        HypRectangle_2.Attributes.Add("navi", ss.Value)
                End Select
            End If
        Next
    End Sub
    Private Sub loadCompanyFields()

        Dim UserCompany As DataObjects.Company = DataObjects.Company.GetFromUser(SessionHelper.UserLogin)
        txtAdress.Text = UserCompany.Address
        txtName.Text = UserCompany.Name
        txtNumber.Text = UserCompany.PhoneNumber
        imgLogo.Value = UserCompany.CompanyLogo
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnCDReset.Click
        loadCompanyFields()
    End Sub

    Protected Sub btnFPSave_Click(sender As Object, e As EventArgs) Handles btnFPSave.Click
        Dim cs As DataObjects.CompanySettings = DataObjects.CompanySettings.GetFromObjectNameForUser(txtSelected.Text, SessionHelper.UserLogin)
        If cs Is Nothing Then
            Dim newDbObj As New DataObjects.CompanySettings
            newDbObj.ObjectName = txtSelected.Text
            newDbObj.Attribute = "href"
            newDbObj.Value = txtUrl.Text
            newDbObj.CompanyId = SessionHelper.UserLogin.CompanyID
            DataObjects.CompanySettings.Create(newDbObj)
        Else

            cs.ObjectName = txtSelected.Text
            cs.Attribute = "href"
            cs.Value = txtUrl.Text
            cs.CompanyID = SessionHelper.UserLogin.CompanyID
            DataObjects.CompanySettings.Update(cs)
        End If
        loadSettingFields()
    End Sub

    Protected Sub btnFPClear_Click(sender As Object, e As EventArgs) Handles btnFPClear.Click
        Dim cs As DataObjects.CompanySettings = DataObjects.CompanySettings.GetFromObjectNameForUser(txtSelected.Text, SessionHelper.UserLogin)
        If cs IsNot Nothing Then DataObjects.CompanySettings.Delete(cs)
        
        HypTriangle.Attributes.Remove("navi")
        HypRectangle_1.Attributes.Remove("navi")
        HypCircle_1.Attributes.Remove("navi")
        HypCircle_2.Attributes.Remove("navi")
        HypCircle_3.Attributes.Remove("navi")
        HypCircle_4.Attributes.Remove("navi")
        HypRectangle_2.Attributes.Remove("navi")
        loadSettingFields()

        txtUrl.Text = ""
        ASPxComboBox1.SelectedIndex = ASPxComboBox1.Items.IndexOfValue("[None]")
    End Sub

    Protected Sub odsLogin_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles odsLogin.Selecting

        e.InputParameters("login") = SessionHelper.UserLogin
    End Sub

    Protected Sub odsUser_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles odsUser.Selecting
        e.InputParameters("x") = SessionHelper.UserLogin.LoginID
    End Sub

    Protected Sub btnPCSave_Click(sender As Object, e As EventArgs) Handles btnPCSave.Click

        Dim dbobj As DataObjects.Login = DataObjects.Login.GetAllForId(SessionHelper.UserLogin.LoginID)

        dbobj.Password = ASPxFormLayout1.GetNestedControlValueByFieldName("Password")
        dbobj.UserName = ASPxFormLayout1.GetNestedControlValueByFieldName("UserName")
        dbobj.EmailAddress = ASPxFormLayout1.GetNestedControlValueByFieldName("EmailAddress")
        DataObjects.Login.Update(dbobj)

    End Sub

    Protected Sub btnPCReset_Click(sender As Object, e As EventArgs) Handles btnPCReset.Click
        ASPxFormLayout1.DataBind()
    End Sub

    Protected Sub ASPxGridView2_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridViewEditorEventArgs)
        If e.Column.FieldName = "CompanyID" Then
            e.Editor.ClientEnabled = False
            e.Editor.Value = SessionHelper.UserLogin.CompanyID
        End If
    End Sub

    Protected Sub ASPxComboBox1_Callback(sender As Object, e As DevExpress.Web.CallbackEventArgsBase)

        ASPxComboBox1.DataBind()
        ASPxComboBox1.Items.Add("[None]", Nothing)

        If Not txtUrl.Text.Equals("") Then
            ASPxComboBox1.SelectedIndex = ASPxComboBox1.Items.IndexOfValue(txtUrl.Text)
        End If
    End Sub
End Class