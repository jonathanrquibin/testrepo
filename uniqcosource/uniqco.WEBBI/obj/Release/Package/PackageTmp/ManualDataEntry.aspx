﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/content/UniqcoAdminMaster.master" CodeBehind="ManualDataEntry.aspx.vb" Inherits="uniqco.WEBBI.ManualDataEntry" %>

<%@ Register assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script src="/content/js/jquery-3.0.0.min.js"></script>
    <link rel="stylesheet" href="content/css/bootstrap.min.css" />
    <script src="content/js/tether.min.js"></script>
    <script src="content/js/bootstrap.min.js"></script>
    <script src="content/js/jquery.twbsPagination.js"></script>
    <%--<script src="content/js/jquery-ui.js"></script>
        <link href="content/css/jquery-ui.css" rel="stylesheet" />--%>
    <script>
        var currentPage = 1;

        $(function () {
            // $("#tab-1,#tab-2,#tab-3").height($(document).height() - 125);
        });
        $(window).resize(function () {
            // $("#tab-1,#tab-2,#tab-3").height($(document).height() - 125);
            // resizegrid();
        });
        function CommonEvent(s, e) {
            resizegrid();
            $(".container").mousedown(
                function (e) {
                    if (e.button == 2) {
                        var parent = this;
                        var sec_class = this.classList[1];

                        var pattern = /DXDataRow/;
                        var ps = this.parentNode;
                        while (!pattern.test(ps.id)) {
                            ps = ps.parentNode
                        }

                        var name_val = ps.children[0].innerText;
                        $.ajax({
                            type: "POST",
                            url: 'ManualDataEntry.aspx/GetPopupData',
                            dataType: "json",
                            data: JSON.stringify({ asset: sec_class, nameval: name_val }),
                            contentType: "application/json",
                            crossDomain: true,
                            success: function (data) {
                                $('#comp-val').html(data.d.comp ?  data.d.comp : "&lt;NULL&gt;");
                                $('#manual-val').html(data.d.manual ?  data.d.manual : "&lt;NULL&gt;");
                                $('#dump-val').html(data.d.dump ?  data.d.dump : "&lt;NULL&gt;");
                                $('#disp-val').html(data.d.disp ?  data.d.disp : "&lt;NULL&gt;");
                                $('#purch-val').html(data.d.purch ? data.d.purch : "&lt;NULL&gt;");
                                popupCellDetail.ShowAtElement($(parent)[0]);
                            }
                        });
                        return false
                    }
                return true
                }
                //handlerOut
                //, function () {
                //    popupCellDetail.Hide();
                //}
            );
        }
        function resizegrid() {
            //ASPxGridView1.SetHeight($(document).height() - 160);
            //ASPxGridView2.SetHeight($(document).height() - 125);
            //ASPxGridView3.SetHeight($(document).height() - 160);
        }

        function OnFileUploadComplete(s, e) {
            if (e.callbackData !== "") {
                lblFileName.SetText(e.callbackData);
            }
        }

        function onCellClick(rowIndex, fieldName, valueField, event) {
            var valField0 = valueField.split('|')[0];
            var valField1 = valueField.split('|')[1];
            var valField2 = valueField.split('|')[2];
            OverrideLable.SetText(valField0);
            CompositePK.SetText(valField1);
            OverrideValue.SetText(valField2);
            if (event.button == 0) {
                viewOverride.Show();
            } else if (event.button == 2) {
                $.ajax({
                    type: "POST",
                    url: 'ManualDataEntry.aspx/DataEntryOverrideGetValueHistory',
                    dataType: "json",
                    data: JSON.stringify({ 'overrideId': CompositePK.GetText(), 'overrideLabel': OverrideLable.GetText() }),
                    contentType: "application/json",
                    crossDomain: true,
                    success: function (data) {
                        if (data.d.length > 1) {
                            var outputHtml = '';
                            var intCounter = 0;
                            var divHtml = '';
                            $.each(data.d, function (k, v) {
                                var x = v.OverrideLabel;
                                var y = v.OverrideValue;
                                var z = v.UserID;
                                var i = v.OverrideDate;
                                
                                if (intCounter == 0) {
                                    divHtml = 'Original Value: ' + y + '<br/><br/>';
                                } else {
                                    divHtml = i + ' ' + z + ' changed to ' + y + '<br/><br/>'
                                }
                                
                                outputHtml += divHtml;
                                intCounter++;
                            })
                            divHtml = 'Since then, the value has changed from the original.';
                            outputHtml += divHtml;
                            $('#overrideHistory').html(outputHtml);
                            viewOverrideHistory.Show();
                        }
                    }
                });
            }
        }

        function ManualDataEntryOverrideValue() {
            var overrideVal = OverrideValue.GetText();
            var overrideLabel = OverrideLable.GetText();
            var overrideId = CompositePK.GetText();
            var userID = SessionUserID.GetText();
            $.ajax({
                type: "POST",
                url: 'ManualDataEntry.aspx/DataEntryOverrideValue',
                dataType: "json",
                data: JSON.stringify({ 'value': overrideVal, 'overrideLabel': overrideLabel, 'overrideId': overrideId, 'userID': userID }),
                contentType: "application/json",
                crossDomain: true,
                success: function (data) {
                    viewOverride.Hide();
                    ManualOverrideDataGridView.PerformCallback(currentPage);
                }
            });
        }

        function OnCellOver(cell, col, row, counter) {
            if (counter < 2) {
                if (row == 'row_idx_2' || row == 'row_idx_3' || row == 'row_idx_4') {
                    cell.style.backgroundColor = 'light green';
                } else {
                    cell.style.backgroundColor = 'gray';
                }
            }
        }

        function OnCellOut(cell, col, row, counter) {
            if (counter < 2) {
                if (row == 'row_idx_2' || row == 'row_idx_3' || row == 'row_idx_4') {
                    cell.style.backgroundColor = 'light green';
                } else {
                    cell.style.backgroundColor = 'white';
                }
            }
        }
        $(function () {
            var obj = $('#pagination').twbsPagination({
                totalPages: RiskCompositePage.GetText(),
                visiblePages: 10,
                onPageClick: function (event, page) {
                    console.info(page);
                    currentPage = page;
                    ManualOverrideDataGridView.PerformCallback(page);
                }
            });
            console.info(obj.data());
        });
    </script>
    <style>
        .cursor-hand {
            cursor:pointer;
        }
    </style>
    <div oncontextmenu="return false">
        <div style="display:none">
            <dx:ASPxTextBox ID="RiskCompositePage" ClientInstanceName="RiskCompositePage" runat="server"></dx:ASPxTextBox>
        </div>
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Theme="SoftOrange" Width="100%" ActiveTabIndex="0" >
            <TabPages>
                <dx:TabPage Name="Mobile Asset Database" Text="Mobile Asset Database">
                    <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div id="tab-1" style="overflow: auto">
                            <div id="cell-detail-popup">
                                <dx:ASPxPopupControl ClientInstanceName="popupCellDetail" Width="200px" Height="100px"  ID="pcCD"
                                ShowFooter="false" ShowHeader ="false" PopupAnimationType="Fade" runat="server" EnableViewState="false" PopupHorizontalAlign="OutsideRight" Theme="SoftOrange" ShowCloseButton="true" >
                                <ContentCollection>
                                    <dx:PopupControlContentControl runat="server">
                                        <asp:Panel ID="Panel2" runat="server">
                                            <label>Composite: <span id="comp-val">&lt;NULL&gt;</span></label><br />
                                            <label>Manual: <span id="manual-val">&lt;NULL&gt;</span> </label><br />
                                            <label>Dump: <span id="dump-val">&lt;NULL&gt;</span></label><br />
                                            <label>Disposal: <span id="disp-val">&lt;NULL&gt;</span></label><br />
                                            <label>Purchasing: <span id="purch-val">&lt;NULL&gt;</span></label>
                                        </asp:Panel>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                </dx:ASPxPopupControl>
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox style="margin:5px" Caption="Search Asset" ID="ASPxTextBox1" runat="server" Width="470px"></dx:ASPxTextBox>
                                    <%--<dx:ASPxComboBox ID="ASPxComboBox1"  style="margin:5px" Caption="Search Asset" runat="server" Width="470px" ValueType="System.String" DataSourceID="odsAsset"></dx:ASPxComboBox>
                                        <asp:ObjectDataSource OnSelecting="odsAsset_Selecting" ID="odsAsset" runat="server" SelectMethod="GetSearchSource" TypeName="uniqco.Business.DataObjects.MobileAssetData">
                                            <SelectParameters>
                                                <asp:Parameter DbType="Guid" Name="com" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>--%>
                                    </td>
                                    <td><dx:ASPxButton ID="btnSearch" OnClick="btnSearch_Click" ClientInstanceName="btnSearch" runat="server"  style="" Text="Search" Theme="SoftOrange" AutoPostBack="False">
                                <ClientSideEvents Click="function(s, e) {
                                }"/>
                            </dx:ASPxButton></td>
                                </tr>
                            </table>
                              
                            <dx:ASPxGridView ID="ASPxGridView1" OnHtmlRowPrepared="ASPxGridView1_HtmlRowPrepared" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared" 
                                ClientInstanceName="ASPxGridView1" OnCustomCallback="ASPxGridView1_CustomCallback" 
                                runat="server" AutoGenerateColumns="False" KeyFieldName="Name" EnableRowsCache="False" SettingsBehavior-AllowSort="false" EnableTheming="True" Theme="SoftOrange" Width="100%" DataSourceID="odsMAD" >
                                 
                                <SettingsPager PageSize="100">
                                <PageSizeItemSettings Visible="true" />
                                </SettingsPager>
                                <SettingsEditing Mode="Batch" >
                                <BatchEditSettings StartEditAction="Click" />
                                </SettingsEditing>
                                <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                <Columns>
                                    <dx:GridViewDataTextColumn Width="350px"  ReadOnly="True" FieldName="Name" Caption =" " CellStyle-Cursor="pointer" HeaderStyle-Font-Bold="true"  VisibleIndex="0">
                                       
                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                    <CellStyle Cursor="pointer"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset1" ShowInCustomizationForm="True" VisibleIndex="1">
                                    <DataItemTemplate>
                                        <div class="container Asset1">
                                            <%#Eval("Asset1")%>
                                        </div>
                                    </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset2" ShowInCustomizationForm="True" VisibleIndex="2">
                                        <DataItemTemplate>
                                        <div class="container Asset2">
                                            <%#Eval("Asset2")%>
                                        </div>
                                    </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset3" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <DataItemTemplate>
                                        <div class="container Asset3">
                                            <%#Eval("Asset3")%>
                                        </div>
                                    </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset4" ShowInCustomizationForm="True" VisibleIndex="4">
                                        <DataItemTemplate>
                                        <div class="container Asset4">
                                            <%#Eval("Asset4")%>
                                        </div>
                                    </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset5" ShowInCustomizationForm="True" VisibleIndex="5">
                                        <DataItemTemplate>
                                        <div class="container Asset5">
                                            <%#Eval("Asset5")%>
                                        </div>
                                    </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset1ManualId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset2ManualId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset3ManualId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset4ManualId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset5ManualId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="10">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset1CompositeId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="11">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset2CompositeId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset3CompositeId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn  FieldName="Asset4CompositeId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="14">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Asset5CompositeId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="15">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CompanyId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="16">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ID" Visible="false" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="17">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <asp:ObjectDataSource ID="odsMAD" runat="server" OnSelecting="odsMAD_Selecting" SelectMethod="GetForCompany" OnUpdating="odsMAD_Updating" TypeName="uniqco.Business.DataObjects.MobileAssetData" DataObjectTypeName="uniqco.Business.DataObjects.MobileAssetData" InsertMethod="Create" UpdateMethod="Update">
                                <SelectParameters>
                                    <asp:Parameter DbType="Guid" Name="com" />
                                <asp:Parameter Name="Make" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Summary Data" Text="Summary Data">
                    <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div id="tab-2" style="overflow: auto">
                            <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2"  runat="server" AutoGenerateColumns="False"
                                KeyFieldName="ID" SettingsBehavior-AllowSort="false" EnableTheming="True" Theme="SoftOrange" Width="100%" >
                                 
                                <SettingsPager PageSize="100">
                                <PageSizeItemSettings Visible="true" />
                                </SettingsPager>
                                <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                <Columns>
                                </Columns>
                            </dx:ASPxGridView>
                        </div>
                    </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Common Factors" Text="Common Factors">
                    <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div id="tab-3" style="overflow: auto">
                              
                                    <dx:ASPxComboBox ID="btnCF"  ClientInstanceName="btnCF" Width="200px" style="margin-bottom:10px" Caption="Common Factor" runat="server" ValueType="System.String" DataSourceID="odsCommonFactors">
                            <ClientSideEvents ValueChanged="function(s, e) {
                                ASPxGridView3.PerformCallback();
                                }" />
                                        </dx:ASPxComboBox>
                            <asp:ObjectDataSource ID="odsCommonFactors" runat="server" SelectMethod="GetCFList" TypeName="uniqco.Business.DataObjects.MobileAssetData"></asp:ObjectDataSource>
                               
                            <dx:ASPxGridView ID="ASPxGridView3" OnCustomCallback="ASPxGridView3_CustomCallback" KeyFieldName="CommonFactorsId" ClientInstanceName="ASPxGridView3"  runat="server" AutoGenerateColumns="False"  EnableTheming="True" Theme="SoftOrange" Width="100%" DataSourceID="odsCFGrid" >
                                 
                                <SettingsEditing Mode="Inline"></SettingsEditing>
                                <SettingsPager PageSize="100">
                                <PageSizeItemSettings Visible="true" />
                                </SettingsPager>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0"  Width="100px">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Visible="false" FieldName="CommonFactorsId" ShowInCustomizationForm="True" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Visible="false" FieldName="CommonFactors" ShowInCustomizationForm="True" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TableShowingOption" ShowInCustomizationForm="True" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="Include" ShowInCustomizationForm="True" VisibleIndex="3">
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataComboboxColumn FieldName="Source" ShowInCustomizationForm="True" VisibleIndex="4">
                                    <PropertiesComboBox>
                                        <Items>
                                    <dx:ListEditItem Text="Manual Entry" Value ="ManualEntry" />
                                    <dx:ListEditItem Text="Composite Report" Value ="CompositeReport" />
                                    <dx:ListEditItem Text="Disposal Report" Value ="DisposalReport" />
                                    <dx:ListEditItem Text="Purchasing Guide" Value ="PurchasingGuide" />
                                    <dx:ListEditItem Text="Dump Report" Value ="DumpReport" />
                                </Items>
                                    </PropertiesComboBox>
                                    </dx:GridViewDataComboboxColumn>
                                    <dx:GridViewDataTextColumn Visible="false" FieldName="CompanyID" ShowInCustomizationForm="True" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <ClientSideEvents Init="CommonEvent" EndCallback="CommonEvent" />
                            </dx:ASPxGridView>
                            <asp:ObjectDataSource OnInserting="odsCFGrid_Inserting" OnSelecting="odsCFGrid_Selecting" ID="odsCFGrid" runat="server" SelectMethod="GetForCompany" TypeName="uniqco.Business.DataObjects.CommonFactors" DataObjectTypeName="uniqco.Business.DataObjects.CommonFactors" DeleteMethod="Delete" InsertMethod="Create" UpdateMethod="Update">
                                <SelectParameters>
                                    <asp:Parameter DbType="Guid" Name="com" />
                                    <asp:Parameter Name="cf" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Manual Override Data" Text="Manual Override Data">
                    <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div id="tab-4" style="overflow: auto">
                            <dx:ASPxGridView ID="ManualOverrideDataGridView" ClientInstanceName="ManualOverrideDataGridView" runat="server"
                                KeyFieldName="ColumnHeaders" AutoGenerateColumns="False" EnableTheming="True" Theme="SoftOrange" Width="100%"
                                OnHtmlRowPrepared="ManualOverrideDataGridView_HtmlRowPrepared" 
                                OnHtmlDataCellPrepared="ManualOverrideDataGridView_HtmlDataCellPrepared"
                                OnCustomCallback="ManualOverrideDataGridView_CustomCallback">
                                <Settings ShowGroupPanel="True" ShowTitlePanel="true"></Settings>
                                <Templates>
                                    <TitlePanel>Manual Override Data</TitlePanel>
                                </Templates>
                                <SettingsPager PageSize="1000">
                                </SettingsPager>
                                <Settings ShowPreview="true" />
                                <Settings ShowColumnHeaders="false" />
                                <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1" />
                                <SettingsPopup>
                                    <EditForm Modal="true"
                                        VerticalAlign="WindowCenter"
                                        HorizontalAlign="WindowCenter" />
                                </SettingsPopup>
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="ColumnHeaders" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column1" VisibleIndex="1"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column2" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column3" VisibleIndex="3"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column4" VisibleIndex="4"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column5" VisibleIndex="5"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column6" VisibleIndex="6"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column7" VisibleIndex="7"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column8" VisibleIndex="8"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column9" VisibleIndex="9"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column10" VisibleIndex="10"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Column11" VisibleIndex="11"></dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </div>
                        <div class="container">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" id="pagination"></ul>
                            </nav>
                        </div>
                    </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Global Data Entry" Text="Global Data Entry">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-5" style="overflow: auto">
                                <dx:ASPxGridView ID="GlobalDataEntryGridView" DataSourceID="odsGlobalDataEntry" runat="server" KeyFieldName="GlobalId" AutoGenerateColumns="False" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                    <Settings ShowGroupPanel="True" ShowFilterRow="True" ShowTitlePanel="true"></Settings>
                                    <Templates>
                                        <TitlePanel>Global Data Entry</TitlePanel>
                                    </Templates>
                                    <SettingsSearchPanel Visible="True"></SettingsSearchPanel>
                                    <Settings ShowPreview="true" />
                                    <SettingsPager PageSize="10" />
                                    <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1" />
                                    <SettingsPopup>
                                        <EditForm Modal="true"
                                            VerticalAlign="WindowCenter"
                                            HorizontalAlign="WindowCenter" />
                                    </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px"></dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="GlobalId" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="InflationRate" VisibleIndex="1"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="CostDiesel" VisibleIndex="2"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="CostULP" VisibleIndex="3"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="CostPULP" VisibleIndex="4"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="CostBioDieselB20" VisibleIndex="5"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="CostBioDieselB100" VisibleIndex="6"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="CostOfElectricity" VisibleIndex="7"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="FuelRebate" VisibleIndex="8"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="InterestRate" VisibleIndex="9"></dx:GridViewDataSpinEditColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Tyre Data" Text="Tyre Data">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-6" style="overflow: auto">
                                <dx:ASPxGridView ID="TyreDataGridView" DataSourceID="odsTyreTrack" runat="server" AutoGenerateColumns="False"
                                    KeyFieldName="TrackId" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                    <Settings ShowGroupPanel="True" ShowFilterRow="True" ShowTitlePanel="true"></Settings>
                                    <Templates>
                                        <TitlePanel>Tyre Data</TitlePanel>
                                    </Templates>
                                    <SettingsSearchPanel Visible="True"></SettingsSearchPanel>
                                    <Settings ShowPreview="true" />
                                    <SettingsPager PageSize="10" />
                                    <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1" />
                                    <SettingsPopup>
                                        <EditForm Modal="true"
                                            VerticalAlign="WindowCenter"
                                            HorizontalAlign="WindowCenter" />
                                    </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px"></dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="TrackId" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="TrackSize" VisibleIndex="1"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="TrackCost" VisibleIndex="2"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataTextColumn FieldName="TrackLift" VisibleIndex="3"></dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Manual Data Entry" Text="Manual Data Entry">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-7" style="overflow: auto">
                                <dx:ASPxGridView ID="ManualDataEntryGridView" DataSourceID="odsManualDataEntry" KeyFieldName="AssetId"  runat="server" AutoGenerateColumns="False" EnableTheming="True" Theme="SoftOrange" Width="100%"
                                    OnCustomButtonCallback="ManualDataEntryGridView_CustomButtonCallback" OnInitNewRow="ManualDataEntryGridView_InitNewRow" OnCustomErrorText="ManualDataEntryGridView_CustomErrorText">
                                    <Settings ShowGroupPanel="True" ShowFilterRow="True" ShowTitlePanel="true"></Settings>
                                    <Templates>
                                        <TitlePanel>Tyre Data</TitlePanel>
                                    </Templates>
                                    <SettingsSearchPanel Visible="True"></SettingsSearchPanel>
                                    <Settings ShowPreview="true" />
                                    <SettingsPager PageSize="10" />
                                    <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="3" />
                                    <SettingsPopup>
                                        <EditForm Modal="true"
                                            VerticalAlign="WindowCenter"
                                            HorizontalAlign="WindowCenter" Width="1200px" />
                                    </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px">
                                            <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="Copy">
                                                    <%--<Image ToolTip="Copy Data" Url="Images/copy.png" />--%>
                                                </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="AssetId" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Make" VisibleIndex="1"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Group" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type" VisibleIndex="3"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Model" VisibleIndex="4"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Model_Series_Base" VisibleIndex="5"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Model_Series_Level" VisibleIndex="6"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Model_Series_Vin" VisibleIndex="7"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Model_ID" VisibleIndex="8"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_Meter" VisibleIndex="9"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_Transmission" VisibleIndex="10"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_Body" VisibleIndex="11"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_Drive" VisibleIndex="12"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_VehicleBody" VisibleIndex="13"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_Value" VisibleIndex="14"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type_Image" VisibleIndex="15"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Fuel_Type" VisibleIndex="16"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Category" VisibleIndex="17"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="EngineCapacity" VisibleIndex="18"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Weight" VisibleIndex="19"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataTextColumn FieldName="GVM" VisibleIndex="20"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="GCM" VisibleIndex="21"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="MaxTowingCapacity" VisibleIndex="22"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Tyre_Track_Size_Front_Left" VisibleIndex="23"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Tyre_Track_Number_Front_Left" VisibleIndex="24"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Tyre_Track_Size_Rear_Right" VisibleIndex="25"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Tyre_Track_Number_Rear_Right" VisibleIndex="26"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Tyre_Track_Life_Meter" VisibleIndex="27"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Warranty_yrs" VisibleIndex="28"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Warranty_Meter" VisibleIndex="29"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Average_Replacement_Yrs" VisibleIndex="30"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Average_Replacement_Meter" VisibleIndex="31"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Average_Annual_Meter" VisibleIndex="32"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="AveragePrice_Today" VisibleIndex="33"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="DisposalPrice_OptimumPct" VisibleIndex="34"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="DisposalPrice_Year1Pct" VisibleIndex="35"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="DisposalPrice_Year2Pct" VisibleIndex="36"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="DisposalPrice_Year3Pct" VisibleIndex="37"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="DisposalPrice_Year4Pct" VisibleIndex="38"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="DisposalPrice_Year5Pct" VisibleIndex="39"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Maint_Cost_Per_Meter" VisibleIndex="40"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Fuel_Cons_Meter" VisibleIndex="41"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataTextColumn FieldName="Fuel_Rebate_Status" VisibleIndex="42"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Fuel_Rebate_Rate" VisibleIndex="43"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CO2_Output" VisibleIndex="44"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Air_Pollution_Rating" VisibleIndex="45"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ANCAP_Crash_Rating" VisibleIndex="46" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Manufacturers_First_Service_Meter" VisibleIndex="47" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Manufacturers_First_Service_Months" VisibleIndex="48" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Manufacturers_First_Service_OnceOnly" VisibleIndex="49" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Tyre_Rotation_Meter" VisibleIndex="50" EditFormSettings-Visible="true"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Inspection_Interval_Months" VisibleIndex="51" EditFormSettings-Visible="true"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Manufacturers_Service_Meter" VisibleIndex="52" EditFormSettings-Visible="true"></dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataTextColumn FieldName="Manufacturers_Service_Months" VisibleIndex="53" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Safety_Insp_Meter" VisibleIndex="54" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Safety_Insp_Months" VisibleIndex="55" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Exclude_Fuel_Consumption" VisibleIndex="56" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="In_Service_Program" VisibleIndex="57" EditFormSettings-Visible="true"></dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Maintenance Override" Text="Maintenance Override">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-8" style="overflow: auto">
                                <dx:ASPxGridView ID="MaintenanceOverrideGridView" ClientInstanceName="MaintenanceOverrideGridView" DataSourceID="odsMaintenanceOverride" runat="server" AutoGenerateColumns="False"
                                    KeyFieldName="MaintenanceAssetDataID" SettingsBehavior-AllowSort="false" EnableTheming="True" Theme="SoftOrange" Width="100%">

                                    <SettingsEditing Mode="Inline"></SettingsEditing>
                                    <SettingsPager PageSize="100">
                                        <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                    <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <EditButton Text="Override"></EditButton>
                                    </SettingsCommandButton>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="MaintenanceAssetDataID" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Group" VisibleIndex="0" ReadOnly="true">
                                            <HeaderStyle BackColor="Red" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Make" VisibleIndex="1" ReadOnly="true">
                                            <HeaderStyle BackColor="Red" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MeterType" VisibleIndex="2" ReadOnly="true">
                                            <HeaderStyle BackColor="Red" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MaintenanceCostPerMeterCalculated" VisibleIndex="3" ReadOnly="true">
                                            <HeaderStyle BackColor="#009900" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MaintenanceCostPerMeterOverride" VisibleIndex="4">
                                            <HeaderStyle BackColor="#cc3300" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Columns>
                                    </Columns>
                                </dx:ASPxGridView>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Fuel Override" Text="Fuel Override">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-9" style="overflow: auto">
                                <dx:ASPxGridView ID="FuelOverride" ClientInstanceName="FuelOverride" DataSourceID="odsFuelOverride" runat="server" AutoGenerateColumns="False"
                                    KeyFieldName="FuelAssetDataID" SettingsBehavior-AllowSort="false" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                    <SettingsEditing Mode="Inline"></SettingsEditing>
                                    <SettingsPager PageSize="100">
                                        <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                    <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                    <SettingsCommandButton>
                                        <EditButton Text="Override"></EditButton>
                                    </SettingsCommandButton>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0" Width="100px"></dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="FuelAssetDataID" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Group" VisibleIndex="1" ReadOnly="true">
                                            <HeaderStyle BackColor="Yellow" ForeColor="Black" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Type" VisibleIndex="2" ReadOnly="true">
                                            <HeaderStyle BackColor="Yellow" ForeColor="Black" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Make" VisibleIndex="3" ReadOnly="true">
                                            <HeaderStyle BackColor="Red" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Model" VisibleIndex="4" ReadOnly="true">
                                            <HeaderStyle BackColor="Red" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MeterType" VisibleIndex="5" ReadOnly="true">
                                            <HeaderStyle BackColor="Red" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="LiterMeterCalculated" VisibleIndex="6" ReadOnly="true">
                                            <HeaderStyle BackColor="#009900" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MeterOverrideValue" VisibleIndex="7">
                                            <HeaderStyle BackColor="#cc3300" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Columns>
                                    </Columns>
                                </dx:ASPxGridView>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
        </dx:ASPxPageControl>
        <asp:ObjectDataSource ID="odsTyreTrack" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.TyreTrack" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.TyreTrack" UpdateMethod="Update"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsGlobalDataEntry" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.GlobalData" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.GlobalData" UpdateMethod="Update"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsManualDataEntry" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.AssetData" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.AssetData" UpdateMethod="Update"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsMaintenanceOverride" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.usp_GetAssetDataForMaintenance" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.usp_GetAssetDataForMaintenance" UpdateMethod="Update"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsFuelOverride" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.usp_GetAssetDataForFuel" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.usp_GetAssetDataForFuel" UpdateMethod="Update"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="odsManualOverrideData" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.usp_GetManualOverrideData" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.usp_GetManualOverrideData" UpdateMethod="Update"></asp:ObjectDataSource>
    </div>
    <dx:ASPxPopupControl ID="viewOverride" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="viewOverride" 
        HeaderText="Override" AllowDragging="True" PopupAnimationType="None" EnableViewState="False" Width="230" >        
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel2" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent runat="server">   
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-5">
                                        <dx:ASPxLabel ID="OverrideLable" ClientInstanceName="OverrideLable" Font-Bold="true" Font-Size="Larger" runat="server" Text="" Width="100px"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-5">
                                        <dx:ASPxTextBox ID="OverrideValue" Font-Bold="true" Font-Size="Larger" ClientInstanceName="OverrideValue"  runat="server" Width="200px"></dx:ASPxTextBox>&nbsp;
                                    </div>
                                    <div style="display: none">
                                        <dx:ASPxTextBox ID="CompositePK" ClientInstanceName="CompositePK" AutoPostBack="false" runat="server" Text=""></dx:ASPxTextBox>
                                        <dx:ASPxTextBox ID="SessionUserID" ClientInstanceName="SessionUserID" AutoPostBack="false" runat="server" Text=""></dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="row"></div>
                                <div class="row"></div>
                            </div>
                            <div style="text-align: right; padding: 2px; padding-top:20px">
                                <dx:ASPxButton ID="aspxButton4" Width="50px" runat="server" AutoPostBack="false" Text="Override">
                                    <ClientSideEvents Click="function(s,e) {
                                        ManualDataEntryOverrideValue();
                                    }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="aspxButton5"  Width="50px" runat="server" AutoPostBack="false" Text="Exit">                                        
                                    <ClientSideEvents Click="function(s, e) { viewOverride.Hide(); }" />
                                </dx:ASPxButton>
                            </div>
                                    
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>  
    <dx:ASPxPopupControl ID="viewOverrideHistory" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="viewOverrideHistory" 
        HeaderText="Data Entry History" AllowDragging="True" PopupAnimationType="None" EnableViewState="False" Width="200" Height="100" >        
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent runat="server">   
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <span id="overrideHistory"></span>
                                    </div>
                                    <div style="display: none">
                                        <dx:ASPxTextBox ID="ASPxTextBox3" ClientInstanceName="CompositePK" AutoPostBack="false" runat="server" Text=""></dx:ASPxTextBox>
                                        <dx:ASPxTextBox ID="ASPxTextBox4" ClientInstanceName="UserID" AutoPostBack="false" runat="server" Text=""></dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="row"></div>
                                <div class="row"></div>
                            </div>
                            <div style="text-align: right; padding: 2px; padding-top:20px">
                                <dx:ASPxButton ID="aspxButton2"  Width="50px" runat="server" AutoPostBack="false" Text="Exit">                                        
                                    <ClientSideEvents Click="function(s, e) { viewOverrideHistory.Hide(); }" />
                                </dx:ASPxButton>
                            </div>
                                    
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>  
</asp:Content>

