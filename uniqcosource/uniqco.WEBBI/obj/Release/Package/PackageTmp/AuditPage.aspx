﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/content/UniqcoAdminMaster.master" CodeBehind="AuditPage.aspx.vb" Inherits="uniqco.WEBBI.AuditPage" %>

<%@ Register assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <link href="content/css/uniqco_frontpage.css" rel="stylesheet" />
        <script src="content/js/jquery-3.0.0.min.js"></script>
        <script src="content/js/default.js"></script>
        <style></style>
        <script>
            function OnInit(s, e) {
               // AdjustSize();
            }
            function OnEndCallback(s, e) {
                //AdjustSize();
            }
            function AdjustSize() {
                //ASPxGridView1.SetHeight($(document).height() - 50);
            }</script>

         <div>
          <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server"  EnableTheming="True" Theme="SoftOrange" Width="100%" AutoGenerateColumns ="False" DataSourceID="odsAudit">
              
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Visible="true" />
        </SettingsPager>
                                    <SettingsEditing Mode="PopupEditForm" >
                                    </SettingsEditing>
                              <SettingsPopup EditForm-HorizontalAlign="WindowCenter" CustomizationWindow-VerticalAlign="WindowCenter">

<EditForm HorizontalAlign="WindowCenter"></EditForm>

<CustomizationWindow VerticalAlign="WindowCenter"></CustomizationWindow>

                              </SettingsPopup>
              <Columns>
                  <dx:GridViewDataTextColumn FieldName="PK" Visible="false" VisibleIndex="0">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="UID"  Visible="false" VisibleIndex="1">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataDateColumn FieldName="ProcessedDate" VisibleIndex="2">
                  </dx:GridViewDataDateColumn>
                  <dx:GridViewDataComboBoxColumn FieldName="CompanyId" VisibleIndex="3" Caption="Company Name">
                       <PropertiesComboBox DataSourceID="odsCompanies" TextField="Name" ValueField="CompanyID">
                      </PropertiesComboBox>
                  </dx:GridViewDataComboBoxColumn>
                  <dx:GridViewDataTextColumn FieldName="Subject" VisibleIndex="4">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="Sender" VisibleIndex="5">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="Report" Caption="File/ Attachment" VisibleIndex="6">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataCheckColumn FieldName="Exception" Caption="Has Error?" VisibleIndex="7">
                  </dx:GridViewDataCheckColumn>
                  <dx:GridViewDataTextColumn FieldName="ExceptionMessage" Caption="Error Message" VisibleIndex="8">
                  </dx:GridViewDataTextColumn>
              </Columns>
          </dx:ASPxGridView>
             <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>
                             
             <asp:ObjectDataSource ID="odsAudit" runat="server" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.ProcessedEmail">
             </asp:ObjectDataSource>
             
        <asp:ObjectDataSource ID="odsCompanies" runat="server" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.Company"></asp:ObjectDataSource>
             <dx:ASPxButton ID="ASPxButton1" style="margin-top:10px;float:right" OnClick="ASPxButton1_Click" runat="server" Text="Export CSV" Theme="SoftOrange" AutoPostBack="False"></dx:ASPxButton>
         </div>
          </asp:Content>