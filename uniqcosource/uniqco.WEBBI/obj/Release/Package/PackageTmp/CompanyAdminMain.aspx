﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/content/UniqcoAdminMaster.master" CodeBehind="CompanyAdminMain.aspx.vb" Inherits="uniqco.WEBBI.CompanyAdminMain" %>

<%@ Register assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <link href="content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script src="content/js/jquery-3.0.0.min.js"></script>
    <script src="content/js/default.js"></script>
    <%--<script src="content/js/jquery-ui.js"></script>
        <link href="content/css/jquery-ui.css" rel="stylesheet" />--%>
    <script>
        $(document).ready(function () {
            $('.selection').on('click', function () {
                txtSelected.SetValue($(this).attr('obj'));
                txtUrl.SetValue($(this).attr('navi'));
                ASPxComboBox1.PerformCallback();
                updateTextFields();
            });
        });
        $(function () {
           // $("#tab-1,#tab-2,#tab-3,#tab-4").height($(document).height() - 125);
        });
        $(window).resize(function () {
            //$("#tab-1,#tab-2,#tab-3,#tab-4").height($(document).height() - 125);
            //ASPxGridView2.SetHeight($(document).height() - 125);
        });
        function updateTextFields() {
            //if (txtSelected.GetValue() == 'Specify' || txtSelected.GetValue() == 'Procure' || txtSelected.GetValue() == 'Operate') {
            //    txtUrl.SetVisible(false);
            //    ASPxComboBox1.SetVisible(true);
            //}
            //else {
            //    txtUrl.SetVisible(true);
            //    ASPxComboBox1.SetVisible(false);
            //}
        }
        function OnInit(s, e) {
            //AdjustSize();
        }
        function OnEndCallback(s, e) {
           // AdjustSize();
        }
        function AdjustSize() {
            //ASPxGridView2.SetHeight($(document).height() - 125);
        }
  </script>
    <div>
         
        <dx:ASPxPageControl ID="ASPxPageControl2" runat="server" Theme="SoftOrange" Width="100%" >
            <TabPages>
                    <dx:TabPage Name="User Admin" Text="User Admin">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                
                                      <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" OnCellEditorInitialize="ASPxGridView2_CellEditorInitialize" KeyFieldName="LoginID"
                                           runat="server" AutoGenerateColumns="False" DataSourceID="odsLogin" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                   
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Visible="true" />
        </SettingsPager>
                                    <SettingsSearchPanel Visible="True" />
                                    <SettingsEditing Mode="PopupEditForm" >
                                    </SettingsEditing>
                              <SettingsPopup EditForm-HorizontalAlign="WindowCenter" CustomizationWindow-VerticalAlign="WindowCenter">

<EditForm HorizontalAlign="WindowCenter"></EditForm>

<CustomizationWindow VerticalAlign="WindowCenter"></CustomizationWindow>

                              </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0"  Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="LoginID" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="UserName" ShowInCustomizationForm="True" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Password" ShowInCustomizationForm="True" VisibleIndex="6" PropertiesTextEdit-Password="true">
<PropertiesTextEdit Password="True"></PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Active" ShowInCustomizationForm="True" VisibleIndex="7"  Width="100px">
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Rights" ShowInCustomizationForm="True" VisibleIndex="5">
                                            <PropertiesComboBox DataSourceID="odsloginRights">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="EmailAddress" ShowInCustomizationForm="True" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="CompanyID" PropertiesComboBox-ClientInstanceName="CompanyID" ShowInCustomizationForm="True" VisibleIndex="2" Caption="Company Name" ReadOnly ="true" >
                                            <PropertiesComboBox DataSourceID="odsloginCompanies" TextField="Name" ValueField="CompanyID">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                               <asp:ObjectDataSource ID="odsLogin" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.Login" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAllFromUser" TypeName="uniqco.Business.DataObjects.Login" UpdateMethod="Update">
                                    <SelectParameters>
                                        <asp:Parameter Name="login" Type="Object" />
                                        <asp:Parameter DefaultValue="false" Name="rights" Type="Boolean" />
                                    </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <asp:ObjectDataSource ID="odsloginCompanies" runat="server" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.Company"></asp:ObjectDataSource>
                                  
                                    <asp:ObjectDataSource ID="odsloginRights" runat="server" SelectMethod="GetRightsList" TypeName="uniqco.Business.DataObjects.Login">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="false" Name="isUniqco" Type="Boolean" />
                                        </SelectParameters>
                                      </asp:ObjectDataSource>
                           
                                </dx:ContentControl>
                                </ContentCollection>
                        </dx:TabPage>
                    <dx:TabPage Name="Password Change" Text="Password Change">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <div style="width:100%;margin:0;padding:0;background:rgba(228, 228, 228, 0.64);height:30px;display:inline-block">
                                        <dx:ASPxButton Theme="SoftOrange" runat ="server" ID="btnPCSave" Text="Save" style="width:100px;height:100%; margin:0px;border:0px;font-size:13px;"> </dx:ASPxButton>
                                        <dx:ASPxButton Theme="SoftOrange" runat="server" ID="btnPCReset" Text="Reset" style="width:100px;height:100%; margin:0px;border:0px;font-size:13px;"> </dx:ASPxButton>
                                    </div>
                                    <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" DataSourceID="odsUser" EnableTheming="True" Theme="SoftOrange">
                                        <Items>
                                            <dx:LayoutItem FieldName="LoginID" Visible="false">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E1" runat="server" Width="170px">
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="UserName">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E2" runat="server" Width="170px">
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="Password">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E3" runat="server" Width="170px" Password="true">
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="Active">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxCheckBox ID="ASPxFormLayout1_E4" runat="server" CheckState="Unchecked" ReadOnly="true">
                                                        </dx:ASPxCheckBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="Rights">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E5" runat="server" Width="170px" ReadOnly="true" >
                                                        
                                                        </dx:ASPxTextBox>
                                                        
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="CompanyName">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E6" runat="server" Width="170px" ReadOnly="true">
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="EmailAddress">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E7" runat="server" Width="170px">
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem FieldName="CompanyID" Visible="false">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer runat="server">
                                                        <dx:ASPxTextBox ID="ASPxFormLayout1_E8" runat="server" Width="170px">
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                    </dx:ASPxFormLayout> 
                                    <asp:ObjectDataSource ID="odsUser" runat="server" SelectMethod="GetAllForId" TypeName="uniqco.Business.DataObjects.Login">
                                        <SelectParameters>
                                            <asp:Parameter DbType="Guid" Name="x" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </dx:ContentControl>
                                </ContentCollection>
                        </dx:TabPage>
                    <dx:TabPage Name="Company Details" Text="Company Details">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                
                                    <div style="width:100%;margin:0;padding:0;background:rgba(228, 228, 228, 0.64);height:30px;display:inline-block">
                                        <dx:ASPxButton Theme="SoftOrange" runat="server" ID="btnCDSave" Text="Save" style="width:100px;height:100%; margin:0px;border:0px;font-size:13px;"> </dx:ASPxButton>
                                        <dx:ASPxButton Theme="SoftOrange" runat="server" ID="btnCDReset" Text="Reset" style="width:100px;height:100%; margin:0px;border:0px;font-size:13px;"> </dx:ASPxButton>
                                    </div>
                                    <table style="margin-top:20px;">
                                        <tr>
                                            <td>
                                     <dx:ASPxBinaryImage ID="imgLogo" runat="server" Height="206px" Width="241px">
                                         <EditingSettings Enabled="True">
                                         </EditingSettings>
                                     </dx:ASPxBinaryImage></td>
                                            <td style="display:block;padding:0 20px">
                                                <table>
                                                    <tr>
                                                        <td>Company Name</td>
                                                        <td>
                                                             <dx:ASPxTextBox ID="txtName" runat="server" Width="245px">
                                                             </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone Number</td>
                                                        <td>
                                                             <dx:ASPxTextBox ID="txtNumber" runat="server" Width="245px">
                                                             </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Address</td>
                                                        <td>
                                                            <dx:ASPxMemo ID="txtAdress" runat="server" Height="71px" Width="245px"></dx:ASPxMemo>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                                </ContentCollection>
                        </dx:TabPage>
                    <dx:TabPage Name="Front Page Settings" Text="Front Page Settings">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <table>
                                        <tr>
                                            <td style="vertical-align:top;">
                                    <div style="display:block;border:1px solid #707070;border-radius:5px;width:370px;margin-top:30px;height:200px;background:#e8e8e8">
                                        <div style="width:100%;margin:0;padding:0;background:rgba(147, 146, 146, 0.54);height:25px;display:inline-block">
                                            <span style="color:white;margin-left:5px;">Front Page Settings</span>
                                        </div>
                                                <table style="padding:0 20px;">
                                                    <tr>
                                                        <td>Button/ Link</td>
                                                        <td>
                                                             <dx:ASPxComboBox ID="txtSelected" ClientInstanceName="txtSelected" runat="server" Width="245px" ValueType="System.String" DataSourceID="ObjectDataSource1">
                                                                 <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                        txtUrl.SetValue($('.selection[obj=\''+s.lastSuccessValue+'\']').attr('navi'));
                                                                        ASPxComboBox1.PerformCallback();

                                                                         updateTextFields();
                                                                     }" />
                                                             </dx:ASPxComboBox>
                                                             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetFrontPageLinks" TypeName="uniqco.Business.DataObjects.CompanySettings"></asp:ObjectDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Navigatin URL</td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtUrl" AutoCompleteType="Disabled" ClientInstanceName="txtUrl" runat="server" Width="245px" >

                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxComboBox ID="ASPxComboBox1" ClientVisible="false" ClientInstanceName="ASPxComboBox1" OnCallback="ASPxComboBox1_Callback" runat="server" Width="245px" TextField="Name" ValueField="Href" DataSourceID="ObjectDataSource2">
                                                                 <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                        txtUrl.SetValue(ASPxComboBox1.GetValue());
                                                                     }" />
                                                             </dx:ASPxComboBox>
                                                            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetStaticFrontPageLinks" TypeName="uniqco.Business.DataObjects.CompanySettings">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="txtSelected" Name="classification" PropertyName="Value" Type="String" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                        
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td><dx:ASPxButton runat="server" ID="btnFPSave" Text="Save" style="" Theme="SoftOrange"> </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" ID="btnFPClear" Text="Remove" style="" Theme="SoftOrange"> </dx:ASPxButton>
                                    </td>
                                                    </tr>
                                                </table>

                                            </div></td>
                                            <td >
                                    <div style="margin-left:100px;position:relative;">
            
            <dx:ASPxHyperLink ID="HypTriangle" CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="<div id='triangle-up'> <div style='position: absolute; top: 50px; left: 210px; color: white'>Fleet Policy</div> </div>" obj="Fleet Policy" style="color: white; text-decoration: none;font:15px Arial; font-weight: bold; " encodehtml="false">
                    </dx:ASPxHyperLink>
        
        <div class="spaces"></div>
            
            <dx:ASPxHyperLink ID="HypRectangle_1" CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="<div id='rectangle'>  <div style='text-align: center; padding-top: 10px; color: white'>Fleet Strategy</div> </div>" obj="Fleet Strategy" style="color: white; text-decoration: none;font:15px Arial; font-weight: bold; " encodehtml="false">
                    </dx:ASPxHyperLink>
        

        <div class="spaces"></div>
        <div class="graphicsection group">
            <div id="circleHolder">
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_1" CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="<div id='circle1'> <div style='text-align: center; padding-top: 40px; color: black'>Specify</div> </div>" obj="Specify" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                </div>
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_2" CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="<div id='circle2'> <div style='text-align: center; padding-top: 40px;'>Procure</div> </div>" obj="Procure" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                   
                </div>
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_3" CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="<div id='circle3'> <div style='text-align: center; padding-top: 40px;'>Operate</div> </div>" obj="Operate" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                </div>
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_4" CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="<div id='circle4'> <div style='text-align: center; padding-top: 40px;'>Dispose</div> </div>" obj="Dispose" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                </div>
            </div>
        </div>
        <div class="spaces"></div>
        <div id="rectangle1">
            <div style="padding-top: 10px; padding-left: 30px">
                <dx:ASPxHyperLink ID="HypRectangle_2"  CssClass="selection" NavigateUrl="javascript:void(0)" runat="server" text="Procedure, Behaviours <br /> & System" obj="Procedure, Behaviours & System" style="color: #072663; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
            </div>
            <img src="content/images/unifleet.png" style="position: absolute; top: 330px; left: 310px;" />
        </div>
    </div></td>
                                        </tr>
                                    </table>
                                  
      
                                </dx:ContentControl>
                                </ContentCollection>
                        </dx:TabPage>
                </TabPages>
        </dx:ASPxPageControl>

    </div>
        </asp:Content>