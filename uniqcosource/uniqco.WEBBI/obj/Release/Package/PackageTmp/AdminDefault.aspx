﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/content/UniqcoAdminMaster.master" CodeBehind="AdminDefault.aspx.vb" Inherits="uniqco.WEBBI.AdminDefault" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    

       <link href="/content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script src="/content/js/jquery-3.0.0.min.js"></script>
    <script src="/content/js/default.js"></script>
    <script>
        $(document).ready(function () {
            $('.selection').on('click', function () {
                if ($(this).attr('href')) {
                    var i = this.id;
                    if (i == 'HypCircle_1' || i == 'HypCircle_2' || i == 'HypCircle_3')
                        ASPxPopupLoading.Show();
                    loadingPanel.Show();
                }
            });
        });
    </script>
    <div>
                            <dx:ASPxPopupControl CloseAction="None" ClientInstanceName="ASPxPopupLoading" Width="100px" Height="50px"
        MaxWidth="100px" MaxHeight="50px" MinHeight="50px" MinWidth="100px" ID="ASPxPopupLoading"
        ShowFooter="false" ShowHeader="false"
        runat="server" EnableViewState="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" EnableHierarchyRecreation="True" 
                                             PopupAnimationType="None" CloseAnimationType="None" AllowDragging="false" AllowResize ="false" ShowCloseButton="false" >
                    
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:Panel ID="Panel2" runat="server">
                    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" Theme="SoftOrange"  Width="100px" Height="50px" ClientInstanceName="loadingPanel"  runat="server"></dx:ASPxLoadingPanel>
        
                </asp:Panel>
            </dx:PopupControlContentControl>
        </ContentCollection>                  
        </dx:ASPxPopupControl>
        <div class="divCenter">
            <%--<img src="content/images/unifleet.png" />--%>
       <%-- <div style="position: absolute; top: 0px; left: -50px; width: 300px;">
            <dx:ASPxBinaryImage ID="imgAdminlogo" runat="server" Height="40px"  >
                                     </dx:ASPxBinaryImage>
            
        </div>--%>
            
            <dx:ASPxHyperLink ID="HypTriangle" ClientInstanceName="FleetPolicy" Target="_blank" CssClass="selection"  NavigateUrl="" runat="server" text="<div id='triangle-up'> <div style='position: absolute; top: 50px; left: 210px; color: white'>Fleet Policy</div> </div>" style="color: white; text-decoration: none;font:15px Arial; font-weight: bold; " encodehtml="false">
                    </dx:ASPxHyperLink>
        
        <div class="spaces"></div>
            
            <dx:ASPxHyperLink ID="HypRectangle_1" ClientInstanceName="FleetStategy" Target="_blank" CssClass="selection" NavigateUrl="" runat="server" text="<div id='rectangle'>  <div style='text-align: center; padding-top: 10px; color: white'>Fleet Strategy</div> </div>" style="color: white; text-decoration: none;font:15px Arial; font-weight: bold; " encodehtml="false">
                    </dx:ASPxHyperLink>
        

        <div class="spaces"></div>
        <div class="graphicsection group">
            <div id="circleHolder">
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_1" ClientInstanceName="Specify" CssClass="selection" NavigateUrl="" runat="server" text="<div id='circle1'> <div style='text-align: center; padding-top: 40px; color: black'>Specify</div> </div>" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                </div>
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_2" ClientInstanceName="Procure" CssClass="selection" NavigateUrl="" runat="server" text="<div id='circle2'> <div style='text-align: center; padding-top: 40px;'>Procure</div> </div>" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                   
                </div>
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_3" ClientInstanceName="Operate" CssClass="selection" NavigateUrl="" runat="server" text="<div id='circle3'> <div style='text-align: center; padding-top: 40px;'>Operate</div> </div>" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                </div>
                <div class="col span_1_of_4">
                    
                    <dx:ASPxHyperLink ID="HypCircle_4" ClientInstanceName="Dispose" Target="_blank" CssClass="selection" NavigateUrl="" runat="server" text="<div id='circle4'> <div style='text-align: center; padding-top: 40px;'>Dispose</div> </div>" style="color: black; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
                </div>
            </div>
        </div>
        <div class="spaces"></div>
        <div id="rectangle1">
            <div style="padding-top: 10px; padding-left: 30px">
                <dx:ASPxHyperLink ID="HypRectangle_2" ClientInstanceName="PRS" Target="_blank" CssClass="selection" NavigateUrl="" runat="server" text="Procedure, Behaviours <br /> & System" style="color: #072663; text-decoration: none; font:15px Arial; font-weight: bold;" encodehtml="false">
                    </dx:ASPxHyperLink>
            </div>
            <img src="/content/images/unifleet.png" style="position: absolute; top: 330px; left: 310px;" />
        </div>
    </div>
    </div>

</asp:Content>


