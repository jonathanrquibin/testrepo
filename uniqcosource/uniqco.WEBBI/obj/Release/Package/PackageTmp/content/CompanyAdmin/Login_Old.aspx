﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Login_Old.aspx.vb" Inherits="uniqco.WEBBI.Login_Old" %>
<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
        <style type="text/css">
            .loginButton {
                float: right;
                margin-top:10px;
            }
            .loginContainer{
                left:40%;
                position:absolute;
                top:200px;

            }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div>
            <dx:ASPxRoundPanel ID="panelLoginDetails" CssClass="loginContainer" runat="server" Width="200px" EnableTheming="True" HeaderText="Please enter login details:" Theme="SoftOrange">
                <PanelCollection>
                    <dx:PanelContent runat="server">
                        <table>
                           
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Email:"></dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtUserName" AutoCompleteType="Email" runat="server" Width="170px"></dx:ASPxTextBox>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Password:"></dx:ASPxLabel>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtPassword" Password="true" runat="server" Width="170px"></dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <dx:ASPxButton ID="btnLogin" CssClass="loginButton" runat="server" Width="100px" Text="Login" Theme="SoftOrange"></dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxRoundPanel>
        </div>

</asp:Content>
