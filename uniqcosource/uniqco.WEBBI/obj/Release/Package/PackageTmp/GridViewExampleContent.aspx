﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GridViewExampleContent.aspx.vb" Inherits="uniqco.WEBBI.GridViewExampleContent" %>

<%@ Register Assembly="DevExpress.XtraCharts.v15.1.Web, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v15.1.Web, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web.Designer" TagPrefix="dxchartdesigner" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <script src="content/js/jquery-3.0.0.min.js"></script>
        <script type="text/javascript">


            function resizeThings() {
                //$('#containerDIV').height($(window).height() - 180);
                mainGrid.SetHeight($(window).height() - 20);
            }

            $(window).resize(function () {
                resizeThings();
            });

            $(document).ready(function () {
                resizeThings();
            })


        </script>

    <div>
        <dx:ASPxGridView Width="100%"  
            ID="ASPxGridView2" 
            runat="server" 
            ClientInstanceName="mainGrid"
            AutoGenerateColumns="False" 
            DataSourceID="odsRiskComposite" 
            KeyFieldName="ID"
            Theme="SoftOrange" 
            EnableTheming="True">

            <SettingsContextMenu EnableColumnMenu="True"></SettingsContextMenu>

            <SettingsPager AlwaysShowPager="True"></SettingsPager>

            <Settings ShowGroupPanel="True" ShowFilterRow="True" HorizontalScrollBarMode="Visible" ShowFooter="True" ShowGroupFooter="VisibleAlways" ShowHeaderFilterButton="True" VerticalScrollBarMode="Visible"></Settings>
            <SettingsBehavior AllowFocusedRow="True"></SettingsBehavior>

            <SettingsSearchPanel Visible="True"></SettingsSearchPanel>
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowClearFilterButton="True" VisibleIndex="0"></dx:GridViewCommandColumn>
                                                <dx:GridViewDataDateColumn FieldName="ReportDate" VisibleIndex="1">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="PlantNumber" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ParentPlant" VisibleIndex="3"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FleetNumber" VisibleIndex="4"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AssetNumber" VisibleIndex="5"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Category" VisibleIndex="6"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Group" VisibleIndex="7"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Type" VisibleIndex="8"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Make" VisibleIndex="9"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Model" VisibleIndex="10"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ModelYear" VisibleIndex="11"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="RegoNo" VisibleIndex="12"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="RegoDate" VisibleIndex="13">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="RegoState" VisibleIndex="14"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="MeterType" VisibleIndex="15"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="CurrentAsset" VisibleIndex="16"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="PlantImage" VisibleIndex="17"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="VINNumber" VisibleIndex="18"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="EngineNumber" VisibleIndex="19"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ChassisNumber" VisibleIndex="20"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BodyNumber" VisibleIndex="21"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="BuildDate" VisibleIndex="22">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="PreviousRego" VisibleIndex="23"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="RadioSerialNo" VisibleIndex="24"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="RadioCallSign" VisibleIndex="25"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="MiscInformationDoc" VisibleIndex="26"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Region" VisibleIndex="27"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BusinessUnit" VisibleIndex="28"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Department" VisibleIndex="29"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Location" VisibleIndex="30"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="OperatorDriver" VisibleIndex="31"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="UsageType" VisibleIndex="32"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Supervisor" VisibleIndex="33"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="SubjectToFBT" VisibleIndex="34"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ChargeAccount" VisibleIndex="35"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="PurchaseOrLease" VisibleIndex="36"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="PurchaseDate" VisibleIndex="37">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="AnnualPredictedIncomeUnits" VisibleIndex="38"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AnnualBudgetIncomeUnits" VisibleIndex="39"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="IncomeUnitsVar" VisibleIndex="40"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="KmsHrsVar" VisibleIndex="41"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AnnualAveKms" VisibleIndex="42"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AnnualAveHrs" VisibleIndex="43"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AnnualBudgetKms" VisibleIndex="44"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AnnualBudgetHrs" VisibleIndex="45"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Hours" VisibleIndex="46"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LitresHr" VisibleIndex="47"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ExpectedFuelEconHr" VisibleIndex="48"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LitresPerHour" VisibleIndex="49"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FuelTypeHr" VisibleIndex="50"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Kilometre" VisibleIndex="51"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LitresKms" VisibleIndex="52"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ExpectedFuelEconKm" VisibleIndex="53"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LitresPer100Kms" VisibleIndex="54"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FuelTypeKms" VisibleIndex="55"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Deviation" VisibleIndex="56"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="OverheadCost" VisibleIndex="57"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FuelCost" VisibleIndex="58"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="MaintenanceCost" VisibleIndex="59"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FBTCost" VisibleIndex="60"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LeaseCost" VisibleIndex="61"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="MonthlyCharge" VisibleIndex="62"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="HireCharge" VisibleIndex="63"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="TimeSheetHrs" VisibleIndex="64"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="CostSubTotal" VisibleIndex="65"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="IncomeSubTotal" VisibleIndex="66"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ProfitLoss" VisibleIndex="67"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ReserveOverRecovery" VisibleIndex="68"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="CostVariation" VisibleIndex="69"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BudgetReplacementPrice" VisibleIndex="70"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BudgetReplacementPriceExcGST" VisibleIndex="71"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BudgetDisposalPrice" VisibleIndex="72"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BudgetDisposalPriceExcGST" VisibleIndex="73"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="BudgetChangeoverExcGST" VisibleIndex="74"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="OptimumReplacementDate" VisibleIndex="75">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="CurrentKmsHrs" VisibleIndex="76"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DowntimeHrs" VisibleIndex="77"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LabourHoursNonSched" VisibleIndex="78"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LabourHoursSched" VisibleIndex="79"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LabourHoursAdmin" VisibleIndex="80"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DowntimeVariation" VisibleIndex="81"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="LastSafetyServiceDate" VisibleIndex="82">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn FieldName="NextSafetyServiceDue" VisibleIndex="83">
                                                    <PropertiesDateEdit>
                                                        <TimeSectionProperties>
                                                            <TimeEditProperties>
                                                                <ClearButton Visibility="Auto"></ClearButton>
                                                            </TimeEditProperties>
                                                        </TimeSectionProperties>

                                                        <ClearButton Visibility="Auto"></ClearButton>
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="FlatRate" VisibleIndex="84"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="LabourHrs" VisibleIndex="85"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FlatRateVar" VisibleIndex="86"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DowntimeHours" VisibleIndex="87"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="TotalNWT" VisibleIndex="88"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="TotalMaintenance" VisibleIndex="89"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="NWTVar" VisibleIndex="90"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="AssetDataRisk" VisibleIndex="91"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FuelDataRisk" VisibleIndex="92"></dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="CostDataRisk" VisibleIndex="93"></dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>

                                        <asp:ObjectDataSource runat="server" ID="odsRiskComposite" SelectMethod="GetFromDB" TypeName="uniqco.Business.DataObjects.RiskComposite"></asp:ObjectDataSource>
                           
    </div>
    </form>
</body>
</html>
