﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="GridViewExample.aspx.vb" Inherits="uniqco.WEBBI.GridViewExample" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <script src="content/js/jquery-3.0.0.min.js"></script>

    <style type="text/css">
       
    </style>

    <script type="text/javascript">


        function resizeThings() {
            $('#containerDIV').height($(window).height() - 100);
        }

        $(window).resize(function () {
            resizeThings();
        });

        $(document).ready(function () {
            resizeThings();
        })


    </script>

    <iframe id="containerDIV" frameBorder="0" src="GridViewExampleContent.aspx"></iframe>


</asp:Content>
