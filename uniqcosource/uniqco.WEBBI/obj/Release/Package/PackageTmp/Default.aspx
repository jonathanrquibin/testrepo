﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="Default.aspx.vb" Inherits="uniqco.WEBBI.FrontPage" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
    
        <link href="content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script src="content/js/jquery-3.0.0.min.js"></script>
    <script src="content/js/default.js"></script>
    <script>
        document.location = "/content/companyadmin/login.aspx";

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
 <%--<div class="admin-header" style="margin:10px;width:100%;">
     <div style="float:right;display:inline-block;margin-right:10px">
         <a id="hypLogin"  style=" margin: 25px;  color: #002060;font-size:15px;cursor:pointer; float: right;" href="/content/companyadmin/login.aspx">Login</a>
          
                </div>
     
     </div>--%>
 
        <div class="divCenter">
        <div style="position: absolute; top: 0px; left: -50px; width: 300px;">
            <img src="/content/images/unifleet.png" />
        </div>
        <a style="color: white; text-decoration: none;" href="PolicyTemplate.aspx">
            <div id="triangle-up">
                <div style="position: absolute; top: 50px; left: 210px; color: white">Fleet Policy</div>
            </div>
        </a>
        <div class="spaces"></div>
        <a style="color: white; text-decoration: none;" href="ManagementPlanTemplate.aspx">
            <div id="rectangle">
                <div style="text-align: center; padding-top: 10px; color: white">Fleet Strategy</div>
            </div>
        </a>
        <div class="spaces"></div>
        <div class="graphicsection group">
            <div id="circleHolder">
                <div class="col span_1_of_4">
                    <a style="color: black; text-decoration: none;" href="LightFleetComparisonTool.aspx">
                        <div id="circle1">
                            <div style="text-align: center; padding-top: 40px; color: black">Specify</div>
                        </div>
                    </a>
                </div>
                <div class="col span_1_of_4">
                    <a style="color: black; text-decoration: none;" href="AssetRegistrationTool.aspx">
                        <div id="circle2">
                            <div style="text-align: center; padding-top: 40px;">Procure</div>
                        </div>
                    </a>
                </div>
                <div class="col span_1_of_4">
                    <a style="color: black; text-decoration: none;" href="AssetRegistrationTool.aspx">
                        <div id="circle3">
                            <div style="text-align: center; padding-top: 40px;">Operate</div>
                        </div>
                    </a>
                </div>
                <div class="col span_1_of_4">
                    <a style="color: black; text-decoration: none;" href="DisposalTemplate.aspx">
                        <div id="circle4">
                            <div style="text-align: center; padding-top: 40px;">Dispose</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="spaces"></div>
        <div id="rectangle1">
            <div style="padding-top: 10px; padding-left: 30px">
                <a style="color: #072663; text-decoration: none;" href="AdministrationOfTheirAssetDatabase.aspx">Procedure, Behaviours
                <br />
                    & System</a>
            </div>
            <img src="content/images/unifleet.png" style="position: absolute; top: 330px; left: 310px;" />
        </div>
    </div>

</asp:Content>
