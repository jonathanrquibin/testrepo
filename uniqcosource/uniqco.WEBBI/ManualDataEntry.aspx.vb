﻿Imports System.Drawing
Imports DevExpress.Web
Imports System.Web.Script.Serialization

Public Class ManualDataEntry
    Inherits System.Web.UI.Page

    Private Column1, Column2, Column3, Column4, Column5, Column6, Column7, Column8, Column9, Column10, Column11 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SessionUserID.Text = SessionHelper.UserLogin.UserName
        If Not IsPostBack Then
            Me.RiskCompositePage.Text = uniqco.Business.DataObjects.usp_GetManualOverrideData.GetRiskCompositePages()
        End If
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function GetPopupData(ByVal asset As String, ByVal nameval As String)
        Dim compId = 0
        Dim cc = DirectCast(HttpContext.Current.Session("MADsource"), List(Of Business.DataObjects.MobileAssetData))
        Dim dt = DirectCast(HttpContext.Current.Session("MADsource"), List(Of Business.DataObjects.MobileAssetData)).Where(Function(x) x.Name = nameval.Trim()).Single
        If asset.Trim() = "Asset1" Then
            compId = dt.Asset1CompositeId
        ElseIf asset.Trim() = "Asset2" Then
            compId = dt.Asset2CompositeId
        ElseIf asset.Trim() = "Asset3" Then
            compId = dt.Asset3CompositeId
        ElseIf asset.Trim() = "Asset4" Then
            compId = dt.Asset4CompositeId
        ElseIf asset.Trim() = "Asset5" Then
            compId = dt.Asset5CompositeId
        End If
        Dim retval = Business.DataObjects.MobileAssetData.GetForPopup(compId, nameval.Trim())

        Return retval
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If SessionHelper.UserLogin Is Nothing Then
            Dim queryStr As String = Request.Url.Query
            queryStr = String.Format("~/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)
            Response.Redirect(queryStr)
        Else
            If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
                Me.MasterPageFile = "~/content/UniqcoAdminMaster.master"
            ElseIf SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                Me.MasterPageFile = "~/content/CompanyAdminMaster.master"
            Else
                Throw New Exception("ERROR! Failed to load Master Page.")
            End If
        End If
    End Sub
    Protected Sub ASPxGridView1_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "Name" Then
            e.Cell.Font.Bold = True
            If Not (e.CellValue = "Make" Or e.CellValue = "Model" Or e.CellValue = "Year") Then
                e.Cell.BackColor = Color.LightGray
            End If
        End If
    End Sub

    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs)

    End Sub

    Protected Sub odsMAD_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        e.InputParameters("Make") = ASPxTextBox1.Text
        e.InputParameters("com") = SessionHelper.UserLogin.CompanyID
    End Sub

    Protected Sub odsMAD_Updating(sender As Object, e As ObjectDataSourceMethodEventArgs)
        Dim c = DirectCast(e.InputParameters("c"), Business.DataObjects.MobileAssetData)
        Dim dt = DirectCast(HttpContext.Current.Session("MADsource"), List(Of Business.DataObjects.MobileAssetData)).Where(Function(x) x.Name = c.Name).Single

        c.Asset1ManualId = dt.Asset1ManualId
        c.Asset2ManualId = dt.Asset2ManualId
        c.Asset3ManualId = dt.Asset3ManualId
        c.Asset4ManualId = dt.Asset4ManualId
        c.Asset5ManualId = dt.Asset5ManualId
        c.Asset1CompositeId = dt.Asset1CompositeId
        c.Asset2CompositeId = dt.Asset2CompositeId
        c.Asset3CompositeId = dt.Asset3CompositeId
        c.Asset4CompositeId = dt.Asset4CompositeId
        c.Asset5CompositeId = dt.Asset5CompositeId

        c.CompanyId = SessionHelper.UserLogin.CompanyID

        If c.Asset1CompositeId = 0 And c.Asset2CompositeId = 0 And c.Asset3CompositeId = 0 And c.Asset3CompositeId = 0 And
            c.Asset5CompositeId = 0 Then
            e.Cancel = True
        End If

    End Sub
    Protected Sub ImageHyperLink_Load(sender As Object, e As EventArgs)
        Dim urlFile As String = "http://" + HttpContext.Current.Request.Url.Authority + "/files/"
        Dim hLink As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim grd As GridViewDataItemTemplateContainer = TryCast(hLink.NamingContainer, GridViewDataItemTemplateContainer)
        If (Not String.IsNullOrWhiteSpace(grd.Text)) AndAlso (Not String.IsNullOrWhiteSpace(grd.Text)) Then

            hLink.NavigateUrl = urlFile + grd.Text
        End If
    End Sub
    Protected Sub ImageUpload_FileUploadComplete(ByVal sender As Object, ByVal e As DevExpress.Web.FileUploadCompleteEventArgs)
        If e.IsValid Then
            Dim fileType As String = e.UploadedFile.FileName.Split(".")(1).ToString()
            Dim fileName As String = Guid.NewGuid().ToString("N") + "." + fileType
            Dim path As String = ""

            HttpContext.Current.Session("ImageLocByByteArray") = e.UploadedFile.FileBytes

            e.CallbackData = e.UploadedFile.FileName.ToString()
        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        ASPxGridView1.DataBind()
        HttpContext.Current.Session("MADsource") = odsMAD.Select()
    End Sub

    Protected Sub odsAsset_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        e.InputParameters("com") = SessionHelper.UserLogin.CompanyID
    End Sub

    Protected Sub odsCFGrid_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        e.InputParameters("com") = SessionHelper.UserLogin.CompanyID
        e.InputParameters("cf") = btnCF.Value
    End Sub

    Protected Sub odsCFGrid_Inserting(sender As Object, e As ObjectDataSourceMethodEventArgs)
        If btnCF.Value Is Nothing Then
            e.Cancel = True
        Else
            Dim c = DirectCast(e.InputParameters("x"), Business.DataObjects.CommonFactors)
            c.CompanyID = SessionHelper.UserLogin.CompanyID
            c.CommonFactors = btnCF.Value
        End If

    End Sub


    Protected Sub ASPxGridView3_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)
        ASPxGridView3.DataBind()
    End Sub

    Protected Sub ASPxGridView1_HtmlRowPrepared(sender As Object, e As ASPxGridViewTableRowEventArgs)
        If e.RowType = GridViewRowType.Data Then
            Dim name = e.GetValue("Name")
            If name = "Make" Or name = "Model" Or name = "Year" Then
                e.Row.BackColor = Color.Gray
                e.Row.ForeColor = Color.White
                e.Row.CssClass &= " cursor-hand"
                'e.Row.Font.Bold = True
            End If
        End If
    End Sub

    Protected Sub ManualOverrideDataGridView_HtmlRowPrepared(sender As Object, e As ASPxGridViewTableRowEventArgs)
        If e.RowType = GridViewRowType.Data Then
            Dim RowName = e.GetValue("ColumnHeaders")
            If RowName = "Make" Or RowName = "Model" Or RowName = "ModelYear" Then
                e.Row.BackColor = Color.LightGreen
            End If
            If RowName = "PK" Or RowName = "PlantNumber" Then
                e.Row.Style(HtmlTextWriterStyle.Display) = "none"
            End If
        End If
    End Sub

    Protected Sub ManualOverrideDataGridView_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs)
        'Dim colIdx As String = "col_idx_" + e.DataColumn.Index.ToString()
        'Dim rowIdx As String = "row_idx_" + e.VisibleIndex.ToString()
        'e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "');")
        'e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "');")
        'e.Cell.CssClass += " " + colIdx + " " + rowIdx

        If e.VisibleIndex.Equals(0) Then
            Select Case e.DataColumn.FieldName
                Case "Column1"
                    Column1 = e.CellValue
                Case "Column2"
                    Column2 = e.CellValue
                Case "Column3"
                    Column3 = e.CellValue
                Case "Column4"
                    Column4 = e.CellValue
                Case "Column5"
                    Column5 = e.CellValue
                Case "Column6"
                    Column6 = e.CellValue
                Case "Column7"
                    Column7 = e.CellValue
                Case "Column8"
                    Column8 = e.CellValue
                Case "Column9"
                    Column9 = e.CellValue
                Case "Column10"
                    Column10 = e.CellValue
                Case "Column11"
                    Column11 = e.CellValue
            End Select
        Else
            Select Case e.DataColumn.FieldName
                Case "Column1"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column1 & "|" & e.CellValue))
                Case "Column2"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column2 & "|" & e.CellValue))
                Case "Column3"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column3 & "|" & e.CellValue))
                Case "Column4"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column4 & "|" & e.CellValue))
                Case "Column5"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column5 & "|" & e.CellValue))
                Case "Column6"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column6 & "|" & e.CellValue))
                Case "Column7"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column7 & "|" & e.CellValue))
                Case "Column8"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column8 & "|" & e.CellValue))
                Case "Column9"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column9 & "|" & e.CellValue))
                Case "Column10"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column10 & "|" & e.CellValue))
                Case "Column11"
                    e.Cell.Attributes.Add("onmousedown", String.Format("onCellClick({0}, '{1}', '{2}', event)", e.VisibleIndex, e.DataColumn.FieldName, e.KeyValue.ToString() & "|" & Column11 & "|" & e.CellValue))
            End Select
        End If
        Dim strRowName = GetGridRowName(e.VisibleIndex)
        Dim colIdx As String = "col_idx_" + e.DataColumn.Index.ToString()
        Dim rowIdx As String = "row_idx_" + e.VisibleIndex.ToString()
        If Not strRowName.Equals("") Then
            Select Case e.DataColumn.FieldName
                Case "Column1"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column1), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                    
                Case "Column2"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column2), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column3"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column3), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column4"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column4), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column5"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column5), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column6"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column6), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column7"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column7), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column8"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column8), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column9"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column9), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column10"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column10), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
                Case "Column11"
                    Dim xxx = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(Convert.ToInt32(Column11), strRowName)
                    If xxx.Count = 2 Then
                        e.Cell.BackColor = Color.Yellow
                    ElseIf xxx.Count > 2 Then
                        e.Cell.BackColor = Color.Red
                    End If
                    e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "','" + xxx.Count.ToString() + "');")
                    e.Cell.CssClass += " " + colIdx + " " + rowIdx
            End Select
        Else
            e.Cell.Attributes.Add("onmouseover", "OnCellOver(this, '" + colIdx + "', '" + rowIdx + "');")
            e.Cell.Attributes.Add("onmouseout", "OnCellOut(this, '" + colIdx + "', '" + rowIdx + "');")
            e.Cell.CssClass += " " + colIdx + " " + rowIdx
        End If

    End Sub

    Protected Sub ManualOverrideDataGridView_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)
        Dim parameter As String = e.Parameters
        Dim param As Integer = Integer.Parse(parameter)
        ManualOverrideDataGridView.DataSource = uniqco.Business.DataObjects.usp_GetManualOverrideData.GetAllByParameter(param)
        ManualOverrideDataGridView.DataBind()
    End Sub

    Private Function GetGridRowName(rowIndex As Integer) As String
        Dim strRet As String = ""
        Select Case rowIndex
            Case 2
                strRet = "Make"
            Case 3
                strRet = "Model"
            Case 4
                strRet = "ModelYear"
            Case 5
                strRet = "AnnualPredictedIncomeUnits"
            Case 6
                strRet = "AnnualBudgetIncomeUnits"
            Case 7
                strRet = "IncomeUnitsVar"
            Case 8
                strRet = "KmsHrsVar"
            Case 9
                strRet = "AnnualAveKms"
            Case 10
                strRet = "AnnualAveHrs"
            Case 11
                strRet = "AnnualBudgetKms"
            Case 12
                strRet = "AnnualBudgetHrs"
        End Select
        Return strRet
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function DataEntryOverrideValue(value As String, overrideLabel As String, overrideId As String, userID As String)
        Dim modeHistory As New uniqco.Business.DataObjects.ManualOverrideDataEntryHistory()
        modeHistory.PK = overrideId
        modeHistory.OverrideLabel = overrideLabel
        modeHistory.OverrideValue = value
        modeHistory.OverrideDate = DateTime.Now()
        modeHistory.UserID = userID
        uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.Create(modeHistory)
        Return Nothing
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function DataEntryOverrideGetValueHistory(overrideId As String, overrideLabel As String)
        Dim modeHistoryList = uniqco.Business.DataObjects.ManualOverrideDataEntryHistory.GetByPkAndLabel(overrideId, overrideLabel)
        Return modeHistoryList
    End Function

    'Protected Sub ManualOverrideDataGridView_DataBinding(sender As Object, e As EventArgs)
    '    TryCast(sender, ASPxGridView).DataSource = HttpContext.Current.Session("manualoverride")
    'End Sub
    Private copiedValues As Hashtable = Nothing
    Private copiedFields() As String = {"AssetId", "Make", "Group", "Type", "Model", "Model_Series_Base", "Model_Series_Level", "Model_Series_Vin", "Model_ID",
                                         "Type_Meter", "Type_Transmission", "Type_Body", "Type_Drive", "Type_VehicleBody", "Type_Value", "Type_Image", "Fuel_Type",
                                         "Category", "EngineCapacity", "Weight", "GVM", "GCM", "MaxTowingCapacity", "Tyre_Track_Size_Front_Left", "Tyre_Track_Number_Front_Left",
                                         "Tyre_Track_Size_Rear_Right", "Tyre_Track_Number_Rear_Right", "Tyre_Track_Life_Meter", "Warranty_yrs", "Warranty_Meter",
                                         "Average_Replacement_Yrs", "Average_Replacement_Meter", "Average_Annual_Meter", "AveragePrice_Today", "DisposalPrice_OptimumPct",
                                         "DisposalPrice_Year1Pct", "DisposalPrice_Year2Pct", "DisposalPrice_Year3Pct", "DisposalPrice_Year4Pct", "DisposalPrice_Year5Pct",
                                         "Maint_Cost_Per_Meter", "Fuel_Cons_Meter", "Fuel_Rebate_Status", "Fuel_Rebate_Rate", "CO2_Output", "Air_Pollution_Rating",
                                         "ANCAP_Crash_Rating", "Manufacturers_First_Service_Meter", "Manufacturers_First_Service_Months", "Manufacturers_First_Service_OnceOnly",
                                         "Tyre_Rotation_Meter", "Inspection_Interval_Months", "Manufacturers_Service_Meter", "Manufacturers_Service_Months",
                                         "Safety_Insp_Meter", "Safety_Insp_Months", "Exclude_Fuel_Consumption", "In_Service_Program"}
    Protected Sub ManualDataEntryGridView_CustomButtonCallback(sender As Object, e As ASPxGridViewCustomButtonCallbackEventArgs)
        If e.ButtonID <> "Copy" Then
            Return
        End If
        copiedValues = New Hashtable()
        For Each fieldName As String In copiedFields
            copiedValues(fieldName) = ManualDataEntryGridView.GetRowValues(e.VisibleIndex, fieldName)
            If fieldName.Equals("AssetId") Then
                HttpContext.Current.Session("AssetId") = copiedValues(fieldName)
            End If
        Next fieldName
        ManualDataEntryGridView.AddNewRow()
    End Sub

    Protected Sub ManualDataEntryGridView_InitNewRow(sender As Object, e As Data.ASPxDataInitNewRowEventArgs)
        If copiedValues Is Nothing Then
            Return
        End If
        For Each fieldName As String In copiedFields
            e.NewValues(fieldName) = copiedValues(fieldName)
        Next fieldName
    End Sub

    Protected Sub ManualDataEntryGridView_CustomErrorText(sender As Object, e As ASPxGridViewCustomErrorTextEventArgs)
        If e.Exception IsNot Nothing Then
            e.ErrorText = e.Exception.InnerException.Message
        End If
    End Sub
End Class