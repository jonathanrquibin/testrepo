﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/content/UniqcoAdminMaster.master" CodeBehind="UniqcoAdminMain.aspx.vb" Inherits="uniqco.WEBBI.UniqcoAdminMain" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script src="/content/js/jquery-3.0.0.min.js"></script>
    <%--<script src="content/js/jquery-ui.js"></script>
        <link href="content/css/jquery-ui.css" rel="stylesheet" />--%>
    <script>
        $(function () {
            //$("#tab-1,#tab-2,#tab-3,#tab-4").height($(document).height() - 125);
        });
        $(window).resize(function () {
            //$("#tab-1,#tab-2,#tab-3,#tab-4").height($(document).height() - 125);
            //ASPxGridView1.SetHeight($(document).height() - 125);
            //ASPxGridView2.SetHeight($(document).height() - 125);
            //ASPxGridView3.SetHeight($(document).height() - 125);
        });
        function OnInit(s, e) {
            //AdjustSize(s);
        }
        function OnEndCallback(s, e) {
            //AdjustSize(s);
        }
        function AdjustSize(s) {
            //s.SetHeight($(document).height() - 125);
        }
    </script>

    <div>
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Theme="SoftOrange" Width="100%">
            <TabPages>
                <dx:TabPage Name="Company Management" Text="Company Management">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-1" style="overflow: auto">

                                <dx:ASPxGridView KeyFieldName="CompanyID" ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="odsCompanies" EnableTheming="True" Theme="SoftOrange" Width="100%">

                                    <SettingsPager PageSize="20">
                                        <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                    <SettingsSearchPanel Visible="True" />
                                    <SettingsEditing Mode="PopupEditForm">
                                    </SettingsEditing>
                                    <SettingsPopup EditForm-HorizontalAlign="WindowCenter" CustomizationWindow-VerticalAlign="WindowCenter">

                                        <EditForm HorizontalAlign="WindowCenter"></EditForm>

                                        <CustomizationWindow VerticalAlign="WindowCenter"></CustomizationWindow>

                                    </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="CompanyID" ShowInCustomizationForm="True" VisibleIndex="2" Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="3" Caption="Company Name">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Address" ShowInCustomizationForm="True" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PhoneNumber" ShowInCustomizationForm="True" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataBinaryImageColumn FieldName="CompanyLogo" ShowInCustomizationForm="True" VisibleIndex="1" Width="200px">
                                            <PropertiesBinaryImage ImageWidth="200px">
                                                <EditingSettings Enabled="True">
                                                </EditingSettings>
                                            </PropertiesBinaryImage>
                                        </dx:GridViewDataBinaryImageColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                                <asp:ObjectDataSource ID="odsCompanies" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.Company" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.Company" UpdateMethod="Update"></asp:ObjectDataSource>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="User Management" Text="User Management">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-2" style="overflow: auto">

                                <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" KeyFieldName="LoginID" runat="server" AutoGenerateColumns="False" DataSourceID="odsLogin" EnableTheming="True" Theme="SoftOrange" Width="100%">

                                    <SettingsPager PageSize="20">
                                        <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                    <SettingsSearchPanel Visible="True" />

                                    <SettingsEditing Mode="PopupEditForm">
                                    </SettingsEditing>
                                    <SettingsPopup EditForm-HorizontalAlign="WindowCenter" CustomizationWindow-VerticalAlign="WindowCenter">

                                        <EditForm HorizontalAlign="WindowCenter"></EditForm>

                                        <CustomizationWindow VerticalAlign="WindowCenter"></CustomizationWindow>

                                    </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="LoginID" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="UserName" ShowInCustomizationForm="True" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Password" ShowInCustomizationForm="True" VisibleIndex="5" Visible="True" PropertiesTextEdit-Password="true">
                                            <PropertiesTextEdit Password="True"></PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataCheckColumn FieldName="Active" ShowInCustomizationForm="True" VisibleIndex="7" Width="100px">
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="Rights" ShowInCustomizationForm="True" VisibleIndex="6">
                                            <PropertiesComboBox DataSourceID="odsloginRights">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn FieldName="EmailAddress" ShowInCustomizationForm="True" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="CompanyID" ShowInCustomizationForm="True" VisibleIndex="2" Caption="Company Name">
                                            <PropertiesComboBox DataSourceID="odsloginCompanies" TextField="Name" ValueField="CompanyID">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                                <asp:ObjectDataSource ID="odsLogin" runat="server" DataObjectTypeName="uniqco.Business.DataObjects.Login" DeleteMethod="Delete" InsertMethod="Create" SelectMethod="GetAllFromUser" TypeName="uniqco.Business.DataObjects.Login" UpdateMethod="Update">
                                    <SelectParameters>
                                        <asp:Parameter Name="login" Type="Object" />
                                        <asp:Parameter DefaultValue="true" Name="rights" Type="Boolean" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <asp:ObjectDataSource ID="odsloginRights" runat="server" SelectMethod="GetRightsList" TypeName="uniqco.Business.DataObjects.Login">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="true" Name="isUniqco" Type="Boolean" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Safety Incidents" Text="Safety Incidents">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <div id="tab-3" style="overflow: auto">

                                <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="ASPxGridView3" runat="server" KeyFieldName="IncidentId" AutoGenerateColumns="False" EnableTheming="True" Theme="SoftOrange" Width="100%" DataSourceID="odsSafetyIncident">

                                    <SettingsPager PageSize="20">
                                        <PageSizeItemSettings Visible="true" />
                                    </SettingsPager>
                                    <SettingsSearchPanel Visible="True" />

                                    <SettingsEditing Mode="PopupEditForm">
                                    </SettingsEditing>
                                    <SettingsPopup EditForm-HorizontalAlign="WindowCenter" CustomizationWindow-VerticalAlign="WindowCenter">

                                        <EditForm HorizontalAlign="WindowCenter"></EditForm>

                                        <CustomizationWindow VerticalAlign="WindowCenter"></CustomizationWindow>

                                    </SettingsPopup>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="IncidentId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn FieldName="CompanyID" ShowInCustomizationForm="True" VisibleIndex="2" Caption="Company Name">
                                            <PropertiesComboBox DataSourceID="odsloginCompanies" TextField="Name" ValueField="CompanyID">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataDateColumn FieldName="Date" Caption="Incident Date" ShowInCustomizationForm="True" VisibleIndex="2">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataSpinEditColumn FieldName="Number" Caption="No. of Incidents" ShowInCustomizationForm="True" VisibleIndex="3">
                                        </dx:GridViewDataSpinEditColumn>
                                    </Columns>
                                </dx:ASPxGridView>

                                <asp:ObjectDataSource ID="odsSafetyIncident" runat="server" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.SafetyIncident" DataObjectTypeName="uniqco.Business.DataObjects.SafetyIncident" DeleteMethod="Delete" InsertMethod="Create" UpdateMethod="Update"></asp:ObjectDataSource>

                            </div>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>

        </dx:ASPxPageControl>
        <asp:ObjectDataSource ID="odsloginCompanies" runat="server" SelectMethod="GetAll" TypeName="uniqco.Business.DataObjects.Company"></asp:ObjectDataSource>


    </div>

</asp:Content>
