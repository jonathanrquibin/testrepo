﻿Imports uniqco.Business

Public Class AdminDefault
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If SessionHelper.UserLogin IsNot Nothing Then
                Dim UserCompany As DataObjects.Company = DataObjects.Company.GetFromUser(SessionHelper.UserLogin)
                'imgAdminlogo.Value = UserCompany.CompanyLogo
            End If
            loadFields()
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If SessionHelper.UserLogin Is Nothing Then
            Dim queryStr As String = Request.Url.Query
            queryStr = String.Format("~/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)
            Response.Redirect(queryStr)
        Else
            If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
                Me.MasterPageFile = "~/content/UniqcoAdminMaster.master"
            ElseIf SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                Me.MasterPageFile = "~/content/CompanyAdminMaster.master"
            Else
                Throw New Exception("ERROR! Failed to load Master Page.")
            End If
        End If
    End Sub

    Private Sub loadFields()
        Dim Settings As List(Of DataObjects.CompanySettings) = DataObjects.CompanySettings.GetAllForUserCompany(SessionHelper.UserLogin)

        For Each ss As DataObjects.CompanySettings In Settings
            If ss.Attribute = "href" Then
                Select Case ss.ObjectName
                    Case "Fleet Policy"
                        HypTriangle.Attributes("href") = ss.Value

                    Case "Fleet Strategy"
                        HypRectangle_1.Attributes("href") = ss.Value
                    Case "Specify"
                        HypCircle_1.NavigateUrl = ss.Value
                    Case "Procure"
                        HypCircle_2.NavigateUrl = ss.Value
                    Case "Operate"
                        HypCircle_3.NavigateUrl = ss.Value
                    Case "Dispose"
                        HypCircle_4.NavigateUrl = ss.Value
                    Case "Procedure, Behaviours & System"
                        HypRectangle_2.NavigateUrl = ss.Value
                End Select
            End If
        Next
    End Sub
End Class