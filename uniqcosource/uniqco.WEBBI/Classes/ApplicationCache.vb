﻿Public Class ApplicationCache

    'this is a wrapper class, all of the initial caching is done in the global.asax application_start method

    Public Shared Property usp_AmalgamatedRAGChart_v3_DataTable As DataTable
        Get
            Return HttpContext.Current.Application("usp_AmalgamatedRAGChart_v3_DataTable")
        End Get
        Set(value As DataTable)
            HttpContext.Current.Application("usp_AmalgamatedRAGChart_v3_DataTable") = value
        End Set
    End Property

    Public Shared Function SetRagChartDataTable(value As Dictionary(Of String, DataTable))

        HttpContext.Current.Application("GetRagChartDataTable") = value

    End Function


    Public Shared Function GetRagChartDataTable(companyID As Guid, repDate As Date) As DataTable

        Dim dict As Dictionary(Of String, DataTable) = HttpContext.Current.Application("GetRagChartDataTable")

        Dim key As String = String.Format("{0},{1}", companyID.ToString, CStr(repDate))

        'if contains key, then add it 
        If Not dict.ContainsKey(key) Then

            Dim dataTable As DataTable = Business.DataObjects.RiskComposite.GetForRAGChart(companyID, repDate)

            dict.Add(key, dataTable)

            'store this new query so that we will cache the result again when we restart IIS
            Business.DataObjects.CacheSetting.UpsertCacheSetting("GetRagChartDataTable", dict.Keys.ToList)

            SetRagChartDataTable(dict)
        End If

        Return dict.Item(key)

    End Function

End Class
