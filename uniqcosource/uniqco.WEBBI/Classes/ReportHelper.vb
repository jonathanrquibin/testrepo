﻿Namespace reporting



    Public Class ReportHelper

        Public Shared Function GetBarChartData() As List(Of Business.DataObjects.BarChartExampleLine)

            Dim grpStr As String = Web.HttpContext.Current.Session("grpStr")

            If String.IsNullOrEmpty(grpStr) Then grpStr = "Light Fleet"

            Dim retObj As List(Of Business.DataObjects.BarChartExampleLine) = _
                            Business.DataObjects.BarChartExampleLine.GetForGroup(grpStr)

            Return retObj

        End Function

    End Class


End Namespace
