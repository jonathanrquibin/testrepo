﻿

Namespace WCFReturnClasses

    <Serializable()>
    Public Class ExampleObject


        Public Property Name As String

        Public Property Number As String


        Public Property TimeFromServer As String

        Public Property MessageFromClient As String

        ''' <summary>
        ''' required for serialization 
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

    End Class

End Namespace


