﻿Imports System.IO
Imports uniqco.Business
Imports System.Reflection
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Web.Http

Public Class GetData
    Public Shared Function ViewQueryAsTable(userName As String, password As String, GetQuery As String) As HttpResponseMessage
        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)
        Dim strMessage As String = String.Empty
        Dim response = New HttpResponseMessage()
        Dim strHtml As String = String.Empty
        If Not login Is Nothing Then
            Try
                Dim RiskCompositeTest = DataObjects.RiskComposite.GetQuery(GetQuery)
                Using sw As New StringWriter()
                    Using htw As New HtmlTextWriter(sw)
                        Dim dg As New DataGrid()
                        dg.DataSource = RiskCompositeTest.Tables(0)
                        dg.DataBind()
                        dg.RenderControl(htw)

                        strHtml = htw.InnerWriter.ToString()
                    End Using
                End Using
            Catch ex As Exception
                strHtml = ex.Message.ToString()
            End Try
        Else
            strHtml = "Unauthorized Access."
        End If
        response.Content = New StringContent(strHtml)
        response.Content.Headers.ContentType = New MediaTypeHeaderValue("text/html")
        Return response
    End Function

    Public Shared Function ViewQueryAsExcel(userName As String, password As String, GetQuery As String) As String
        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)
        Dim strMessage As String = String.Empty
        If Not login Is Nothing Then
            Try
                Dim RiskCompositeTest = DataObjects.RiskComposite.GetQuery(GetQuery)
                Dim response As HttpResponse = HttpContext.Current.Response
                Dim filename As String = "BIToolReport_" & Date.Now().ToString("MMddyyyyHHmmss").ToString() & ".xls"

                response.Clear()
                response.Charset = ""
                response.ContentType = "application/vnd.ms-excel"
                response.AddHeader("Content-Disposition", "attachment;filename=""" + filename + """")

                Using sw As New StringWriter()
                    Using htw As New HtmlTextWriter(sw)
                        Dim dg As New DataGrid()
                        dg.DataSource = RiskCompositeTest.Tables(0)
                        dg.DataBind()
                        dg.RenderControl(htw)

                        Dim strHtml As String = htw.InnerWriter.ToString()
                        response.Write(strHtml)
                        response.[End]()
                    End Using
                End Using
                strMessage = "Successfully downloaded."
            Catch ex As Exception
                strMessage = ex.Message.ToString()
            End Try
        Else
            strMessage = "Unauthorized Access."
        End If
        Return strMessage
    End Function

    Public Shared Function ViewQueryData(userName As String, password As String, StartDate As Date, EndDate As Date) As String
        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)
        Dim strMessage As String = String.Empty
        If Not login Is Nothing Then
            Dim RiskCompositeList = DataObjects.RiskComposite.GetRiskCompositeTable(StartDate, EndDate)
            Dim response As HttpResponse = HttpContext.Current.Response
            Dim filename As String = "RiskComposite_" & Date.Now().ToString("MMddyyyyHHmmss").ToString() & ".xls"

            response.Clear()
            response.Charset = ""
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment;filename=""" + filename + """")
            Using sw As New StringWriter()
                Using htw As New HtmlTextWriter(sw)
                    ' instantiate a datagrid  
                    Dim dg As New DataGrid()
                    dg.DataSource = RiskCompositeList

                    dg.DataBind()
                    dg.RenderControl(htw)

                    Dim strHtml As String = htw.InnerWriter.ToString()
                    response.Write(strHtml)
                    response.[End]()
                End Using
            End Using
            strMessage = "Successfully downloaded."
        Else
            strMessage = "Unauthorized Access."
        End If
        Return strMessage
    End Function

    Public Shared Function ViewQueryAsJson(userName As String, password As String, GetQuery As String) As HttpResponseMessage
        Dim login As DataObjects.Login = DataObjects.Login.GetFromLoginDetails(userName, password)
        Dim strMessage As String = String.Empty
        Dim response = New HttpResponseMessage()
        Dim strJson As String = String.Empty
        If Not login Is Nothing Then
            Try
                Dim RiskCompositeTest = DataObjects.RiskComposite.GetQuery(GetQuery)
                strJson = ConvertDataTableToJson(RiskCompositeTest.Tables(0))
            Catch ex As Exception
                strJson = ex.Message.ToString()
            End Try
        Else
            strJson = "Unauthorized Access."
        End If
        response.Content = New StringContent(strJson)
        response.Content.Headers.ContentType = New MediaTypeHeaderValue("application/json")
        Return response
    End Function

    Public Shared Function ConvertDataTableToJson(ByVal dt As DataTable) As String
        Dim serializer As New JavaScriptSerializer() With {.MaxJsonLength = Int32.MaxValue, .RecursionLimit = 100}
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object) = Nothing
        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()
            For Each dc As DataColumn In dt.Columns
                row.Add(dc.ColumnName.Trim(), dr(dc))
            Next
            rows.Add(row)
        Next

        Return serializer.Serialize(rows)
    End Function
End Class
