﻿Public Class SessionHelper



    Public Shared thisLock As New Object

    Public Shared Property UserLogin As Business.DataObjects.Login
        Get
            Dim o As Business.DataObjects.Login = HttpContext.Current.Session("UserLogin")

            If o Is Nothing Then Return Nothing Else Return o

        End Get
        Set(value As Business.DataObjects.Login)
            HttpContext.Current.Session("UserLogin") = value
        End Set
    End Property


    Public Shared Function GetAmalChartData() As DataTable
        Return Business.DataObjects.RiskComposite.GetForRAGChartAmalgamated(DateTime.Now)
    End Function


End Class
