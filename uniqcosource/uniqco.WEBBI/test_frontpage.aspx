﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="test_frontpage.aspx.vb" Inherits="uniqco.WEBBI.Test2" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <script src="content/js/jquery-3.0.0.min.js"></script>
    <link href="content/css/uniqco_frontpage.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#triangle-up').on('click mouseover', function () {
                $(this).css('border-color', 'transparent transparent #0000FF transparent').css('cursor', 'pointer');
            });
            $('#triangle-up').on('click mouseout', function () {
                $(this).css('border-color', 'transparent transparent #002e62 transparent');
            });

            $('#rectangle').on('click mouseover', function () {
                $(this).css('background', '#ff9900').css('cursor', 'pointer');
            });
            $('#rectangle').on('click mouseout', function () {
                $(this).css('background', '#ff4500');
            });

            $('#circle1').on('click mouseover', function () {
                $(this).css('background', '#ffff99').css('cursor', 'pointer');
            });
            $('#circle1').on('click mouseout', function () {
                $(this).css('background', '#FFFF00');
            });

            $('#circle2').on('click mouseover', function () {
                $(this).css('background', '#ffe066').css('cursor', 'pointer');
            });
            $('#circle2').on('click mouseout', function () {
                $(this).css('background', '#FFC000');
            });

            $('#circle3').on('click mouseover', function () {
                $(this).css('background', '#ccff33').css('cursor', 'pointer');
            });
            $('#circle3').on('click mouseout', function () {
                $(this).css('background', '#92d050');
            });

            $('#circle4').on('click mouseover', function () {
                $(this).css('background', '#4dffff').css('cursor', 'pointer');
            });
            $('#circle4').on('click mouseout', function () {
                $(this).css('background', '#00b0f0');
            });

            $('#rectangle1').on('click mouseover', function () {
                $(this).css('background', '#eee6ff').css('cursor', 'pointer');
            });
            $('#rectangle1').on('click mouseout', function () {
                $(this).css('background', '#bfbfbf');
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="top:1px; float:right; padding-right:10px;">
        <dx:ASPxHyperLink ID="lnkUser" runat="server" Text="Login">
        </dx:ASPxHyperLink>&nbsp;/&nbsp;
        <dx:ASPxHyperLink ID="lnkLogin" runat="server" Text="Login">
        </dx:ASPxHyperLink>
    </div>
    <div class="divCenter">
        <div style="position:absolute; top:0px; left:-50px; width:300px;"><img src="content/images/unifleet.png"></img></div>
        <a style="color:white; text-decoration:none;" href="PolicyTemplate.aspx"><div id="triangle-up"><div style="position: absolute; top: 50px;  left: 210px; color:white">Fleet Policy</div></div></a>
        <div class="spaces"></div>
        <a style="color:white; text-decoration:none;" href="ManagementPlanTemplate.aspx"><div id="rectangle"><div style="text-align:center; padding-top:10px; color:white">Fleet Strategy</div></div></a>
        <div class="spaces"></div>
        <div class="section group">
            <div id="circleHolder">
	            <div class="col span_1_of_4">
	                <a style="color:black; text-decoration:none;" href="LightFleetComparisonTool.aspx"><div id="circle1"><div style="text-align:center; padding-top:40px; color:black">Specify</div></div></a>
	            </div>
	            <div class="col span_1_of_4">
	                <a style="color:black; text-decoration:none;" href="AssetRegistrationTool.aspx"><div id="circle2"><div style="text-align:center; padding-top:40px;">Procure</div></div></a>
	            </div>
	            <div class="col span_1_of_4">
	                <a style="color:black; text-decoration:none;" href="AssetRegistrationTool.aspx"><div id="circle3"><div style="text-align:center; padding-top:40px;">Operate</div></div></a>
	            </div>
	            <div class="col span_1_of_4">
	                <a style="color:black; text-decoration:none;" href="DisposalTemplate.aspx"><div id="circle4"><div style="text-align:center; padding-top:40px;">Dispose</div></div></a>
	            </div>
            </div>
        </div>
        <div class="spaces"></div>
        <div id="rectangle1">
            <div style="padding-top:10px; padding-left:30px"><a style="color:#072663; text-decoration:none; " href="AdministrationOfTheirAssetDatabase.aspx">Procedure, Behaviours <br /> & System</a></div>
            <img src="content/images/unifleet.png" style="position: absolute; top: 330px;  left: 310px;" />
        </div>
    </div>
    </form>
</body>
</html>
