﻿Imports DevExpress.XtraCharts
Imports DevExpress.XtraCharts.Web
Imports System.Globalization
Imports System.Data
Imports DevExpress.Web
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Export
Public Class BusinessInformationTool
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsCallback Then
            '    If SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
            '        txtCompany.ClientVisible = False
            '        lblCompany.ClientVisible = False
            '        txtCompany.ReadOnly = True
            '    End If
            txtCompany.SelectedIndex = 0
        End If

        If Not IsPostBack Then
            HttpContext.Current.Session("fe") = ""
            HttpContext.Current.Session("dt") = Nothing
            HttpContext.Current.Session("popSource") = Nothing
            HttpContext.Current.Session("gridBound") = Nothing
            txtReportDate.Text = getlatestreport()
            createmainds()
            'createchartds("")
            WebChartControl2.DataBind()
            'If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
            '    ASPxGridView3.Columns("Name").Width = Unit.Pixel(150)
            '    ASPxGridView4.Columns("Name").Width = Unit.Pixel(150)
            '    ASPxGridView3.Columns("Value").Width = Unit.Pixel(100)
            '    ASPxGridView4.Columns("Value").Width = Unit.Pixel(100)
            '    ASPxGridView3.Columns("Uniqco").Visible = True
            '    ASPxGridView4.Columns("Uniqco").Visible = True
            'End If
            'ASPxGridView3.Columns("Value").Caption = SessionHelper.UserLogin.CompanyName
            'ASPxGridView4.Columns("Value").Caption = SessionHelper.UserLogin.CompanyName
            txtPieView.SelectedIndex = txtPieView.Items.Count - 1
            txtChartText.SelectedIndex = 0
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If HttpContext.Current.Session("gridBound") IsNot Nothing AndAlso CBool(HttpContext.Current.Session("gridBound")) Then
            ASPxGridView2.DataBind()
        End If
    End Sub
    Private Function createmainds()
        If txtCompany.Value IsNot Nothing Then
            Dim repdate = Convert.ToDateTime(txtReportDate.Value).AddMonths(1).AddDays(-1)
            HttpContext.Current.Session("dt") = Business.DataObjects.RiskComposite.GetForRAGChart(Guid.Parse(txtCompany.Value.ToString()), repdate)

        End If
        Return Nothing
    End Function
    Private Shared Function getchartds(fc As String) As DataTable
        Dim dtSource As DataTable
        Try
            dtSource = (CType(HttpContext.Current.Session("dt"), DataTable)).Select(fc).CopyToDataTable()
        Catch ex As Exception
            dtSource = Nothing
        End Try
        Return dtSource
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetPieData(ByVal com As Guid, ByVal ty As String, ByVal fe As String, ByVal rd As String)

        'Dim groups = Business.DataObjects.PIEChartLine.GetFromDB(com, ty)
        'Dim artnrGroups = (From a In groups _
        '                    Where a.PropertyName IsNot Nothing And (a.PropertyValue <> Nothing And a.PropertyValue > 0)
        '                    Select Name = a.PropertyName, Value = a.PropertyValue).ToList

        Dim ds = getchartds(fe)

        Dim artnrGroups = If(ds IsNot Nothing, (From a In ds Where a("ReportDateMonthYear").Equals(rd) Group By PropertyName = a(ty)
                                Into PropertyValue = Sum(CType(a("CurrentAssetValue"), Decimal))
                                Where (PropertyName IsNot Nothing Or Not IsDBNull(PropertyName)) And
                                        ((PropertyValue <> Nothing Or Not IsDBNull(PropertyValue)) And PropertyValue > 0)
                           Select New With {
                               .Name = PropertyName, .Value = PropertyValue
                           }).ToList, Nothing)

        Dim drilldown = New Dictionary(Of String, Object)

        'If artnrGroups IsNot Nothing Then
        '    For Each a In artnrGroups
        '        Dim ddartnrGroups = (From b In ds Where b(ty).Equals(a.Name)
        '                             Group By PropertyName = b("Group")
        '                                Into PropertyValue = Sum(CType(b("CurrentAssetValue"), Decimal))
        '                                Where (PropertyName IsNot Nothing Or Not IsDBNull(PropertyName)) And
        '                                ((PropertyValue <> Nothing Or Not IsDBNull(PropertyValue)) And PropertyValue > 0)
        '                            Select New With {
        '                               .Name = PropertyName, .Value = PropertyValue
        '                           }).ToList

        '        drilldown.Add(a.Name, ddartnrGroups)
        '    Next

        'End If
        Dim retval = New With {
            .main = artnrGroups,
            .sub = drilldown.ToList
            }
        Return retval
    End Function
    Private Function getFilterExpression()
        Dim Fex = New List(Of String)

        If BIToolParams.Contains("FilterExpression_Drilldown") Then
            Dim fe_drilldown = BIToolParams.Get("FilterExpression_Drilldown")
            If Not String.IsNullOrEmpty(fe_drilldown.ToString().Trim) Then Fex.Add(fe_drilldown)
        End If
        If BIToolParams.Contains("FilterExpression") Then
            Dim fe = BIToolParams.Get("FilterExpression")
            If Not String.IsNullOrEmpty(fe.ToString().Trim) Then Fex.Add(fe)
        End If
        Return If(Fex.Count > 0, String.Join(" And ", Fex.ToArray), "")
    End Function
    Protected Sub WebChartControl_CustomCallback(sender As Object, e As CustomCallbackEventArgs)
        Dim chrt As WebChartControl = DirectCast(sender, WebChartControl)

        Dim TEST = getFilterExpression()

        HttpContext.Current.Session("fe") = getFilterExpression()
        CreateSeriesFor(chrt, txtChartText.Value)
        chrt.DataBind()
    End Sub
    Private Sub CreateSeriesFor(chrt As WebChartControl, name As String)
        If name = "Utilisation" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedUtilisation])"
            chrt.Series(0).ValueDataMembersSerializable = "RedUtilisation"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberUtilisation])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberUtilisation"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenUtilisation])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenUtilisation"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberUtilisation])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberUtilisation"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedUtilisation])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedUtilisation"
        ElseIf name = "OperationalProductivity" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedOperationalProductivity])"
            chrt.Series(0).ValueDataMembersSerializable = "RedOperationalProductivity"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberOperationalProductivity])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberOperationalProductivity"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenOperationalProductivity])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenOperationalProductivity"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberOperationalProductivity])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberOperationalProductivity"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedOperationalProductivity])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedOperationalProductivity"
        ElseIf name = "FuelConsumption" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -30%"
            chrt.Series(0).SummaryFunction = "SUM([RedFuelConsumption])"
            chrt.Series(0).ValueDataMembersSerializable = "RedFuelConsumption"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -20%"
            chrt.Series(1).SummaryFunction = "SUM([AmberFuelConsumption])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberFuelConsumption"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 20%"
            chrt.Series(2).SummaryFunction = "SUM([GreenFuelConsumption])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenFuelConsumption"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 20%"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberFuelConsumption])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberFuelConsumption"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 30%"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedFuelConsumption])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedFuelConsumption"
        ElseIf name = "OptimumReplacement" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "90 Days + Overdue"
            chrt.Series(0).SummaryFunction = "SUM([RedOptimumReplacement])"
            chrt.Series(0).ValueDataMembersSerializable = "RedOptimumReplacement"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< 90 Days Overdue"
            chrt.Series(1).SummaryFunction = "SUM([AmberOptimumReplacement])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberOptimumReplacement"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Current"
            chrt.Series(2).SummaryFunction = "SUM([GreenOptimumReplacement])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenOptimumReplacement"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "ServiceDue" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "Overdue 45+ Days"
            chrt.Series(0).SummaryFunction = "SUM([RedServiceDue])"
            chrt.Series(0).ValueDataMembersSerializable = "RedServiceDue"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "Overdue < 45 and > 14"
            chrt.Series(1).SummaryFunction = "SUM([AmberServiceDue])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberServiceDue"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Overdue < 14 Days"
            chrt.Series(2).SummaryFunction = "SUM([GreenServiceDue])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenServiceDue"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        ElseIf name = "WOLCostVariation" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "< -10% of Budget"
            chrt.Series(0).SummaryFunction = "SUM([RedWOLCostVariation])"
            chrt.Series(0).ValueDataMembersSerializable = "RedWOLCostVariation"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "< -5% of Budget"
            chrt.Series(1).SummaryFunction = "SUM([AmberWOLCostVariation])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberWOLCostVariation"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "+/- 5% of Budget"
            chrt.Series(2).SummaryFunction = "SUM([GreenWOLCostVariation])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenWOLCostVariation"

            chrt.Series(3).Visible = True
            chrt.Series(3).LegendText = "> 5% of Budget"
            chrt.Series(3).SummaryFunction = "SUM([FadedAmberWOLCostVariation])"
            chrt.Series(3).ValueDataMembersSerializable = "FadedAmberWOLCostVariation"

            chrt.Series(4).Visible = True
            chrt.Series(4).LegendText = "> 10% of Budget"
            chrt.Series(4).SummaryFunction = "SUM([FadedRedWOLCostVariation])"
            chrt.Series(4).ValueDataMembersSerializable = "FadedRedWOLCostVariation"
        ElseIf name = "MaintenanceRatio" Then
            chrt.Series(0).Visible = True
            chrt.Series(0).LegendText = "Overdue 45+ Days"
            chrt.Series(0).SummaryFunction = "SUM([RedMaintenanceRatio])"
            chrt.Series(0).ValueDataMembersSerializable = "RedMaintenanceRatio"

            chrt.Series(1).Visible = True
            chrt.Series(1).LegendText = "Overdue < 45 and > 14"
            chrt.Series(1).SummaryFunction = "SUM([AmberMaintenanceRatio])"
            chrt.Series(1).ValueDataMembersSerializable = "AmberMaintenanceRatio"

            chrt.Series(2).Visible = True
            chrt.Series(2).LegendText = "Overdue < 14 Days"
            chrt.Series(2).SummaryFunction = "SUM([GreenMaintenanceRatio])"
            chrt.Series(2).ValueDataMembersSerializable = "GreenMaintenanceRatio"

            chrt.Series(3).Visible = False
            chrt.Series(3).LegendText = ""
            chrt.Series(3).SummaryFunction = ""
            chrt.Series(3).ValueDataMembersSerializable = ""

            chrt.Series(4).Visible = False
            chrt.Series(4).LegendText = ""
            chrt.Series(4).SummaryFunction = ""
            chrt.Series(4).ValueDataMembersSerializable = ""
        End If
    End Sub
    Protected Sub ASPxGridView2_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs)
        If e.Parameters = "open" Then
            'ASPxGridView2.Columns.Clear()
            'ASPxGridView2.AutoGenerateColumns = False
            'ViewState("needBind") = True
            Dim header = BIToolParams.Get("header").ToString().Replace(" ", "")
            Dim arg = BIToolParams.Get("arg").ToString()
            Dim series = BIToolParams.Get("series").ToString()
            Dim chartname = txtChartText.Value
            'Dim groups = Business.DataObjects.RagChart.GetForCompanyAsDataTable(txtCompany.Text)


            Try
                Dim IdList = (From a In getchartds(HttpContext.Current.Session("fe")) _
                            Where a(header).ToString() = series.ToUpper() And a("ReportDateMonthYear") = arg _
                                   Select a("AssetId")).ToList


                Dim datasource = Business.DataObjects.RiskComposite.GetFromDB().Where(Function(x) IdList.Contains(x.PK.ToString()))

                'Sort 
                If series = "FadedAmber" Or series = "FadedRed" Then
                    If chartname = "Utilisation" Then
                        datasource = datasource.OrderByDescending(Function(x) x.KmsHrsVar)
                    ElseIf chartname = "OperationalProductivity" Then
                        datasource = datasource.OrderByDescending(Function(x) x.KmsHrsVar - x.IncomeUnitsVar)
                    ElseIf chartname = "FuelConsumption" Then
                        datasource = datasource.OrderByDescending(Function(x) x.Deviation)
                    ElseIf chartname = "OptimumReplacement" Then
                        datasource = datasource.OrderByDescending(Function(x) x.OptimumReplacementDate)
                    ElseIf chartname = "ServiceDue" Then
                        datasource = datasource.OrderByDescending(Function(x) x.NextStandardServiceDue)
                    ElseIf chartname = "WOLCostVariation" Then
                        datasource = datasource.OrderByDescending(Function(x) x.CostVariation)
                    ElseIf chartname = "MaintenanceRatio" Then
                        datasource = datasource.OrderByDescending(Function(x) x.DowntimeVariation)
                    End If
                Else
                    If chartname = "Utilisation" Then
                        datasource = datasource.OrderBy(Function(x) x.KmsHrsVar)
                    ElseIf chartname = "OperationalProductivity" Then
                        datasource = datasource.OrderBy(Function(x) x.KmsHrsVar - x.IncomeUnitsVar)
                    ElseIf chartname = "FuelConsumption" Then
                        datasource = datasource.OrderBy(Function(x) x.Deviation)
                    ElseIf chartname = "OptimumReplacement" Then
                        datasource = datasource.OrderBy(Function(x) x.OptimumReplacementDate)
                    ElseIf chartname = "ServiceDue" Then
                        datasource = datasource.OrderBy(Function(x) x.NextStandardServiceDue)
                    ElseIf chartname = "WOLCostVariation" Then
                        datasource = datasource.OrderBy(Function(x) x.CostVariation)
                    ElseIf chartname = "MaintenanceRatio" Then
                        datasource = datasource.OrderBy(Function(x) x.DowntimeVariation)
                    End If
                End If
                HttpContext.Current.Session("gridBound") = True
                HttpContext.Current.Session("popsource") = datasource
                ASPxGridView2.Columns.Clear()
                ASPxGridView2.DataBind()
                Dim l = Business.DataObjects.RiskComposite.GetColumns(header)
                For Each col As GridViewDataColumn In ASPxGridView2.Columns
                    col.Visible = l.Contains(col.FieldName)
                Next
            Catch ex As Exception

            End Try
        End If

    End Sub

    Protected Sub ASPxGridView2_DataBinding(sender As Object, e As EventArgs)
        TryCast(sender, ASPxGridView).DataSource = HttpContext.Current.Session("popsource")
        'TryCast(sender, ASPxGridView).KeyFieldName = "PK"
        'AddColumns()
        'End If
    End Sub
    Private Sub AddColumns()
        ASPxGridView2.Columns.Clear()

        AddTextColumn("PK")
        AddTextColumn("PlantNumber")
        AddTextColumn("ParentPlant")
        AddTextColumn("FleetNumber")
        AddTextColumn("AssetNumber")
        AddTextColumn("Category")
        AddTextColumn("Group")
        AddTextColumn("Type")
        AddTextColumn("Make")
        AddTextColumn("Model")
        AddTextColumn("ModelYear")
        AddTextColumn("RegoNo")
        AddTextColumn("RegoDate")
        AddTextColumn("RegoState")
        AddTextColumn("MeterType")
        AddTextColumn("CurrentAsset")
        AddTextColumn("PlantImage")
        AddTextColumn("VINNumber")
        AddTextColumn("EngineNumber")
        AddTextColumn("ChassisNumber")
        AddTextColumn("BodyNumber")
        AddTextColumn("BuildDate")
        AddTextColumn("PreviousRego")

        'ASPxGridView2.KeyFieldName = "PK"
        'ASPxGridView2.Columns("PK").Visible = True
    End Sub
    Private Sub AddTextColumn(ByVal fieldName As String)
        Dim c As New GridViewDataTextColumn()
        c.FieldName = fieldName
        ASPxGridView2.Columns.Add(c)
    End Sub

    Protected Sub WebChartControl2_DataBinding(sender As Object, e As EventArgs)
        TryCast(sender, WebChartControl).DataSource = getchartds(HttpContext.Current.Session("fe"))
    End Sub

    Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs)
        If e.Parameter = "cc" Then
            createmainds()
        End If
    End Sub

    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)
        Dim grid As ASPxGridView = DirectCast(sender, ASPxGridView)
        HttpContext.Current.Session("fe") = getFilterExpression()
        grid.DataBind()
        updateCH(grid)
    End Sub
    Private Function updateCH(grid As ASPxGridView)
        Try
            grid.Columns("Value").Caption = txtCompany.Text
        Catch ex As Exception

        End Try
        Return Nothing
    End Function
    Protected Sub ASPxGridView1_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "Name" Then
            e.Cell.ForeColor = Color.White
            e.Cell.Font.Bold = True
            e.Cell.BackColor = Color.Gray
        End If
    End Sub
    Protected Sub ASPxGridView3_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs)
        If e.DataColumn.FieldName = "Name" Then
            'e.Cell.Font.Bold = True
            e.Cell.BackColor = Color.LightGray
        End If
    End Sub

    Protected Sub ASPxGridView1_HtmlRowCreated(sender As Object, e As ASPxGridViewTableRowEventArgs)
        e.Row.Height = 27
    End Sub
    Protected Sub ASPxGridView4_HtmlRowCreated(sender As Object, e As ASPxGridViewTableRowEventArgs)
        e.Row.Height = 22.5
    End Sub

    Protected Sub odsloginCompanies_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs)
    End Sub

    Protected Sub txtCompany_DataBound(sender As Object, e As EventArgs)

        'txtCompany.SelectedIndex = txtCompany.Items.IndexOfText(SessionHelper.UserLogin.CompanyName)
    End Sub
    Protected Sub cbxGRGroup_Callback(sender As Object, e As CallbackEventArgsBase)
        Dim cbx As ASPxComboBox = DirectCast(sender, ASPxComboBox)
        cbx.Items.Add("[None]")
        If cbx.ID = "cbxGRGroup" Then
            cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("group", Guid.Parse(txtCompany.Value.ToString())))
        ElseIf cbx.ID = "cbxTyType" Then
            cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("type", Guid.Parse(txtCompany.Value.ToString())))
        ElseIf cbx.ID = "cbxMkMake" Then
            cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("make", Guid.Parse(txtCompany.Value.ToString())))
        ElseIf cbx.ID = "cbxMdModel" Then
            cbx.Items.AddRange(Business.DataObjects.RiskComposite.GetDistinct("model", Guid.Parse(txtCompany.Value.ToString())))
        End If
        cbx.Text = "[None]"
    End Sub

    Protected Sub ASPxButton3_Click(sender As Object, e As EventArgs)
        gridExport.WriteCsvToResponse(txtChartText.Text, New CsvExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub odssummarydata_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim data = New Dictionary(Of String, Double)
        data.Add("ARV", If(dt IsNot Nothing, DVal(dt.Compute("Sum(BudgetReplacementPriceExcGST)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("CAV", If(dt IsNot Nothing, DVal(dt.Compute("Sum(CurrentAssetValue)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("DB", If(dt IsNot Nothing, DVal(dt.Compute("Sum(ReserveOverRecovery)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))

        data.Add("TPL", If(dt IsNot Nothing, DVal(dt.Compute("Sum(ProfitLoss)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("CD", If(dt IsNot Nothing, DVal(dt.Compute("Sum(CostOfDownTime)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("CUU", If(dt IsNot Nothing, DVal(dt.Compute("Sum(CostofUnderUtilisation)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("SI", 0)
        e.InputParameters("data") = data
    End Sub

    Protected Sub odsperassetdata_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim data = New Dictionary(Of String, Double)
        data.Add("FC", If(dt IsNot Nothing, DVal(dt.Compute("Sum(FConsumption)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")), 0))
        data.Add("C", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(CostSubTotal)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / dt.Rows.Count, 0), 0))
        data.Add("D", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(DowntimeHrs)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / dt.Rows.Count, 0), 0))
        e.InputParameters("data") = data
    End Sub

    Protected Sub odsmaintenancedata_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs)
        Dim dt = getchartds(HttpContext.Current.Session("fe"))
        Dim data = New Dictionary(Of String, Double)
        data.Add("SC", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(LabourHoursSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / dt.Rows.Count, 0), 0))
        data.Add("UN", If(dt IsNot Nothing, If(dt.Rows.Count > 0, (DVal(dt.Compute("Sum(LabourHoursNonSched)", "ReportDateMonthYear = '" + txtReportDate.Text + "'")) * 2) / dt.Rows.Count, 0), 0))
        data.Add("S", 0)
        e.InputParameters("data") = data

    End Sub
    Private Function DVal(a As Object)
        Return If(IsDBNull(a), 0, a)
    End Function

    Private Function getlatestreport()
        If txtCompany.Value IsNot Nothing Then
            Dim ds = Business.DataObjects.CompanyReport.GetForCompanyID(txtCompany.Value).Last
            Return ds.Reportdate.ToString("MMM yyyy")
        Else
            Return DateTime.Now.ToString("MMM yyyy")

        End If
    End Function

End Class