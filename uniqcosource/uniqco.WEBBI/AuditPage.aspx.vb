﻿Imports DevExpress.XtraPrinting
Imports DevExpress.Export

Public Class AuditPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If SessionHelper.UserLogin Is Nothing Then
            Dim queryStr As String = Request.Url.Query
            queryStr = String.Format("~/content/companyadmin/login.aspx{0}{1}{2}={3}", "", _
                                                    If(String.IsNullOrEmpty(queryStr), "?", "&"), Constants.queryString_redirect, Request.Url.LocalPath)
            Response.Redirect(queryStr)
        Else
            If SessionHelper.UserLogin.Rights.Contains("UNIQCO") Then
                Me.MasterPageFile = "~/content/UniqcoAdminMaster.master"
            ElseIf SessionHelper.UserLogin.Rights.Contains("COMPANY") Then
                Me.MasterPageFile = "~/content/CompanyAdminMaster.master"
            Else
                Throw New Exception("ERROR! Failed to load Master Page.")
            End If
        End If
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs)
        gridExport.WriteCsvToResponse("Processed Email", New CsvExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub
End Class