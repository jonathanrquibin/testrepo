﻿Imports System.Web.SessionState
Imports DevExpress.Web
Imports System.Web.Routing
Imports System.Web.Http

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)

        RouteTable.Routes.MapHttpRoute(name:="MyAPIDefault",
                                       routeTemplate:="api/{controller}/{Action}",
                                       defaults:=New With { _
                                             Key .id = System.Web.Http.RouteParameter.[Optional] _
                                         })


        'get settings for SetRagChartDataTable

        Dim dict As New Dictionary(Of String, DataTable)
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-GB")
        Dim cache_setting = Business.DataObjects.CacheSetting.GetSetting("GetRagChartDataTable")

        If cache_setting IsNot Nothing Then

            For Each setting In cache_setting.CacheSettingList

                Dim vals() As String = setting.Split(","c)

                Dim companyID As Guid = Guid.Parse(vals(0))
                Dim repDate As Date = DateTime.Parse(vals(1))

                Dim key As String = String.Format("{0},{1}", companyID.ToString, CStr(repDate))

                Dim dataTable As DataTable = Business.DataObjects.RiskComposite.GetForRAGChart(companyID, repDate)

                dict.Add(key, dataTable)
            Next

        End If

        ApplicationCache.SetRagChartDataTable(dict)

        'update generic data table
        ApplicationCache.usp_AmalgamatedRAGChart_v3_DataTable = Business.DataObjects.RiskComposite.GetForRAGChartAmalgamated(Now)

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class