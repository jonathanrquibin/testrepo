﻿

Public Class Service1

    Public Property main_thread As System.Threading.Thread

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        main_thread = New System.Threading.Thread(AddressOf uniqco.filereader.ServiceWrapper.Run_MainLoop)
        main_thread.Start()

    End Sub

    Protected Overrides Sub OnPause()
        MyBase.OnPause()

        If main_thread IsNot Nothing Then main_thread.Suspend()

    End Sub


    Protected Overrides Sub OnContinue()
        MyBase.OnContinue()



    End Sub

    Protected Overrides Sub OnStop()
        If main_thread IsNot Nothing Then main_thread.Abort()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

End Class
